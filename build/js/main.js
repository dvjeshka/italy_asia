/*
 Third party
 */
/*! jQuery v2.2.1 | (c) jQuery Foundation | jquery.org/license */
!function(a,b){"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=a.document,e=c.slice,f=c.concat,g=c.push,h=c.indexOf,i={},j=i.toString,k=i.hasOwnProperty,l={},m="2.2.1",n=function(a,b){return new n.fn.init(a,b)},o=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,p=/^-ms-/,q=/-([\da-z])/gi,r=function(a,b){return b.toUpperCase()};n.fn=n.prototype={jquery:m,constructor:n,selector:"",length:0,toArray:function(){return e.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:e.call(this)},pushStack:function(a){var b=n.merge(this.constructor(),a);return b.prevObject=this,b.context=this.context,b},each:function(a){return n.each(this,a)},map:function(a){return this.pushStack(n.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(e.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor()},push:g,sort:c.sort,splice:c.splice},n.extend=n.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||n.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(a=arguments[h]))for(b in a)c=g[b],d=a[b],g!==d&&(j&&d&&(n.isPlainObject(d)||(e=n.isArray(d)))?(e?(e=!1,f=c&&n.isArray(c)?c:[]):f=c&&n.isPlainObject(c)?c:{},g[b]=n.extend(j,f,d)):void 0!==d&&(g[b]=d));return g},n.extend({expando:"jQuery"+(m+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===n.type(a)},isArray:Array.isArray,isWindow:function(a){return null!=a&&a===a.window},isNumeric:function(a){var b=a&&a.toString();return!n.isArray(a)&&b-parseFloat(b)+1>=0},isPlainObject:function(a){return"object"!==n.type(a)||a.nodeType||n.isWindow(a)?!1:a.constructor&&!k.call(a.constructor.prototype,"isPrototypeOf")?!1:!0},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?i[j.call(a)]||"object":typeof a},globalEval:function(a){var b,c=eval;a=n.trim(a),a&&(1===a.indexOf("use strict")?(b=d.createElement("script"),b.text=a,d.head.appendChild(b).parentNode.removeChild(b)):c(a))},camelCase:function(a){return a.replace(p,"ms-").replace(q,r)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b){var c,d=0;if(s(a)){for(c=a.length;c>d;d++)if(b.call(a[d],d,a[d])===!1)break}else for(d in a)if(b.call(a[d],d,a[d])===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(o,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(s(Object(a))?n.merge(c,"string"==typeof a?[a]:a):g.call(c,a)),c},inArray:function(a,b,c){return null==b?-1:h.call(b,a,c)},merge:function(a,b){for(var c=+b.length,d=0,e=a.length;c>d;d++)a[e++]=b[d];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,e,g=0,h=[];if(s(a))for(d=a.length;d>g;g++)e=b(a[g],g,c),null!=e&&h.push(e);else for(g in a)e=b(a[g],g,c),null!=e&&h.push(e);return f.apply([],h)},guid:1,proxy:function(a,b){var c,d,f;return"string"==typeof b&&(c=a[b],b=a,a=c),n.isFunction(a)?(d=e.call(arguments,2),f=function(){return a.apply(b||this,d.concat(e.call(arguments)))},f.guid=a.guid=a.guid||n.guid++,f):void 0},now:Date.now,support:l}),"function"==typeof Symbol&&(n.fn[Symbol.iterator]=c[Symbol.iterator]),n.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(a,b){i["[object "+b+"]"]=b.toLowerCase()});function s(a){var b=!!a&&"length"in a&&a.length,c=n.type(a);return"function"===c||n.isWindow(a)?!1:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var t=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ga(),z=ga(),A=ga(),B=function(a,b){return a===b&&(l=!0),0},C=1<<31,D={}.hasOwnProperty,E=[],F=E.pop,G=E.push,H=E.push,I=E.slice,J=function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},K="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",L="[\\x20\\t\\r\\n\\f]",M="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",N="\\["+L+"*("+M+")(?:"+L+"*([*^$|!~]?=)"+L+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+M+"))|)"+L+"*\\]",O=":("+M+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+N+")*)|.*)\\)|)",P=new RegExp(L+"+","g"),Q=new RegExp("^"+L+"+|((?:^|[^\\\\])(?:\\\\.)*)"+L+"+$","g"),R=new RegExp("^"+L+"*,"+L+"*"),S=new RegExp("^"+L+"*([>+~]|"+L+")"+L+"*"),T=new RegExp("="+L+"*([^\\]'\"]*?)"+L+"*\\]","g"),U=new RegExp(O),V=new RegExp("^"+M+"$"),W={ID:new RegExp("^#("+M+")"),CLASS:new RegExp("^\\.("+M+")"),TAG:new RegExp("^("+M+"|[*])"),ATTR:new RegExp("^"+N),PSEUDO:new RegExp("^"+O),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+L+"*(even|odd|(([+-]|)(\\d*)n|)"+L+"*(?:([+-]|)"+L+"*(\\d+)|))"+L+"*\\)|)","i"),bool:new RegExp("^(?:"+K+")$","i"),needsContext:new RegExp("^"+L+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+L+"*((?:-\\d)?\\d*)"+L+"*\\)|)(?=[^-]|$)","i")},X=/^(?:input|select|textarea|button)$/i,Y=/^h\d$/i,Z=/^[^{]+\{\s*\[native \w/,$=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,_=/[+~]/,aa=/'|\\/g,ba=new RegExp("\\\\([\\da-f]{1,6}"+L+"?|("+L+")|.)","ig"),ca=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)},da=function(){m()};try{H.apply(E=I.call(v.childNodes),v.childNodes),E[v.childNodes.length].nodeType}catch(ea){H={apply:E.length?function(a,b){G.apply(a,I.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function fa(a,b,d,e){var f,h,j,k,l,o,r,s,w=b&&b.ownerDocument,x=b?b.nodeType:9;if(d=d||[],"string"!=typeof a||!a||1!==x&&9!==x&&11!==x)return d;if(!e&&((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,p)){if(11!==x&&(o=$.exec(a)))if(f=o[1]){if(9===x){if(!(j=b.getElementById(f)))return d;if(j.id===f)return d.push(j),d}else if(w&&(j=w.getElementById(f))&&t(b,j)&&j.id===f)return d.push(j),d}else{if(o[2])return H.apply(d,b.getElementsByTagName(a)),d;if((f=o[3])&&c.getElementsByClassName&&b.getElementsByClassName)return H.apply(d,b.getElementsByClassName(f)),d}if(c.qsa&&!A[a+" "]&&(!q||!q.test(a))){if(1!==x)w=b,s=a;else if("object"!==b.nodeName.toLowerCase()){(k=b.getAttribute("id"))?k=k.replace(aa,"\\$&"):b.setAttribute("id",k=u),r=g(a),h=r.length,l=V.test(k)?"#"+k:"[id='"+k+"']";while(h--)r[h]=l+" "+qa(r[h]);s=r.join(","),w=_.test(a)&&oa(b.parentNode)||b}if(s)try{return H.apply(d,w.querySelectorAll(s)),d}catch(y){}finally{k===u&&b.removeAttribute("id")}}}return i(a.replace(Q,"$1"),b,d,e)}function ga(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function ha(a){return a[u]=!0,a}function ia(a){var b=n.createElement("div");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function ja(a,b){var c=a.split("|"),e=c.length;while(e--)d.attrHandle[c[e]]=b}function ka(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||C)-(~a.sourceIndex||C);if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function la(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function ma(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function na(a){return ha(function(b){return b=+b,ha(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function oa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a}c=fa.support={},f=fa.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},m=fa.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=n.documentElement,p=!f(n),(e=n.defaultView)&&e.top!==e&&(e.addEventListener?e.addEventListener("unload",da,!1):e.attachEvent&&e.attachEvent("onunload",da)),c.attributes=ia(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ia(function(a){return a.appendChild(n.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=Z.test(n.getElementsByClassName),c.getById=ia(function(a){return o.appendChild(a).id=u,!n.getElementsByName||!n.getElementsByName(u).length}),c.getById?(d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);return c?[c]:[]}},d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return"undefined"!=typeof b.getElementsByClassName&&p?b.getElementsByClassName(a):void 0},r=[],q=[],(c.qsa=Z.test(n.querySelectorAll))&&(ia(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\r\\' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+L+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+L+"*(?:value|"+K+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")}),ia(function(a){var b=n.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+L+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=Z.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ia(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",O)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=Z.test(o.compareDocumentPosition),t=b||Z.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===n||a.ownerDocument===v&&t(v,a)?-1:b===n||b.ownerDocument===v&&t(v,b)?1:k?J(k,a)-J(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,e=a.parentNode,f=b.parentNode,g=[a],h=[b];if(!e||!f)return a===n?-1:b===n?1:e?-1:f?1:k?J(k,a)-J(k,b):0;if(e===f)return ka(a,b);c=a;while(c=c.parentNode)g.unshift(c);c=b;while(c=c.parentNode)h.unshift(c);while(g[d]===h[d])d++;return d?ka(g[d],h[d]):g[d]===v?-1:h[d]===v?1:0},n):n},fa.matches=function(a,b){return fa(a,null,null,b)},fa.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(T,"='$1']"),c.matchesSelector&&p&&!A[b+" "]&&(!r||!r.test(b))&&(!q||!q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return fa(b,n,null,[a]).length>0},fa.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},fa.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&D.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},fa.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},fa.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=fa.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=fa.selectors={cacheLength:50,createPseudo:ha,match:W,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(ba,ca),a[3]=(a[3]||a[4]||a[5]||"").replace(ba,ca),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||fa.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&fa.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return W.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&U.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(ba,ca).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+L+")"+a+"("+L+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=fa.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(P," ")+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h,t=!1;if(q){if(f){while(p){m=b;while(m=m[p])if(h?m.nodeName.toLowerCase()===r:1===m.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){m=q,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n&&j[2],m=n&&q.childNodes[n];while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if(1===m.nodeType&&++t&&m===b){k[a]=[w,n,t];break}}else if(s&&(m=b,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n),t===!1)while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if((h?m.nodeName.toLowerCase()===r:1===m.nodeType)&&++t&&(s&&(l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),k[a]=[w,t]),m===b))break;return t-=e,t===d||t%d===0&&t/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||fa.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ha(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=J(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:ha(function(a){var b=[],c=[],d=h(a.replace(Q,"$1"));return d[u]?ha(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()}}),has:ha(function(a){return function(b){return fa(a,b).length>0}}),contains:ha(function(a){return a=a.replace(ba,ca),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:ha(function(a){return V.test(a||"")||fa.error("unsupported lang: "+a),a=a.replace(ba,ca).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===!1},disabled:function(a){return a.disabled===!0},checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return Y.test(a.nodeName)},input:function(a){return X.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:na(function(){return[0]}),last:na(function(a,b){return[b-1]}),eq:na(function(a,b,c){return[0>c?c+b:c]}),even:na(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:na(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:na(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:na(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=la(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=ma(b);function pa(){}pa.prototype=d.filters=d.pseudos,d.setFilters=new pa,g=fa.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){(!c||(e=R.exec(h)))&&(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=S.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(Q," ")}),h=h.slice(c.length));for(g in d.filter)!(e=W[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?fa.error(a):z(a,i).slice(0)};function qa(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function ra(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;return b.first?function(b,c,f){while(b=b[d])if(1===b.nodeType||e)return a(b,c,f)}:function(b,c,g){var h,i,j,k=[w,f];if(g){while(b=b[d])if((1===b.nodeType||e)&&a(b,c,g))return!0}else while(b=b[d])if(1===b.nodeType||e){if(j=b[u]||(b[u]={}),i=j[b.uniqueID]||(j[b.uniqueID]={}),(h=i[d])&&h[0]===w&&h[1]===f)return k[2]=h[2];if(i[d]=k,k[2]=a(b,c,g))return!0}}}function sa(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function ta(a,b,c){for(var d=0,e=b.length;e>d;d++)fa(a,b[d],c);return c}function ua(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(!c||c(f,d,e))&&(g.push(f),j&&b.push(h));return g}function va(a,b,c,d,e,f){return d&&!d[u]&&(d=va(d)),e&&!e[u]&&(e=va(e,f)),ha(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||ta(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:ua(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=ua(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?J(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=ua(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):H.apply(g,r)})}function wa(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=ra(function(a){return a===b},h,!0),l=ra(function(a){return J(b,a)>-1},h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));return b=null,e}];f>i;i++)if(c=d.relative[a[i].type])m=[ra(sa(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;f>e;e++)if(d.relative[a[e].type])break;return va(i>1&&sa(m),i>1&&qa(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(Q,"$1"),c,e>i&&wa(a.slice(i,e)),f>e&&wa(a=a.slice(e)),f>e&&qa(a))}m.push(c)}return sa(m)}function xa(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,o,q,r=0,s="0",t=f&&[],u=[],v=j,x=f||e&&d.find.TAG("*",k),y=w+=null==v?1:Math.random()||.1,z=x.length;for(k&&(j=g===n||g||k);s!==z&&null!=(l=x[s]);s++){if(e&&l){o=0,g||l.ownerDocument===n||(m(l),h=!p);while(q=a[o++])if(q(l,g||n,h)){i.push(l);break}k&&(w=y)}c&&((l=!q&&l)&&r--,f&&t.push(l))}if(r+=s,c&&s!==r){o=0;while(q=b[o++])q(t,u,g,h);if(f){if(r>0)while(s--)t[s]||u[s]||(u[s]=F.call(i));u=ua(u)}H.apply(i,u),k&&!f&&u.length>0&&r+b.length>1&&fa.uniqueSort(i)}return k&&(w=y,j=v),t};return c?ha(f):f}return h=fa.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=wa(b[c]),f[u]?d.push(f):e.push(f);f=A(a,xa(e,d)),f.selector=a}return f},i=fa.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(ba,ca),b)||[])[0],!b)return e;n&&(b=b.parentNode),a=a.slice(j.shift().value.length)}i=W.needsContext.test(a)?0:j.length;while(i--){if(k=j[i],d.relative[l=k.type])break;if((m=d.find[l])&&(f=m(k.matches[0].replace(ba,ca),_.test(j[0].type)&&oa(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&qa(j),!a)return H.apply(e,f),e;break}}}return(n||h(a,o))(f,b,!p,e,!b||_.test(a)&&oa(b.parentNode)||b),e},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ia(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))}),ia(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||ja("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ia(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||ja("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),ia(function(a){return null==a.getAttribute("disabled")})||ja(K,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),fa}(a);n.find=t,n.expr=t.selectors,n.expr[":"]=n.expr.pseudos,n.uniqueSort=n.unique=t.uniqueSort,n.text=t.getText,n.isXMLDoc=t.isXML,n.contains=t.contains;var u=function(a,b,c){var d=[],e=void 0!==c;while((a=a[b])&&9!==a.nodeType)if(1===a.nodeType){if(e&&n(a).is(c))break;d.push(a)}return d},v=function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c},w=n.expr.match.needsContext,x=/^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,y=/^.[^:#\[\.,]*$/;function z(a,b,c){if(n.isFunction(b))return n.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return n.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(y.test(b))return n.filter(b,a,c);b=n.filter(b,a)}return n.grep(a,function(a){return h.call(b,a)>-1!==c})}n.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?n.find.matchesSelector(d,a)?[d]:[]:n.find.matches(a,n.grep(b,function(a){return 1===a.nodeType}))},n.fn.extend({find:function(a){var b,c=this.length,d=[],e=this;if("string"!=typeof a)return this.pushStack(n(a).filter(function(){for(b=0;c>b;b++)if(n.contains(e[b],this))return!0}));for(b=0;c>b;b++)n.find(a,e[b],d);return d=this.pushStack(c>1?n.unique(d):d),d.selector=this.selector?this.selector+" "+a:a,d},filter:function(a){return this.pushStack(z(this,a||[],!1))},not:function(a){return this.pushStack(z(this,a||[],!0))},is:function(a){return!!z(this,"string"==typeof a&&w.test(a)?n(a):a||[],!1).length}});var A,B=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,C=n.fn.init=function(a,b,c){var e,f;if(!a)return this;if(c=c||A,"string"==typeof a){if(e="<"===a[0]&&">"===a[a.length-1]&&a.length>=3?[null,a,null]:B.exec(a),!e||!e[1]&&b)return!b||b.jquery?(b||c).find(a):this.constructor(b).find(a);if(e[1]){if(b=b instanceof n?b[0]:b,n.merge(this,n.parseHTML(e[1],b&&b.nodeType?b.ownerDocument||b:d,!0)),x.test(e[1])&&n.isPlainObject(b))for(e in b)n.isFunction(this[e])?this[e](b[e]):this.attr(e,b[e]);return this}return f=d.getElementById(e[2]),f&&f.parentNode&&(this.length=1,this[0]=f),this.context=d,this.selector=a,this}return a.nodeType?(this.context=this[0]=a,this.length=1,this):n.isFunction(a)?void 0!==c.ready?c.ready(a):a(n):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),n.makeArray(a,this))};C.prototype=n.fn,A=n(d);var D=/^(?:parents|prev(?:Until|All))/,E={children:!0,contents:!0,next:!0,prev:!0};n.fn.extend({has:function(a){var b=n(a,this),c=b.length;return this.filter(function(){for(var a=0;c>a;a++)if(n.contains(this,b[a]))return!0})},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=w.test(a)||"string"!=typeof a?n(a,b||this.context):0;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&n.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?n.uniqueSort(f):f)},index:function(a){return a?"string"==typeof a?h.call(n(a),this[0]):h.call(this,a.jquery?a[0]:a):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(n.uniqueSort(n.merge(this.get(),n(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function F(a,b){while((a=a[b])&&1!==a.nodeType);return a}n.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return u(a,"parentNode")},parentsUntil:function(a,b,c){return u(a,"parentNode",c)},next:function(a){return F(a,"nextSibling")},prev:function(a){return F(a,"previousSibling")},nextAll:function(a){return u(a,"nextSibling")},prevAll:function(a){return u(a,"previousSibling")},nextUntil:function(a,b,c){return u(a,"nextSibling",c)},prevUntil:function(a,b,c){return u(a,"previousSibling",c)},siblings:function(a){return v((a.parentNode||{}).firstChild,a)},children:function(a){return v(a.firstChild)},contents:function(a){return a.contentDocument||n.merge([],a.childNodes)}},function(a,b){n.fn[a]=function(c,d){var e=n.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=n.filter(d,e)),this.length>1&&(E[a]||n.uniqueSort(e),D.test(a)&&e.reverse()),this.pushStack(e)}});var G=/\S+/g;function H(a){var b={};return n.each(a.match(G)||[],function(a,c){b[c]=!0}),b}n.Callbacks=function(a){a="string"==typeof a?H(a):n.extend({},a);var b,c,d,e,f=[],g=[],h=-1,i=function(){for(e=a.once,d=b=!0;g.length;h=-1){c=g.shift();while(++h<f.length)f[h].apply(c[0],c[1])===!1&&a.stopOnFalse&&(h=f.length,c=!1)}a.memory||(c=!1),b=!1,e&&(f=c?[]:"")},j={add:function(){return f&&(c&&!b&&(h=f.length-1,g.push(c)),function d(b){n.each(b,function(b,c){n.isFunction(c)?a.unique&&j.has(c)||f.push(c):c&&c.length&&"string"!==n.type(c)&&d(c)})}(arguments),c&&!b&&i()),this},remove:function(){return n.each(arguments,function(a,b){var c;while((c=n.inArray(b,f,c))>-1)f.splice(c,1),h>=c&&h--}),this},has:function(a){return a?n.inArray(a,f)>-1:f.length>0},empty:function(){return f&&(f=[]),this},disable:function(){return e=g=[],f=c="",this},disabled:function(){return!f},lock:function(){return e=g=[],c||(f=c=""),this},locked:function(){return!!e},fireWith:function(a,c){return e||(c=c||[],c=[a,c.slice?c.slice():c],g.push(c),b||i()),this},fire:function(){return j.fireWith(this,arguments),this},fired:function(){return!!d}};return j},n.extend({Deferred:function(a){var b=[["resolve","done",n.Callbacks("once memory"),"resolved"],["reject","fail",n.Callbacks("once memory"),"rejected"],["notify","progress",n.Callbacks("memory")]],c="pending",d={state:function(){return c},always:function(){return e.done(arguments).fail(arguments),this},then:function(){var a=arguments;return n.Deferred(function(c){n.each(b,function(b,f){var g=n.isFunction(a[b])&&a[b];e[f[1]](function(){var a=g&&g.apply(this,arguments);a&&n.isFunction(a.promise)?a.promise().progress(c.notify).done(c.resolve).fail(c.reject):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)})}),a=null}).promise()},promise:function(a){return null!=a?n.extend(a,d):d}},e={};return d.pipe=d.then,n.each(b,function(a,f){var g=f[2],h=f[3];d[f[1]]=g.add,h&&g.add(function(){c=h},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this},e[f[0]+"With"]=g.fireWith}),d.promise(e),a&&a.call(e,e),e},when:function(a){var b=0,c=e.call(arguments),d=c.length,f=1!==d||a&&n.isFunction(a.promise)?d:0,g=1===f?a:n.Deferred(),h=function(a,b,c){return function(d){b[a]=this,c[a]=arguments.length>1?e.call(arguments):d,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)}},i,j,k;if(d>1)for(i=new Array(d),j=new Array(d),k=new Array(d);d>b;b++)c[b]&&n.isFunction(c[b].promise)?c[b].promise().progress(h(b,j,i)).done(h(b,k,c)).fail(g.reject):--f;return f||g.resolveWith(k,c),g.promise()}});var I;n.fn.ready=function(a){return n.ready.promise().done(a),this},n.extend({isReady:!1,readyWait:1,holdReady:function(a){a?n.readyWait++:n.ready(!0)},ready:function(a){(a===!0?--n.readyWait:n.isReady)||(n.isReady=!0,a!==!0&&--n.readyWait>0||(I.resolveWith(d,[n]),n.fn.triggerHandler&&(n(d).triggerHandler("ready"),n(d).off("ready"))))}});function J(){d.removeEventListener("DOMContentLoaded",J),a.removeEventListener("load",J),n.ready()}n.ready.promise=function(b){return I||(I=n.Deferred(),"complete"===d.readyState||"loading"!==d.readyState&&!d.documentElement.doScroll?a.setTimeout(n.ready):(d.addEventListener("DOMContentLoaded",J),a.addEventListener("load",J))),I.promise(b)},n.ready.promise();var K=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===n.type(c)){e=!0;for(h in c)K(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,n.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(n(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f},L=function(a){return 1===a.nodeType||9===a.nodeType||!+a.nodeType};function M(){this.expando=n.expando+M.uid++}M.uid=1,M.prototype={register:function(a,b){var c=b||{};return a.nodeType?a[this.expando]=c:Object.defineProperty(a,this.expando,{value:c,writable:!0,configurable:!0}),a[this.expando]},cache:function(a){if(!L(a))return{};var b=a[this.expando];return b||(b={},L(a)&&(a.nodeType?a[this.expando]=b:Object.defineProperty(a,this.expando,{value:b,configurable:!0}))),b},set:function(a,b,c){var d,e=this.cache(a);if("string"==typeof b)e[b]=c;else for(d in b)e[d]=b[d];return e},get:function(a,b){return void 0===b?this.cache(a):a[this.expando]&&a[this.expando][b]},access:function(a,b,c){var d;return void 0===b||b&&"string"==typeof b&&void 0===c?(d=this.get(a,b),void 0!==d?d:this.get(a,n.camelCase(b))):(this.set(a,b,c),void 0!==c?c:b)},remove:function(a,b){var c,d,e,f=a[this.expando];if(void 0!==f){if(void 0===b)this.register(a);else{n.isArray(b)?d=b.concat(b.map(n.camelCase)):(e=n.camelCase(b),b in f?d=[b,e]:(d=e,d=d in f?[d]:d.match(G)||[])),c=d.length;while(c--)delete f[d[c]]}(void 0===b||n.isEmptyObject(f))&&(a.nodeType?a[this.expando]=void 0:delete a[this.expando])}},hasData:function(a){var b=a[this.expando];return void 0!==b&&!n.isEmptyObject(b)}};var N=new M,O=new M,P=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,Q=/[A-Z]/g;function R(a,b,c){var d;if(void 0===c&&1===a.nodeType)if(d="data-"+b.replace(Q,"-$&").toLowerCase(),c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:P.test(c)?n.parseJSON(c):c}catch(e){}O.set(a,b,c);
}else c=void 0;return c}n.extend({hasData:function(a){return O.hasData(a)||N.hasData(a)},data:function(a,b,c){return O.access(a,b,c)},removeData:function(a,b){O.remove(a,b)},_data:function(a,b,c){return N.access(a,b,c)},_removeData:function(a,b){N.remove(a,b)}}),n.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=O.get(f),1===f.nodeType&&!N.get(f,"hasDataAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=n.camelCase(d.slice(5)),R(f,d,e[d])));N.set(f,"hasDataAttrs",!0)}return e}return"object"==typeof a?this.each(function(){O.set(this,a)}):K(this,function(b){var c,d;if(f&&void 0===b){if(c=O.get(f,a)||O.get(f,a.replace(Q,"-$&").toLowerCase()),void 0!==c)return c;if(d=n.camelCase(a),c=O.get(f,d),void 0!==c)return c;if(c=R(f,d,void 0),void 0!==c)return c}else d=n.camelCase(a),this.each(function(){var c=O.get(this,d);O.set(this,d,b),a.indexOf("-")>-1&&void 0!==c&&O.set(this,a,b)})},null,b,arguments.length>1,null,!0)},removeData:function(a){return this.each(function(){O.remove(this,a)})}}),n.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=N.get(a,b),c&&(!d||n.isArray(c)?d=N.access(a,b,n.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=n.queue(a,b),d=c.length,e=c.shift(),f=n._queueHooks(a,b),g=function(){n.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return N.get(a,c)||N.access(a,c,{empty:n.Callbacks("once memory").add(function(){N.remove(a,[b+"queue",c])})})}}),n.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?n.queue(this[0],a):void 0===b?this:this.each(function(){var c=n.queue(this,a,b);n._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&n.dequeue(this,a)})},dequeue:function(a){return this.each(function(){n.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=n.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=N.get(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}});var S=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,T=new RegExp("^(?:([+-])=|)("+S+")([a-z%]*)$","i"),U=["Top","Right","Bottom","Left"],V=function(a,b){return a=b||a,"none"===n.css(a,"display")||!n.contains(a.ownerDocument,a)};function W(a,b,c,d){var e,f=1,g=20,h=d?function(){return d.cur()}:function(){return n.css(a,b,"")},i=h(),j=c&&c[3]||(n.cssNumber[b]?"":"px"),k=(n.cssNumber[b]||"px"!==j&&+i)&&T.exec(n.css(a,b));if(k&&k[3]!==j){j=j||k[3],c=c||[],k=+i||1;do f=f||".5",k/=f,n.style(a,b,k+j);while(f!==(f=h()/i)&&1!==f&&--g)}return c&&(k=+k||+i||0,e=c[1]?k+(c[1]+1)*c[2]:+c[2],d&&(d.unit=j,d.start=k,d.end=e)),e}var X=/^(?:checkbox|radio)$/i,Y=/<([\w:-]+)/,Z=/^$|\/(?:java|ecma)script/i,$={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};$.optgroup=$.option,$.tbody=$.tfoot=$.colgroup=$.caption=$.thead,$.th=$.td;function _(a,b){var c="undefined"!=typeof a.getElementsByTagName?a.getElementsByTagName(b||"*"):"undefined"!=typeof a.querySelectorAll?a.querySelectorAll(b||"*"):[];return void 0===b||b&&n.nodeName(a,b)?n.merge([a],c):c}function aa(a,b){for(var c=0,d=a.length;d>c;c++)N.set(a[c],"globalEval",!b||N.get(b[c],"globalEval"))}var ba=/<|&#?\w+;/;function ca(a,b,c,d,e){for(var f,g,h,i,j,k,l=b.createDocumentFragment(),m=[],o=0,p=a.length;p>o;o++)if(f=a[o],f||0===f)if("object"===n.type(f))n.merge(m,f.nodeType?[f]:f);else if(ba.test(f)){g=g||l.appendChild(b.createElement("div")),h=(Y.exec(f)||["",""])[1].toLowerCase(),i=$[h]||$._default,g.innerHTML=i[1]+n.htmlPrefilter(f)+i[2],k=i[0];while(k--)g=g.lastChild;n.merge(m,g.childNodes),g=l.firstChild,g.textContent=""}else m.push(b.createTextNode(f));l.textContent="",o=0;while(f=m[o++])if(d&&n.inArray(f,d)>-1)e&&e.push(f);else if(j=n.contains(f.ownerDocument,f),g=_(l.appendChild(f),"script"),j&&aa(g),c){k=0;while(f=g[k++])Z.test(f.type||"")&&c.push(f)}return l}!function(){var a=d.createDocumentFragment(),b=a.appendChild(d.createElement("div")),c=d.createElement("input");c.setAttribute("type","radio"),c.setAttribute("checked","checked"),c.setAttribute("name","t"),b.appendChild(c),l.checkClone=b.cloneNode(!0).cloneNode(!0).lastChild.checked,b.innerHTML="<textarea>x</textarea>",l.noCloneChecked=!!b.cloneNode(!0).lastChild.defaultValue}();var da=/^key/,ea=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,fa=/^([^.]*)(?:\.(.+)|)/;function ga(){return!0}function ha(){return!1}function ia(){try{return d.activeElement}catch(a){}}function ja(a,b,c,d,e,f){var g,h;if("object"==typeof b){"string"!=typeof c&&(d=d||c,c=void 0);for(h in b)ja(a,h,c,d,b[h],f);return a}if(null==d&&null==e?(e=c,d=c=void 0):null==e&&("string"==typeof c?(e=d,d=void 0):(e=d,d=c,c=void 0)),e===!1)e=ha;else if(!e)return a;return 1===f&&(g=e,e=function(a){return n().off(a),g.apply(this,arguments)},e.guid=g.guid||(g.guid=n.guid++)),a.each(function(){n.event.add(this,b,e,d,c)})}n.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=N.get(a);if(r){c.handler&&(f=c,c=f.handler,e=f.selector),c.guid||(c.guid=n.guid++),(i=r.events)||(i=r.events={}),(g=r.handle)||(g=r.handle=function(b){return"undefined"!=typeof n&&n.event.triggered!==b.type?n.event.dispatch.apply(a,arguments):void 0}),b=(b||"").match(G)||[""],j=b.length;while(j--)h=fa.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o&&(l=n.event.special[o]||{},o=(e?l.delegateType:l.bindType)||o,l=n.event.special[o]||{},k=n.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&n.expr.match.needsContext.test(e),namespace:p.join(".")},f),(m=i[o])||(m=i[o]=[],m.delegateCount=0,l.setup&&l.setup.call(a,d,p,g)!==!1||a.addEventListener&&a.addEventListener(o,g)),l.add&&(l.add.call(a,k),k.handler.guid||(k.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,k):m.push(k),n.event.global[o]=!0)}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=N.hasData(a)&&N.get(a);if(r&&(i=r.events)){b=(b||"").match(G)||[""],j=b.length;while(j--)if(h=fa.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=n.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,m=i[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),g=f=m.length;while(f--)k=m[f],!e&&q!==k.origType||c&&c.guid!==k.guid||h&&!h.test(k.namespace)||d&&d!==k.selector&&("**"!==d||!k.selector)||(m.splice(f,1),k.selector&&m.delegateCount--,l.remove&&l.remove.call(a,k));g&&!m.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||n.removeEvent(a,o,r.handle),delete i[o])}else for(o in i)n.event.remove(a,o+b[j],c,d,!0);n.isEmptyObject(i)&&N.remove(a,"handle events")}},dispatch:function(a){a=n.event.fix(a);var b,c,d,f,g,h=[],i=e.call(arguments),j=(N.get(this,"events")||{})[a.type]||[],k=n.event.special[a.type]||{};if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=n.event.handlers.call(this,a,j),b=0;while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,c=0;while((g=f.handlers[c++])&&!a.isImmediatePropagationStopped())(!a.rnamespace||a.rnamespace.test(g.namespace))&&(a.handleObj=g,a.data=g.data,d=((n.event.special[g.origType]||{}).handle||g.handler).apply(f.elem,i),void 0!==d&&(a.result=d)===!1&&(a.preventDefault(),a.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,a),a.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&("click"!==a.type||isNaN(a.button)||a.button<1))for(;i!==this;i=i.parentNode||this)if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(d=[],c=0;h>c;c++)f=b[c],e=f.selector+" ",void 0===d[e]&&(d[e]=f.needsContext?n(e,this).index(i)>-1:n.find(e,this,null,[i]).length),d[e]&&d.push(f);d.length&&g.push({elem:i,handlers:d})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},props:"altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a}},mouseHooks:{props:"button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,e,f,g=b.button;return null==a.pageX&&null!=b.clientX&&(c=a.target.ownerDocument||d,e=c.documentElement,f=c.body,a.pageX=b.clientX+(e&&e.scrollLeft||f&&f.scrollLeft||0)-(e&&e.clientLeft||f&&f.clientLeft||0),a.pageY=b.clientY+(e&&e.scrollTop||f&&f.scrollTop||0)-(e&&e.clientTop||f&&f.clientTop||0)),a.which||void 0===g||(a.which=1&g?1:2&g?3:4&g?2:0),a}},fix:function(a){if(a[n.expando])return a;var b,c,e,f=a.type,g=a,h=this.fixHooks[f];h||(this.fixHooks[f]=h=ea.test(f)?this.mouseHooks:da.test(f)?this.keyHooks:{}),e=h.props?this.props.concat(h.props):this.props,a=new n.Event(g),b=e.length;while(b--)c=e[b],a[c]=g[c];return a.target||(a.target=d),3===a.target.nodeType&&(a.target=a.target.parentNode),h.filter?h.filter(a,g):a},special:{load:{noBubble:!0},focus:{trigger:function(){return this!==ia()&&this.focus?(this.focus(),!1):void 0},delegateType:"focusin"},blur:{trigger:function(){return this===ia()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return"checkbox"===this.type&&this.click&&n.nodeName(this,"input")?(this.click(),!1):void 0},_default:function(a){return n.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}}},n.removeEvent=function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c)},n.Event=function(a,b){return this instanceof n.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?ga:ha):this.type=a,b&&n.extend(this,b),this.timeStamp=a&&a.timeStamp||n.now(),void(this[n.expando]=!0)):new n.Event(a,b)},n.Event.prototype={constructor:n.Event,isDefaultPrevented:ha,isPropagationStopped:ha,isImmediatePropagationStopped:ha,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=ga,a&&a.preventDefault()},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=ga,a&&a.stopPropagation()},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=ga,a&&a.stopImmediatePropagation(),this.stopPropagation()}},n.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){n.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return(!e||e!==d&&!n.contains(d,e))&&(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),n.fn.extend({on:function(a,b,c,d){return ja(this,a,b,c,d)},one:function(a,b,c,d){return ja(this,a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,n(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return(b===!1||"function"==typeof b)&&(c=b,b=void 0),c===!1&&(c=ha),this.each(function(){n.event.remove(this,a,c,b)})}});var ka=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,la=/<script|<style|<link/i,ma=/checked\s*(?:[^=]|=\s*.checked.)/i,na=/^true\/(.*)/,oa=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;function pa(a,b){return n.nodeName(a,"table")&&n.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function qa(a){return a.type=(null!==a.getAttribute("type"))+"/"+a.type,a}function ra(a){var b=na.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function sa(a,b){var c,d,e,f,g,h,i,j;if(1===b.nodeType){if(N.hasData(a)&&(f=N.access(a),g=N.set(b,f),j=f.events)){delete g.handle,g.events={};for(e in j)for(c=0,d=j[e].length;d>c;c++)n.event.add(b,e,j[e][c])}O.hasData(a)&&(h=O.access(a),i=n.extend({},h),O.set(b,i))}}function ta(a,b){var c=b.nodeName.toLowerCase();"input"===c&&X.test(a.type)?b.checked=a.checked:("input"===c||"textarea"===c)&&(b.defaultValue=a.defaultValue)}function ua(a,b,c,d){b=f.apply([],b);var e,g,h,i,j,k,m=0,o=a.length,p=o-1,q=b[0],r=n.isFunction(q);if(r||o>1&&"string"==typeof q&&!l.checkClone&&ma.test(q))return a.each(function(e){var f=a.eq(e);r&&(b[0]=q.call(this,e,f.html())),ua(f,b,c,d)});if(o&&(e=ca(b,a[0].ownerDocument,!1,a,d),g=e.firstChild,1===e.childNodes.length&&(e=g),g||d)){for(h=n.map(_(e,"script"),qa),i=h.length;o>m;m++)j=e,m!==p&&(j=n.clone(j,!0,!0),i&&n.merge(h,_(j,"script"))),c.call(a[m],j,m);if(i)for(k=h[h.length-1].ownerDocument,n.map(h,ra),m=0;i>m;m++)j=h[m],Z.test(j.type||"")&&!N.access(j,"globalEval")&&n.contains(k,j)&&(j.src?n._evalUrl&&n._evalUrl(j.src):n.globalEval(j.textContent.replace(oa,"")))}return a}function va(a,b,c){for(var d,e=b?n.filter(b,a):a,f=0;null!=(d=e[f]);f++)c||1!==d.nodeType||n.cleanData(_(d)),d.parentNode&&(c&&n.contains(d.ownerDocument,d)&&aa(_(d,"script")),d.parentNode.removeChild(d));return a}n.extend({htmlPrefilter:function(a){return a.replace(ka,"<$1></$2>")},clone:function(a,b,c){var d,e,f,g,h=a.cloneNode(!0),i=n.contains(a.ownerDocument,a);if(!(l.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||n.isXMLDoc(a)))for(g=_(h),f=_(a),d=0,e=f.length;e>d;d++)ta(f[d],g[d]);if(b)if(c)for(f=f||_(a),g=g||_(h),d=0,e=f.length;e>d;d++)sa(f[d],g[d]);else sa(a,h);return g=_(h,"script"),g.length>0&&aa(g,!i&&_(a,"script")),h},cleanData:function(a){for(var b,c,d,e=n.event.special,f=0;void 0!==(c=a[f]);f++)if(L(c)){if(b=c[N.expando]){if(b.events)for(d in b.events)e[d]?n.event.remove(c,d):n.removeEvent(c,d,b.handle);c[N.expando]=void 0}c[O.expando]&&(c[O.expando]=void 0)}}}),n.fn.extend({domManip:ua,detach:function(a){return va(this,a,!0)},remove:function(a){return va(this,a)},text:function(a){return K(this,function(a){return void 0===a?n.text(this):this.empty().each(function(){(1===this.nodeType||11===this.nodeType||9===this.nodeType)&&(this.textContent=a)})},null,a,arguments.length)},append:function(){return ua(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=pa(this,a);b.appendChild(a)}})},prepend:function(){return ua(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=pa(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return ua(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return ua(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},empty:function(){for(var a,b=0;null!=(a=this[b]);b++)1===a.nodeType&&(n.cleanData(_(a,!1)),a.textContent="");return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return n.clone(this,a,b)})},html:function(a){return K(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a&&1===b.nodeType)return b.innerHTML;if("string"==typeof a&&!la.test(a)&&!$[(Y.exec(a)||["",""])[1].toLowerCase()]){a=n.htmlPrefilter(a);try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(n.cleanData(_(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=[];return ua(this,arguments,function(b){var c=this.parentNode;n.inArray(this,a)<0&&(n.cleanData(_(this)),c&&c.replaceChild(b,this))},a)}}),n.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){n.fn[a]=function(a){for(var c,d=[],e=n(a),f=e.length-1,h=0;f>=h;h++)c=h===f?this:this.clone(!0),n(e[h])[b](c),g.apply(d,c.get());return this.pushStack(d)}});var wa,xa={HTML:"block",BODY:"block"};function ya(a,b){var c=n(b.createElement(a)).appendTo(b.body),d=n.css(c[0],"display");return c.detach(),d}function za(a){var b=d,c=xa[a];return c||(c=ya(a,b),"none"!==c&&c||(wa=(wa||n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=wa[0].contentDocument,b.write(),b.close(),c=ya(a,b),wa.detach()),xa[a]=c),c}var Aa=/^margin/,Ba=new RegExp("^("+S+")(?!px)[a-z%]+$","i"),Ca=function(b){var c=b.ownerDocument.defaultView;return c&&c.opener||(c=a),c.getComputedStyle(b)},Da=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e},Ea=d.documentElement;!function(){var b,c,e,f,g=d.createElement("div"),h=d.createElement("div");if(h.style){h.style.backgroundClip="content-box",h.cloneNode(!0).style.backgroundClip="",l.clearCloneStyle="content-box"===h.style.backgroundClip,g.style.cssText="border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",g.appendChild(h);function i(){h.style.cssText="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%",h.innerHTML="",Ea.appendChild(g);var d=a.getComputedStyle(h);b="1%"!==d.top,f="2px"===d.marginLeft,c="4px"===d.width,h.style.marginRight="50%",e="4px"===d.marginRight,Ea.removeChild(g)}n.extend(l,{pixelPosition:function(){return i(),b},boxSizingReliable:function(){return null==c&&i(),c},pixelMarginRight:function(){return null==c&&i(),e},reliableMarginLeft:function(){return null==c&&i(),f},reliableMarginRight:function(){var b,c=h.appendChild(d.createElement("div"));return c.style.cssText=h.style.cssText="-webkit-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",c.style.marginRight=c.style.width="0",h.style.width="1px",Ea.appendChild(g),b=!parseFloat(a.getComputedStyle(c).marginRight),Ea.removeChild(g),h.removeChild(c),b}})}}();function Fa(a,b,c){var d,e,f,g,h=a.style;return c=c||Ca(a),g=c?c.getPropertyValue(b)||c[b]:void 0,""!==g&&void 0!==g||n.contains(a.ownerDocument,a)||(g=n.style(a,b)),c&&!l.pixelMarginRight()&&Ba.test(g)&&Aa.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f),void 0!==g?g+"":g}function Ga(a,b){return{get:function(){return a()?void delete this.get:(this.get=b).apply(this,arguments)}}}var Ha=/^(none|table(?!-c[ea]).+)/,Ia={position:"absolute",visibility:"hidden",display:"block"},Ja={letterSpacing:"0",fontWeight:"400"},Ka=["Webkit","O","Moz","ms"],La=d.createElement("div").style;function Ma(a){if(a in La)return a;var b=a[0].toUpperCase()+a.slice(1),c=Ka.length;while(c--)if(a=Ka[c]+b,a in La)return a}function Na(a,b,c){var d=T.exec(b);return d?Math.max(0,d[2]-(c||0))+(d[3]||"px"):b}function Oa(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=n.css(a,c+U[f],!0,e)),d?("content"===c&&(g-=n.css(a,"padding"+U[f],!0,e)),"margin"!==c&&(g-=n.css(a,"border"+U[f]+"Width",!0,e))):(g+=n.css(a,"padding"+U[f],!0,e),"padding"!==c&&(g+=n.css(a,"border"+U[f]+"Width",!0,e)));return g}function Pa(b,c,e){var f=!0,g="width"===c?b.offsetWidth:b.offsetHeight,h=Ca(b),i="border-box"===n.css(b,"boxSizing",!1,h);if(d.msFullscreenElement&&a.top!==a&&b.getClientRects().length&&(g=Math.round(100*b.getBoundingClientRect()[c])),0>=g||null==g){if(g=Fa(b,c,h),(0>g||null==g)&&(g=b.style[c]),Ba.test(g))return g;f=i&&(l.boxSizingReliable()||g===b.style[c]),g=parseFloat(g)||0}return g+Oa(b,c,e||(i?"border":"content"),f,h)+"px"}function Qa(a,b){for(var c,d,e,f=[],g=0,h=a.length;h>g;g++)d=a[g],d.style&&(f[g]=N.get(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&V(d)&&(f[g]=N.access(d,"olddisplay",za(d.nodeName)))):(e=V(d),"none"===c&&e||N.set(d,"olddisplay",e?c:n.css(d,"display"))));for(g=0;h>g;g++)d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"));return a}n.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Fa(a,"opacity");return""===c?"1":c}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":"cssFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=n.camelCase(b),i=a.style;return b=n.cssProps[h]||(n.cssProps[h]=Ma(h)||h),g=n.cssHooks[b]||n.cssHooks[h],void 0===c?g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b]:(f=typeof c,"string"===f&&(e=T.exec(c))&&e[1]&&(c=W(a,b,e),f="number"),null!=c&&c===c&&("number"===f&&(c+=e&&e[3]||(n.cssNumber[h]?"":"px")),l.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),g&&"set"in g&&void 0===(c=g.set(a,c,d))||(i[b]=c)),void 0)}},css:function(a,b,c,d){var e,f,g,h=n.camelCase(b);return b=n.cssProps[h]||(n.cssProps[h]=Ma(h)||h),g=n.cssHooks[b]||n.cssHooks[h],g&&"get"in g&&(e=g.get(a,!0,c)),void 0===e&&(e=Fa(a,b,d)),"normal"===e&&b in Ja&&(e=Ja[b]),""===c||c?(f=parseFloat(e),c===!0||isFinite(f)?f||0:e):e}}),n.each(["height","width"],function(a,b){n.cssHooks[b]={get:function(a,c,d){return c?Ha.test(n.css(a,"display"))&&0===a.offsetWidth?Da(a,Ia,function(){return Pa(a,b,d)}):Pa(a,b,d):void 0},set:function(a,c,d){var e,f=d&&Ca(a),g=d&&Oa(a,b,d,"border-box"===n.css(a,"boxSizing",!1,f),f);return g&&(e=T.exec(c))&&"px"!==(e[3]||"px")&&(a.style[b]=c,c=n.css(a,b)),Na(a,c,g)}}}),n.cssHooks.marginLeft=Ga(l.reliableMarginLeft,function(a,b){return b?(parseFloat(Fa(a,"marginLeft"))||a.getBoundingClientRect().left-Da(a,{marginLeft:0},function(){return a.getBoundingClientRect().left}))+"px":void 0}),n.cssHooks.marginRight=Ga(l.reliableMarginRight,function(a,b){return b?Da(a,{display:"inline-block"},Fa,[a,"marginRight"]):void 0}),n.each({margin:"",padding:"",border:"Width"},function(a,b){n.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+U[d]+b]=f[d]||f[d-2]||f[0];return e}},Aa.test(a)||(n.cssHooks[a+b].set=Na)}),n.fn.extend({css:function(a,b){return K(this,function(a,b,c){var d,e,f={},g=0;if(n.isArray(b)){for(d=Ca(a),e=b.length;e>g;g++)f[b[g]]=n.css(a,b[g],!1,d);return f}return void 0!==c?n.style(a,b,c):n.css(a,b)},a,b,arguments.length>1)},show:function(){return Qa(this,!0)},hide:function(){return Qa(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){V(this)?n(this).show():n(this).hide()})}});function Ra(a,b,c,d,e){return new Ra.prototype.init(a,b,c,d,e)}n.Tween=Ra,Ra.prototype={constructor:Ra,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||n.easing._default,this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(n.cssNumber[c]?"":"px")},cur:function(){var a=Ra.propHooks[this.prop];return a&&a.get?a.get(this):Ra.propHooks._default.get(this)},run:function(a){var b,c=Ra.propHooks[this.prop];return this.options.duration?this.pos=b=n.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):Ra.propHooks._default.set(this),this}},Ra.prototype.init.prototype=Ra.prototype,Ra.propHooks={_default:{get:function(a){var b;return 1!==a.elem.nodeType||null!=a.elem[a.prop]&&null==a.elem.style[a.prop]?a.elem[a.prop]:(b=n.css(a.elem,a.prop,""),b&&"auto"!==b?b:0)},set:function(a){n.fx.step[a.prop]?n.fx.step[a.prop](a):1!==a.elem.nodeType||null==a.elem.style[n.cssProps[a.prop]]&&!n.cssHooks[a.prop]?a.elem[a.prop]=a.now:n.style(a.elem,a.prop,a.now+a.unit)}}},Ra.propHooks.scrollTop=Ra.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},n.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2},_default:"swing"},n.fx=Ra.prototype.init,n.fx.step={};var Sa,Ta,Ua=/^(?:toggle|show|hide)$/,Va=/queueHooks$/;function Wa(){return a.setTimeout(function(){Sa=void 0}),Sa=n.now()}function Xa(a,b){var c,d=0,e={height:a};for(b=b?1:0;4>d;d+=2-b)c=U[d],e["margin"+c]=e["padding"+c]=a;return b&&(e.opacity=e.width=a),e}function Ya(a,b,c){for(var d,e=(_a.tweeners[b]||[]).concat(_a.tweeners["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function Za(a,b,c){var d,e,f,g,h,i,j,k,l=this,m={},o=a.style,p=a.nodeType&&V(a),q=N.get(a,"fxshow");c.queue||(h=n._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()}),h.unqueued++,l.always(function(){l.always(function(){h.unqueued--,n.queue(a,"fx").length||h.empty.fire()})})),1===a.nodeType&&("height"in b||"width"in b)&&(c.overflow=[o.overflow,o.overflowX,o.overflowY],j=n.css(a,"display"),k="none"===j?N.get(a,"olddisplay")||za(a.nodeName):j,"inline"===k&&"none"===n.css(a,"float")&&(o.display="inline-block")),c.overflow&&(o.overflow="hidden",l.always(function(){o.overflow=c.overflow[0],o.overflowX=c.overflow[1],o.overflowY=c.overflow[2]}));for(d in b)if(e=b[d],Ua.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(p?"hide":"show")){if("show"!==e||!q||void 0===q[d])continue;p=!0}m[d]=q&&q[d]||n.style(a,d)}else j=void 0;if(n.isEmptyObject(m))"inline"===("none"===j?za(a.nodeName):j)&&(o.display=j);else{q?"hidden"in q&&(p=q.hidden):q=N.access(a,"fxshow",{}),f&&(q.hidden=!p),p?n(a).show():l.done(function(){n(a).hide()}),l.done(function(){var b;N.remove(a,"fxshow");for(b in m)n.style(a,b,m[b])});for(d in m)g=Ya(p?q[d]:0,d,l),d in q||(q[d]=g.start,p&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))}}function $a(a,b){var c,d,e,f,g;for(c in a)if(d=n.camelCase(c),e=b[d],f=a[c],n.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=n.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function _a(a,b,c){var d,e,f=0,g=_a.prefilters.length,h=n.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=Sa||Wa(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:n.extend({},b),opts:n.extend(!0,{specialEasing:{},easing:n.easing._default},c),originalProperties:b,originalOptions:c,startTime:Sa||Wa(),duration:c.duration,tweens:[],createTween:function(b,c){var d=n.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?(h.notifyWith(a,[j,1,0]),h.resolveWith(a,[j,b])):h.rejectWith(a,[j,b]),this}}),k=j.props;for($a(k,j.opts.specialEasing);g>f;f++)if(d=_a.prefilters[f].call(j,a,k,j.opts))return n.isFunction(d.stop)&&(n._queueHooks(j.elem,j.opts.queue).stop=n.proxy(d.stop,d)),d;return n.map(k,Ya,j),n.isFunction(j.opts.start)&&j.opts.start.call(a,j),n.fx.timer(n.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}n.Animation=n.extend(_a,{tweeners:{"*":[function(a,b){var c=this.createTween(a,b);return W(c.elem,a,T.exec(b),c),c}]},tweener:function(a,b){n.isFunction(a)?(b=a,a=["*"]):a=a.match(G);for(var c,d=0,e=a.length;e>d;d++)c=a[d],_a.tweeners[c]=_a.tweeners[c]||[],_a.tweeners[c].unshift(b)},prefilters:[Za],prefilter:function(a,b){b?_a.prefilters.unshift(a):_a.prefilters.push(a)}}),n.speed=function(a,b,c){var d=a&&"object"==typeof a?n.extend({},a):{complete:c||!c&&b||n.isFunction(a)&&a,duration:a,easing:c&&b||b&&!n.isFunction(b)&&b};return d.duration=n.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in n.fx.speeds?n.fx.speeds[d.duration]:n.fx.speeds._default,(null==d.queue||d.queue===!0)&&(d.queue="fx"),d.old=d.complete,d.complete=function(){n.isFunction(d.old)&&d.old.call(this),d.queue&&n.dequeue(this,d.queue)},d},n.fn.extend({fadeTo:function(a,b,c,d){return this.filter(V).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=n.isEmptyObject(a),f=n.speed(b,c,d),g=function(){var b=_a(this,n.extend({},a),f);(e||N.get(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=n.timers,g=N.get(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&Va.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));(b||!c)&&n.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=N.get(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=n.timers,g=d?d.length:0;for(c.finish=!0,n.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),n.each(["toggle","show","hide"],function(a,b){var c=n.fn[b];n.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(Xa(b,!0),a,d,e)}}),n.each({slideDown:Xa("show"),slideUp:Xa("hide"),slideToggle:Xa("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){n.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),n.timers=[],n.fx.tick=function(){var a,b=0,c=n.timers;for(Sa=n.now();b<c.length;b++)a=c[b],a()||c[b]!==a||c.splice(b--,1);c.length||n.fx.stop(),Sa=void 0},n.fx.timer=function(a){n.timers.push(a),a()?n.fx.start():n.timers.pop()},n.fx.interval=13,n.fx.start=function(){Ta||(Ta=a.setInterval(n.fx.tick,n.fx.interval))},n.fx.stop=function(){a.clearInterval(Ta),Ta=null},n.fx.speeds={slow:600,fast:200,_default:400},n.fn.delay=function(b,c){return b=n.fx?n.fx.speeds[b]||b:b,c=c||"fx",this.queue(c,function(c,d){var e=a.setTimeout(c,b);d.stop=function(){a.clearTimeout(e)}})},function(){var a=d.createElement("input"),b=d.createElement("select"),c=b.appendChild(d.createElement("option"));a.type="checkbox",l.checkOn=""!==a.value,l.optSelected=c.selected,b.disabled=!0,l.optDisabled=!c.disabled,a=d.createElement("input"),a.value="t",a.type="radio",l.radioValue="t"===a.value}();var ab,bb=n.expr.attrHandle;n.fn.extend({attr:function(a,b){return K(this,n.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){n.removeAttr(this,a)})}}),n.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return"undefined"==typeof a.getAttribute?n.prop(a,b,c):(1===f&&n.isXMLDoc(a)||(b=b.toLowerCase(),e=n.attrHooks[b]||(n.expr.match.bool.test(b)?ab:void 0)),void 0!==c?null===c?void n.removeAttr(a,b):e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:(a.setAttribute(b,c+""),c):e&&"get"in e&&null!==(d=e.get(a,b))?d:(d=n.find.attr(a,b),null==d?void 0:d))},attrHooks:{type:{set:function(a,b){if(!l.radioValue&&"radio"===b&&n.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(G);if(f&&1===a.nodeType)while(c=f[e++])d=n.propFix[c]||c,n.expr.match.bool.test(c)&&(a[d]=!1),a.removeAttribute(c)}}),ab={set:function(a,b,c){return b===!1?n.removeAttr(a,c):a.setAttribute(c,c),c}},n.each(n.expr.match.bool.source.match(/\w+/g),function(a,b){var c=bb[b]||n.find.attr;bb[b]=function(a,b,d){var e,f;return d||(f=bb[b],bb[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,bb[b]=f),e}});var cb=/^(?:input|select|textarea|button)$/i,db=/^(?:a|area)$/i;n.fn.extend({prop:function(a,b){return K(this,n.prop,a,b,arguments.length>1)},removeProp:function(a){return this.each(function(){delete this[n.propFix[a]||a]})}}),n.extend({prop:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return 1===f&&n.isXMLDoc(a)||(b=n.propFix[b]||b,
e=n.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){var b=n.find.attr(a,"tabindex");return b?parseInt(b,10):cb.test(a.nodeName)||db.test(a.nodeName)&&a.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),l.optSelected||(n.propHooks.selected={get:function(a){var b=a.parentNode;return b&&b.parentNode&&b.parentNode.selectedIndex,null}}),n.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){n.propFix[this.toLowerCase()]=this});var eb=/[\t\r\n\f]/g;function fb(a){return a.getAttribute&&a.getAttribute("class")||""}n.fn.extend({addClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).addClass(a.call(this,b,fb(this)))});if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=fb(c),d=1===c.nodeType&&(" "+e+" ").replace(eb," ")){g=0;while(f=b[g++])d.indexOf(" "+f+" ")<0&&(d+=f+" ");h=n.trim(d),e!==h&&c.setAttribute("class",h)}}return this},removeClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).removeClass(a.call(this,b,fb(this)))});if(!arguments.length)return this.attr("class","");if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=fb(c),d=1===c.nodeType&&(" "+e+" ").replace(eb," ")){g=0;while(f=b[g++])while(d.indexOf(" "+f+" ")>-1)d=d.replace(" "+f+" "," ");h=n.trim(d),e!==h&&c.setAttribute("class",h)}}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):n.isFunction(a)?this.each(function(c){n(this).toggleClass(a.call(this,c,fb(this),b),b)}):this.each(function(){var b,d,e,f;if("string"===c){d=0,e=n(this),f=a.match(G)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else(void 0===a||"boolean"===c)&&(b=fb(this),b&&N.set(this,"__className__",b),this.setAttribute&&this.setAttribute("class",b||a===!1?"":N.get(this,"__className__")||""))})},hasClass:function(a){var b,c,d=0;b=" "+a+" ";while(c=this[d++])if(1===c.nodeType&&(" "+fb(c)+" ").replace(eb," ").indexOf(b)>-1)return!0;return!1}});var gb=/\r/g;n.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=n.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,n(this).val()):a,null==e?e="":"number"==typeof e?e+="":n.isArray(e)&&(e=n.map(e,function(a){return null==a?"":a+""})),b=n.valHooks[this.type]||n.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=n.valHooks[e.type]||n.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(gb,""):null==c?"":c)}}}),n.extend({valHooks:{option:{get:function(a){return n.trim(a.value)}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],(c.selected||i===e)&&(l.optDisabled?!c.disabled:null===c.getAttribute("disabled"))&&(!c.parentNode.disabled||!n.nodeName(c.parentNode,"optgroup"))){if(b=n(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=n.makeArray(b),g=e.length;while(g--)d=e[g],(d.selected=n.inArray(n.valHooks.option.get(d),f)>-1)&&(c=!0);return c||(a.selectedIndex=-1),f}}}}),n.each(["radio","checkbox"],function(){n.valHooks[this]={set:function(a,b){return n.isArray(b)?a.checked=n.inArray(n(a).val(),b)>-1:void 0}},l.checkOn||(n.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})});var hb=/^(?:focusinfocus|focusoutblur)$/;n.extend(n.event,{trigger:function(b,c,e,f){var g,h,i,j,l,m,o,p=[e||d],q=k.call(b,"type")?b.type:b,r=k.call(b,"namespace")?b.namespace.split("."):[];if(h=i=e=e||d,3!==e.nodeType&&8!==e.nodeType&&!hb.test(q+n.event.triggered)&&(q.indexOf(".")>-1&&(r=q.split("."),q=r.shift(),r.sort()),l=q.indexOf(":")<0&&"on"+q,b=b[n.expando]?b:new n.Event(q,"object"==typeof b&&b),b.isTrigger=f?2:3,b.namespace=r.join("."),b.rnamespace=b.namespace?new RegExp("(^|\\.)"+r.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=e),c=null==c?[b]:n.makeArray(c,[b]),o=n.event.special[q]||{},f||!o.trigger||o.trigger.apply(e,c)!==!1)){if(!f&&!o.noBubble&&!n.isWindow(e)){for(j=o.delegateType||q,hb.test(j+q)||(h=h.parentNode);h;h=h.parentNode)p.push(h),i=h;i===(e.ownerDocument||d)&&p.push(i.defaultView||i.parentWindow||a)}g=0;while((h=p[g++])&&!b.isPropagationStopped())b.type=g>1?j:o.bindType||q,m=(N.get(h,"events")||{})[b.type]&&N.get(h,"handle"),m&&m.apply(h,c),m=l&&h[l],m&&m.apply&&L(h)&&(b.result=m.apply(h,c),b.result===!1&&b.preventDefault());return b.type=q,f||b.isDefaultPrevented()||o._default&&o._default.apply(p.pop(),c)!==!1||!L(e)||l&&n.isFunction(e[q])&&!n.isWindow(e)&&(i=e[l],i&&(e[l]=null),n.event.triggered=q,e[q](),n.event.triggered=void 0,i&&(e[l]=i)),b.result}},simulate:function(a,b,c){var d=n.extend(new n.Event,c,{type:a,isSimulated:!0});n.event.trigger(d,null,b),d.isDefaultPrevented()&&c.preventDefault()}}),n.fn.extend({trigger:function(a,b){return this.each(function(){n.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?n.event.trigger(a,b,c,!0):void 0}}),n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){n.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),n.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}}),l.focusin="onfocusin"in a,l.focusin||n.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){n.event.simulate(b,a.target,n.event.fix(a))};n.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=N.access(d,b);e||d.addEventListener(a,c,!0),N.access(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=N.access(d,b)-1;e?N.access(d,b,e):(d.removeEventListener(a,c,!0),N.remove(d,b))}}});var ib=a.location,jb=n.now(),kb=/\?/;n.parseJSON=function(a){return JSON.parse(a+"")},n.parseXML=function(b){var c;if(!b||"string"!=typeof b)return null;try{c=(new a.DOMParser).parseFromString(b,"text/xml")}catch(d){c=void 0}return(!c||c.getElementsByTagName("parsererror").length)&&n.error("Invalid XML: "+b),c};var lb=/#.*$/,mb=/([?&])_=[^&]*/,nb=/^(.*?):[ \t]*([^\r\n]*)$/gm,ob=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,pb=/^(?:GET|HEAD)$/,qb=/^\/\//,rb={},sb={},tb="*/".concat("*"),ub=d.createElement("a");ub.href=ib.href;function vb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(G)||[];if(n.isFunction(c))while(d=f[e++])"+"===d[0]?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function wb(a,b,c,d){var e={},f=a===sb;function g(h){var i;return e[h]=!0,n.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function xb(a,b){var c,d,e=n.ajaxSettings.flatOptions||{};for(c in b)void 0!==b[c]&&((e[c]?a:d||(d={}))[c]=b[c]);return d&&n.extend(!0,a,d),a}function yb(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===d&&(d=a.mimeType||b.getResponseHeader("Content-Type"));if(d)for(e in h)if(h[e]&&h[e].test(d)){i.unshift(e);break}if(i[0]in c)f=i[0];else{for(e in c){if(!i[0]||a.converters[e+" "+i[0]]){f=e;break}g||(g=e)}f=f||g}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function zb(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}n.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:ib.href,type:"GET",isLocal:ob.test(ib.protocol),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":tb,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":n.parseJSON,"text xml":n.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?xb(xb(a,n.ajaxSettings),b):xb(n.ajaxSettings,a)},ajaxPrefilter:vb(rb),ajaxTransport:vb(sb),ajax:function(b,c){"object"==typeof b&&(c=b,b=void 0),c=c||{};var e,f,g,h,i,j,k,l,m=n.ajaxSetup({},c),o=m.context||m,p=m.context&&(o.nodeType||o.jquery)?n(o):n.event,q=n.Deferred(),r=n.Callbacks("once memory"),s=m.statusCode||{},t={},u={},v=0,w="canceled",x={readyState:0,getResponseHeader:function(a){var b;if(2===v){if(!h){h={};while(b=nb.exec(g))h[b[1].toLowerCase()]=b[2]}b=h[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return 2===v?g:null},setRequestHeader:function(a,b){var c=a.toLowerCase();return v||(a=u[c]=u[c]||a,t[a]=b),this},overrideMimeType:function(a){return v||(m.mimeType=a),this},statusCode:function(a){var b;if(a)if(2>v)for(b in a)s[b]=[s[b],a[b]];else x.always(a[x.status]);return this},abort:function(a){var b=a||w;return e&&e.abort(b),z(0,b),this}};if(q.promise(x).complete=r.add,x.success=x.done,x.error=x.fail,m.url=((b||m.url||ib.href)+"").replace(lb,"").replace(qb,ib.protocol+"//"),m.type=c.method||c.type||m.method||m.type,m.dataTypes=n.trim(m.dataType||"*").toLowerCase().match(G)||[""],null==m.crossDomain){j=d.createElement("a");try{j.href=m.url,j.href=j.href,m.crossDomain=ub.protocol+"//"+ub.host!=j.protocol+"//"+j.host}catch(y){m.crossDomain=!0}}if(m.data&&m.processData&&"string"!=typeof m.data&&(m.data=n.param(m.data,m.traditional)),wb(rb,m,c,x),2===v)return x;k=n.event&&m.global,k&&0===n.active++&&n.event.trigger("ajaxStart"),m.type=m.type.toUpperCase(),m.hasContent=!pb.test(m.type),f=m.url,m.hasContent||(m.data&&(f=m.url+=(kb.test(f)?"&":"?")+m.data,delete m.data),m.cache===!1&&(m.url=mb.test(f)?f.replace(mb,"$1_="+jb++):f+(kb.test(f)?"&":"?")+"_="+jb++)),m.ifModified&&(n.lastModified[f]&&x.setRequestHeader("If-Modified-Since",n.lastModified[f]),n.etag[f]&&x.setRequestHeader("If-None-Match",n.etag[f])),(m.data&&m.hasContent&&m.contentType!==!1||c.contentType)&&x.setRequestHeader("Content-Type",m.contentType),x.setRequestHeader("Accept",m.dataTypes[0]&&m.accepts[m.dataTypes[0]]?m.accepts[m.dataTypes[0]]+("*"!==m.dataTypes[0]?", "+tb+"; q=0.01":""):m.accepts["*"]);for(l in m.headers)x.setRequestHeader(l,m.headers[l]);if(m.beforeSend&&(m.beforeSend.call(o,x,m)===!1||2===v))return x.abort();w="abort";for(l in{success:1,error:1,complete:1})x[l](m[l]);if(e=wb(sb,m,c,x)){if(x.readyState=1,k&&p.trigger("ajaxSend",[x,m]),2===v)return x;m.async&&m.timeout>0&&(i=a.setTimeout(function(){x.abort("timeout")},m.timeout));try{v=1,e.send(t,z)}catch(y){if(!(2>v))throw y;z(-1,y)}}else z(-1,"No Transport");function z(b,c,d,h){var j,l,t,u,w,y=c;2!==v&&(v=2,i&&a.clearTimeout(i),e=void 0,g=h||"",x.readyState=b>0?4:0,j=b>=200&&300>b||304===b,d&&(u=yb(m,x,d)),u=zb(m,u,x,j),j?(m.ifModified&&(w=x.getResponseHeader("Last-Modified"),w&&(n.lastModified[f]=w),w=x.getResponseHeader("etag"),w&&(n.etag[f]=w)),204===b||"HEAD"===m.type?y="nocontent":304===b?y="notmodified":(y=u.state,l=u.data,t=u.error,j=!t)):(t=y,(b||!y)&&(y="error",0>b&&(b=0))),x.status=b,x.statusText=(c||y)+"",j?q.resolveWith(o,[l,y,x]):q.rejectWith(o,[x,y,t]),x.statusCode(s),s=void 0,k&&p.trigger(j?"ajaxSuccess":"ajaxError",[x,m,j?l:t]),r.fireWith(o,[x,y]),k&&(p.trigger("ajaxComplete",[x,m]),--n.active||n.event.trigger("ajaxStop")))}return x},getJSON:function(a,b,c){return n.get(a,b,c,"json")},getScript:function(a,b){return n.get(a,void 0,b,"script")}}),n.each(["get","post"],function(a,b){n[b]=function(a,c,d,e){return n.isFunction(c)&&(e=e||d,d=c,c=void 0),n.ajax(n.extend({url:a,type:b,dataType:e,data:c,success:d},n.isPlainObject(a)&&a))}}),n._evalUrl=function(a){return n.ajax({url:a,type:"GET",dataType:"script",async:!1,global:!1,"throws":!0})},n.fn.extend({wrapAll:function(a){var b;return n.isFunction(a)?this.each(function(b){n(this).wrapAll(a.call(this,b))}):(this[0]&&(b=n(a,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstElementChild)a=a.firstElementChild;return a}).append(this)),this)},wrapInner:function(a){return n.isFunction(a)?this.each(function(b){n(this).wrapInner(a.call(this,b))}):this.each(function(){var b=n(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=n.isFunction(a);return this.each(function(c){n(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){n.nodeName(this,"body")||n(this).replaceWith(this.childNodes)}).end()}}),n.expr.filters.hidden=function(a){return!n.expr.filters.visible(a)},n.expr.filters.visible=function(a){return a.offsetWidth>0||a.offsetHeight>0||a.getClientRects().length>0};var Ab=/%20/g,Bb=/\[\]$/,Cb=/\r?\n/g,Db=/^(?:submit|button|image|reset|file)$/i,Eb=/^(?:input|select|textarea|keygen)/i;function Fb(a,b,c,d){var e;if(n.isArray(b))n.each(b,function(b,e){c||Bb.test(a)?d(a,e):Fb(a+"["+("object"==typeof e&&null!=e?b:"")+"]",e,c,d)});else if(c||"object"!==n.type(b))d(a,b);else for(e in b)Fb(a+"["+e+"]",b[e],c,d)}n.param=function(a,b){var c,d=[],e=function(a,b){b=n.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};if(void 0===b&&(b=n.ajaxSettings&&n.ajaxSettings.traditional),n.isArray(a)||a.jquery&&!n.isPlainObject(a))n.each(a,function(){e(this.name,this.value)});else for(c in a)Fb(c,a[c],b,e);return d.join("&").replace(Ab,"+")},n.fn.extend({serialize:function(){return n.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=n.prop(this,"elements");return a?n.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!n(this).is(":disabled")&&Eb.test(this.nodeName)&&!Db.test(a)&&(this.checked||!X.test(a))}).map(function(a,b){var c=n(this).val();return null==c?null:n.isArray(c)?n.map(c,function(a){return{name:b.name,value:a.replace(Cb,"\r\n")}}):{name:b.name,value:c.replace(Cb,"\r\n")}}).get()}}),n.ajaxSettings.xhr=function(){try{return new a.XMLHttpRequest}catch(b){}};var Gb={0:200,1223:204},Hb=n.ajaxSettings.xhr();l.cors=!!Hb&&"withCredentials"in Hb,l.ajax=Hb=!!Hb,n.ajaxTransport(function(b){var c,d;return l.cors||Hb&&!b.crossDomain?{send:function(e,f){var g,h=b.xhr();if(h.open(b.type,b.url,b.async,b.username,b.password),b.xhrFields)for(g in b.xhrFields)h[g]=b.xhrFields[g];b.mimeType&&h.overrideMimeType&&h.overrideMimeType(b.mimeType),b.crossDomain||e["X-Requested-With"]||(e["X-Requested-With"]="XMLHttpRequest");for(g in e)h.setRequestHeader(g,e[g]);c=function(a){return function(){c&&(c=d=h.onload=h.onerror=h.onabort=h.onreadystatechange=null,"abort"===a?h.abort():"error"===a?"number"!=typeof h.status?f(0,"error"):f(h.status,h.statusText):f(Gb[h.status]||h.status,h.statusText,"text"!==(h.responseType||"text")||"string"!=typeof h.responseText?{binary:h.response}:{text:h.responseText},h.getAllResponseHeaders()))}},h.onload=c(),d=h.onerror=c("error"),void 0!==h.onabort?h.onabort=d:h.onreadystatechange=function(){4===h.readyState&&a.setTimeout(function(){c&&d()})},c=c("abort");try{h.send(b.hasContent&&b.data||null)}catch(i){if(c)throw i}},abort:function(){c&&c()}}:void 0}),n.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(a){return n.globalEval(a),a}}}),n.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET")}),n.ajaxTransport("script",function(a){if(a.crossDomain){var b,c;return{send:function(e,f){b=n("<script>").prop({charset:a.scriptCharset,src:a.url}).on("load error",c=function(a){b.remove(),c=null,a&&f("error"===a.type?404:200,a.type)}),d.head.appendChild(b[0])},abort:function(){c&&c()}}}});var Ib=[],Jb=/(=)\?(?=&|$)|\?\?/;n.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=Ib.pop()||n.expando+"_"+jb++;return this[a]=!0,a}}),n.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(Jb.test(b.url)?"url":"string"==typeof b.data&&0===(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&Jb.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=n.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(Jb,"$1"+e):b.jsonp!==!1&&(b.url+=(kb.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||n.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){void 0===f?n(a).removeProp(e):a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,Ib.push(e)),g&&n.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),l.createHTMLDocument=function(){var a=d.implementation.createHTMLDocument("").body;return a.innerHTML="<form></form><form></form>",2===a.childNodes.length}(),n.parseHTML=function(a,b,c){if(!a||"string"!=typeof a)return null;"boolean"==typeof b&&(c=b,b=!1),b=b||(l.createHTMLDocument?d.implementation.createHTMLDocument(""):d);var e=x.exec(a),f=!c&&[];return e?[b.createElement(e[1])]:(e=ca([a],b,f),f&&f.length&&n(f).remove(),n.merge([],e.childNodes))};var Kb=n.fn.load;n.fn.load=function(a,b,c){if("string"!=typeof a&&Kb)return Kb.apply(this,arguments);var d,e,f,g=this,h=a.indexOf(" ");return h>-1&&(d=n.trim(a.slice(h)),a=a.slice(0,h)),n.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(e="POST"),g.length>0&&n.ajax({url:a,type:e||"GET",dataType:"html",data:b}).done(function(a){f=arguments,g.html(d?n("<div>").append(n.parseHTML(a)).find(d):a)}).always(c&&function(a,b){g.each(function(){c.apply(g,f||[a.responseText,b,a])})}),this},n.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){n.fn[b]=function(a){return this.on(b,a)}}),n.expr.filters.animated=function(a){return n.grep(n.timers,function(b){return a===b.elem}).length};function Lb(a){return n.isWindow(a)?a:9===a.nodeType&&a.defaultView}n.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=n.css(a,"position"),l=n(a),m={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=n.css(a,"top"),i=n.css(a,"left"),j=("absolute"===k||"fixed"===k)&&(f+i).indexOf("auto")>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),n.isFunction(b)&&(b=b.call(a,c,n.extend({},h))),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using"in b?b.using.call(a,m):l.css(m)}},n.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){n.offset.setOffset(this,a,b)});var b,c,d=this[0],e={top:0,left:0},f=d&&d.ownerDocument;if(f)return b=f.documentElement,n.contains(b,d)?(e=d.getBoundingClientRect(),c=Lb(f),{top:e.top+c.pageYOffset-b.clientTop,left:e.left+c.pageXOffset-b.clientLeft}):e},position:function(){if(this[0]){var a,b,c=this[0],d={top:0,left:0};return"fixed"===n.css(c,"position")?b=c.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),n.nodeName(a[0],"html")||(d=a.offset()),d.top+=n.css(a[0],"borderTopWidth",!0),d.left+=n.css(a[0],"borderLeftWidth",!0)),{top:b.top-d.top-n.css(c,"marginTop",!0),left:b.left-d.left-n.css(c,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent;while(a&&"static"===n.css(a,"position"))a=a.offsetParent;return a||Ea})}}),n.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c="pageYOffset"===b;n.fn[a]=function(d){return K(this,function(a,d,e){var f=Lb(a);return void 0===e?f?f[b]:a[d]:void(f?f.scrollTo(c?f.pageXOffset:e,c?e:f.pageYOffset):a[d]=e)},a,d,arguments.length)}}),n.each(["top","left"],function(a,b){n.cssHooks[b]=Ga(l.pixelPosition,function(a,c){return c?(c=Fa(a,b),Ba.test(c)?n(a).position()[b]+"px":c):void 0})}),n.each({Height:"height",Width:"width"},function(a,b){n.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){n.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");return K(this,function(b,c,d){var e;return n.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?n.css(b,c,g):n.style(b,c,d,g)},b,f?d:void 0,f,null)}})}),n.fn.extend({bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)},size:function(){return this.length}}),n.fn.andSelf=n.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return n});var Mb=a.jQuery,Nb=a.$;return n.noConflict=function(b){return a.$===n&&(a.$=Nb),b&&a.jQuery===n&&(a.jQuery=Mb),n},b||(a.jQuery=a.$=n),n});
/*
 * jQuery Orbit Plugin 1.2.3
 * www.ZURB.com/playground
 * Copyright 2010, ZURB
 * Free to use under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 */


(function($) {

    $.fn.orbit = function(options) {

        //Defaults to extend options
        var defaults = {
            animation: 'horizontal-push', 		// fade, horizontal-slide, vertical-slide, horizontal-push
            animationSpeed: 600, 				// how fast animtions are
            timer: true, 						// true or false to have the timer
            advanceSpeed: 4000, 				// if timer is enabled, time between transitions
            pauseOnHover: false, 				// if you hover pauses the slider
            startClockOnMouseOut: false, 		// if clock should start on MouseOut
            startClockOnMouseOutAfter: 1000, 	// how long after MouseOut should the timer start again
            directionalNav: true, 				// manual advancing directional navs
            captions: true, 					// do you want captions?
            captionAnimation: 'fade', 			// fade, slideOpen, none
            captionAnimationSpeed: 600, 		// if so how quickly should they animate in
            bullets: false,						// true or false to activate the bullet navigation
            bulletThumbs: false,				// thumbnails for the bullets
            bulletThumbLocation: '',			// location from this file where thumbs will be
            afterSlideChange: function(){},     // empty function
            numbers: false
        };

        //Extend those options
        var options = $.extend(defaults, options);

        return this.each(function() {

// ==============
// ! SETUP
// ==============

            //Global Variables
            var activeSlide = 0,
                numberSlides = 0,
                orbitWidth,
                orbitHeight,
                locked;

            //Initialize
            var orbit = $(this).addClass('orbit'),
                orbitWrapper = orbit.wrap('<div class="orbit-wrapper" />').parent();
            orbit.add(orbitWidth).width('1px').height('1px');

            //Collect all slides and set slider size of largest image
            var slides = orbit.children('img, a, div');
            slides.each(function() {
                var _slide = $(this),
                    _slideWidth = _slide.width(),
                    _slideHeight = _slide.height();
                if(_slideWidth > orbit.width()) {
                    orbit.add(orbitWrapper).width(_slideWidth);
                    orbitWidth = orbit.width();
                }
                if(_slideHeight > orbit.height()) {
                    orbit.add(orbitWrapper).height(_slideHeight);
                    orbitHeight = orbit.height();
                }
                numberSlides++;
            });

            //Animation locking functions
            function unlock() {
                locked = false;
            }
            function lock() {
                locked = true;
            }

            //If there is only a single slide remove nav, timer and bullets
            if(slides.length == 1) {
                options.directionalNav = false;
                options.timer = false;
                options.bullets = false;
                options.numbers = false;
            }

            //Set initial front photo z-index and fades it in
            slides.eq(activeSlide)
                .css({"z-index" : 3})
                .fadeIn(function() {
                    //brings in all other slides IF css declares a display: none
                    slides.css({"display":"inline-block"})
                });

// ==============
// ! TIMER
// ==============

            //Timer Execution
            function startClock() {
                if(!options.timer  || options.timer == 'false') {
                    return false;
                    //if timer is hidden, don't need to do crazy calculations
                } else if(timer.is(':hidden')) {
                    clock = setInterval(function(e){
                        shift("next");
                    }, options.advanceSpeed);
                    //if timer is visible and working, let's do some math
                } else {
                    timerRunning = true;
                    pause.removeClass('active');
                    clock = setInterval(function(e){

                        /*  shift("next");
                         timerLineLeft
                         .css ({"width": 0})
                         .animate({"width": 40}, options.animationSpeed, resetAndUnlock);
                         timerLineRight.css ({
                         "width": widthTimerCss
                         });*/

                        /*var positionBackgroundCss = "-" + positionBackground +'px';

                         positionBackground += 0.4;*/

                        /* slides
                         .eq(activeSlide)
                         .css({"backgroundPosition":positionBackgroundCss});*/

                        /*  if(positionBackground > 40) {
                         positionBackground  = 0;



                         }*/



                        widthTimerline += widthTimerlineK;



                        slides
                            .eq(activeSlide)
                            .css({"backgroundPosition":widthTimerline*-2+'px'});

                        timerLineLeft.css ({
                            "width": widthTimerline+'%'
                        });
                        timerLineRight.css ({
                            "width": widthTimerline+'%'
                        });


                            ornament.css ({
                                "width": widthTimerline+'%'
                            });

                        if(widthTimerline > 100) {
                            /*  widthTimerline  = 0;
                             timerLineLeft.animate({"width":0}, options.animationSpeed);
                             timerLineRight.animate({"width":0}, options.animationSpeed);*/

                            shift("next");

                        }
                        /*    var degreeCSS = "rotate("+degrees+"deg)"
                         degrees += 2
                         rotator.css({
                         "-webkit-transform": degreeCSS,
                         "-moz-transform": degreeCSS,
                         "-o-transform": degreeCSS
                         });

                         if(degrees > options.advanceSpeed) {
                         rotator.removeClass('move');
                         mask.removeClass('move');
                         degrees = 0;
                         shift("next");
                         }*/
                    }, options.advanceSpeed/180);
                }
            }
            function stopClock() {
                if(!options.timer || options.timer == 'false') { return false; } else {
                    timerRunning = false;
                    clearInterval(clock);
                    pause.addClass('active');
                }
            }

            //Timer Setup
            if(options.timer) {
                var timerHTML = '<div class="timer">' +
                    '<div class="orbit-ornament"><div class="orbit-ornament2"></div></div>' +
                    '<div class="timer__left"> ' +
                    '<div class="timer-line timer-line-left"></div>' +
                    '<div class="timer-line2 timer-line2-left"></div>' +
                    '</div>' +
                    '<div class="timer__right"> ' +
                    '<div class="timer-line timer-line-right"></div>' +
                    '<div class="timer-line2 timer-line2-right"></div>' +
                    '</div>'+
                    '<span class="mask"><span class="rotator"></span></span><span class="pause"></span></div>'
                //var timerHTML = '<div class="timer"><span class="mask"><span class="rotator"></span></span><span class="pause"></span></div>'
                orbitWrapper.append(timerHTML);
                var timer = orbitWrapper.children('div.timer'),
                    timerRunning;
                if(timer.length != 0) {
                    var rotator = $('div.timer span.rotator'),
                        ornament = $('.orbit-ornament2'),
                        positionBackground = 0,
                        timerLineLeft = $('div.timer-line-left'),
                        timerLineRight = $('div.timer-line-right'),
                        widthTimerline = 0,
                        mask = $('div.timer span.mask'),
                        pause = $('div.timer span.pause'),
                        degrees = 0,
                        widthTimerlineK = options.advanceSpeed/180/100*1.5,
                        positionBackgroundK = options.advanceSpeed/180/100,
                        clock;
                    startClock();
                    timer.click(function() {
                        if(!timerRunning) {
                            startClock();
                        } else {
                            stopClock();
                        }
                    });
                    if(options.startClockOnMouseOut){
                        var outTimer;
                        orbitWrapper.mouseleave(function() {
                            outTimer = setTimeout(function() {
                                if(!timerRunning){
                                    startClock();
                                }
                            }, options.startClockOnMouseOutAfter)
                        })
                        orbitWrapper.mouseenter(function() {
                            clearTimeout(outTimer);
                        })
                    }
                }
            }

            //Pause Timer on hover
            if(options.pauseOnHover) {
                orbitWrapper.mouseenter(function() {
                    stopClock();
                });
            }

// ==============
// ! CAPTIONS
// ==============

            //Caption Setup
            if(options.captions) {
                var captionHTML = '<div class="orbit-caption"></div>';
                orbitWrapper.append(captionHTML);
                var caption = orbitWrapper.children('.orbit-caption');
                setCaption();
            }

            //Caption Execution
            function setCaption() {
                if(!options.captions || options.captions =="false") {
                    return false;
                } else {
                    console.log('caption');
                    var _captionLocation = slides.eq(activeSlide).data('caption'); //get ID from rel tag on image
                    _captionHTML = $(_captionLocation).html(); //get HTML from the matching HTML entity
                    //Set HTML for the caption if it exists
                    if(_captionHTML) {
                        caption
                            .attr('id',_captionLocation) // Add ID caption
                            .html(_captionHTML); // Change HTML in Caption
                        //Animations for Caption entrances
                        if(options.captionAnimation == 'none') {
                            caption.show();
                        }
                        if(options.captionAnimation == 'fade') {
                            caption.fadeIn(options.captionAnimationSpeed);

                            $(".orbit-caption__fade").fadeToggle(0);
                            $(".orbit-caption__fade").fadeToggle(400);

                        }
                        if(options.captionAnimation == 'slideOpen') {
                            caption.slideDown(options.captionAnimationSpeed);
                        }
                    } else {
                        //Animations for Caption exits
                        if(options.captionAnimation == 'none') {
                            caption.hide();
                        }
                        if(options.captionAnimation == 'fade') {
                            caption.fadeOut(options.captionAnimationSpeed);
                        }
                        if(options.captionAnimation == 'slideOpen') {
                            caption.slideUp(options.captionAnimationSpeed);
                        }
                    }
                }
            }

// ==================
// ! DIRECTIONAL NAV
// ==================

            //DirectionalNav { rightButton --> shift("next"), leftButton --> shift("prev");
            if(options.directionalNav) {
                if(options.directionalNav == "false") { return false; }
                var directionalNavHTML = '<div class="slider-nav"><span class="right">Right</span><span class="left">Left</span></div>';
                orbitWrapper.append(directionalNavHTML);
                var leftBtn = orbitWrapper.children('div.slider-nav').children('span.left'),
                    rightBtn = orbitWrapper.children('div.slider-nav').children('span.right');
             /*   var  divOrbit = $('.orbit').append(directionalNavHTML),
                    leftBtn = divOrbit.children('div.slider-nav').children('span.left'),
                    rightBtn = divOrbit.children('div.slider-nav').children('span.right');*/

                leftBtn.click(function() {
                    /* stopClock();*/
                    shift("prev");
                });
                rightBtn.click(function() {
                    /*  stopClock();*/
                    shift("next")
                });
            }
// ==================
// ! SLIDER NUMBERS
// ==================
            if(options.numbers == true) {
                var slideNumbersHTML = '<div class="orbit-numbers"><span class="orbit-numbers__current">1</span>/<span class="orbit-numbers__last">'+numberSlides+'</span></div>';
                orbitWrapper.append(slideNumbersHTML);
            }



            function sliderView(){
                $('.content-slider').css({position:'static'},{"width":500});

            }

// ==================
// ! BULLET NAV
// ==================

            //Bullet Nav Setup
            if(options.bullets) {
                var bulletHTML = '<ul class="orbit-bullets"></ul>';
                orbitWrapper.append(bulletHTML);
                var bullets = orbitWrapper.children('ul.orbit-bullets');

                for(i=0; i<numberSlides; i++) {
                    var liMarkup = $('<li>'+(i+1)+'</li>');
                    if(options.bulletThumbs) {

                        var	thumbName = slides.eq(i).data('thumb');
                        if(thumbName) {
                            var liMarkup = $('<li class="has-thumb">'+i+'</li>');

                            liMarkup.css({"background" : "url("+options.bulletThumbLocation+thumbName+") no-repeat"});
                        }


                    }

                    orbitWrapper.children('ul.orbit-bullets').append(liMarkup);
                    liMarkup.data('index',i);
                    liMarkup.click(function() {
                        stopClock();
                        shift($(this).data('index'));
                    });

                }

                setActiveBullet();

            }

            //Bullet Nav Execution
            function setActiveBullet() {
                if(!options.bullets) { return false; } else {
                    bullets.children('li').removeClass('active').eq(activeSlide).addClass('active');
                }
            }

// ====================
// ! SHIFT ANIMATIONS
// ====================

            //Animating the shift!
            function shift(direction) {
                //remember previous activeSlide
                var prevActiveSlide = activeSlide,
                    slideDirection = direction;
                //exit function if bullet clicked is same as the current image
                if(prevActiveSlide == slideDirection) { return false; }
                //reset Z & Unlock
                function resetAndUnlock() {
                    slides
                        .eq(prevActiveSlide)
                        .css({"z-index" : 1});
                    unlock();
                    options.afterSlideChange.call(this);
                }
                if(slides.length == "1") { return false; }
                if(!locked) {
                    lock();
                    //deduce the proper activeImage
                    if(direction == "next") {
                        activeSlide++
                        if(activeSlide == numberSlides) {
                            activeSlide = 0;
                        }
                    } else if(direction == "prev") {
                        activeSlide--
                        if(activeSlide < 0) {
                            activeSlide = numberSlides-1;
                        }
                    } else {
                        activeSlide = direction;
                        if (prevActiveSlide < activeSlide) {
                            slideDirection = "next";
                        } else if (prevActiveSlide > activeSlide) {
                            slideDirection = "prev"
                        }
                    }

                    if(options.timer== true){
                        widthTimerline  = 0;
                        timerLineLeft.animate({"width":0}, options.animationSpeed);
                        timerLineRight.animate({"width":0}, options.animationSpeed);
                    }
                    if(options.numbers == true ){
                        orbitWrapper.find(".orbit-numbers__current").text(activeSlide+1);
                    }



                    //set to correct bullet
                    setActiveBullet();

                    //set previous slide z-index to one below what new activeSlide will be
                    slides
                        .eq(prevActiveSlide)
                        .css({"z-index" : 2});

                    //fade
                    if(options.animation == "fade") {
                        slides
                            .eq(activeSlide)
                            .css({"opacity" : 0, "z-index" : 3})
                            .animate({"opacity" : 1}, options.animationSpeed, resetAndUnlock);
                    }
                    //horizontal-slide
                    if(options.animation == "horizontal-slide") {
                        if(slideDirection == "next") {
                            slides
                                .eq(activeSlide)
                                .css({"left": orbitWidth, "z-index" : 3})
                                .animate({"left" : 0}, options.animationSpeed, resetAndUnlock);
                        }
                        if(slideDirection == "prev") {
                            slides
                                .eq(activeSlide)
                                .css({"left": -orbitWidth, "z-index" : 3})
                                .animate({"left" : 0}, options.animationSpeed, resetAndUnlock);
                        }
                    }
                    //horizontal-rom
                    if(options.animation == "horizontal-rom") {
                        if(slideDirection == "next") {
                            slides
                                .eq(activeSlide)
                                .css({"z-index" : 2});
                            slides
                                .eq(prevActiveSlide)
                                .css({"width":orbitWidth, "z-index" : 3})
                                .animate({"width" :0 }, options.animationSpeed, function(){

                                    slides
                                        .eq(activeSlide)
                                        .css({"z-index" : 3})
                                        .animate({}, options.animationSpeed, resetAndUnlock);

                                    slides
                                        .eq(prevActiveSlide)
                                        .css({"width" : orbitWidth});

                                });

                        }
                        if(slideDirection == "prev") {
                            slides
                                .eq(activeSlide)
                                .css({"width":0,"left":0, "z-index" : 3})
                                .animate({"width" : orbitWidth}, options.animationSpeed, resetAndUnlock);
                        }
                    }
                    //vertical-slide
                    if(options.animation == "vertical-slide") {
                        if(slideDirection == "prev") {
                            slides
                                .eq(activeSlide)
                                .css({"top": orbitHeight, "z-index" : 3})
                                .animate({"top" : 0}, options.animationSpeed, resetAndUnlock);
                        }
                        if(slideDirection == "next") {
                            slides
                                .eq(activeSlide)
                                .css({"top": -orbitHeight, "z-index" : 3})
                                .animate({"top" : 0}, options.animationSpeed, resetAndUnlock);
                        }
                    }
                    //push-over
                    if(options.animation == "horizontal-push") {
                        if(slideDirection == "next") {
                            slides
                                .eq(activeSlide)
                                .css({"left": orbitWidth, "z-index" : 3})
                                .animate({"left" : 0}, options.animationSpeed, resetAndUnlock);
                            slides
                                .eq(prevActiveSlide)
                                .animate({"left" : -orbitWidth}, options.animationSpeed);
                        }
                        if(slideDirection == "prev") {
                            slides
                                .eq(activeSlide)
                                .css({"left": -orbitWidth, "z-index" : 3})
                                .animate({"left" : 0}, options.animationSpeed, resetAndUnlock);
                            slides
                                .eq(prevActiveSlide)
                                .animate({"left" : orbitWidth}, options.animationSpeed);
                        }
                    }
                    setCaption();
                } //lock
            }//orbit function
        });//each call
    }//orbit plugin call
})(jQuery);
/* jQuery Form Styler v1.7.4 | (c) Dimox | https://github.com/Dimox/jQueryFormStyler */
(function(b){"function"===typeof define&&define.amd?define(["../bower_components/jquery-form-styler/jquery.formstyler"],b):"object"===typeof exports?module.exports=b(require("jquery")):b(jQuery)})(function(b){function z(c, a){this.element=c;this.options=b.extend({},N,a);this.init()}function G(c){if(!b(c.target).parents().hasClass("jq-selectbox")&&"OPTION"!=c.target.nodeName&&b("div.jq-selectbox.opened").length){c=b("div.jq-selectbox.opened");var a=b("div.jq-selectbox__search input",c),f=b("div.jq-selectbox__dropdown",c);c.find("select").data("_"+
h).options.onSelectClosed.call(c);a.length&&a.val("").keyup();f.hide().find("li.sel").addClass("selected");c.removeClass("focused opened dropup dropdown")}}var h="styler",N={wrapper:"form",idSuffix:"-styler",filePlaceholder:"\u0424\u0430\u0439\u043b \u043d\u0435 \u0432\u044b\u0431\u0440\u0430\u043d",fileBrowse:"\u041e\u0431\u0437\u043e\u0440...",fileNumber:"\u0412\u044b\u0431\u0440\u0430\u043d\u043e \u0444\u0430\u0439\u043b\u043e\u0432: %s",selectPlaceholder:"\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435...",
selectSearch:!1,selectSearchLimit:10,selectSearchNotFound:"\u0421\u043e\u0432\u043f\u0430\u0434\u0435\u043d\u0438\u0439 \u043d\u0435 \u043d\u0430\u0439\u0434\u0435\u043d\u043e",selectSearchPlaceholder:"\u041f\u043e\u0438\u0441\u043a...",selectVisibleOptions:0,singleSelectzIndex:"100",selectSmartPositioning:!0,onSelectOpened:function(){},onSelectClosed:function(){},onFormStyled:function(){}};z.prototype={init:function(){function c(){var b="",d="",c="",e="";void 0!==a.attr("id")&&""!==a.attr("id")&&
(b=' id="'+a.attr("id")+f.idSuffix+'"');void 0!==a.attr("title")&&""!==a.attr("title")&&(d=' title="'+a.attr("title")+'"');void 0!==a.attr("class")&&""!==a.attr("class")&&(c=" "+a.attr("class"));var l=a.data(),t;for(t in l)""!==l[t]&&"_styler"!==t&&(e+=" data-"+t+'="'+l[t]+'"');this.id=b+e;this.title=d;this.classes=c}var a=b(this.element),f=this.options,y=navigator.userAgent.match(/(iPad|iPhone|iPod)/i)&&!navigator.userAgent.match(/(Windows\sPhone)/i)?!0:!1,h=navigator.userAgent.match(/Android/i)&&
!navigator.userAgent.match(/(Windows\sPhone)/i)?!0:!1;if(a.is(":checkbox")){var z=function(){var f=new c,d=b("<div"+f.id+' class="jq-checkbox'+f.classes+'"'+f.title+'><div class="jq-checkbox__div"></div></div>');a.css({position:"absolute",zIndex:"-1",opacity:0,margin:0,padding:0}).after(d).prependTo(d);d.attr("unselectable","on").css({"-webkit-user-select":"none","-moz-user-select":"none","-ms-user-select":"none","-o-user-select":"none","user-select":"none",display:"inline-block",position:"relative",
overflow:"hidden"});a.is(":checked")&&d.addClass("checked");a.is(":disabled")&&d.addClass("disabled");d.click(function(b){b.preventDefault();d.is(".disabled")||(a.is(":checked")?(a.prop("checked",!1),d.removeClass("checked")):(a.prop("checked",!0),d.addClass("checked")),a.focus().change())});a.closest("label").add('label[for="'+a.attr("id")+'"]').on("click.styler",function(a){b(a.target).is("a")||b(a.target).closest(d).length||(d.triggerHandler("click"),a.preventDefault())});a.on("change.styler",
function(){a.is(":checked")?d.addClass("checked"):d.removeClass("checked")}).on("keydown.styler",function(a){32==a.which&&d.click()}).on("focus.styler",function(){d.is(".disabled")||d.addClass("focused")}).on("blur.styler",function(){d.removeClass("focused")})};z();a.on("refresh",function(){a.closest("label").add('label[for="'+a.attr("id")+'"]').off(".styler");a.off(".styler").parent().before(a).remove();z()})}else if(a.is(":radio")){var B=function(){var x=new c,d=b("<div"+x.id+' class="jq-radio'+
x.classes+'"'+x.title+'><div class="jq-radio__div"></div></div>');a.css({position:"absolute",zIndex:"-1",opacity:0,margin:0,padding:0}).after(d).prependTo(d);d.attr("unselectable","on").css({"-webkit-user-select":"none","-moz-user-select":"none","-ms-user-select":"none","-o-user-select":"none","user-select":"none",display:"inline-block",position:"relative"});a.is(":checked")&&d.addClass("checked");a.is(":disabled")&&d.addClass("disabled");d.click(function(b){b.preventDefault();d.is(".disabled")||
(d.closest(f.wrapper).find('input[name="'+a.attr("name")+'"]').prop("checked",!1).parent().removeClass("checked"),a.prop("checked",!0).parent().addClass("checked"),a.focus().change())});a.closest("label").add('label[for="'+a.attr("id")+'"]').on("click.styler",function(a){b(a.target).is("a")||b(a.target).closest(d).length||(d.triggerHandler("click"),a.preventDefault())});a.on("change.styler",function(){a.parent().addClass("checked")}).on("focus.styler",function(){d.is(".disabled")||d.addClass("focused")}).on("blur.styler",
function(){d.removeClass("focused")})};B();a.on("refresh",function(){a.closest("label").add('label[for="'+a.attr("id")+'"]').off(".styler");a.off(".styler").parent().before(a).remove();B()})}else if(a.is(":file")){a.css({position:"absolute",top:0,right:0,width:"100%",height:"100%",opacity:0,margin:0,padding:0});var C=function(){var x=new c,d=a.data("placeholder");void 0===d&&(d=f.filePlaceholder);var A=a.data("browse");if(void 0===A||""===A)A=f.fileBrowse;var e=b("<div"+x.id+' class="jq-file'+x.classes+
'"'+x.title+' style="display: inline-block; position: relative; overflow: hidden"></div>'),l=b('<div class="jq-file__name">'+d+"</div>").appendTo(e);b('<div class="jq-file__browse">'+A+"</div>").appendTo(e);a.after(e).appendTo(e);a.is(":disabled")&&e.addClass("disabled");a.on("change.styler",function(){var b=a.val();if(a.is("[multiple]")){var b="",c=a[0].files.length;0<c&&(b=a.data("number"),void 0===b&&(b=f.fileNumber),b=b.replace("%s",c))}l.text(b.replace(/.+[\\\/]/,""));""===b?(l.text(d),e.removeClass("changed")):
e.addClass("changed")}).on("focus.styler",function(){e.addClass("focused")}).on("blur.styler",function(){e.removeClass("focused")}).on("click.styler",function(){e.removeClass("focused")})};C();a.on("refresh",function(){a.off(".styler").parent().before(a).remove();C()})}else if(a.is('input[type="number"]')){var D=function(){var c=b('<div class="jq-number"><div class="jq-number__spin minus"></div><div class="jq-number__spin plus"></div></div>');a.after(c).prependTo(c).wrap('<div class="jq-number__field"></div>');
a.is(":disabled")&&c.addClass("disabled");var d,f,e,l=null,t=null;void 0!==a.attr("min")&&(d=a.attr("min"));void 0!==a.attr("max")&&(f=a.attr("max"));e=void 0!==a.attr("step")&&b.isNumeric(a.attr("step"))?Number(a.attr("step")):Number(1);var K=function(s){var c=a.val(),k;b.isNumeric(c)||(c=0,a.val("0"));s.is(".minus")?(k=parseInt(c,10)-e,0<e&&(k=Math.ceil(k/e)*e)):s.is(".plus")&&(k=parseInt(c,10)+e,0<e&&(k=Math.floor(k/e)*e));b.isNumeric(d)&&b.isNumeric(f)?k>=d&&k<=f&&a.val(k):b.isNumeric(d)&&!b.isNumeric(f)?
k>=d&&a.val(k):!b.isNumeric(d)&&b.isNumeric(f)?k<=f&&a.val(k):a.val(k)};c.is(".disabled")||(c.on("mousedown","div.jq-number__spin",function(){var a=b(this);K(a);l=setTimeout(function(){t=setInterval(function(){K(a)},40)},350)}).on("mouseup mouseout","div.jq-number__spin",function(){clearTimeout(l);clearInterval(t)}),a.on("focus.styler",function(){c.addClass("focused")}).on("blur.styler",function(){c.removeClass("focused")}))};D();a.on("refresh",function(){a.off(".styler").closest(".jq-number").before(a).remove();
D()})}else if(a.is("select")){var M=function(){function x(a){a.off("mousewheel DOMMouseScroll").on("mousewheel DOMMouseScroll",function(a){var c=null;"mousewheel"==a.type?c=-1*a.originalEvent.wheelDelta:"DOMMouseScroll"==a.type&&(c=40*a.originalEvent.detail);c&&(a.stopPropagation(),a.preventDefault(),b(this).scrollTop(c+b(this).scrollTop()))})}function d(){for(var a=0;a<l.length;a++){var b=l.eq(a),c="",d="",e=c="",u="",p="",v="",w="",g="";b.prop("selected")&&(d="selected sel");b.is(":disabled")&&
(d="disabled");b.is(":selected:disabled")&&(d="selected sel disabled");void 0!==b.attr("id")&&""!==b.attr("id")&&(e=' id="'+b.attr("id")+f.idSuffix+'"');void 0!==b.attr("title")&&""!==l.attr("title")&&(u=' title="'+b.attr("title")+'"');void 0!==b.attr("class")&&(v=" "+b.attr("class"),g=' data-jqfs-class="'+b.attr("class")+'"');var h=b.data(),r;for(r in h)""!==h[r]&&(p+=" data-"+r+'="'+h[r]+'"');""!==d+v&&(c=' class="'+d+v+'"');c="<li"+g+p+c+u+e+">"+b.html()+"</li>";b.parent().is("optgroup")&&(void 0!==
b.parent().attr("class")&&(w=" "+b.parent().attr("class")),c="<li"+g+p+' class="'+d+v+" option"+w+'"'+u+e+">"+b.html()+"</li>",b.is(":first-child")&&(c='<li class="optgroup'+w+'">'+b.parent().attr("label")+"</li>"+c));t+=c}}function z(){var e=new c,s="",H=a.data("placeholder"),k=a.data("search"),h=a.data("search-limit"),u=a.data("search-not-found"),p=a.data("search-placeholder"),v=a.data("z-index"),w=a.data("smart-positioning");void 0===H&&(H=f.selectPlaceholder);if(void 0===k||""===k)k=f.selectSearch;
if(void 0===h||""===h)h=f.selectSearchLimit;if(void 0===u||""===u)u=f.selectSearchNotFound;void 0===p&&(p=f.selectSearchPlaceholder);if(void 0===v||""===v)v=f.singleSelectzIndex;if(void 0===w||""===w)w=f.selectSmartPositioning;var g=b("<div"+e.id+' class="jq-selectbox jqselect'+e.classes+'" style="display: inline-block; position: relative; z-index:'+v+'"><div class="jq-selectbox__select"'+e.title+' style="position: relative"><div class="jq-selectbox__select-text"></div><div class="jq-selectbox__trigger"><div class="jq-selectbox__trigger-arrow"></div></div></div></div>');
a.css({margin:0,padding:0}).after(g).prependTo(g);var L=b("div.jq-selectbox__select",g),r=b("div.jq-selectbox__select-text",g),e=l.filter(":selected");d();k&&(s='<div class="jq-selectbox__search"><input type="search" autocomplete="off" placeholder="'+p+'"></div><div class="jq-selectbox__not-found">'+u+"</div>");var m=b('<div class="jq-selectbox__dropdown" style="position: absolute">'+s+'<ul style="position: relative; list-style: none; overflow: auto; overflow-x: hidden">'+t+"</ul></div>");g.append(m);
var q=b("ul",m),n=b("li",m),E=b("input",m),A=b("div.jq-selectbox__not-found",m).hide();n.length<h&&E.parent().hide();""===a.val()?r.text(H).addClass("placeholder"):r.text(e.text());var F=0,B=0;n.each(function(){var a=b(this);a.css({display:"inline-block"});a.innerWidth()>F&&(F=a.innerWidth(),B=a.width());a.css({display:""})});r.is(".placeholder")&&r.width()>F?r.width(r.width()):(s=g.clone().appendTo("body").width("auto"),k=s.outerWidth(),s.remove(),k==g.outerWidth()&&r.width(B));F>g.width()&&m.width(F);
""===l.first().text()&&""!==a.data("placeholder")&&n.first().hide();a.css({position:"absolute",left:0,top:0,width:"100%",height:"100%",opacity:0});var C=g.outerHeight(),I=E.outerHeight(),J=q.css("max-height"),s=n.filter(".selected");1>s.length&&n.first().addClass("selected sel");void 0===n.data("li-height")&&n.data("li-height",n.outerHeight());var D=m.css("top");"auto"==m.css("left")&&m.css({left:0});"auto"==m.css("top")&&m.css({top:C});m.hide();s.length&&(l.first().text()!=e.text()&&g.addClass("changed"),
g.data("jqfs-class",s.data("jqfs-class")),g.addClass(s.data("jqfs-class")));if(a.is(":disabled"))return g.addClass("disabled"),!1;L.click(function(){b("div.jq-selectbox").filter(".opened").length&&f.onSelectClosed.call(b("div.jq-selectbox").filter(".opened"));a.focus();if(!y){var c=b(window),d=n.data("li-height"),e=g.offset().top,k=c.height()-C-(e-c.scrollTop()),p=a.data("visible-options");if(void 0===p||""===p)p=f.selectVisibleOptions;var s=5*d,h=d*p;0<p&&6>p&&(s=h);0===p&&(h="auto");var p=function(){m.height("auto").css({bottom:"auto",
top:D});var a=function(){q.css("max-height",Math.floor((k-20-I)/d)*d)};a();q.css("max-height",h);"none"!=J&&q.css("max-height",J);k<m.outerHeight()+20&&a()},r=function(){m.height("auto").css({top:"auto",bottom:D});var a=function(){q.css("max-height",Math.floor((e-c.scrollTop()-20-I)/d)*d)};a();q.css("max-height",h);"none"!=J&&q.css("max-height",J);e-c.scrollTop()-20<m.outerHeight()+20&&a()};!0===w||1===w?k>s+I+20?(p(),g.removeClass("dropup").addClass("dropdown")):(r(),g.removeClass("dropdown").addClass("dropup")):
(!1===w||0===w)&&k>s+I+20&&(p(),g.removeClass("dropup").addClass("dropdown"));g.offset().left+m.outerWidth()>c.width()&&m.css({left:"auto",right:0});b("div.jqselect").css({zIndex:v-1}).removeClass("opened");g.css({zIndex:v});m.is(":hidden")?(b("div.jq-selectbox__dropdown:visible").hide(),m.show(),g.addClass("opened focused"),f.onSelectOpened.call(g)):(m.hide(),g.removeClass("opened dropup dropdown"),b("div.jq-selectbox").filter(".opened").length&&f.onSelectClosed.call(g));E.length&&(E.val("").keyup(),
A.hide(),E.keyup(function(){var c=b(this).val();n.each(function(){b(this).html().match(RegExp(".*?"+c+".*?","i"))?b(this).show():b(this).hide()});""===l.first().text()&&""!==a.data("placeholder")&&n.first().hide();1>n.filter(":visible").length?A.show():A.hide()}));n.filter(".selected").length&&(""===a.val()?q.scrollTop(0):(0!==q.innerHeight()/d%2&&(d/=2),q.scrollTop(q.scrollTop()+n.filter(".selected").position().top-q.innerHeight()/2+d)));x(q)}});n.hover(function(){b(this).siblings().removeClass("selected")});
n.filter(".selected").text();n.filter(":not(.disabled):not(.optgroup)").click(function(){a.focus();var c=b(this),d=c.text();if(!c.is(".selected")){var e=c.index(),e=e-c.prevAll(".optgroup").length;c.addClass("selected sel").siblings().removeClass("selected sel");l.prop("selected",!1).eq(e).prop("selected",!0);r.text(d);g.data("jqfs-class")&&g.removeClass(g.data("jqfs-class"));g.data("jqfs-class",c.data("jqfs-class"));g.addClass(c.data("jqfs-class"));a.change()}m.hide();g.removeClass("opened dropup dropdown");
f.onSelectClosed.call(g)});m.mouseout(function(){b("li.sel",m).addClass("selected")});a.on("change.styler",function(){r.text(l.filter(":selected").text()).removeClass("placeholder");n.removeClass("selected sel").not(".optgroup").eq(a[0].selectedIndex).addClass("selected sel");l.first().text()!=n.filter(".selected").text()?g.addClass("changed"):g.removeClass("changed")}).on("focus.styler",function(){g.addClass("focused");b("div.jqselect").not(".focused").removeClass("opened dropup dropdown").find("div.jq-selectbox__dropdown").hide()}).on("blur.styler",
function(){g.removeClass("focused")}).on("keydown.styler keyup.styler",function(b){var c=n.data("li-height");""===a.val()?r.text(H).addClass("placeholder"):r.text(l.filter(":selected").text());n.removeClass("selected sel").not(".optgroup").eq(a[0].selectedIndex).addClass("selected sel");if(38==b.which||37==b.which||33==b.which||36==b.which)""===a.val()?q.scrollTop(0):q.scrollTop(q.scrollTop()+n.filter(".selected").position().top);40!=b.which&&39!=b.which&&34!=b.which&&35!=b.which||q.scrollTop(q.scrollTop()+
n.filter(".selected").position().top-q.innerHeight()+c);13==b.which&&(b.preventDefault(),m.hide(),g.removeClass("opened dropup dropdown"),f.onSelectClosed.call(g))}).on("keydown.styler",function(a){32==a.which&&(a.preventDefault(),L.click())});G.registered||(b(document).on("click",G),G.registered=!0)}function e(){var e=new c,f=b("<div"+e.id+' class="jq-select-multiple jqselect'+e.classes+'"'+e.title+' style="display: inline-block; position: relative"></div>');a.css({margin:0,padding:0}).after(f);
d();f.append("<ul>"+t+"</ul>");var h=b("ul",f).css({position:"relative","overflow-x":"hidden","-webkit-overflow-scrolling":"touch"}),k=b("li",f).attr("unselectable","on"),e=a.attr("size"),y=h.outerHeight(),u=k.outerHeight();void 0!==e&&0<e?h.css({height:u*e}):h.css({height:4*u});y>f.height()&&(h.css("overflowY","scroll"),x(h),k.filter(".selected").length&&h.scrollTop(h.scrollTop()+k.filter(".selected").position().top));a.prependTo(f).css({position:"absolute",left:0,top:0,width:"100%",height:"100%",
opacity:0});if(a.is(":disabled"))f.addClass("disabled"),l.each(function(){b(this).is(":selected")&&k.eq(b(this).index()).addClass("selected")});else if(k.filter(":not(.disabled):not(.optgroup)").click(function(c){a.focus();var d=b(this);c.ctrlKey||c.metaKey||d.addClass("selected");c.shiftKey||d.addClass("first");c.ctrlKey||(c.metaKey||c.shiftKey)||d.siblings().removeClass("selected first");if(c.ctrlKey||c.metaKey)d.is(".selected")?d.removeClass("selected first"):d.addClass("selected first"),d.siblings().removeClass("first");
if(c.shiftKey){var e=!1,f=!1;d.siblings().removeClass("selected").siblings(".first").addClass("selected");d.prevAll().each(function(){b(this).is(".first")&&(e=!0)});d.nextAll().each(function(){b(this).is(".first")&&(f=!0)});e&&d.prevAll().each(function(){if(b(this).is(".selected"))return!1;b(this).not(".disabled, .optgroup").addClass("selected")});f&&d.nextAll().each(function(){if(b(this).is(".selected"))return!1;b(this).not(".disabled, .optgroup").addClass("selected")});1==k.filter(".selected").length&&
d.addClass("first")}l.prop("selected",!1);k.filter(".selected").each(function(){var a=b(this),c=a.index();a.is(".option")&&(c-=a.prevAll(".optgroup").length);l.eq(c).prop("selected",!0)});a.change()}),l.each(function(a){b(this).data("optionIndex",a)}),a.on("change.styler",function(){k.removeClass("selected");var a=[];l.filter(":selected").each(function(){a.push(b(this).data("optionIndex"))});k.not(".optgroup").filter(function(c){return-1<b.inArray(c,a)}).addClass("selected")}).on("focus.styler",function(){f.addClass("focused")}).on("blur.styler",
function(){f.removeClass("focused")}),y>f.height())a.on("keydown.styler",function(a){38!=a.which&&37!=a.which&&33!=a.which||h.scrollTop(h.scrollTop()+k.filter(".selected").position().top-u);40!=a.which&&39!=a.which&&34!=a.which||h.scrollTop(h.scrollTop()+k.filter(".selected:last").position().top-h.innerHeight()+2*u)})}var l=b("option",a),t="";a.is("[multiple]")?h||y||e():z()};M();a.on("refresh",function(){a.off(".styler").parent().before(a).remove();M()})}else if(a.is(":reset"))a.on("click",function(){setTimeout(function(){a.closest(f.wrapper).find("input, select").trigger("refresh")},
1)})},destroy:function(){var c=b(this.element);c.is(":checkbox")||c.is(":radio")?(c.removeData("_"+h).off(".styler refresh").removeAttr("style").parent().before(c).remove(),c.closest("label").add('label[for="'+c.attr("id")+'"]').off(".styler")):c.is('input[type="number"]')?c.removeData("_"+h).off(".styler refresh").closest(".jq-number").before(c).remove():(c.is(":file")||c.is("select"))&&c.removeData("_"+h).off(".styler refresh").removeAttr("style").parent().before(c).remove()}};b.fn[h]=function(c){var a=
arguments;if(void 0===c||"object"===typeof c)return this.each(function(){b.data(this,"_"+h)||b.data(this,"_"+h,new z(this,c))}).promise().done(function(){var a=b(this[0]).data("_"+h);a&&a.options.onFormStyled.call()}),this;if("string"===typeof c&&"_"!==c[0]&&"init"!==c){var f;this.each(function(){var y=b.data(this,"_"+h);y instanceof z&&"function"===typeof y[c]&&(f=y[c].apply(y,Array.prototype.slice.call(a,1)))});return void 0!==f?f:this}};G.registered=!1});
/**
 * vivus - JavaScript library to make drawing animation on SVG
 * @version v0.3.0
 * @link https://github.com/maxwellito/vivus
 * @license MIT
 */

'use strict';

(function (window, document) {

  'use strict';

/**
 * Pathformer
 * Beta version
 *
 * Take any SVG version 1.1 and transform
 * child elements to 'path' elements
 *
 * This code is purely forked from
 * https://github.com/Waest/SVGPathConverter
 */

/**
 * Class constructor
 *
 * @param {DOM|String} element Dom element of the SVG or id of it
 */
function Pathformer(element) {
  // Test params
  if (typeof element === 'undefined') {
    throw new Error('Pathformer [constructor]: "element" parameter is required');
  }

  // Set the element
  if (element.constructor === String) {
    element = document.getElementById(element);
    if (!element) {
      throw new Error('Pathformer [constructor]: "element" parameter is not related to an existing ID');
    }
  }
  if (element.constructor instanceof window.SVGElement || /^svg$/i.test(element.nodeName)) {
    this.el = element;
  } else {
    throw new Error('Pathformer [constructor]: "element" parameter must be a string or a SVGelement');
  }

  // Start
  this.scan(element);
}

/**
 * List of tags which can be transformed
 * to path elements
 *
 * @type {Array}
 */
Pathformer.prototype.TYPES = ['line', 'ellipse', 'circle', 'polygon', 'polyline', 'rect'];

/**
 * List of attribute names which contain
 * data. This array list them to check if
 * they contain bad values, like percentage. 
 *
 * @type {Array}
 */
Pathformer.prototype.ATTR_WATCH = ['cx', 'cy', 'points', 'r', 'rx', 'ry', 'x', 'x1', 'x2', 'y', 'y1', 'y2'];

/**
 * Finds the elements compatible for transform
 * and apply the liked method
 *
 * @param  {object} options Object from the constructor
 */
Pathformer.prototype.scan = function (svg) {
  var fn, element, pathData, pathDom,
    elements = svg.querySelectorAll(this.TYPES.join(','));
  for (var i = 0; i < elements.length; i++) {
    element = elements[i];
    fn = this[element.tagName.toLowerCase() + 'ToPath'];
    pathData = fn(this.parseAttr(element.attributes));
    pathDom = this.pathMaker(element, pathData);
    element.parentNode.replaceChild(pathDom, element);
  }
};


/**
 * Read `line` element to extract and transform
 * data, to make it ready for a `path` object.
 *
 * @param  {DOMelement} element Line element to transform
 * @return {object}             Data for a `path` element
 */
Pathformer.prototype.lineToPath = function (element) {
  var newElement = {};
  newElement.d = 'M' + element.x1 + ',' + element.y1 + 'L' + element.x2 + ',' + element.y2;
  return newElement;
};

/**
 * Read `rect` element to extract and transform
 * data, to make it ready for a `path` object.
 * The radius-border is not taken in charge yet.
 * (your help is more than welcomed)
 *
 * @param  {DOMelement} element Rect element to transform
 * @return {object}             Data for a `path` element
 */
Pathformer.prototype.rectToPath = function (element) {
  var newElement = {},
    x = parseFloat(element.x) || 0,
    y = parseFloat(element.y) || 0,
    width = parseFloat(element.width) || 0,
    height = parseFloat(element.height) || 0;
  newElement.d  = 'M' + x + ' ' + y + ' ';
  newElement.d += 'L' + (x + width) + ' ' + y + ' ';
  newElement.d += 'L' + (x + width) + ' ' + (y + height) + ' ';
  newElement.d += 'L' + x + ' ' + (y + height) + ' Z';
  return newElement;
};

/**
 * Read `polyline` element to extract and transform
 * data, to make it ready for a `path` object.
 *
 * @param  {DOMelement} element Polyline element to transform
 * @return {object}             Data for a `path` element
 */
Pathformer.prototype.polylineToPath = function (element) {
  var i, path;
  var newElement = {};
  var points = element.points.trim().split(' ');
  
  // Reformatting if points are defined without commas
  if (element.points.indexOf(',') === -1) {
    var formattedPoints = [];
    for (i = 0; i < points.length; i+=2) {
      formattedPoints.push(points[i] + ',' + points[i+1]);
    }
    points = formattedPoints;
  }

  // Generate the path.d value
  path = 'M' + points[0];
  for(i = 1; i < points.length; i++) {
    if (points[i].indexOf(',') !== -1) {
      path += 'L' + points[i];
    }
  }
  newElement.d = path;
  return newElement;
};

/**
 * Read `polygon` element to extract and transform
 * data, to make it ready for a `path` object.
 * This method rely on polylineToPath, because the
 * logic is similar. The path created is just closed,
 * so it needs an 'Z' at the end.
 *
 * @param  {DOMelement} element Polygon element to transform
 * @return {object}             Data for a `path` element
 */
Pathformer.prototype.polygonToPath = function (element) {
  var newElement = Pathformer.prototype.polylineToPath(element);
  newElement.d += 'Z';
  return newElement;
};

/**
 * Read `ellipse` element to extract and transform
 * data, to make it ready for a `path` object.
 *
 * @param  {DOMelement} element ellipse element to transform
 * @return {object}             Data for a `path` element
 */
Pathformer.prototype.ellipseToPath = function (element) {
  var startX = element.cx - element.rx,
      startY = element.cy;
  var endX = parseFloat(element.cx) + parseFloat(element.rx),
      endY = element.cy;

  var newElement = {};
  newElement.d = 'M' + startX + ',' + startY +
                 'A' + element.rx + ',' + element.ry + ' 0,1,1 ' + endX + ',' + endY +
                 'A' + element.rx + ',' + element.ry + ' 0,1,1 ' + startX + ',' + endY;
  return newElement;
};

/**
 * Read `circle` element to extract and transform
 * data, to make it ready for a `path` object.
 *
 * @param  {DOMelement} element Circle element to transform
 * @return {object}             Data for a `path` element
 */
Pathformer.prototype.circleToPath = function (element) {
  var newElement = {};
  var startX = element.cx - element.r,
      startY = element.cy;
  var endX = parseFloat(element.cx) + parseFloat(element.r),
      endY = element.cy;
  newElement.d =  'M' + startX + ',' + startY +
                  'A' + element.r + ',' + element.r + ' 0,1,1 ' + endX + ',' + endY +
                  'A' + element.r + ',' + element.r + ' 0,1,1 ' + startX + ',' + endY;
  return newElement;
};

/**
 * Create `path` elements form original element
 * and prepared objects
 *
 * @param  {DOMelement} element  Original element to transform
 * @param  {object} pathData     Path data (from `toPath` methods)
 * @return {DOMelement}          Path element
 */
Pathformer.prototype.pathMaker = function (element, pathData) {
  var i, attr, pathTag = document.createElementNS('http://www.w3.org/2000/svg','path');
  for(i = 0; i < element.attributes.length; i++) {
    attr = element.attributes[i];
    if (this.ATTR_WATCH.indexOf(attr.name) === -1) {
      pathTag.setAttribute(attr.name, attr.value);
    }
  }
  for(i in pathData) {
    pathTag.setAttribute(i, pathData[i]);
  }
  return pathTag;
};

/**
 * Parse attributes of a DOM element to
 * get an object of attribute => value
 *
 * @param  {NamedNodeMap} attributes Attributes object from DOM element to parse
 * @return {object}                  Object of attributes
 */
Pathformer.prototype.parseAttr = function (element) {
  var attr, output = {};
  for (var i = 0; i < element.length; i++) {
    attr = element[i];
    // Check if no data attribute contains '%', or the transformation is impossible
    if (this.ATTR_WATCH.indexOf(attr.name) !== -1 && attr.value.indexOf('%') !== -1) {
      throw new Error('Pathformer [parseAttr]: a SVG shape got values in percentage. This cannot be transformed into \'path\' tags. Please use \'viewBox\'.');
    }
    output[attr.name] = attr.value;
  }
  return output;
};

  'use strict';

var requestAnimFrame, cancelAnimFrame, parsePositiveInt;

/**
 * Vivus
 * Beta version
 *
 * Take any SVG and make the animation
 * to give give the impression of live drawing
 *
 * This in more than just inspired from codrops
 * At that point, it's a pure fork.
 */

/**
 * Class constructor
 * option structure
 *   type: 'delayed'|'async'|'oneByOne'|'script' (to know if the item must be drawn asynchronously or not, default: delayed)
 *   duration: <int> (in frames)
 *   start: 'inViewport'|'manual'|'autostart' (start automatically the animation, default: inViewport)
 *   delay: <int> (delay between the drawing of first and last path)
 *   dashGap <integer> whitespace extra margin between dashes
 *   pathTimingFunction <function> timing animation function for each path element of the SVG
 *   animTimingFunction <function> timing animation function for the complete SVG
 *   forceRender <boolean> force the browser to re-render all updated path items
 *   selfDestroy <boolean> removes all extra styling on the SVG, and leaves it as original
 *
 * The attribute 'type' is by default on 'delayed'.
 *  - 'delayed'
 *    all paths are draw at the same time but with a
 *    little delay between them before start
 *  - 'async'
 *    all path are start and finish at the same time
 *  - 'oneByOne'
 *    only one path is draw at the time
 *    the end of the first one will trigger the draw
 *    of the next one
 *
 * All these values can be overwritten individually
 * for each path item in the SVG
 * The value of frames will always take the advantage of
 * the duration value.
 * If you fail somewhere, an error will be thrown.
 * Good luck.
 *
 * @constructor
 * @this {Vivus}
 * @param {DOM|String}   element  Dom element of the SVG or id of it
 * @param {Object}       options  Options about the animation
 * @param {Function}     callback Callback for the end of the animation
 */
function Vivus (element, options, callback) {

  // Setup
  this.isReady = false;
  this.setElement(element, options);
  this.setOptions(options);
  this.setCallback(callback);

  if (this.isReady) {
    this.init();
  }
}

/**
 * Timing functions
 ************************************** 
 * 
 * Default functions to help developers.
 * It always take a number as parameter (between 0 to 1) then
 * return a number (between 0 and 1)
 */
Vivus.LINEAR          = function (x) {return x;};
Vivus.EASE            = function (x) {return -Math.cos(x * Math.PI) / 2 + 0.5;};
Vivus.EASE_OUT        = function (x) {return 1 - Math.pow(1-x, 3);};
Vivus.EASE_IN         = function (x) {return Math.pow(x, 3);};
Vivus.EASE_OUT_BOUNCE = function (x) {
  var base = -Math.cos(x * (0.5 * Math.PI)) + 1,
    rate = Math.pow(base,1.5),
    rateR = Math.pow(1 - x, 2),
    progress = -Math.abs(Math.cos(rate * (2.5 * Math.PI) )) + 1;
  return (1- rateR) + (progress * rateR);
};


/**
 * Setters
 **************************************
 */

/**
 * Check and set the element in the instance
 * The method will not return anything, but will throw an
 * error if the parameter is invalid
 *
 * @param {DOM|String}   element  SVG Dom element or id of it
 */
Vivus.prototype.setElement = function (element, options) {
  // Basic check
  if (typeof element === 'undefined') {
    throw new Error('Vivus [constructor]: "element" parameter is required');
  }

  // Set the element
  if (element.constructor === String) {
    element = document.getElementById(element);
    if (!element) {
      throw new Error('Vivus [constructor]: "element" parameter is not related to an existing ID');
    }
  }
  this.parentEl = element;

  // Create the object element if the property `file` exists in the options object
  if (options && options.file) {
    var objElm = document.createElement('object');
    objElm.setAttribute('type', 'image/svg+xml');
    objElm.setAttribute('data', options.file);
    objElm.setAttribute('built-by-vivus', 'true');
    element.appendChild(objElm);
    element = objElm;
  }

  switch (element.constructor) {
  case window.SVGSVGElement:
  case window.SVGElement:
    this.el = element;
    this.isReady = true;
    break;

  case window.HTMLObjectElement:
    // If we have to wait for it
    var onLoad, self;
    
    self = this;
    onLoad = function (e) {
      if (self.isReady) {
        return;
      }
      self.el = element.contentDocument && element.contentDocument.querySelector('svg');
      if (!self.el && e) {
        throw new Error('Vivus [constructor]: object loaded does not contain any SVG');
      }
      else if (self.el) {
        if (element.getAttribute('built-by-vivus')) {
          self.parentEl.insertBefore(self.el, element);
          self.parentEl.removeChild(element);
          self.el.setAttribute('width', '100%');
          self.el.setAttribute('height', '100%');
        }
        self.isReady = true;
        self.init();
        return true;
      }
    };

    if (!onLoad()) {
      element.addEventListener('load', onLoad);
    }
    break;

  default:
    throw new Error('Vivus [constructor]: "element" parameter is not valid (or miss the "file" attribute)');
  }
};

/**
 * Set up user option to the instance
 * The method will not return anything, but will throw an
 * error if the parameter is invalid
 *
 * @param  {object} options Object from the constructor
 */
Vivus.prototype.setOptions = function (options) {
  var allowedTypes = ['delayed', 'async', 'oneByOne', 'scenario', 'scenario-sync'];
  var allowedStarts =  ['inViewport', 'manual', 'autostart'];

  // Basic check
  if (options !== undefined && options.constructor !== Object) {
    throw new Error('Vivus [constructor]: "options" parameter must be an object');
  }
  else {
    options = options || {};
  }

  // Set the animation type
  if (options.type && allowedTypes.indexOf(options.type) === -1) {
    throw new Error('Vivus [constructor]: ' + options.type + ' is not an existing animation `type`');
  }
  else {
    this.type = options.type || allowedTypes[0];
  }

  // Set the start type
  if (options.start && allowedStarts.indexOf(options.start) === -1) {
    throw new Error('Vivus [constructor]: ' + options.start + ' is not an existing `start` option');
  }
  else {
    this.start = options.start || allowedStarts[0];
  }

  this.isIE        = (window.navigator.userAgent.indexOf('MSIE') !== -1 || window.navigator.userAgent.indexOf('Trident/') !== -1 || window.navigator.userAgent.indexOf('Edge/') !== -1 );
  this.duration    = parsePositiveInt(options.duration, 120);
  this.delay       = parsePositiveInt(options.delay, null);
  this.dashGap     = parsePositiveInt(options.dashGap, 1);
  this.forceRender = options.hasOwnProperty('forceRender') ? !!options.forceRender : this.isIE;
  this.selfDestroy = !!options.selfDestroy;
  this.onReady     = options.onReady;
  this.frameLength = this.currentFrame = this.map = this.delayUnit = this.speed = this.handle = null;

  this.ignoreInvisible = options.hasOwnProperty('ignoreInvisible') ? !!options.ignoreInvisible : false;

  this.animTimingFunction = options.animTimingFunction || Vivus.LINEAR;
  this.pathTimingFunction = options.pathTimingFunction || Vivus.LINEAR;

  if (this.delay >= this.duration) {
    throw new Error('Vivus [constructor]: delay must be shorter than duration');
  }
};

/**
 * Set up callback to the instance
 * The method will not return enything, but will throw an
 * error if the parameter is invalid
 *
 * @param  {Function} callback Callback for the animation end
 */
Vivus.prototype.setCallback = function (callback) {
  // Basic check
  if (!!callback && callback.constructor !== Function) {
    throw new Error('Vivus [constructor]: "callback" parameter must be a function');
  }
  this.callback = callback || function () {};
};


/**
 * Core
 **************************************
 */

/**
 * Map the svg, path by path.
 * The method return nothing, it just fill the
 * `map` array. Each item in this array represent
 * a path element from the SVG, with informations for
 * the animation.
 *
 * ```
 * [
 *   {
 *     el: <DOMobj> the path element
 *     length: <number> length of the path line
 *     startAt: <number> time start of the path animation (in frames)
 *     duration: <number> path animation duration (in frames)
 *   },
 *   ...
 * ]
 * ```
 *
 */
Vivus.prototype.mapping = function () {
  var i, paths, path, pAttrs, pathObj, totalLength, lengthMeter, timePoint;
  timePoint = totalLength = lengthMeter = 0;
  paths = this.el.querySelectorAll('path');

  for (i = 0; i < paths.length; i++) {
    path = paths[i];
    if (this.isInvisible(path)) {
      continue;
    }
    pathObj = {
      el: path,
      length: Math.ceil(path.getTotalLength())
    };
    // Test if the path length is correct
    if (isNaN(pathObj.length)) {
      if (window.console && console.warn) {
        console.warn('Vivus [mapping]: cannot retrieve a path element length', path);
      }
      continue;
    }
    this.map.push(pathObj);
    path.style.strokeDasharray  = pathObj.length + ' ' + (pathObj.length + this.dashGap * 2);
    path.style.strokeDashoffset = pathObj.length + this.dashGap;
    pathObj.length += this.dashGap;
    totalLength += pathObj.length;

    this.renderPath(i);
  }

  totalLength = totalLength === 0 ? 1 : totalLength;
  this.delay = this.delay === null ? this.duration / 3 : this.delay;
  this.delayUnit = this.delay / (paths.length > 1 ? paths.length - 1 : 1);

  for (i = 0; i < this.map.length; i++) {
    pathObj = this.map[i];

    switch (this.type) {
    case 'delayed':
      pathObj.startAt = this.delayUnit * i;
      pathObj.duration = this.duration - this.delay;
      break;

    case 'oneByOne':
      pathObj.startAt = lengthMeter / totalLength * this.duration;
      pathObj.duration = pathObj.length / totalLength * this.duration;
      break;

    case 'async':
      pathObj.startAt = 0;
      pathObj.duration = this.duration;
      break;

    case 'scenario-sync':
      path = paths[i];
      pAttrs = this.parseAttr(path);
      pathObj.startAt = timePoint + (parsePositiveInt(pAttrs['data-delay'], this.delayUnit) || 0);
      pathObj.duration = parsePositiveInt(pAttrs['data-duration'], this.duration);
      timePoint = pAttrs['data-async'] !== undefined ? pathObj.startAt : pathObj.startAt + pathObj.duration;
      this.frameLength = Math.max(this.frameLength, (pathObj.startAt + pathObj.duration));
      break;

    case 'scenario':
      path = paths[i];
      pAttrs = this.parseAttr(path);
      pathObj.startAt = parsePositiveInt(pAttrs['data-start'], this.delayUnit) || 0;
      pathObj.duration = parsePositiveInt(pAttrs['data-duration'], this.duration);
      this.frameLength = Math.max(this.frameLength, (pathObj.startAt + pathObj.duration));
      break;
    }
    lengthMeter += pathObj.length;
    this.frameLength = this.frameLength || this.duration;
  }
};

/**
 * Interval method to draw the SVG from current
 * position of the animation. It update the value of
 * `currentFrame` and re-trace the SVG.
 *
 * It use this.handle to store the requestAnimationFrame
 * and clear it one the animation is stopped. So this
 * attribute can be used to know if the animation is
 * playing.
 *
 * Once the animation at the end, this method will
 * trigger the Vivus callback.
 *
 */
Vivus.prototype.drawer = function () {
  var self = this;
  this.currentFrame += this.speed;

  if (this.currentFrame <= 0) {
    this.stop();
    this.reset();
    this.callback(this);
  } else if (this.currentFrame >= this.frameLength) {
    this.stop();
    this.currentFrame = this.frameLength;
    this.trace();
    if (this.selfDestroy) {
      this.destroy();
    }
    this.callback(this);
  } else {
    this.trace();
    this.handle = requestAnimFrame(function () {
      self.drawer();
    });
  }
};

/**
 * Draw the SVG at the current instant from the
 * `currentFrame` value. Here is where most of the magic is.
 * The trick is to use the `strokeDashoffset` style property.
 *
 * For optimisation reasons, a new property called `progress`
 * is added in each item of `map`. This one contain the current
 * progress of the path element. Only if the new value is different
 * the new value will be applied to the DOM element. This
 * method save a lot of resources to re-render the SVG. And could
 * be improved if the animation couldn't be played forward.
 *
 */
Vivus.prototype.trace = function () {
  var i, progress, path, currentFrame;
  currentFrame = this.animTimingFunction(this.currentFrame / this.frameLength) * this.frameLength;
  for (i = 0; i < this.map.length; i++) {
    path = this.map[i];
    progress = (currentFrame - path.startAt) / path.duration;
    progress = this.pathTimingFunction(Math.max(0, Math.min(1, progress)));
    if (path.progress !== progress) {
      path.progress = progress;
      path.el.style.strokeDashoffset = Math.floor(path.length * (1 - progress));
      this.renderPath(i);
    }
  }
};

/**
 * Method forcing the browser to re-render a path element
 * from it's index in the map. Depending on the `forceRender`
 * value.
 * The trick is to replace the path element by it's clone.
 * This practice is not recommended because it's asking more
 * ressources, too much DOM manupulation..
 * but it's the only way to let the magic happen on IE.
 * By default, this fallback is only applied on IE.
 * 
 * @param  {Number} index Path index
 */
Vivus.prototype.renderPath = function (index) {
  if (this.forceRender && this.map && this.map[index]) {
    var pathObj = this.map[index],
        newPath = pathObj.el.cloneNode(true);
    pathObj.el.parentNode.replaceChild(newPath, pathObj.el);
    pathObj.el = newPath;
  }
};

/**
 * When the SVG object is loaded and ready,
 * this method will continue the initialisation.
 *
 * This this mainly due to the case of passing an
 * object tag in the constructor. It will wait
 * the end of the loading to initialise.
 * 
 */
Vivus.prototype.init = function () {
  // Set object variables
  this.frameLength = 0;
  this.currentFrame = 0;
  this.map = [];

  // Start
  new Pathformer(this.el);
  this.mapping();
  this.starter();

  if (this.onReady) {
    this.onReady(this);
  }
};

/**
 * Trigger to start of the animation.
 * Depending on the `start` value, a different script
 * will be applied.
 *
 * If the `start` value is not valid, an error will be thrown.
 * Even if technically, this is impossible.
 *
 */
Vivus.prototype.starter = function () {
  switch (this.start) {
  case 'manual':
    return;

  case 'autostart':
    this.play();
    break;

  case 'inViewport':
    var self = this,
    listener = function () {
      if (self.isInViewport(self.parentEl, 1)) {
        self.play();
        window.removeEventListener('scroll', listener);
      }
    };
    window.addEventListener('scroll', listener);
    listener();
    break;
  }
};


/**
 * Controls
 **************************************
 */

/**
 * Get the current status of the animation between
 * three different states: 'start', 'progress', 'end'.
 * @return {string} Instance status
 */
Vivus.prototype.getStatus = function () {
  return this.currentFrame === 0 ? 'start' : this.currentFrame === this.frameLength ? 'end' : 'progress';
};

/**
 * Reset the instance to the initial state : undraw
 * Be careful, it just reset the animation, if you're
 * playing the animation, this won't stop it. But just
 * make it start from start.
 *
 */
Vivus.prototype.reset = function () {
  return this.setFrameProgress(0);
};

/**
 * Set the instance to the final state : drawn
 * Be careful, it just set the animation, if you're
 * playing the animation on rewind, this won't stop it.
 * But just make it start from the end.
 *
 */
Vivus.prototype.finish = function () {
  return this.setFrameProgress(1);
};

/**
 * Set the level of progress of the drawing.
 * 
 * @param {number} progress Level of progress to set
 */
Vivus.prototype.setFrameProgress = function (progress) {
  progress = Math.min(1, Math.max(0, progress));
  this.currentFrame = Math.round(this.frameLength * progress);
  this.trace();
  return this;
};

/**
 * Play the animation at the desired speed.
 * Speed must be a valid number (no zero).
 * By default, the speed value is 1.
 * But a negative value is accepted to go forward.
 *
 * And works with float too.
 * But don't forget we are in JavaScript, se be nice
 * with him and give him a 1/2^x value.
 *
 * @param  {number} speed Animation speed [optional]
 */
Vivus.prototype.play = function (speed) {
  if (speed && typeof speed !== 'number') {
    throw new Error('Vivus [play]: invalid speed');
  }
  this.speed = speed || 1;
  if (!this.handle) {
    this.drawer();
  }
  return this;
};

/**
 * Stop the current animation, if on progress.
 * Should not trigger any error.
 *
 */
Vivus.prototype.stop = function () {
  if (this.handle) {
    cancelAnimFrame(this.handle);
    this.handle = null;
  }
  return this;
};

/**
 * Destroy the instance.
 * Remove all bad styling attributes on all
 * path tags
 *
 */
Vivus.prototype.destroy = function () {
  var i, path;
  for (i = 0; i < this.map.length; i++) {
    path = this.map[i];
    path.el.style.strokeDashoffset = null;
    path.el.style.strokeDasharray = null;
    this.renderPath(i);
  }
};


/**
 * Utils methods
 * include methods from Codrops
 **************************************
 */

/**
 * Method to best guess if a path should added into
 * the animation or not.
 *
 * 1. Use the `data-vivus-ignore` attribute if set
 * 2. Check if the instance must ignore invisible paths
 * 3. Check if the path is visible
 *
 * For now the visibility checking is unstable.
 * It will be used for a beta phase.
 *
 * Other improvments are planned. Like detecting
 * is the path got a stroke or a valid opacity.
 */
Vivus.prototype.isInvisible = function (el) {
  var rect,
    ignoreAttr = el.getAttribute('data-ignore');

  if (ignoreAttr !== null) {
    return ignoreAttr !== 'false';
  }

  if (this.ignoreInvisible) {
    rect = el.getBoundingClientRect();
    return !rect.width && !rect.height;
  }
  else {
    return false;
  }
};

/**
 * Parse attributes of a DOM element to
 * get an object of {attributeName => attributeValue}
 *
 * @param  {object} element DOM element to parse
 * @return {object}         Object of attributes
 */
Vivus.prototype.parseAttr = function (element) {
  var attr, output = {};
  if (element && element.attributes) {
    for (var i = 0; i < element.attributes.length; i++) {
      attr = element.attributes[i];
      output[attr.name] = attr.value;
    }
  }
  return output;
};

/**
 * Reply if an element is in the page viewport
 *
 * @param  {object} el Element to observe
 * @param  {number} h  Percentage of height
 * @return {boolean}
 */
Vivus.prototype.isInViewport = function (el, h) {
  var scrolled   = this.scrollY(),
    viewed       = scrolled + this.getViewportH(),
    elBCR        = el.getBoundingClientRect(),
    elHeight     = elBCR.height,
    elTop        = scrolled + elBCR.top,
    elBottom     = elTop + elHeight;

  // if 0, the element is considered in the viewport as soon as it enters.
  // if 1, the element is considered in the viewport only when it's fully inside
  // value in percentage (1 >= h >= 0)
  h = h || 0;

  return (elTop + elHeight * h) <= viewed && (elBottom) >= scrolled;
};

/**
 * Alias for document element
 *
 * @type {DOMelement}
 */
Vivus.prototype.docElem = window.document.documentElement;

/**
 * Get the viewport height in pixels
 *
 * @return {integer} Viewport height
 */
Vivus.prototype.getViewportH = function () {
  var client = this.docElem.clientHeight,
    inner = window.innerHeight;

  if (client < inner) {
    return inner;
  }
  else {
    return client;
  }
};

/**
 * Get the page Y offset
 *
 * @return {integer} Page Y offset
 */
Vivus.prototype.scrollY = function () {
  return window.pageYOffset || this.docElem.scrollTop;
};

/**
 * Alias for `requestAnimationFrame` or
 * `setTimeout` function for deprecated browsers.
 *
 */
requestAnimFrame = (function () {
  return (
    window.requestAnimationFrame       ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    window.oRequestAnimationFrame      ||
    window.msRequestAnimationFrame     ||
    function(/* function */ callback){
      return window.setTimeout(callback, 1000 / 60);
    }
  );
})();

/**
 * Alias for `cancelAnimationFrame` or
 * `cancelTimeout` function for deprecated browsers.
 *
 */
cancelAnimFrame = (function () {
  return (
    window.cancelAnimationFrame       ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelAnimationFrame    ||
    window.oCancelAnimationFrame      ||
    window.msCancelAnimationFrame     ||
    function(id){
      return window.clearTimeout(id);
    }
  );
})();

/**
 * Parse string to integer.
 * If the number is not positive or null
 * the method will return the default value
 * or 0 if undefined
 *
 * @param {string} value String to parse
 * @param {*} defaultValue Value to return if the result parsed is invalid
 * @return {number}
 *
 */
parsePositiveInt = function (value, defaultValue) {
  var output = parseInt(value, 10);
  return (output >= 0) ? output : defaultValue;
};


  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define([], function() {
      return Vivus;
    });
  } else if (typeof exports === 'object') {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = Vivus;
  } else {
    // Browser globals
    window.Vivus = Vivus;
  }

}(window, document));

(function( $ ) {
	var
	ValidState=false,
	RequiredState=false,
	VisibleState=false,
	FORM,//Глобальный (для плагина) объект формы, на которую он натравлен
	ACCEPTABLE;


	jQuery.fn.lemongrab = function(options,RULES) {
		ACCEPTABLE=$.extend({	"subselector":"*",
													"classValid":"ACCEPTABLE",
													"classInvalid":"UNACCEPTABLE",
													"classRequired":"REQUIRED",
													"classNorequired":"NOREQUIRED",
													"classEnabled":"ENABLED",
													"classDisabled":"DISABLED",
													"classVisible":"VISIBLE",
													"classHidden":"HIDDEN",
													"classChanged":"CHANGED",
													"classUnchanged":"UNCHANGED",
													"action":"input",
													"actions":{},
													"onAction":false,
													"allowInvalidSubmit":true,
													"useRequiredAttr":true,
													"nativeEnabled":true,
													"nativeVisible":true,
													"autograb":true,
													"ignore_autograb_change":true,//Игнорировать смену состояний Changed/Unchanged при autograb=true
													"ignore_autograb_action":false//Игнорировать вызов события onAction при autograb=true
												},options);
		FORM=this;
		
		var rule,conditions,selector_count,conditions_count,rule_options={},lemons,lemon,rules;
		
		//если в RULES что-то задано - считаем, что там JSON, парсим его.
		if (typeof(RULES)!=='undefined' && RULES!=='') {
			var for_selector;
			for (selector_count=0;selector_count<RULES.length;selector_count++){//Разбиваем переданный параметр на селекторы
				for_selector=RULES[selector_count].selector;
				//Перебираем все условия валидации
				for (e_rule in RULES[selector_count].rule) {
					rule=e_rule.toUpperCase();
					conditions=RULES[selector_count].rule[e_rule];
					//В conditions-набор условий правила. В rule-само правило.
					rule_options={};
					for (conditions_count=0;conditions_count<conditions.length;conditions_count++){//Пройдёмся по условиям, дополнив умолчания. Можно использовать существующий объект
						rule_options[conditions_count]=complete_condition(conditions[conditions_count]);
					}
					rule_options.actions=RULES[selector_count].actions;
					apply_rule($(for_selector),rule,rule_options);
				}
			}
		}
		
		lemons=FORM.find(ACCEPTABLE.subselector);
		lemons.each(function(){
			lemon=$(this);
			rules=lemon.data();//Получили список data-атрибутов
			for (var rule_ in rules) {
				if (['ruleValid','ruleRequired','ruleEnabled','ruleVisible','ruleLemongrab'].indexOf(rule_)===-1) continue;
				conditions=rules[rule_];
				switch (rule_){
					case 'ruleValid':
						rule="VALID";
					break;
					case 'ruleRequired':
						rule="REQUIRED";
					break;
					case 'ruleEnabled':
						rule="ENABLED";
					break;
					case 'ruleVisible':
						rule="VISIBLE";
					break;
					case 'ruleLemongrab': //Отдельный случай: одно большое правило
						var parameters=conditions,
								parameter;
						for (parameter in parameters) {
							conditions=parameters[parameter];
							switch (parameter){
								case 'valid':
									rule="VALID";
								break;
								case 'required':
									rule="REQUIRED";
								break;
								case 'enabled':
									rule="ENABLED";
								break;
								case 'visible':
									rule="VISIBLE";
								break;
							}
							//В conditions-набор условий правила. В rule-само правило.
							rule_options={};
							for (conditions_count=0;conditions_count<conditions.length;conditions_count++){//Пройдёмся по условиям, дополнив умолчания. Можно использовать существующий объект
								rule_options[conditions_count]=complete_condition(conditions[conditions_count]);
							}
							apply_rule(lemon,rule,rule_options);
						}
					break;
				}
				//В conditions-набор условий правила. В rule-само правило.
				rule_options={};
				for (conditions_count=0;conditions_count<conditions.length;conditions_count++){//Пройдёмся по условиям, дополнив умолчания. Можно использовать существующий объект
					rule_options[conditions_count]=complete_condition(conditions[conditions_count]);
				}
				//rule_options.actions=RULES[selector_count].actions;
				apply_rule(lemon,rule,rule_options);
			}
		});
		ValidState=FORM.isValid();
		RequiredState=!FORM.isNotRequired();
		VisibleState=FORM.is(':visible');
		//Устанавливаем обработчик onsubmit
		FORM.on('submit',function(){
			if (!FORM.isNotRequired()) return (false);
			if (FORM.isValid()) return (true);
			return (ACCEPTABLE.allowInvalidSubmit===true);
		});
	};

	jQuery.fn.isValid = function(){
		return (this.find("."+ACCEPTABLE.classInvalid).length===0);
	};
	
	jQuery.fn.isNotRequired = function(){
		return (this.find("."+ACCEPTABLE.classRequired).length===0);
	};
	
	function complete_condition(condition){//Проверяет условие на все параметры, дополняет умолчательными. TODO: разрешить использовать сокращённую запись (& вместо and и тд).
		condition.key=(condition.key||'native');//Получаем ключ правила, если не задан - считаем, что там нативный JS.
		
		condition.invert=(condition.invert||false);//Инвертировать ли полученное значение.
		if (condition.key[0]==='!') {//Инвертирование может быть задано в ключе. В случае, если оно задано и в ключе, и в параметре, ключ имеет приоритет
			condition.invert=true;
			condition.key=condition.key.substr(1);
		}
		
		switch (condition.key.toLowerCase()){//Задаём алиасы ключей
			case 'r':
			case 'regexp':
				condition.key="regexp";
			break;
			case 'c':
			case 'checked':
			case 'checkbox':
			case 'radio':
				condition.key="checked";
			break;
			case 'select':
			case 's':
				condition.key="select";
			break;
			case 'set':
				condition.key="set";
			break;
			case 'j':
			case 'n':
			case 'f':
			case 'javascript':
			case 'native':
			case 'function':
				condition.key="native";
			break;
		}
		
		if (condition.key==="select"){//Нужно проверить, что в value
			var i,tmp_value="[";
			if (typeof(condition.value)==='object'){//Отлично, это массив
				for (i=0;i<condition.value.length;i++){
					tmp_value+="\""+condition.value[i]+"\",";
				}
			} else if (typeof(condition.value)==='string'){//Это не массив, но мы добры к пользователю
				condition.value=condition.value.split(',');
				for (i=0;i<condition.value.length;i++){
					tmp_value+="\""+condition.value[i]+"\",";
				}
			} else {
				tmp_value="[ ";
			}
			condition.value=tmp_value.substr(0,tmp_value.length-1)+"]";
		} else if (condition.key==="set") {
			condition.value=(condition.value||[""]);
		}

		condition.selector=(condition.selector||'');//Селектор связуемого поля, если не задан - привязываемся к полю правила.
		condition.strict=(condition.strict||false);//Строгость проверки для select/set
		condition.logic=(condition.logic||'&&');//Логика совмещения правил
		condition.min=(condition.min||false);/*минимальное*/
		condition.max=(condition.max||false);/*и максимальное количество выбранных значений*/
		
		switch (condition.logic.toLowerCase()) {
			default:
			case "&":
			case "&&":
			case "and":
				condition.logic="&&";
			break;
			case "|":
			case "||":
			case "or":
				condition.logic="||";
			break;
		}
		return (condition);
	}

	/*Возвращает в виде массива набор значений указанного <select>
		Входное значение - jQuery-объект с селектом
	*/
	function get_selected_options(select){
		var	options=select.find("option:selected"),
				option_count,
				ret=[];
		for (option_count=0;option_count<options.length;option_count++){
			ret.push($(options[option_count]).val());
		}
		return (ret);
	}
	
	/*
		проверяет, чекнуты ли в block поля, отмеченные в selectors.
		если strict==true, считается норм, если чекнуто хотя бы одно поле, попадающее под каждый селектор
		иначе должны быть чекнуты все поля, попадающие в селектор
	*/
	function is_fields_checked(block,selectors,strict){
		if (typeof(strict)==='undefined') strict=false;
		var index,selector,cl=0;
		for (index=0;index<selectors.length;index++){
			selector=selectors[index];
			cl+=block.find(selector+':checked').length;//Количество чекнутых для прошлого селектора не сбрасываем
			if (strict) {//TODO: условие можно и сократить для компактности
				if (cl!==block.find(selector).length) return (false);
			} else {
				if (cl>block.find(selector).length || cl===0) return (false);
			}
		}
		return (true);
	}
	
	/*
	 * По заданному набору параметров генерирует JS-код условия
	*/
	function get_js_condition (condition,field){
		var ret={
			handler:[],
			condition:""
		},_condition,summary_selector,index;
		
		switch (condition.key.toLowerCase()){
			case 'regexp':
				if (condition.selector!=='') {//Задан селектор внешнего поля, дополнительно вешаем обработчик на него
					ret.condition = "RegExp(/"+condition.value+"/).test($('"+condition.selector+"').val())";
					ret.handler.push("$('"+condition.selector+"')");
				} else {//Селектор не задан, проверяем собственное поле
					ret.condition = "RegExp(/"+condition.value+"/).test(field.val())";//field дальше будет обрабатываться eval
				}
			break;
			case 'checked':
				if (condition.selector!=='') {//Задан селектор внешнего поля, дополнительно вешаем обработчик на него
					ret.condition = "$('"+condition.selector+"').is(':checked')";
					ret.handler.push("$('"+condition.selector+"')");
				} else {//Селектор не задан, проверяем собственное поле
					ret.condition = "field.is(':checked')";
				}
			break;
			case 'select':
				_condition='';
				//В condition.value должен быть массив переведённый в строку - об этом заботимся в complete_condition
				if (condition.selector!=='') {//Задан селектор внешнего поля, дополнительно вешаем обработчик на него
					if (condition.min) {//Генерируем условие для min, если оно задано
						_condition+=" && (get_selected_options($('"+condition.selector+"')).length >= "+condition.min+")";
					}
					if (condition.max) {//Генерируем условие для max, если оно задано
						_condition+=" && (get_selected_options($('"+condition.selector+"')).length <= "+condition.max+")";
					}
					ret.condition = condition.value+".equals (get_selected_options($('"+condition.selector+"')),"+condition.strict+")"+_condition;
					ret.handler.push("$('"+condition.selector+"')");
				} else {//Селектор не задан, проверяем собственное поле
					
					if (condition.min) {//Генерируем условие для min, если оно задано
						_condition+=" && (get_selected_options(field).length >= "+condition.min+")";
					}
					if (condition.max) {//Генерируем условие для max, если оно задано
						_condition+=" && (get_selected_options(field).length <= "+condition.max+")";
					}
					ret.condition = condition.value+".equals (get_selected_options(field),"+condition.strict+")"+_condition;
				}
			break;
			case 'set':
				_condition='';
				summary_selector='';
				//Поскольку в condition.value должен быть массив селекторов, нужно разобрать его, сделав один большой селектор
				for (index=0;index<condition.value.length;index++){
					summary_selector+=", "+condition.value[index]+":checked";
				}
				summary_selector=summary_selector.substr(1);
				
				if (condition.selector!=='') {//Задан селектор внешнего поля, дополнительно вешаем обработчик на него
					if (condition.min) {//Генерируем условие для min, если оно задано
						_condition+=" && ($('"+condition.selector+summary_selector+"').length >= "+condition.min+")";
					}
					if (condition.max) {//Генерируем условие для max, если оно задано
						_condition+=" && ($('"+condition.selector+summary_selector+"').length <= "+condition.max+")";
					}
					ret.condition = "is_fields_checked($('"+condition.selector+"'),"+ JSON.stringify(condition.value)+","+condition.strict+")"+_condition;
					/*Для блоков требуется эмулировать onchange, поэтому передаём вот такой селектор*/
					ret.handler.push("$('"+condition.selector+"').find('input[type=checkbox], input[type=radio]')");
				} else {//Селектор не задан, проверяем собственное поле
					if (condition.min) {//Генерируем условие для min, если оно задано
						_condition+=" && (field.find('"+summary_selector+"').length >= "+condition.min+")";
					}
					if (condition.max) {//Генерируем условие для max, если оно задано
						_condition+=" && (field.find('"+summary_selector+"').length <= "+condition.max+")";
					}
					ret.condition = "is_fields_checked(field,"+ JSON.stringify(condition.value)+","+condition.strict+")"+_condition;
					/*Для блоков требуется эмулировать onchange, поэтому передаём вот такой селектор*/
					ret.handler.push("field.find('input[type=checkbox], input[type=radio]')");
				}
			break;
			case 'native':
				ret.condition=condition.value;
				if (condition.selector!=='') { //Задан селектор внешнего поля, дополнительно вешаем обработчик на него
					ret.handler.push("$('"+condition.selector+"')");
				}
			break;
		}
		ret.handler.push("field");
		if (condition.invert) {
			ret.condition=condition.logic+" !("+ret.condition+")";
		} else {
			ret.condition=condition.logic+" ("+ret.condition+")";
		}
		return (ret);
		
	}

	/*
	 * field - jQuery-объект поля, к которому применяется правило
	 * rule - применяемое правило
	 * conditions - набор условий, при которых правило применяется
	*/
	function apply_rule(field,rule,conditions){
		var summary_conditions={
			condition:"",
			handler:[]
		},
		js_condition,i,h,condition, action;
		for (condition in conditions){
			if ($.isNumeric(condition)) js_condition=get_js_condition(conditions[condition],field);//В conditions может быть нецифровой параметр, дополняющий набор правил
			summary_conditions.condition+=" "+js_condition.condition;
			for (i=0;i<js_condition.handler.length;i++){//Избавляемся от навешиваний одинаковых обработчиков на одно поле
				if (!summary_conditions.handler.object_in_array(js_condition.handler[i])) summary_conditions.handler.push(js_condition.handler[i]);
			}
		}
		//Обрезаем от условия первые четыре символа с первым ненужным условием. Если будет глючить - переписать регуляркой.
		summary_conditions.condition=summary_conditions.condition.substr(4);
		for (i=0;i<summary_conditions.handler.length;i++){
			h=(summary_conditions.handler[i]==='')?field:eval(summary_conditions.handler[i]);//На всякий случай
			
			action=get_rule_action(rule,conditions.actions);//Получим событие, заданное в правиле непосредственно 
			action+=" "+get_rule_action(rule,ACCEPTABLE.actions);//Получим событие, заданное для всех правил
			
			switch (checkable(h)){//Получим глобальное правило + правила для "особенных" элементов
				case 0:
					action+=" "+ ACCEPTABLE.action;
				break;
				case 1:
					action= "change";
				break;
				case 2://С радиокнопками всё плохо, у них нет нормального onChange
					if (typeof(h.attr('name'))!=='undefined') h=$('input[name="'+h.attr('name')+'\"]');
					action+=" "+ "change";
				break;
				case 3://Для селектов нужно обрабатывать оба события
					action+=" "+ ACCEPTABLE.action+" change";
				break;
			}
			
			action=action.trim();
			
			h.on(action,function(event,ignore_change,ignore_action){
				var x=set_class(field,rule,eval(summary_conditions.condition));
				if (x) {
					h.trigger(x.action+x.rule);
				}
				
				if (!ignore_action || typeof(ignore_action)==='undefined') {//вызываем срабатывании функции в onAction только для реально произошедших событий, а не для вызванных в коде
					if (ACCEPTABLE.onAction) ACCEPTABLE.onAction(field);
				}
				initiateEvents(FORM);
				
				if (!ignore_change || typeof(ignore_change)==='undefined') {//вызываем изменение состояний changed/unchanged только для реально произошедших событий, а не для вызванных в коде
					if (ACCEPTABLE.classUnchanged!==false)h.removeClass(ACCEPTABLE.classUnchanged);
					if (ACCEPTABLE.classChanged!==false) h.addClass(ACCEPTABLE.classChanged);
				}
			});
			
			if (ACCEPTABLE.classUnchanged!==false) h.addClass(ACCEPTABLE.classUnchanged);
			
			if (ACCEPTABLE.autograb) {
				var a=action.split(' ');
				$(a).each (function(val){
					h.trigger(a[val],[ACCEPTABLE.ignore_autograb_change,ACCEPTABLE.ignore_autograb_action]);//Инициализация состояния
				});
			}
		}
	}
	
	function get_rule_action(rule,actions) {
		if (typeof(actions)==='undefined') return "";
		return(actions[rule.toLowerCase()]||"");
	}
	
	//Возвращает true, если переданный элемент поддерживает только onChange
	function checkable(field){
		//TODO: Проверить, что будет, если в field много полей
		if (field.is('[type=checkbox]')) {
			return (1);
		} else if (field.is('[type=radio]')) {
			return (2)
		} else if (field.is('select')) {
			return (3);
		} else return (0);
	}
	/**
	 * Проверяет, произошло ли переключение состояния для valid и required, если произошло - инициирует соответствующее событие. Состояние visible проверяется непосредственно в setClass
	**/
	function initiateEvents(lemonform){
		var	cur_valid=lemonform.isValid(),
				cur_require=!lemonform.isNotRequired(),
				cur_visible=lemonform.is(':visible');
		if (cur_valid && !ValidState) {
			lemonform.trigger('lemongrab.valid');
			ValidState=true;
		} else if (!cur_valid && ValidState) {
			lemonform.trigger('lemongrab.novalid');
			ValidState=false;
		}
		if (cur_require && !RequiredState) {
			lemonform.trigger('lemongrab.require');
			RequiredState=true;
		} else if (!cur_require && RequiredState) {
			lemonform.trigger('lemongrab.norequire');
			RequiredState=false;
		}
	}
	
	//Проверяет, есть ли точно такой же объект в массиве (вплоть до значений полей).
	Array.prototype.object_in_array = function(srch) {
		var i;
		for(i=0; i < this.length; i++) {
			if (typeof(this[i])==='object'){
				//Обойдёмся без рекурсии. Если понадобится рекурсия: http://javascript.ru/forum/misc/10792-sravnenie-obektov-2.html#post209343
				if (JSON.stringify(this[i])===JSON.stringify(srch)) {
					return true;
				}
			}
		}
		return false;
	};
	
	/*
	 * Сравнивает двумерные массивы между собой. Массивы трактуются как множества - т.е. одинаковыми считаются массивы с равным количеством элементов и их одинаковыми значениями, но порядок элементов может быть разный, т.е. [1,2,3]===[2,1,3]
	 * strict задаёт строгость вхождения. false - array может быть подмножеством, true - должен точно совпадать
	*/
	Array.prototype.equals = function (array,strict){
		if (typeof(strict)==='undefined') strict=false;
		if ((strict && this.length!==array.length)||(!strict && this.length>array.length)) return (false);
		
		var i;
		if (!Array.prototype.indexOf) {/*честно скопипащено из интернетов и не проверялось, т.к. все современные браузеры должны поддерживать indexOf*/
			Array.prototype.indexOf = function(elt /*, from*/) {
				var len = this.length >>> 0;
				var from = Number(arguments[1]) || 0;
				from = (from < 0) ? Math.ceil(from) : Math.floor(from);
				if (from < 0) from += len;
				for (; from < len; from++){
					if (from in this && this[from] === elt) return from;
				}
				return -1;
			};
		}
		
		for (i=0;i<this.length;i++){
			if(array.indexOf(this[i])===-1) return (false);
		}
		return (true);
	};
	
	/**
	 * Меняет класс и сообщает, произошло ли изменение состояния.
	 */

	function set_class(field,rule,setc){
		var ret=false;
		var positiveClass,negativeClass;
		if (typeof(rule)==='undefined') return(0);
		switch (rule){
			case 'VALID':
				positiveClass=ACCEPTABLE.classValid;
				negativeClass=ACCEPTABLE.classInvalid;
			break;
			case 'REQUIRED':
				positiveClass=ACCEPTABLE.classRequired;
				negativeClass=ACCEPTABLE.classNorequired;
			break;
			case 'ENABLED':
				positiveClass=ACCEPTABLE.classEnabled;
				negativeClass=ACCEPTABLE.classDisabled;
			break;
			case 'VISIBLE':
				positiveClass=ACCEPTABLE.classVisible;
				negativeClass=ACCEPTABLE.classHidden;
			break;
		}
		if(setc){
			if (field.hasClass(negativeClass)) {
				ret={
					"rule":rule,
					"action":"SET"
				}
			}
		
			field.removeClass(negativeClass).addClass(positiveClass);
			if (rule==='REQUIRED' && ACCEPTABLE.useRequiredAttr) field.attr("required","required");
			if (rule==='ENABLED' && ACCEPTABLE.nativeEnabled ) field.removeAttr("disabled");
			if (rule==='VISIBLE' && ACCEPTABLE.nativeVisible) {
				field.show();
				field.trigger('lemongrab.visible');
			}
		} else {
		
			if (field.hasClass(positiveClass)) {
				ret={
					"rule":rule,
					"action":"REMOVE"
				}
			}
		
			field.removeClass(positiveClass).addClass(negativeClass);
			if (rule==='REQUIRED' && ACCEPTABLE.useRequiredAttr) field.removeAttr("required");
			if (rule==='ENABLED' && ACCEPTABLE.nativeEnabled ) field.attr("disabled","disabled");
			if (rule==='VISIBLE' && ACCEPTABLE.nativeVisible) {
				field.hide();
				field.trigger('lemongrab.hide');
			}
		}
		return (ret);
	}

}( jQuery ));


/*
    Custom
 */


$('.menu__item-dropmenu').hover(

    function(){
        if (!$(this).hasClass('menu__item-dropmenu-active') && $(this).siblings('.menu__item-dropmenu-active').length) {
            $(this).siblings('.menu__item-dropmenu-active').addClass('menu__item-dropmenu-active-off');
            console.log('qweqweqweqwe');
        }

    },
    function(){
        if (!$(this).hasClass('menu__item-dropmenu-active') && $(this).siblings('.menu__item-dropmenu-active').length) {
            $(this).siblings('.menu__item-dropmenu-active-off').removeClass('menu__item-dropmenu-active-off');

        }
    }
);




$(window).load(function() {
    $('#featured').orbit({
        'bullets': false,
        'timer' : true,
        'numbers':true,
        'advanceSpeed': 6000,
        'animation' : 'horizontal-rom',
        captions: true,
        captionAnimation: 'fade',
        captionAnimationSpeed: 800,
        bulletThumbs: true,				// thumbnails for the bullets
        bulletThumbLocation: 'img/catalog/'
    });





});

$('#featured2').orbit({
    'bullets': false,
    'timer' : false,
    'numbers':true,
    'advanceSpeed': 6000,
    'animation' : 'fade',
    captions: false,
    captionAnimation: 'fade',
    captionAnimationSpeed: 800

});

$('#sliderReviews').orbit({
    'bullets': false,
    'timer' : false,
    'advanceSpeed': 6000,
    'animation' : 'fade',
    captions: false,
    captionAnimation: 'fade',
    captionAnimationSpeed: 800

});

$('#sliderPosition').orbit({
    'bullets': true,
    'timer' : false,
    'advanceSpeed': 6000,
    'animation' : 'fade',
    captions: false,
    captionAnimation: 'fade',
    captionAnimationSpeed: 800,
    bulletThumbs: true,				// thumbnails for the bullets
    bulletThumbLocation: 'img/catalog/'

});

$('#sliderPost').orbit({
    'bullets': false,
    'timer' : false,
    'numbers':true,
    'advanceSpeed': 6000,
    'animation' : 'fade',
    captions: false,
    captionAnimation: 'fade',
    captionAnimationSpeed: 800


});

$(function() {
    $.fn.goSvg = function(){

        var svg = $(this),
            margin = $(window).height()/5, // ���� �� 33% ������ �����������
            topScroll = 0,
            procent, topPos, windowH, ornamentId;

        function Load(){

            if(svg.length == 0) return false;

            topPos = svg.offset().top,
                windowH = $(window).height();

            ornamentId = new Vivus(svg[0].id, {
                start: 'manual',
                type: 'async',
                dashGap:20

            });

        }Load();

        $(window).on("scroll", function(){

            topScroll = $(window).scrollTop();

            if (topScroll+windowH >= topPos && topScroll <= topPos) {

                procent = (topScroll-topPos+windowH)/(windowH-margin);
                ornamentId.setFrameProgress(procent);

            }


        });

    }
});

(function($){
    $(function(){

        $("#ornamentId1").goSvg();
        $("#ornamentId2").goSvg();

    });
})(jQuery);


/*
$(document).ready(function(){
    svgAnimation();
});

function svgAnimation() {

    if($("#ornamentId2").length > 0) {
        
        ornamentId2X = $('#ornamentId2').offset().top; //отметка начала орнамента
        windowH = $(window).height(); //высота браузера
        var ornamentId2 = new Vivus('ornamentId2', {
            start:'manual',
            type:'async'
        });
        var s=0,
            step= 0,
            scrollCursor,
            prevScrollCursor=0;
        step = (ornamentId2X-$(window).scrollTop()+windowH)/100000;
        $(window).scroll(function(){

            scrollCursor = $(window).scrollTop();


            console.log('scrollCursor = '+scrollCursor+' высота браузера = '+windowH+' отметка начала орнамента = '+ornamentId2X);

             if (scrollCursor+windowH>= ornamentId2X && scrollCursor<= ornamentId2X) {
                 if (scrollCursor >= prevScrollCursor) {
                     ornamentId2.setFrameProgress(s=s+step);

                 } else {
                     ornamentId2.setFrameProgress(s=s-step);
                 }
                 prevScrollCursor = scrollCursor;
                 console.log(s);
                 if (s>1) s==1;
                 if(s<0) s==0;

             }


        });
    }

}
*/



/*$(document).ready(function(){
    var  ornamentId2X;


  

});*/



/*new Vivus('ornamentId2', {
    /!*  type:'async',*!/
    delay:50,
    duration: 200
});*/

/*
new Vivus('ornamentId', {
    duration: 200,
    file: 'img/ornament.svg',
    onReady: function (myVivus) {
        // `el` property is the SVG element
        myVivus.el.setAttribute('height', '87px');
    }
});*/



$(".order__liked-link-clear").on("click", function(e){
    $('.order__liked').toggleClass("order__liked-clear");
    console.log('order__liked-link click');

});

$('.radio-catalog .radio-input1').on("click",function(e){
    console.log('sdfsdfdsf');
    $('.catalog__list').addClass('catalog__list-view');
});

$('.radio-catalog .radio-input2').click(function(){
    $('.catalog__list').removeClass('catalog__list-view');
});

$('.radio-collection .radio-input1').on("click",function(e){
    console.log('sdfsdfdsf');
    $('.markers').addClass('markers-yes');
});

$('.radio-collection .radio-input2').click(function(){
    $('.markers').removeClass('markers-yes');
});


$('.radio-slider .radio-input1').on("click",function(e){
    console.log('sdfsdfdsf');
    $('.collection__slider1').toggleClass('collection__slider-none');
    $('.collection__slider2').toggleClass('collection__slider-none');
});

$('.radio-slider .radio-input2').click(function(){
    console.log('sdfsdfdsf');
    $('.collection__slider1').toggleClass('collection__slider-none');
    $('.collection__slider2').toggleClass('collection__slider-none');
});


/*$('.catalog__dropmenu__item').click(function(){
    var text = $(this).text(),
        id =$(this).attr('id'),
        textHeader = $('catalog__dropmenu__header').text(),
        idHeader = $('catalog__dropmenu__header').attr('id');
    console.log(textHeader,idHeader);
    $(this).text($('catalog__dropmenu__header').text()).attr('id',($('catalog__dropmenu__header').attr('id')));

});*/

$('.catalog__dropmenu__list').styler();





var checkHash = function(){
    if(window.location.hash == "#popup-position" || window.location.hash == "#popup-post") {
        console.log('скрол');
        $('.position__close').css({right:0});
        $('.position__arrow-right').css({right:23});

        $(window).on("resize", function(){
            $('.position__close').css({right:5});
            $('.position__arrow-right').css({right:23});

        });

        $('.popup-position').scroll(function(){
            var top = $(this).scrollTop(),
                arrow =$('.position__nav').offset().top;
            /*if($(window).height()/2 >= $('.position__nav').offset().top) {
                $('.position__nav').addClass('position__nav-fixed');
            } else {
                $('.position__nav').removeClass('position__nav-fixed');
            }*/
            if(137 <= top) {
                $('.position__nav').addClass('position__nav-fixed');
                $('.position__arrow-right').css({right:41});
            } else {
                $('.position__nav').removeClass('position__nav-fixed');
                $('.position__arrow-right').css({right:23});
            }

            console.log(top);
            if (322  <= top ) {
                $('.position__close').css({right:17}).addClass('position__close-fixed');
            } else {
                $('.position__close').css({right:0}).removeClass('position__close-fixed');
            }
        });
    }


    if(window.location.hash.indexOf("#popup") != -1/*window.location.hash == "#popup-position" || window.location.hash == "#popup-gallery"*/) {
        $('body').addClass('overflow-hidden');

    } else $('body').removeClass('overflow-hidden');
    /* history.pushState('', document.title, window.location.pathname);*/ //удаляет hash
};

checkHash();

$(window).bind('hashchange', function() {
    checkHash();
});



/*$('.popup-open').click(function(){
    $('body').addClass('overflow-hidden');
});

$('.position__close').click(function(){
    $('body').removeClass('overflow-hidden');
});*/


$(".catalog__icon").on("click", function(e){
    e.preventDefault();
    $(this).toggleClass("catalog__icon-active");
    console.log('catalog__icon click');
});

$(".order__liked .catalog__icon").each(function(indx, element){
    $(element).addClass("catalog__icon-active");
});

$(".gallery__icon").click(function(e){
    e.preventDefault();
    $(this).toggleClass("gallery__icon-active");
});

$(".position__icon").click(function(e){
    e.preventDefault();
    $(this).toggleClass("position__icon-active");
});

$(".link-other-time").on("click", function(e){
    e.preventDefault();
    $(this).hide();
    $('.popup-call .form').toggleClass("form__call-hidden");
    console.log('order__liked-link click');
    $("#form-call-textarea").lemongrab();

});

var rules = [
    {
        selector:'#name',
        rule:{
            required:[
                {
                    key: "!regexp",
                    value: '^[Ð°-ÑÐ-Ð¯]{3,}$"}]'
                }
            ]
        }
    },
    {
        selector:'#mobil-phone',
        rule:{
            required:[
                {
                    key: "!regexp",
                    value: '^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$"}]'
                }
            ]
        }
    }
];
var inputWrap="<div class='form__input-wrap' style='display: inline-block'></div>";
$(".form .form__input-name").wrap(inputWrap).after("<div class='input__valid'>Неправильно введено имя</div>");
$(".form .form__input-phone").wrap(inputWrap).after("<div class='input__valid'>Неправильно введен введен номер телефона</div>");
$(".form .form__input-email").wrap(inputWrap).after("<div class='input__valid'>Неправильно введен email</div>");
$(".form .form__input-textarea").wrap(inputWrap).after("<div class='input__valid'>Слишком короткое сообщение</div>");

/*$("#form-call-textarea").lemongrab('');*/


if($("#form-call").length > 0) { $("#form-call").lemongrab(''); }
if($("#form-question").length > 0) { $("#form-question").lemongrab(''); }
if($("#form-subscribe").length > 0) { $("#form-subscribe").lemongrab(''); }





$(".form").on("lemongrab.require",function(){
    $(this).find('.btn2').removeClass('btn2-active');
    console.log('не все обязательные поля');
});
$(".form").on("lemongrab.norequire",function(){
    $(this).find('.btn2').addClass('btn2-active');
    console.log('все обязательные поля');
});




$('.gallery__trumb').click(function(e){
    e.preventDefault();
    $(this).addClass('gallery__trumb-active').siblings().removeClass('gallery__trumb-active');
    var srcImg = $(this).children().attr("src");
    $('.gallery__imgbig').attr("src",srcImg);
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbiBUaGlyZCBwYXJ0eVxyXG4gKi9cclxuLyohIGpRdWVyeSB2Mi4yLjEgfCAoYykgalF1ZXJ5IEZvdW5kYXRpb24gfCBqcXVlcnkub3JnL2xpY2Vuc2UgKi9cclxuIWZ1bmN0aW9uKGEsYil7XCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZSYmXCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZS5leHBvcnRzP21vZHVsZS5leHBvcnRzPWEuZG9jdW1lbnQ/YihhLCEwKTpmdW5jdGlvbihhKXtpZighYS5kb2N1bWVudCl0aHJvdyBuZXcgRXJyb3IoXCJqUXVlcnkgcmVxdWlyZXMgYSB3aW5kb3cgd2l0aCBhIGRvY3VtZW50XCIpO3JldHVybiBiKGEpfTpiKGEpfShcInVuZGVmaW5lZFwiIT10eXBlb2Ygd2luZG93P3dpbmRvdzp0aGlzLGZ1bmN0aW9uKGEsYil7dmFyIGM9W10sZD1hLmRvY3VtZW50LGU9Yy5zbGljZSxmPWMuY29uY2F0LGc9Yy5wdXNoLGg9Yy5pbmRleE9mLGk9e30saj1pLnRvU3RyaW5nLGs9aS5oYXNPd25Qcm9wZXJ0eSxsPXt9LG09XCIyLjIuMVwiLG49ZnVuY3Rpb24oYSxiKXtyZXR1cm4gbmV3IG4uZm4uaW5pdChhLGIpfSxvPS9eW1xcc1xcdUZFRkZcXHhBMF0rfFtcXHNcXHVGRUZGXFx4QTBdKyQvZyxwPS9eLW1zLS8scT0vLShbXFxkYS16XSkvZ2kscj1mdW5jdGlvbihhLGIpe3JldHVybiBiLnRvVXBwZXJDYXNlKCl9O24uZm49bi5wcm90b3R5cGU9e2pxdWVyeTptLGNvbnN0cnVjdG9yOm4sc2VsZWN0b3I6XCJcIixsZW5ndGg6MCx0b0FycmF5OmZ1bmN0aW9uKCl7cmV0dXJuIGUuY2FsbCh0aGlzKX0sZ2V0OmZ1bmN0aW9uKGEpe3JldHVybiBudWxsIT1hPzA+YT90aGlzW2ErdGhpcy5sZW5ndGhdOnRoaXNbYV06ZS5jYWxsKHRoaXMpfSxwdXNoU3RhY2s6ZnVuY3Rpb24oYSl7dmFyIGI9bi5tZXJnZSh0aGlzLmNvbnN0cnVjdG9yKCksYSk7cmV0dXJuIGIucHJldk9iamVjdD10aGlzLGIuY29udGV4dD10aGlzLmNvbnRleHQsYn0sZWFjaDpmdW5jdGlvbihhKXtyZXR1cm4gbi5lYWNoKHRoaXMsYSl9LG1hcDpmdW5jdGlvbihhKXtyZXR1cm4gdGhpcy5wdXNoU3RhY2sobi5tYXAodGhpcyxmdW5jdGlvbihiLGMpe3JldHVybiBhLmNhbGwoYixjLGIpfSkpfSxzbGljZTpmdW5jdGlvbigpe3JldHVybiB0aGlzLnB1c2hTdGFjayhlLmFwcGx5KHRoaXMsYXJndW1lbnRzKSl9LGZpcnN0OmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuZXEoMCl9LGxhc3Q6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5lcSgtMSl9LGVxOmZ1bmN0aW9uKGEpe3ZhciBiPXRoaXMubGVuZ3RoLGM9K2ErKDA+YT9iOjApO3JldHVybiB0aGlzLnB1c2hTdGFjayhjPj0wJiZiPmM/W3RoaXNbY11dOltdKX0sZW5kOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMucHJldk9iamVjdHx8dGhpcy5jb25zdHJ1Y3RvcigpfSxwdXNoOmcsc29ydDpjLnNvcnQsc3BsaWNlOmMuc3BsaWNlfSxuLmV4dGVuZD1uLmZuLmV4dGVuZD1mdW5jdGlvbigpe3ZhciBhLGIsYyxkLGUsZixnPWFyZ3VtZW50c1swXXx8e30saD0xLGk9YXJndW1lbnRzLmxlbmd0aCxqPSExO2ZvcihcImJvb2xlYW5cIj09dHlwZW9mIGcmJihqPWcsZz1hcmd1bWVudHNbaF18fHt9LGgrKyksXCJvYmplY3RcIj09dHlwZW9mIGd8fG4uaXNGdW5jdGlvbihnKXx8KGc9e30pLGg9PT1pJiYoZz10aGlzLGgtLSk7aT5oO2grKylpZihudWxsIT0oYT1hcmd1bWVudHNbaF0pKWZvcihiIGluIGEpYz1nW2JdLGQ9YVtiXSxnIT09ZCYmKGomJmQmJihuLmlzUGxhaW5PYmplY3QoZCl8fChlPW4uaXNBcnJheShkKSkpPyhlPyhlPSExLGY9YyYmbi5pc0FycmF5KGMpP2M6W10pOmY9YyYmbi5pc1BsYWluT2JqZWN0KGMpP2M6e30sZ1tiXT1uLmV4dGVuZChqLGYsZCkpOnZvaWQgMCE9PWQmJihnW2JdPWQpKTtyZXR1cm4gZ30sbi5leHRlbmQoe2V4cGFuZG86XCJqUXVlcnlcIisobStNYXRoLnJhbmRvbSgpKS5yZXBsYWNlKC9cXEQvZyxcIlwiKSxpc1JlYWR5OiEwLGVycm9yOmZ1bmN0aW9uKGEpe3Rocm93IG5ldyBFcnJvcihhKX0sbm9vcDpmdW5jdGlvbigpe30saXNGdW5jdGlvbjpmdW5jdGlvbihhKXtyZXR1cm5cImZ1bmN0aW9uXCI9PT1uLnR5cGUoYSl9LGlzQXJyYXk6QXJyYXkuaXNBcnJheSxpc1dpbmRvdzpmdW5jdGlvbihhKXtyZXR1cm4gbnVsbCE9YSYmYT09PWEud2luZG93fSxpc051bWVyaWM6ZnVuY3Rpb24oYSl7dmFyIGI9YSYmYS50b1N0cmluZygpO3JldHVybiFuLmlzQXJyYXkoYSkmJmItcGFyc2VGbG9hdChiKSsxPj0wfSxpc1BsYWluT2JqZWN0OmZ1bmN0aW9uKGEpe3JldHVyblwib2JqZWN0XCIhPT1uLnR5cGUoYSl8fGEubm9kZVR5cGV8fG4uaXNXaW5kb3coYSk/ITE6YS5jb25zdHJ1Y3RvciYmIWsuY2FsbChhLmNvbnN0cnVjdG9yLnByb3RvdHlwZSxcImlzUHJvdG90eXBlT2ZcIik/ITE6ITB9LGlzRW1wdHlPYmplY3Q6ZnVuY3Rpb24oYSl7dmFyIGI7Zm9yKGIgaW4gYSlyZXR1cm4hMTtyZXR1cm4hMH0sdHlwZTpmdW5jdGlvbihhKXtyZXR1cm4gbnVsbD09YT9hK1wiXCI6XCJvYmplY3RcIj09dHlwZW9mIGF8fFwiZnVuY3Rpb25cIj09dHlwZW9mIGE/aVtqLmNhbGwoYSldfHxcIm9iamVjdFwiOnR5cGVvZiBhfSxnbG9iYWxFdmFsOmZ1bmN0aW9uKGEpe3ZhciBiLGM9ZXZhbDthPW4udHJpbShhKSxhJiYoMT09PWEuaW5kZXhPZihcInVzZSBzdHJpY3RcIik/KGI9ZC5jcmVhdGVFbGVtZW50KFwic2NyaXB0XCIpLGIudGV4dD1hLGQuaGVhZC5hcHBlbmRDaGlsZChiKS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGIpKTpjKGEpKX0sY2FtZWxDYXNlOmZ1bmN0aW9uKGEpe3JldHVybiBhLnJlcGxhY2UocCxcIm1zLVwiKS5yZXBsYWNlKHEscil9LG5vZGVOYW1lOmZ1bmN0aW9uKGEsYil7cmV0dXJuIGEubm9kZU5hbWUmJmEubm9kZU5hbWUudG9Mb3dlckNhc2UoKT09PWIudG9Mb3dlckNhc2UoKX0sZWFjaDpmdW5jdGlvbihhLGIpe3ZhciBjLGQ9MDtpZihzKGEpKXtmb3IoYz1hLmxlbmd0aDtjPmQ7ZCsrKWlmKGIuY2FsbChhW2RdLGQsYVtkXSk9PT0hMSlicmVha31lbHNlIGZvcihkIGluIGEpaWYoYi5jYWxsKGFbZF0sZCxhW2RdKT09PSExKWJyZWFrO3JldHVybiBhfSx0cmltOmZ1bmN0aW9uKGEpe3JldHVybiBudWxsPT1hP1wiXCI6KGErXCJcIikucmVwbGFjZShvLFwiXCIpfSxtYWtlQXJyYXk6ZnVuY3Rpb24oYSxiKXt2YXIgYz1ifHxbXTtyZXR1cm4gbnVsbCE9YSYmKHMoT2JqZWN0KGEpKT9uLm1lcmdlKGMsXCJzdHJpbmdcIj09dHlwZW9mIGE/W2FdOmEpOmcuY2FsbChjLGEpKSxjfSxpbkFycmF5OmZ1bmN0aW9uKGEsYixjKXtyZXR1cm4gbnVsbD09Yj8tMTpoLmNhbGwoYixhLGMpfSxtZXJnZTpmdW5jdGlvbihhLGIpe2Zvcih2YXIgYz0rYi5sZW5ndGgsZD0wLGU9YS5sZW5ndGg7Yz5kO2QrKylhW2UrK109YltkXTtyZXR1cm4gYS5sZW5ndGg9ZSxhfSxncmVwOmZ1bmN0aW9uKGEsYixjKXtmb3IodmFyIGQsZT1bXSxmPTAsZz1hLmxlbmd0aCxoPSFjO2c+ZjtmKyspZD0hYihhW2ZdLGYpLGQhPT1oJiZlLnB1c2goYVtmXSk7cmV0dXJuIGV9LG1hcDpmdW5jdGlvbihhLGIsYyl7dmFyIGQsZSxnPTAsaD1bXTtpZihzKGEpKWZvcihkPWEubGVuZ3RoO2Q+ZztnKyspZT1iKGFbZ10sZyxjKSxudWxsIT1lJiZoLnB1c2goZSk7ZWxzZSBmb3IoZyBpbiBhKWU9YihhW2ddLGcsYyksbnVsbCE9ZSYmaC5wdXNoKGUpO3JldHVybiBmLmFwcGx5KFtdLGgpfSxndWlkOjEscHJveHk6ZnVuY3Rpb24oYSxiKXt2YXIgYyxkLGY7cmV0dXJuXCJzdHJpbmdcIj09dHlwZW9mIGImJihjPWFbYl0sYj1hLGE9Yyksbi5pc0Z1bmN0aW9uKGEpPyhkPWUuY2FsbChhcmd1bWVudHMsMiksZj1mdW5jdGlvbigpe3JldHVybiBhLmFwcGx5KGJ8fHRoaXMsZC5jb25jYXQoZS5jYWxsKGFyZ3VtZW50cykpKX0sZi5ndWlkPWEuZ3VpZD1hLmd1aWR8fG4uZ3VpZCsrLGYpOnZvaWQgMH0sbm93OkRhdGUubm93LHN1cHBvcnQ6bH0pLFwiZnVuY3Rpb25cIj09dHlwZW9mIFN5bWJvbCYmKG4uZm5bU3ltYm9sLml0ZXJhdG9yXT1jW1N5bWJvbC5pdGVyYXRvcl0pLG4uZWFjaChcIkJvb2xlYW4gTnVtYmVyIFN0cmluZyBGdW5jdGlvbiBBcnJheSBEYXRlIFJlZ0V4cCBPYmplY3QgRXJyb3IgU3ltYm9sXCIuc3BsaXQoXCIgXCIpLGZ1bmN0aW9uKGEsYil7aVtcIltvYmplY3QgXCIrYitcIl1cIl09Yi50b0xvd2VyQ2FzZSgpfSk7ZnVuY3Rpb24gcyhhKXt2YXIgYj0hIWEmJlwibGVuZ3RoXCJpbiBhJiZhLmxlbmd0aCxjPW4udHlwZShhKTtyZXR1cm5cImZ1bmN0aW9uXCI9PT1jfHxuLmlzV2luZG93KGEpPyExOlwiYXJyYXlcIj09PWN8fDA9PT1ifHxcIm51bWJlclwiPT10eXBlb2YgYiYmYj4wJiZiLTEgaW4gYX12YXIgdD1mdW5jdGlvbihhKXt2YXIgYixjLGQsZSxmLGcsaCxpLGosayxsLG0sbixvLHAscSxyLHMsdCx1PVwic2l6emxlXCIrMSpuZXcgRGF0ZSx2PWEuZG9jdW1lbnQsdz0wLHg9MCx5PWdhKCksej1nYSgpLEE9Z2EoKSxCPWZ1bmN0aW9uKGEsYil7cmV0dXJuIGE9PT1iJiYobD0hMCksMH0sQz0xPDwzMSxEPXt9Lmhhc093blByb3BlcnR5LEU9W10sRj1FLnBvcCxHPUUucHVzaCxIPUUucHVzaCxJPUUuc2xpY2UsSj1mdW5jdGlvbihhLGIpe2Zvcih2YXIgYz0wLGQ9YS5sZW5ndGg7ZD5jO2MrKylpZihhW2NdPT09YilyZXR1cm4gYztyZXR1cm4tMX0sSz1cImNoZWNrZWR8c2VsZWN0ZWR8YXN5bmN8YXV0b2ZvY3VzfGF1dG9wbGF5fGNvbnRyb2xzfGRlZmVyfGRpc2FibGVkfGhpZGRlbnxpc21hcHxsb29wfG11bHRpcGxlfG9wZW58cmVhZG9ubHl8cmVxdWlyZWR8c2NvcGVkXCIsTD1cIltcXFxceDIwXFxcXHRcXFxcclxcXFxuXFxcXGZdXCIsTT1cIig/OlxcXFxcXFxcLnxbXFxcXHctXXxbXlxcXFx4MDAtXFxcXHhhMF0pK1wiLE49XCJcXFxcW1wiK0wrXCIqKFwiK00rXCIpKD86XCIrTCtcIiooWypeJHwhfl0/PSlcIitMK1wiKig/OicoKD86XFxcXFxcXFwufFteXFxcXFxcXFwnXSkqKSd8XFxcIigoPzpcXFxcXFxcXC58W15cXFxcXFxcXFxcXCJdKSopXFxcInwoXCIrTStcIikpfClcIitMK1wiKlxcXFxdXCIsTz1cIjooXCIrTStcIikoPzpcXFxcKCgoJygoPzpcXFxcXFxcXC58W15cXFxcXFxcXCddKSopJ3xcXFwiKCg/OlxcXFxcXFxcLnxbXlxcXFxcXFxcXFxcIl0pKilcXFwiKXwoKD86XFxcXFxcXFwufFteXFxcXFxcXFwoKVtcXFxcXV18XCIrTitcIikqKXwuKilcXFxcKXwpXCIsUD1uZXcgUmVnRXhwKEwrXCIrXCIsXCJnXCIpLFE9bmV3IFJlZ0V4cChcIl5cIitMK1wiK3woKD86XnxbXlxcXFxcXFxcXSkoPzpcXFxcXFxcXC4pKilcIitMK1wiKyRcIixcImdcIiksUj1uZXcgUmVnRXhwKFwiXlwiK0wrXCIqLFwiK0wrXCIqXCIpLFM9bmV3IFJlZ0V4cChcIl5cIitMK1wiKihbPit+XXxcIitMK1wiKVwiK0wrXCIqXCIpLFQ9bmV3IFJlZ0V4cChcIj1cIitMK1wiKihbXlxcXFxdJ1xcXCJdKj8pXCIrTCtcIipcXFxcXVwiLFwiZ1wiKSxVPW5ldyBSZWdFeHAoTyksVj1uZXcgUmVnRXhwKFwiXlwiK00rXCIkXCIpLFc9e0lEOm5ldyBSZWdFeHAoXCJeIyhcIitNK1wiKVwiKSxDTEFTUzpuZXcgUmVnRXhwKFwiXlxcXFwuKFwiK00rXCIpXCIpLFRBRzpuZXcgUmVnRXhwKFwiXihcIitNK1wifFsqXSlcIiksQVRUUjpuZXcgUmVnRXhwKFwiXlwiK04pLFBTRVVETzpuZXcgUmVnRXhwKFwiXlwiK08pLENISUxEOm5ldyBSZWdFeHAoXCJeOihvbmx5fGZpcnN0fGxhc3R8bnRofG50aC1sYXN0KS0oY2hpbGR8b2YtdHlwZSkoPzpcXFxcKFwiK0wrXCIqKGV2ZW58b2RkfCgoWystXXwpKFxcXFxkKilufClcIitMK1wiKig/OihbKy1dfClcIitMK1wiKihcXFxcZCspfCkpXCIrTCtcIipcXFxcKXwpXCIsXCJpXCIpLGJvb2w6bmV3IFJlZ0V4cChcIl4oPzpcIitLK1wiKSRcIixcImlcIiksbmVlZHNDb250ZXh0Om5ldyBSZWdFeHAoXCJeXCIrTCtcIipbPit+XXw6KGV2ZW58b2RkfGVxfGd0fGx0fG50aHxmaXJzdHxsYXN0KSg/OlxcXFwoXCIrTCtcIiooKD86LVxcXFxkKT9cXFxcZCopXCIrTCtcIipcXFxcKXwpKD89W14tXXwkKVwiLFwiaVwiKX0sWD0vXig/OmlucHV0fHNlbGVjdHx0ZXh0YXJlYXxidXR0b24pJC9pLFk9L15oXFxkJC9pLFo9L15bXntdK1xce1xccypcXFtuYXRpdmUgXFx3LywkPS9eKD86IyhbXFx3LV0rKXwoXFx3Kyl8XFwuKFtcXHctXSspKSQvLF89L1srfl0vLGFhPS8nfFxcXFwvZyxiYT1uZXcgUmVnRXhwKFwiXFxcXFxcXFwoW1xcXFxkYS1mXXsxLDZ9XCIrTCtcIj98KFwiK0wrXCIpfC4pXCIsXCJpZ1wiKSxjYT1mdW5jdGlvbihhLGIsYyl7dmFyIGQ9XCIweFwiK2ItNjU1MzY7cmV0dXJuIGQhPT1kfHxjP2I6MD5kP1N0cmluZy5mcm9tQ2hhckNvZGUoZCs2NTUzNik6U3RyaW5nLmZyb21DaGFyQ29kZShkPj4xMHw1NTI5NiwxMDIzJmR8NTYzMjApfSxkYT1mdW5jdGlvbigpe20oKX07dHJ5e0guYXBwbHkoRT1JLmNhbGwodi5jaGlsZE5vZGVzKSx2LmNoaWxkTm9kZXMpLEVbdi5jaGlsZE5vZGVzLmxlbmd0aF0ubm9kZVR5cGV9Y2F0Y2goZWEpe0g9e2FwcGx5OkUubGVuZ3RoP2Z1bmN0aW9uKGEsYil7Ry5hcHBseShhLEkuY2FsbChiKSl9OmZ1bmN0aW9uKGEsYil7dmFyIGM9YS5sZW5ndGgsZD0wO3doaWxlKGFbYysrXT1iW2QrK10pO2EubGVuZ3RoPWMtMX19fWZ1bmN0aW9uIGZhKGEsYixkLGUpe3ZhciBmLGgsaixrLGwsbyxyLHMsdz1iJiZiLm93bmVyRG9jdW1lbnQseD1iP2Iubm9kZVR5cGU6OTtpZihkPWR8fFtdLFwic3RyaW5nXCIhPXR5cGVvZiBhfHwhYXx8MSE9PXgmJjkhPT14JiYxMSE9PXgpcmV0dXJuIGQ7aWYoIWUmJigoYj9iLm93bmVyRG9jdW1lbnR8fGI6dikhPT1uJiZtKGIpLGI9Ynx8bixwKSl7aWYoMTEhPT14JiYobz0kLmV4ZWMoYSkpKWlmKGY9b1sxXSl7aWYoOT09PXgpe2lmKCEoaj1iLmdldEVsZW1lbnRCeUlkKGYpKSlyZXR1cm4gZDtpZihqLmlkPT09ZilyZXR1cm4gZC5wdXNoKGopLGR9ZWxzZSBpZih3JiYoaj13LmdldEVsZW1lbnRCeUlkKGYpKSYmdChiLGopJiZqLmlkPT09ZilyZXR1cm4gZC5wdXNoKGopLGR9ZWxzZXtpZihvWzJdKXJldHVybiBILmFwcGx5KGQsYi5nZXRFbGVtZW50c0J5VGFnTmFtZShhKSksZDtpZigoZj1vWzNdKSYmYy5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lJiZiLmdldEVsZW1lbnRzQnlDbGFzc05hbWUpcmV0dXJuIEguYXBwbHkoZCxiLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoZikpLGR9aWYoYy5xc2EmJiFBW2ErXCIgXCJdJiYoIXF8fCFxLnRlc3QoYSkpKXtpZigxIT09eCl3PWIscz1hO2Vsc2UgaWYoXCJvYmplY3RcIiE9PWIubm9kZU5hbWUudG9Mb3dlckNhc2UoKSl7KGs9Yi5nZXRBdHRyaWJ1dGUoXCJpZFwiKSk/az1rLnJlcGxhY2UoYWEsXCJcXFxcJCZcIik6Yi5zZXRBdHRyaWJ1dGUoXCJpZFwiLGs9dSkscj1nKGEpLGg9ci5sZW5ndGgsbD1WLnRlc3Qoayk/XCIjXCIrazpcIltpZD0nXCIraytcIiddXCI7d2hpbGUoaC0tKXJbaF09bCtcIiBcIitxYShyW2hdKTtzPXIuam9pbihcIixcIiksdz1fLnRlc3QoYSkmJm9hKGIucGFyZW50Tm9kZSl8fGJ9aWYocyl0cnl7cmV0dXJuIEguYXBwbHkoZCx3LnF1ZXJ5U2VsZWN0b3JBbGwocykpLGR9Y2F0Y2goeSl7fWZpbmFsbHl7az09PXUmJmIucmVtb3ZlQXR0cmlidXRlKFwiaWRcIil9fX1yZXR1cm4gaShhLnJlcGxhY2UoUSxcIiQxXCIpLGIsZCxlKX1mdW5jdGlvbiBnYSgpe3ZhciBhPVtdO2Z1bmN0aW9uIGIoYyxlKXtyZXR1cm4gYS5wdXNoKGMrXCIgXCIpPmQuY2FjaGVMZW5ndGgmJmRlbGV0ZSBiW2Euc2hpZnQoKV0sYltjK1wiIFwiXT1lfXJldHVybiBifWZ1bmN0aW9uIGhhKGEpe3JldHVybiBhW3VdPSEwLGF9ZnVuY3Rpb24gaWEoYSl7dmFyIGI9bi5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO3RyeXtyZXR1cm4hIWEoYil9Y2F0Y2goYyl7cmV0dXJuITF9ZmluYWxseXtiLnBhcmVudE5vZGUmJmIucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChiKSxiPW51bGx9fWZ1bmN0aW9uIGphKGEsYil7dmFyIGM9YS5zcGxpdChcInxcIiksZT1jLmxlbmd0aDt3aGlsZShlLS0pZC5hdHRySGFuZGxlW2NbZV1dPWJ9ZnVuY3Rpb24ga2EoYSxiKXt2YXIgYz1iJiZhLGQ9YyYmMT09PWEubm9kZVR5cGUmJjE9PT1iLm5vZGVUeXBlJiYofmIuc291cmNlSW5kZXh8fEMpLSh+YS5zb3VyY2VJbmRleHx8Qyk7aWYoZClyZXR1cm4gZDtpZihjKXdoaWxlKGM9Yy5uZXh0U2libGluZylpZihjPT09YilyZXR1cm4tMTtyZXR1cm4gYT8xOi0xfWZ1bmN0aW9uIGxhKGEpe3JldHVybiBmdW5jdGlvbihiKXt2YXIgYz1iLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk7cmV0dXJuXCJpbnB1dFwiPT09YyYmYi50eXBlPT09YX19ZnVuY3Rpb24gbWEoYSl7cmV0dXJuIGZ1bmN0aW9uKGIpe3ZhciBjPWIubm9kZU5hbWUudG9Mb3dlckNhc2UoKTtyZXR1cm4oXCJpbnB1dFwiPT09Y3x8XCJidXR0b25cIj09PWMpJiZiLnR5cGU9PT1hfX1mdW5jdGlvbiBuYShhKXtyZXR1cm4gaGEoZnVuY3Rpb24oYil7cmV0dXJuIGI9K2IsaGEoZnVuY3Rpb24oYyxkKXt2YXIgZSxmPWEoW10sYy5sZW5ndGgsYiksZz1mLmxlbmd0aDt3aGlsZShnLS0pY1tlPWZbZ11dJiYoY1tlXT0hKGRbZV09Y1tlXSkpfSl9KX1mdW5jdGlvbiBvYShhKXtyZXR1cm4gYSYmXCJ1bmRlZmluZWRcIiE9dHlwZW9mIGEuZ2V0RWxlbWVudHNCeVRhZ05hbWUmJmF9Yz1mYS5zdXBwb3J0PXt9LGY9ZmEuaXNYTUw9ZnVuY3Rpb24oYSl7dmFyIGI9YSYmKGEub3duZXJEb2N1bWVudHx8YSkuZG9jdW1lbnRFbGVtZW50O3JldHVybiBiP1wiSFRNTFwiIT09Yi5ub2RlTmFtZTohMX0sbT1mYS5zZXREb2N1bWVudD1mdW5jdGlvbihhKXt2YXIgYixlLGc9YT9hLm93bmVyRG9jdW1lbnR8fGE6djtyZXR1cm4gZyE9PW4mJjk9PT1nLm5vZGVUeXBlJiZnLmRvY3VtZW50RWxlbWVudD8obj1nLG89bi5kb2N1bWVudEVsZW1lbnQscD0hZihuKSwoZT1uLmRlZmF1bHRWaWV3KSYmZS50b3AhPT1lJiYoZS5hZGRFdmVudExpc3RlbmVyP2UuYWRkRXZlbnRMaXN0ZW5lcihcInVubG9hZFwiLGRhLCExKTplLmF0dGFjaEV2ZW50JiZlLmF0dGFjaEV2ZW50KFwib251bmxvYWRcIixkYSkpLGMuYXR0cmlidXRlcz1pYShmdW5jdGlvbihhKXtyZXR1cm4gYS5jbGFzc05hbWU9XCJpXCIsIWEuZ2V0QXR0cmlidXRlKFwiY2xhc3NOYW1lXCIpfSksYy5nZXRFbGVtZW50c0J5VGFnTmFtZT1pYShmdW5jdGlvbihhKXtyZXR1cm4gYS5hcHBlbmRDaGlsZChuLmNyZWF0ZUNvbW1lbnQoXCJcIikpLCFhLmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiKlwiKS5sZW5ndGh9KSxjLmdldEVsZW1lbnRzQnlDbGFzc05hbWU9Wi50ZXN0KG4uZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSksYy5nZXRCeUlkPWlhKGZ1bmN0aW9uKGEpe3JldHVybiBvLmFwcGVuZENoaWxkKGEpLmlkPXUsIW4uZ2V0RWxlbWVudHNCeU5hbWV8fCFuLmdldEVsZW1lbnRzQnlOYW1lKHUpLmxlbmd0aH0pLGMuZ2V0QnlJZD8oZC5maW5kLklEPWZ1bmN0aW9uKGEsYil7aWYoXCJ1bmRlZmluZWRcIiE9dHlwZW9mIGIuZ2V0RWxlbWVudEJ5SWQmJnApe3ZhciBjPWIuZ2V0RWxlbWVudEJ5SWQoYSk7cmV0dXJuIGM/W2NdOltdfX0sZC5maWx0ZXIuSUQ9ZnVuY3Rpb24oYSl7dmFyIGI9YS5yZXBsYWNlKGJhLGNhKTtyZXR1cm4gZnVuY3Rpb24oYSl7cmV0dXJuIGEuZ2V0QXR0cmlidXRlKFwiaWRcIik9PT1ifX0pOihkZWxldGUgZC5maW5kLklELGQuZmlsdGVyLklEPWZ1bmN0aW9uKGEpe3ZhciBiPWEucmVwbGFjZShiYSxjYSk7cmV0dXJuIGZ1bmN0aW9uKGEpe3ZhciBjPVwidW5kZWZpbmVkXCIhPXR5cGVvZiBhLmdldEF0dHJpYnV0ZU5vZGUmJmEuZ2V0QXR0cmlidXRlTm9kZShcImlkXCIpO3JldHVybiBjJiZjLnZhbHVlPT09Yn19KSxkLmZpbmQuVEFHPWMuZ2V0RWxlbWVudHNCeVRhZ05hbWU/ZnVuY3Rpb24oYSxiKXtyZXR1cm5cInVuZGVmaW5lZFwiIT10eXBlb2YgYi5nZXRFbGVtZW50c0J5VGFnTmFtZT9iLmdldEVsZW1lbnRzQnlUYWdOYW1lKGEpOmMucXNhP2IucXVlcnlTZWxlY3RvckFsbChhKTp2b2lkIDB9OmZ1bmN0aW9uKGEsYil7dmFyIGMsZD1bXSxlPTAsZj1iLmdldEVsZW1lbnRzQnlUYWdOYW1lKGEpO2lmKFwiKlwiPT09YSl7d2hpbGUoYz1mW2UrK10pMT09PWMubm9kZVR5cGUmJmQucHVzaChjKTtyZXR1cm4gZH1yZXR1cm4gZn0sZC5maW5kLkNMQVNTPWMuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSYmZnVuY3Rpb24oYSxiKXtyZXR1cm5cInVuZGVmaW5lZFwiIT10eXBlb2YgYi5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lJiZwP2IuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShhKTp2b2lkIDB9LHI9W10scT1bXSwoYy5xc2E9Wi50ZXN0KG4ucXVlcnlTZWxlY3RvckFsbCkpJiYoaWEoZnVuY3Rpb24oYSl7by5hcHBlbmRDaGlsZChhKS5pbm5lckhUTUw9XCI8YSBpZD0nXCIrdStcIic+PC9hPjxzZWxlY3QgaWQ9J1wiK3UrXCItXFxyXFxcXCcgbXNhbGxvd2NhcHR1cmU9Jyc+PG9wdGlvbiBzZWxlY3RlZD0nJz48L29wdGlvbj48L3NlbGVjdD5cIixhLnF1ZXJ5U2VsZWN0b3JBbGwoXCJbbXNhbGxvd2NhcHR1cmVePScnXVwiKS5sZW5ndGgmJnEucHVzaChcIlsqXiRdPVwiK0wrXCIqKD86Jyd8XFxcIlxcXCIpXCIpLGEucXVlcnlTZWxlY3RvckFsbChcIltzZWxlY3RlZF1cIikubGVuZ3RofHxxLnB1c2goXCJcXFxcW1wiK0wrXCIqKD86dmFsdWV8XCIrSytcIilcIiksYS5xdWVyeVNlbGVjdG9yQWxsKFwiW2lkfj1cIit1K1wiLV1cIikubGVuZ3RofHxxLnB1c2goXCJ+PVwiKSxhLnF1ZXJ5U2VsZWN0b3JBbGwoXCI6Y2hlY2tlZFwiKS5sZW5ndGh8fHEucHVzaChcIjpjaGVja2VkXCIpLGEucXVlcnlTZWxlY3RvckFsbChcImEjXCIrdStcIisqXCIpLmxlbmd0aHx8cS5wdXNoKFwiLiMuK1srfl1cIil9KSxpYShmdW5jdGlvbihhKXt2YXIgYj1uLmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiKTtiLnNldEF0dHJpYnV0ZShcInR5cGVcIixcImhpZGRlblwiKSxhLmFwcGVuZENoaWxkKGIpLnNldEF0dHJpYnV0ZShcIm5hbWVcIixcIkRcIiksYS5xdWVyeVNlbGVjdG9yQWxsKFwiW25hbWU9ZF1cIikubGVuZ3RoJiZxLnB1c2goXCJuYW1lXCIrTCtcIipbKl4kfCF+XT89XCIpLGEucXVlcnlTZWxlY3RvckFsbChcIjplbmFibGVkXCIpLmxlbmd0aHx8cS5wdXNoKFwiOmVuYWJsZWRcIixcIjpkaXNhYmxlZFwiKSxhLnF1ZXJ5U2VsZWN0b3JBbGwoXCIqLDp4XCIpLHEucHVzaChcIiwuKjpcIil9KSksKGMubWF0Y2hlc1NlbGVjdG9yPVoudGVzdChzPW8ubWF0Y2hlc3x8by53ZWJraXRNYXRjaGVzU2VsZWN0b3J8fG8ubW96TWF0Y2hlc1NlbGVjdG9yfHxvLm9NYXRjaGVzU2VsZWN0b3J8fG8ubXNNYXRjaGVzU2VsZWN0b3IpKSYmaWEoZnVuY3Rpb24oYSl7Yy5kaXNjb25uZWN0ZWRNYXRjaD1zLmNhbGwoYSxcImRpdlwiKSxzLmNhbGwoYSxcIltzIT0nJ106eFwiKSxyLnB1c2goXCIhPVwiLE8pfSkscT1xLmxlbmd0aCYmbmV3IFJlZ0V4cChxLmpvaW4oXCJ8XCIpKSxyPXIubGVuZ3RoJiZuZXcgUmVnRXhwKHIuam9pbihcInxcIikpLGI9Wi50ZXN0KG8uY29tcGFyZURvY3VtZW50UG9zaXRpb24pLHQ9Ynx8Wi50ZXN0KG8uY29udGFpbnMpP2Z1bmN0aW9uKGEsYil7dmFyIGM9OT09PWEubm9kZVR5cGU/YS5kb2N1bWVudEVsZW1lbnQ6YSxkPWImJmIucGFyZW50Tm9kZTtyZXR1cm4gYT09PWR8fCEoIWR8fDEhPT1kLm5vZGVUeXBlfHwhKGMuY29udGFpbnM/Yy5jb250YWlucyhkKTphLmNvbXBhcmVEb2N1bWVudFBvc2l0aW9uJiYxNiZhLmNvbXBhcmVEb2N1bWVudFBvc2l0aW9uKGQpKSl9OmZ1bmN0aW9uKGEsYil7aWYoYil3aGlsZShiPWIucGFyZW50Tm9kZSlpZihiPT09YSlyZXR1cm4hMDtyZXR1cm4hMX0sQj1iP2Z1bmN0aW9uKGEsYil7aWYoYT09PWIpcmV0dXJuIGw9ITAsMDt2YXIgZD0hYS5jb21wYXJlRG9jdW1lbnRQb3NpdGlvbi0hYi5jb21wYXJlRG9jdW1lbnRQb3NpdGlvbjtyZXR1cm4gZD9kOihkPShhLm93bmVyRG9jdW1lbnR8fGEpPT09KGIub3duZXJEb2N1bWVudHx8Yik/YS5jb21wYXJlRG9jdW1lbnRQb3NpdGlvbihiKToxLDEmZHx8IWMuc29ydERldGFjaGVkJiZiLmNvbXBhcmVEb2N1bWVudFBvc2l0aW9uKGEpPT09ZD9hPT09bnx8YS5vd25lckRvY3VtZW50PT09diYmdCh2LGEpPy0xOmI9PT1ufHxiLm93bmVyRG9jdW1lbnQ9PT12JiZ0KHYsYik/MTprP0ooayxhKS1KKGssYik6MDo0JmQ/LTE6MSl9OmZ1bmN0aW9uKGEsYil7aWYoYT09PWIpcmV0dXJuIGw9ITAsMDt2YXIgYyxkPTAsZT1hLnBhcmVudE5vZGUsZj1iLnBhcmVudE5vZGUsZz1bYV0saD1bYl07aWYoIWV8fCFmKXJldHVybiBhPT09bj8tMTpiPT09bj8xOmU/LTE6Zj8xOms/SihrLGEpLUooayxiKTowO2lmKGU9PT1mKXJldHVybiBrYShhLGIpO2M9YTt3aGlsZShjPWMucGFyZW50Tm9kZSlnLnVuc2hpZnQoYyk7Yz1iO3doaWxlKGM9Yy5wYXJlbnROb2RlKWgudW5zaGlmdChjKTt3aGlsZShnW2RdPT09aFtkXSlkKys7cmV0dXJuIGQ/a2EoZ1tkXSxoW2RdKTpnW2RdPT09dj8tMTpoW2RdPT09dj8xOjB9LG4pOm59LGZhLm1hdGNoZXM9ZnVuY3Rpb24oYSxiKXtyZXR1cm4gZmEoYSxudWxsLG51bGwsYil9LGZhLm1hdGNoZXNTZWxlY3Rvcj1mdW5jdGlvbihhLGIpe2lmKChhLm93bmVyRG9jdW1lbnR8fGEpIT09biYmbShhKSxiPWIucmVwbGFjZShULFwiPSckMSddXCIpLGMubWF0Y2hlc1NlbGVjdG9yJiZwJiYhQVtiK1wiIFwiXSYmKCFyfHwhci50ZXN0KGIpKSYmKCFxfHwhcS50ZXN0KGIpKSl0cnl7dmFyIGQ9cy5jYWxsKGEsYik7aWYoZHx8Yy5kaXNjb25uZWN0ZWRNYXRjaHx8YS5kb2N1bWVudCYmMTEhPT1hLmRvY3VtZW50Lm5vZGVUeXBlKXJldHVybiBkfWNhdGNoKGUpe31yZXR1cm4gZmEoYixuLG51bGwsW2FdKS5sZW5ndGg+MH0sZmEuY29udGFpbnM9ZnVuY3Rpb24oYSxiKXtyZXR1cm4oYS5vd25lckRvY3VtZW50fHxhKSE9PW4mJm0oYSksdChhLGIpfSxmYS5hdHRyPWZ1bmN0aW9uKGEsYil7KGEub3duZXJEb2N1bWVudHx8YSkhPT1uJiZtKGEpO3ZhciBlPWQuYXR0ckhhbmRsZVtiLnRvTG93ZXJDYXNlKCldLGY9ZSYmRC5jYWxsKGQuYXR0ckhhbmRsZSxiLnRvTG93ZXJDYXNlKCkpP2UoYSxiLCFwKTp2b2lkIDA7cmV0dXJuIHZvaWQgMCE9PWY/ZjpjLmF0dHJpYnV0ZXN8fCFwP2EuZ2V0QXR0cmlidXRlKGIpOihmPWEuZ2V0QXR0cmlidXRlTm9kZShiKSkmJmYuc3BlY2lmaWVkP2YudmFsdWU6bnVsbH0sZmEuZXJyb3I9ZnVuY3Rpb24oYSl7dGhyb3cgbmV3IEVycm9yKFwiU3ludGF4IGVycm9yLCB1bnJlY29nbml6ZWQgZXhwcmVzc2lvbjogXCIrYSl9LGZhLnVuaXF1ZVNvcnQ9ZnVuY3Rpb24oYSl7dmFyIGIsZD1bXSxlPTAsZj0wO2lmKGw9IWMuZGV0ZWN0RHVwbGljYXRlcyxrPSFjLnNvcnRTdGFibGUmJmEuc2xpY2UoMCksYS5zb3J0KEIpLGwpe3doaWxlKGI9YVtmKytdKWI9PT1hW2ZdJiYoZT1kLnB1c2goZikpO3doaWxlKGUtLSlhLnNwbGljZShkW2VdLDEpfXJldHVybiBrPW51bGwsYX0sZT1mYS5nZXRUZXh0PWZ1bmN0aW9uKGEpe3ZhciBiLGM9XCJcIixkPTAsZj1hLm5vZGVUeXBlO2lmKGYpe2lmKDE9PT1mfHw5PT09Znx8MTE9PT1mKXtpZihcInN0cmluZ1wiPT10eXBlb2YgYS50ZXh0Q29udGVudClyZXR1cm4gYS50ZXh0Q29udGVudDtmb3IoYT1hLmZpcnN0Q2hpbGQ7YTthPWEubmV4dFNpYmxpbmcpYys9ZShhKX1lbHNlIGlmKDM9PT1mfHw0PT09ZilyZXR1cm4gYS5ub2RlVmFsdWV9ZWxzZSB3aGlsZShiPWFbZCsrXSljKz1lKGIpO3JldHVybiBjfSxkPWZhLnNlbGVjdG9ycz17Y2FjaGVMZW5ndGg6NTAsY3JlYXRlUHNldWRvOmhhLG1hdGNoOlcsYXR0ckhhbmRsZTp7fSxmaW5kOnt9LHJlbGF0aXZlOntcIj5cIjp7ZGlyOlwicGFyZW50Tm9kZVwiLGZpcnN0OiEwfSxcIiBcIjp7ZGlyOlwicGFyZW50Tm9kZVwifSxcIitcIjp7ZGlyOlwicHJldmlvdXNTaWJsaW5nXCIsZmlyc3Q6ITB9LFwiflwiOntkaXI6XCJwcmV2aW91c1NpYmxpbmdcIn19LHByZUZpbHRlcjp7QVRUUjpmdW5jdGlvbihhKXtyZXR1cm4gYVsxXT1hWzFdLnJlcGxhY2UoYmEsY2EpLGFbM109KGFbM118fGFbNF18fGFbNV18fFwiXCIpLnJlcGxhY2UoYmEsY2EpLFwifj1cIj09PWFbMl0mJihhWzNdPVwiIFwiK2FbM10rXCIgXCIpLGEuc2xpY2UoMCw0KX0sQ0hJTEQ6ZnVuY3Rpb24oYSl7cmV0dXJuIGFbMV09YVsxXS50b0xvd2VyQ2FzZSgpLFwibnRoXCI9PT1hWzFdLnNsaWNlKDAsMyk/KGFbM118fGZhLmVycm9yKGFbMF0pLGFbNF09KyhhWzRdP2FbNV0rKGFbNl18fDEpOjIqKFwiZXZlblwiPT09YVszXXx8XCJvZGRcIj09PWFbM10pKSxhWzVdPSsoYVs3XSthWzhdfHxcIm9kZFwiPT09YVszXSkpOmFbM10mJmZhLmVycm9yKGFbMF0pLGF9LFBTRVVETzpmdW5jdGlvbihhKXt2YXIgYixjPSFhWzZdJiZhWzJdO3JldHVybiBXLkNISUxELnRlc3QoYVswXSk/bnVsbDooYVszXT9hWzJdPWFbNF18fGFbNV18fFwiXCI6YyYmVS50ZXN0KGMpJiYoYj1nKGMsITApKSYmKGI9Yy5pbmRleE9mKFwiKVwiLGMubGVuZ3RoLWIpLWMubGVuZ3RoKSYmKGFbMF09YVswXS5zbGljZSgwLGIpLGFbMl09Yy5zbGljZSgwLGIpKSxhLnNsaWNlKDAsMykpfX0sZmlsdGVyOntUQUc6ZnVuY3Rpb24oYSl7dmFyIGI9YS5yZXBsYWNlKGJhLGNhKS50b0xvd2VyQ2FzZSgpO3JldHVyblwiKlwiPT09YT9mdW5jdGlvbigpe3JldHVybiEwfTpmdW5jdGlvbihhKXtyZXR1cm4gYS5ub2RlTmFtZSYmYS5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpPT09Yn19LENMQVNTOmZ1bmN0aW9uKGEpe3ZhciBiPXlbYStcIiBcIl07cmV0dXJuIGJ8fChiPW5ldyBSZWdFeHAoXCIoXnxcIitMK1wiKVwiK2ErXCIoXCIrTCtcInwkKVwiKSkmJnkoYSxmdW5jdGlvbihhKXtyZXR1cm4gYi50ZXN0KFwic3RyaW5nXCI9PXR5cGVvZiBhLmNsYXNzTmFtZSYmYS5jbGFzc05hbWV8fFwidW5kZWZpbmVkXCIhPXR5cGVvZiBhLmdldEF0dHJpYnV0ZSYmYS5nZXRBdHRyaWJ1dGUoXCJjbGFzc1wiKXx8XCJcIil9KX0sQVRUUjpmdW5jdGlvbihhLGIsYyl7cmV0dXJuIGZ1bmN0aW9uKGQpe3ZhciBlPWZhLmF0dHIoZCxhKTtyZXR1cm4gbnVsbD09ZT9cIiE9XCI9PT1iOmI/KGUrPVwiXCIsXCI9XCI9PT1iP2U9PT1jOlwiIT1cIj09PWI/ZSE9PWM6XCJePVwiPT09Yj9jJiYwPT09ZS5pbmRleE9mKGMpOlwiKj1cIj09PWI/YyYmZS5pbmRleE9mKGMpPi0xOlwiJD1cIj09PWI/YyYmZS5zbGljZSgtYy5sZW5ndGgpPT09YzpcIn49XCI9PT1iPyhcIiBcIitlLnJlcGxhY2UoUCxcIiBcIikrXCIgXCIpLmluZGV4T2YoYyk+LTE6XCJ8PVwiPT09Yj9lPT09Y3x8ZS5zbGljZSgwLGMubGVuZ3RoKzEpPT09YytcIi1cIjohMSk6ITB9fSxDSElMRDpmdW5jdGlvbihhLGIsYyxkLGUpe3ZhciBmPVwibnRoXCIhPT1hLnNsaWNlKDAsMyksZz1cImxhc3RcIiE9PWEuc2xpY2UoLTQpLGg9XCJvZi10eXBlXCI9PT1iO3JldHVybiAxPT09ZCYmMD09PWU/ZnVuY3Rpb24oYSl7cmV0dXJuISFhLnBhcmVudE5vZGV9OmZ1bmN0aW9uKGIsYyxpKXt2YXIgaixrLGwsbSxuLG8scD1mIT09Zz9cIm5leHRTaWJsaW5nXCI6XCJwcmV2aW91c1NpYmxpbmdcIixxPWIucGFyZW50Tm9kZSxyPWgmJmIubm9kZU5hbWUudG9Mb3dlckNhc2UoKSxzPSFpJiYhaCx0PSExO2lmKHEpe2lmKGYpe3doaWxlKHApe209Yjt3aGlsZShtPW1bcF0paWYoaD9tLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk9PT1yOjE9PT1tLm5vZGVUeXBlKXJldHVybiExO289cD1cIm9ubHlcIj09PWEmJiFvJiZcIm5leHRTaWJsaW5nXCJ9cmV0dXJuITB9aWYobz1bZz9xLmZpcnN0Q2hpbGQ6cS5sYXN0Q2hpbGRdLGcmJnMpe209cSxsPW1bdV18fChtW3VdPXt9KSxrPWxbbS51bmlxdWVJRF18fChsW20udW5pcXVlSURdPXt9KSxqPWtbYV18fFtdLG49alswXT09PXcmJmpbMV0sdD1uJiZqWzJdLG09biYmcS5jaGlsZE5vZGVzW25dO3doaWxlKG09KytuJiZtJiZtW3BdfHwodD1uPTApfHxvLnBvcCgpKWlmKDE9PT1tLm5vZGVUeXBlJiYrK3QmJm09PT1iKXtrW2FdPVt3LG4sdF07YnJlYWt9fWVsc2UgaWYocyYmKG09YixsPW1bdV18fChtW3VdPXt9KSxrPWxbbS51bmlxdWVJRF18fChsW20udW5pcXVlSURdPXt9KSxqPWtbYV18fFtdLG49alswXT09PXcmJmpbMV0sdD1uKSx0PT09ITEpd2hpbGUobT0rK24mJm0mJm1bcF18fCh0PW49MCl8fG8ucG9wKCkpaWYoKGg/bS5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpPT09cjoxPT09bS5ub2RlVHlwZSkmJisrdCYmKHMmJihsPW1bdV18fChtW3VdPXt9KSxrPWxbbS51bmlxdWVJRF18fChsW20udW5pcXVlSURdPXt9KSxrW2FdPVt3LHRdKSxtPT09YikpYnJlYWs7cmV0dXJuIHQtPWUsdD09PWR8fHQlZD09PTAmJnQvZD49MH19fSxQU0VVRE86ZnVuY3Rpb24oYSxiKXt2YXIgYyxlPWQucHNldWRvc1thXXx8ZC5zZXRGaWx0ZXJzW2EudG9Mb3dlckNhc2UoKV18fGZhLmVycm9yKFwidW5zdXBwb3J0ZWQgcHNldWRvOiBcIithKTtyZXR1cm4gZVt1XT9lKGIpOmUubGVuZ3RoPjE/KGM9W2EsYSxcIlwiLGJdLGQuc2V0RmlsdGVycy5oYXNPd25Qcm9wZXJ0eShhLnRvTG93ZXJDYXNlKCkpP2hhKGZ1bmN0aW9uKGEsYyl7dmFyIGQsZj1lKGEsYiksZz1mLmxlbmd0aDt3aGlsZShnLS0pZD1KKGEsZltnXSksYVtkXT0hKGNbZF09ZltnXSl9KTpmdW5jdGlvbihhKXtyZXR1cm4gZShhLDAsYyl9KTplfX0scHNldWRvczp7bm90OmhhKGZ1bmN0aW9uKGEpe3ZhciBiPVtdLGM9W10sZD1oKGEucmVwbGFjZShRLFwiJDFcIikpO3JldHVybiBkW3VdP2hhKGZ1bmN0aW9uKGEsYixjLGUpe3ZhciBmLGc9ZChhLG51bGwsZSxbXSksaD1hLmxlbmd0aDt3aGlsZShoLS0pKGY9Z1toXSkmJihhW2hdPSEoYltoXT1mKSl9KTpmdW5jdGlvbihhLGUsZil7cmV0dXJuIGJbMF09YSxkKGIsbnVsbCxmLGMpLGJbMF09bnVsbCwhYy5wb3AoKX19KSxoYXM6aGEoZnVuY3Rpb24oYSl7cmV0dXJuIGZ1bmN0aW9uKGIpe3JldHVybiBmYShhLGIpLmxlbmd0aD4wfX0pLGNvbnRhaW5zOmhhKGZ1bmN0aW9uKGEpe3JldHVybiBhPWEucmVwbGFjZShiYSxjYSksZnVuY3Rpb24oYil7cmV0dXJuKGIudGV4dENvbnRlbnR8fGIuaW5uZXJUZXh0fHxlKGIpKS5pbmRleE9mKGEpPi0xfX0pLGxhbmc6aGEoZnVuY3Rpb24oYSl7cmV0dXJuIFYudGVzdChhfHxcIlwiKXx8ZmEuZXJyb3IoXCJ1bnN1cHBvcnRlZCBsYW5nOiBcIithKSxhPWEucmVwbGFjZShiYSxjYSkudG9Mb3dlckNhc2UoKSxmdW5jdGlvbihiKXt2YXIgYztkbyBpZihjPXA/Yi5sYW5nOmIuZ2V0QXR0cmlidXRlKFwieG1sOmxhbmdcIil8fGIuZ2V0QXR0cmlidXRlKFwibGFuZ1wiKSlyZXR1cm4gYz1jLnRvTG93ZXJDYXNlKCksYz09PWF8fDA9PT1jLmluZGV4T2YoYStcIi1cIik7d2hpbGUoKGI9Yi5wYXJlbnROb2RlKSYmMT09PWIubm9kZVR5cGUpO3JldHVybiExfX0pLHRhcmdldDpmdW5jdGlvbihiKXt2YXIgYz1hLmxvY2F0aW9uJiZhLmxvY2F0aW9uLmhhc2g7cmV0dXJuIGMmJmMuc2xpY2UoMSk9PT1iLmlkfSxyb290OmZ1bmN0aW9uKGEpe3JldHVybiBhPT09b30sZm9jdXM6ZnVuY3Rpb24oYSl7cmV0dXJuIGE9PT1uLmFjdGl2ZUVsZW1lbnQmJighbi5oYXNGb2N1c3x8bi5oYXNGb2N1cygpKSYmISEoYS50eXBlfHxhLmhyZWZ8fH5hLnRhYkluZGV4KX0sZW5hYmxlZDpmdW5jdGlvbihhKXtyZXR1cm4gYS5kaXNhYmxlZD09PSExfSxkaXNhYmxlZDpmdW5jdGlvbihhKXtyZXR1cm4gYS5kaXNhYmxlZD09PSEwfSxjaGVja2VkOmZ1bmN0aW9uKGEpe3ZhciBiPWEubm9kZU5hbWUudG9Mb3dlckNhc2UoKTtyZXR1cm5cImlucHV0XCI9PT1iJiYhIWEuY2hlY2tlZHx8XCJvcHRpb25cIj09PWImJiEhYS5zZWxlY3RlZH0sc2VsZWN0ZWQ6ZnVuY3Rpb24oYSl7cmV0dXJuIGEucGFyZW50Tm9kZSYmYS5wYXJlbnROb2RlLnNlbGVjdGVkSW5kZXgsYS5zZWxlY3RlZD09PSEwfSxlbXB0eTpmdW5jdGlvbihhKXtmb3IoYT1hLmZpcnN0Q2hpbGQ7YTthPWEubmV4dFNpYmxpbmcpaWYoYS5ub2RlVHlwZTw2KXJldHVybiExO3JldHVybiEwfSxwYXJlbnQ6ZnVuY3Rpb24oYSl7cmV0dXJuIWQucHNldWRvcy5lbXB0eShhKX0saGVhZGVyOmZ1bmN0aW9uKGEpe3JldHVybiBZLnRlc3QoYS5ub2RlTmFtZSl9LGlucHV0OmZ1bmN0aW9uKGEpe3JldHVybiBYLnRlc3QoYS5ub2RlTmFtZSl9LGJ1dHRvbjpmdW5jdGlvbihhKXt2YXIgYj1hLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk7cmV0dXJuXCJpbnB1dFwiPT09YiYmXCJidXR0b25cIj09PWEudHlwZXx8XCJidXR0b25cIj09PWJ9LHRleHQ6ZnVuY3Rpb24oYSl7dmFyIGI7cmV0dXJuXCJpbnB1dFwiPT09YS5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpJiZcInRleHRcIj09PWEudHlwZSYmKG51bGw9PShiPWEuZ2V0QXR0cmlidXRlKFwidHlwZVwiKSl8fFwidGV4dFwiPT09Yi50b0xvd2VyQ2FzZSgpKX0sZmlyc3Q6bmEoZnVuY3Rpb24oKXtyZXR1cm5bMF19KSxsYXN0Om5hKGZ1bmN0aW9uKGEsYil7cmV0dXJuW2ItMV19KSxlcTpuYShmdW5jdGlvbihhLGIsYyl7cmV0dXJuWzA+Yz9jK2I6Y119KSxldmVuOm5hKGZ1bmN0aW9uKGEsYil7Zm9yKHZhciBjPTA7Yj5jO2MrPTIpYS5wdXNoKGMpO3JldHVybiBhfSksb2RkOm5hKGZ1bmN0aW9uKGEsYil7Zm9yKHZhciBjPTE7Yj5jO2MrPTIpYS5wdXNoKGMpO3JldHVybiBhfSksbHQ6bmEoZnVuY3Rpb24oYSxiLGMpe2Zvcih2YXIgZD0wPmM/YytiOmM7LS1kPj0wOylhLnB1c2goZCk7cmV0dXJuIGF9KSxndDpuYShmdW5jdGlvbihhLGIsYyl7Zm9yKHZhciBkPTA+Yz9jK2I6YzsrK2Q8YjspYS5wdXNoKGQpO3JldHVybiBhfSl9fSxkLnBzZXVkb3MubnRoPWQucHNldWRvcy5lcTtmb3IoYiBpbntyYWRpbzohMCxjaGVja2JveDohMCxmaWxlOiEwLHBhc3N3b3JkOiEwLGltYWdlOiEwfSlkLnBzZXVkb3NbYl09bGEoYik7Zm9yKGIgaW57c3VibWl0OiEwLHJlc2V0OiEwfSlkLnBzZXVkb3NbYl09bWEoYik7ZnVuY3Rpb24gcGEoKXt9cGEucHJvdG90eXBlPWQuZmlsdGVycz1kLnBzZXVkb3MsZC5zZXRGaWx0ZXJzPW5ldyBwYSxnPWZhLnRva2VuaXplPWZ1bmN0aW9uKGEsYil7dmFyIGMsZSxmLGcsaCxpLGosaz16W2ErXCIgXCJdO2lmKGspcmV0dXJuIGI/MDprLnNsaWNlKDApO2g9YSxpPVtdLGo9ZC5wcmVGaWx0ZXI7d2hpbGUoaCl7KCFjfHwoZT1SLmV4ZWMoaCkpKSYmKGUmJihoPWguc2xpY2UoZVswXS5sZW5ndGgpfHxoKSxpLnB1c2goZj1bXSkpLGM9ITEsKGU9Uy5leGVjKGgpKSYmKGM9ZS5zaGlmdCgpLGYucHVzaCh7dmFsdWU6Yyx0eXBlOmVbMF0ucmVwbGFjZShRLFwiIFwiKX0pLGg9aC5zbGljZShjLmxlbmd0aCkpO2ZvcihnIGluIGQuZmlsdGVyKSEoZT1XW2ddLmV4ZWMoaCkpfHxqW2ddJiYhKGU9altnXShlKSl8fChjPWUuc2hpZnQoKSxmLnB1c2goe3ZhbHVlOmMsdHlwZTpnLG1hdGNoZXM6ZX0pLGg9aC5zbGljZShjLmxlbmd0aCkpO2lmKCFjKWJyZWFrfXJldHVybiBiP2gubGVuZ3RoOmg/ZmEuZXJyb3IoYSk6eihhLGkpLnNsaWNlKDApfTtmdW5jdGlvbiBxYShhKXtmb3IodmFyIGI9MCxjPWEubGVuZ3RoLGQ9XCJcIjtjPmI7YisrKWQrPWFbYl0udmFsdWU7cmV0dXJuIGR9ZnVuY3Rpb24gcmEoYSxiLGMpe3ZhciBkPWIuZGlyLGU9YyYmXCJwYXJlbnROb2RlXCI9PT1kLGY9eCsrO3JldHVybiBiLmZpcnN0P2Z1bmN0aW9uKGIsYyxmKXt3aGlsZShiPWJbZF0paWYoMT09PWIubm9kZVR5cGV8fGUpcmV0dXJuIGEoYixjLGYpfTpmdW5jdGlvbihiLGMsZyl7dmFyIGgsaSxqLGs9W3csZl07aWYoZyl7d2hpbGUoYj1iW2RdKWlmKCgxPT09Yi5ub2RlVHlwZXx8ZSkmJmEoYixjLGcpKXJldHVybiEwfWVsc2Ugd2hpbGUoYj1iW2RdKWlmKDE9PT1iLm5vZGVUeXBlfHxlKXtpZihqPWJbdV18fChiW3VdPXt9KSxpPWpbYi51bmlxdWVJRF18fChqW2IudW5pcXVlSURdPXt9KSwoaD1pW2RdKSYmaFswXT09PXcmJmhbMV09PT1mKXJldHVybiBrWzJdPWhbMl07aWYoaVtkXT1rLGtbMl09YShiLGMsZykpcmV0dXJuITB9fX1mdW5jdGlvbiBzYShhKXtyZXR1cm4gYS5sZW5ndGg+MT9mdW5jdGlvbihiLGMsZCl7dmFyIGU9YS5sZW5ndGg7d2hpbGUoZS0tKWlmKCFhW2VdKGIsYyxkKSlyZXR1cm4hMTtyZXR1cm4hMH06YVswXX1mdW5jdGlvbiB0YShhLGIsYyl7Zm9yKHZhciBkPTAsZT1iLmxlbmd0aDtlPmQ7ZCsrKWZhKGEsYltkXSxjKTtyZXR1cm4gY31mdW5jdGlvbiB1YShhLGIsYyxkLGUpe2Zvcih2YXIgZixnPVtdLGg9MCxpPWEubGVuZ3RoLGo9bnVsbCE9YjtpPmg7aCsrKShmPWFbaF0pJiYoIWN8fGMoZixkLGUpKSYmKGcucHVzaChmKSxqJiZiLnB1c2goaCkpO3JldHVybiBnfWZ1bmN0aW9uIHZhKGEsYixjLGQsZSxmKXtyZXR1cm4gZCYmIWRbdV0mJihkPXZhKGQpKSxlJiYhZVt1XSYmKGU9dmEoZSxmKSksaGEoZnVuY3Rpb24oZixnLGgsaSl7dmFyIGosayxsLG09W10sbj1bXSxvPWcubGVuZ3RoLHA9Znx8dGEoYnx8XCIqXCIsaC5ub2RlVHlwZT9baF06aCxbXSkscT0hYXx8IWYmJmI/cDp1YShwLG0sYSxoLGkpLHI9Yz9lfHwoZj9hOm98fGQpP1tdOmc6cTtpZihjJiZjKHEscixoLGkpLGQpe2o9dWEocixuKSxkKGosW10saCxpKSxrPWoubGVuZ3RoO3doaWxlKGstLSkobD1qW2tdKSYmKHJbbltrXV09IShxW25ba11dPWwpKX1pZihmKXtpZihlfHxhKXtpZihlKXtqPVtdLGs9ci5sZW5ndGg7d2hpbGUoay0tKShsPXJba10pJiZqLnB1c2gocVtrXT1sKTtlKG51bGwscj1bXSxqLGkpfWs9ci5sZW5ndGg7d2hpbGUoay0tKShsPXJba10pJiYoaj1lP0ooZixsKTptW2tdKT4tMSYmKGZbal09IShnW2pdPWwpKX19ZWxzZSByPXVhKHI9PT1nP3Iuc3BsaWNlKG8sci5sZW5ndGgpOnIpLGU/ZShudWxsLGcscixpKTpILmFwcGx5KGcscil9KX1mdW5jdGlvbiB3YShhKXtmb3IodmFyIGIsYyxlLGY9YS5sZW5ndGgsZz1kLnJlbGF0aXZlW2FbMF0udHlwZV0saD1nfHxkLnJlbGF0aXZlW1wiIFwiXSxpPWc/MTowLGs9cmEoZnVuY3Rpb24oYSl7cmV0dXJuIGE9PT1ifSxoLCEwKSxsPXJhKGZ1bmN0aW9uKGEpe3JldHVybiBKKGIsYSk+LTF9LGgsITApLG09W2Z1bmN0aW9uKGEsYyxkKXt2YXIgZT0hZyYmKGR8fGMhPT1qKXx8KChiPWMpLm5vZGVUeXBlP2soYSxjLGQpOmwoYSxjLGQpKTtyZXR1cm4gYj1udWxsLGV9XTtmPmk7aSsrKWlmKGM9ZC5yZWxhdGl2ZVthW2ldLnR5cGVdKW09W3JhKHNhKG0pLGMpXTtlbHNle2lmKGM9ZC5maWx0ZXJbYVtpXS50eXBlXS5hcHBseShudWxsLGFbaV0ubWF0Y2hlcyksY1t1XSl7Zm9yKGU9KytpO2Y+ZTtlKyspaWYoZC5yZWxhdGl2ZVthW2VdLnR5cGVdKWJyZWFrO3JldHVybiB2YShpPjEmJnNhKG0pLGk+MSYmcWEoYS5zbGljZSgwLGktMSkuY29uY2F0KHt2YWx1ZTpcIiBcIj09PWFbaS0yXS50eXBlP1wiKlwiOlwiXCJ9KSkucmVwbGFjZShRLFwiJDFcIiksYyxlPmkmJndhKGEuc2xpY2UoaSxlKSksZj5lJiZ3YShhPWEuc2xpY2UoZSkpLGY+ZSYmcWEoYSkpfW0ucHVzaChjKX1yZXR1cm4gc2EobSl9ZnVuY3Rpb24geGEoYSxiKXt2YXIgYz1iLmxlbmd0aD4wLGU9YS5sZW5ndGg+MCxmPWZ1bmN0aW9uKGYsZyxoLGksayl7dmFyIGwsbyxxLHI9MCxzPVwiMFwiLHQ9ZiYmW10sdT1bXSx2PWoseD1mfHxlJiZkLmZpbmQuVEFHKFwiKlwiLGspLHk9dys9bnVsbD09dj8xOk1hdGgucmFuZG9tKCl8fC4xLHo9eC5sZW5ndGg7Zm9yKGsmJihqPWc9PT1ufHxnfHxrKTtzIT09eiYmbnVsbCE9KGw9eFtzXSk7cysrKXtpZihlJiZsKXtvPTAsZ3x8bC5vd25lckRvY3VtZW50PT09bnx8KG0obCksaD0hcCk7d2hpbGUocT1hW28rK10paWYocShsLGd8fG4saCkpe2kucHVzaChsKTticmVha31rJiYodz15KX1jJiYoKGw9IXEmJmwpJiZyLS0sZiYmdC5wdXNoKGwpKX1pZihyKz1zLGMmJnMhPT1yKXtvPTA7d2hpbGUocT1iW28rK10pcSh0LHUsZyxoKTtpZihmKXtpZihyPjApd2hpbGUocy0tKXRbc118fHVbc118fCh1W3NdPUYuY2FsbChpKSk7dT11YSh1KX1ILmFwcGx5KGksdSksayYmIWYmJnUubGVuZ3RoPjAmJnIrYi5sZW5ndGg+MSYmZmEudW5pcXVlU29ydChpKX1yZXR1cm4gayYmKHc9eSxqPXYpLHR9O3JldHVybiBjP2hhKGYpOmZ9cmV0dXJuIGg9ZmEuY29tcGlsZT1mdW5jdGlvbihhLGIpe3ZhciBjLGQ9W10sZT1bXSxmPUFbYStcIiBcIl07aWYoIWYpe2J8fChiPWcoYSkpLGM9Yi5sZW5ndGg7d2hpbGUoYy0tKWY9d2EoYltjXSksZlt1XT9kLnB1c2goZik6ZS5wdXNoKGYpO2Y9QShhLHhhKGUsZCkpLGYuc2VsZWN0b3I9YX1yZXR1cm4gZn0saT1mYS5zZWxlY3Q9ZnVuY3Rpb24oYSxiLGUsZil7dmFyIGksaixrLGwsbSxuPVwiZnVuY3Rpb25cIj09dHlwZW9mIGEmJmEsbz0hZiYmZyhhPW4uc2VsZWN0b3J8fGEpO2lmKGU9ZXx8W10sMT09PW8ubGVuZ3RoKXtpZihqPW9bMF09b1swXS5zbGljZSgwKSxqLmxlbmd0aD4yJiZcIklEXCI9PT0oaz1qWzBdKS50eXBlJiZjLmdldEJ5SWQmJjk9PT1iLm5vZGVUeXBlJiZwJiZkLnJlbGF0aXZlW2pbMV0udHlwZV0pe2lmKGI9KGQuZmluZC5JRChrLm1hdGNoZXNbMF0ucmVwbGFjZShiYSxjYSksYil8fFtdKVswXSwhYilyZXR1cm4gZTtuJiYoYj1iLnBhcmVudE5vZGUpLGE9YS5zbGljZShqLnNoaWZ0KCkudmFsdWUubGVuZ3RoKX1pPVcubmVlZHNDb250ZXh0LnRlc3QoYSk/MDpqLmxlbmd0aDt3aGlsZShpLS0pe2lmKGs9altpXSxkLnJlbGF0aXZlW2w9ay50eXBlXSlicmVhaztpZigobT1kLmZpbmRbbF0pJiYoZj1tKGsubWF0Y2hlc1swXS5yZXBsYWNlKGJhLGNhKSxfLnRlc3QoalswXS50eXBlKSYmb2EoYi5wYXJlbnROb2RlKXx8YikpKXtpZihqLnNwbGljZShpLDEpLGE9Zi5sZW5ndGgmJnFhKGopLCFhKXJldHVybiBILmFwcGx5KGUsZiksZTticmVha319fXJldHVybihufHxoKGEsbykpKGYsYiwhcCxlLCFifHxfLnRlc3QoYSkmJm9hKGIucGFyZW50Tm9kZSl8fGIpLGV9LGMuc29ydFN0YWJsZT11LnNwbGl0KFwiXCIpLnNvcnQoQikuam9pbihcIlwiKT09PXUsYy5kZXRlY3REdXBsaWNhdGVzPSEhbCxtKCksYy5zb3J0RGV0YWNoZWQ9aWEoZnVuY3Rpb24oYSl7cmV0dXJuIDEmYS5jb21wYXJlRG9jdW1lbnRQb3NpdGlvbihuLmNyZWF0ZUVsZW1lbnQoXCJkaXZcIikpfSksaWEoZnVuY3Rpb24oYSl7cmV0dXJuIGEuaW5uZXJIVE1MPVwiPGEgaHJlZj0nIyc+PC9hPlwiLFwiI1wiPT09YS5maXJzdENoaWxkLmdldEF0dHJpYnV0ZShcImhyZWZcIil9KXx8amEoXCJ0eXBlfGhyZWZ8aGVpZ2h0fHdpZHRoXCIsZnVuY3Rpb24oYSxiLGMpe3JldHVybiBjP3ZvaWQgMDphLmdldEF0dHJpYnV0ZShiLFwidHlwZVwiPT09Yi50b0xvd2VyQ2FzZSgpPzE6Mil9KSxjLmF0dHJpYnV0ZXMmJmlhKGZ1bmN0aW9uKGEpe3JldHVybiBhLmlubmVySFRNTD1cIjxpbnB1dC8+XCIsYS5maXJzdENoaWxkLnNldEF0dHJpYnV0ZShcInZhbHVlXCIsXCJcIiksXCJcIj09PWEuZmlyc3RDaGlsZC5nZXRBdHRyaWJ1dGUoXCJ2YWx1ZVwiKX0pfHxqYShcInZhbHVlXCIsZnVuY3Rpb24oYSxiLGMpe3JldHVybiBjfHxcImlucHV0XCIhPT1hLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk/dm9pZCAwOmEuZGVmYXVsdFZhbHVlfSksaWEoZnVuY3Rpb24oYSl7cmV0dXJuIG51bGw9PWEuZ2V0QXR0cmlidXRlKFwiZGlzYWJsZWRcIil9KXx8amEoSyxmdW5jdGlvbihhLGIsYyl7dmFyIGQ7cmV0dXJuIGM/dm9pZCAwOmFbYl09PT0hMD9iLnRvTG93ZXJDYXNlKCk6KGQ9YS5nZXRBdHRyaWJ1dGVOb2RlKGIpKSYmZC5zcGVjaWZpZWQ/ZC52YWx1ZTpudWxsfSksZmF9KGEpO24uZmluZD10LG4uZXhwcj10LnNlbGVjdG9ycyxuLmV4cHJbXCI6XCJdPW4uZXhwci5wc2V1ZG9zLG4udW5pcXVlU29ydD1uLnVuaXF1ZT10LnVuaXF1ZVNvcnQsbi50ZXh0PXQuZ2V0VGV4dCxuLmlzWE1MRG9jPXQuaXNYTUwsbi5jb250YWlucz10LmNvbnRhaW5zO3ZhciB1PWZ1bmN0aW9uKGEsYixjKXt2YXIgZD1bXSxlPXZvaWQgMCE9PWM7d2hpbGUoKGE9YVtiXSkmJjkhPT1hLm5vZGVUeXBlKWlmKDE9PT1hLm5vZGVUeXBlKXtpZihlJiZuKGEpLmlzKGMpKWJyZWFrO2QucHVzaChhKX1yZXR1cm4gZH0sdj1mdW5jdGlvbihhLGIpe2Zvcih2YXIgYz1bXTthO2E9YS5uZXh0U2libGluZykxPT09YS5ub2RlVHlwZSYmYSE9PWImJmMucHVzaChhKTtyZXR1cm4gY30sdz1uLmV4cHIubWF0Y2gubmVlZHNDb250ZXh0LHg9L148KFtcXHctXSspXFxzKlxcLz8+KD86PFxcL1xcMT58KSQvLHk9L14uW146I1xcW1xcLixdKiQvO2Z1bmN0aW9uIHooYSxiLGMpe2lmKG4uaXNGdW5jdGlvbihiKSlyZXR1cm4gbi5ncmVwKGEsZnVuY3Rpb24oYSxkKXtyZXR1cm4hIWIuY2FsbChhLGQsYSkhPT1jfSk7aWYoYi5ub2RlVHlwZSlyZXR1cm4gbi5ncmVwKGEsZnVuY3Rpb24oYSl7cmV0dXJuIGE9PT1iIT09Y30pO2lmKFwic3RyaW5nXCI9PXR5cGVvZiBiKXtpZih5LnRlc3QoYikpcmV0dXJuIG4uZmlsdGVyKGIsYSxjKTtiPW4uZmlsdGVyKGIsYSl9cmV0dXJuIG4uZ3JlcChhLGZ1bmN0aW9uKGEpe3JldHVybiBoLmNhbGwoYixhKT4tMSE9PWN9KX1uLmZpbHRlcj1mdW5jdGlvbihhLGIsYyl7dmFyIGQ9YlswXTtyZXR1cm4gYyYmKGE9XCI6bm90KFwiK2ErXCIpXCIpLDE9PT1iLmxlbmd0aCYmMT09PWQubm9kZVR5cGU/bi5maW5kLm1hdGNoZXNTZWxlY3RvcihkLGEpP1tkXTpbXTpuLmZpbmQubWF0Y2hlcyhhLG4uZ3JlcChiLGZ1bmN0aW9uKGEpe3JldHVybiAxPT09YS5ub2RlVHlwZX0pKX0sbi5mbi5leHRlbmQoe2ZpbmQ6ZnVuY3Rpb24oYSl7dmFyIGIsYz10aGlzLmxlbmd0aCxkPVtdLGU9dGhpcztpZihcInN0cmluZ1wiIT10eXBlb2YgYSlyZXR1cm4gdGhpcy5wdXNoU3RhY2sobihhKS5maWx0ZXIoZnVuY3Rpb24oKXtmb3IoYj0wO2M+YjtiKyspaWYobi5jb250YWlucyhlW2JdLHRoaXMpKXJldHVybiEwfSkpO2ZvcihiPTA7Yz5iO2IrKyluLmZpbmQoYSxlW2JdLGQpO3JldHVybiBkPXRoaXMucHVzaFN0YWNrKGM+MT9uLnVuaXF1ZShkKTpkKSxkLnNlbGVjdG9yPXRoaXMuc2VsZWN0b3I/dGhpcy5zZWxlY3RvcitcIiBcIithOmEsZH0sZmlsdGVyOmZ1bmN0aW9uKGEpe3JldHVybiB0aGlzLnB1c2hTdGFjayh6KHRoaXMsYXx8W10sITEpKX0sbm90OmZ1bmN0aW9uKGEpe3JldHVybiB0aGlzLnB1c2hTdGFjayh6KHRoaXMsYXx8W10sITApKX0saXM6ZnVuY3Rpb24oYSl7cmV0dXJuISF6KHRoaXMsXCJzdHJpbmdcIj09dHlwZW9mIGEmJncudGVzdChhKT9uKGEpOmF8fFtdLCExKS5sZW5ndGh9fSk7dmFyIEEsQj0vXig/OlxccyooPFtcXHdcXFddKz4pW14+XSp8IyhbXFx3LV0qKSkkLyxDPW4uZm4uaW5pdD1mdW5jdGlvbihhLGIsYyl7dmFyIGUsZjtpZighYSlyZXR1cm4gdGhpcztpZihjPWN8fEEsXCJzdHJpbmdcIj09dHlwZW9mIGEpe2lmKGU9XCI8XCI9PT1hWzBdJiZcIj5cIj09PWFbYS5sZW5ndGgtMV0mJmEubGVuZ3RoPj0zP1tudWxsLGEsbnVsbF06Qi5leGVjKGEpLCFlfHwhZVsxXSYmYilyZXR1cm4hYnx8Yi5qcXVlcnk/KGJ8fGMpLmZpbmQoYSk6dGhpcy5jb25zdHJ1Y3RvcihiKS5maW5kKGEpO2lmKGVbMV0pe2lmKGI9YiBpbnN0YW5jZW9mIG4/YlswXTpiLG4ubWVyZ2UodGhpcyxuLnBhcnNlSFRNTChlWzFdLGImJmIubm9kZVR5cGU/Yi5vd25lckRvY3VtZW50fHxiOmQsITApKSx4LnRlc3QoZVsxXSkmJm4uaXNQbGFpbk9iamVjdChiKSlmb3IoZSBpbiBiKW4uaXNGdW5jdGlvbih0aGlzW2VdKT90aGlzW2VdKGJbZV0pOnRoaXMuYXR0cihlLGJbZV0pO3JldHVybiB0aGlzfXJldHVybiBmPWQuZ2V0RWxlbWVudEJ5SWQoZVsyXSksZiYmZi5wYXJlbnROb2RlJiYodGhpcy5sZW5ndGg9MSx0aGlzWzBdPWYpLHRoaXMuY29udGV4dD1kLHRoaXMuc2VsZWN0b3I9YSx0aGlzfXJldHVybiBhLm5vZGVUeXBlPyh0aGlzLmNvbnRleHQ9dGhpc1swXT1hLHRoaXMubGVuZ3RoPTEsdGhpcyk6bi5pc0Z1bmN0aW9uKGEpP3ZvaWQgMCE9PWMucmVhZHk/Yy5yZWFkeShhKTphKG4pOih2b2lkIDAhPT1hLnNlbGVjdG9yJiYodGhpcy5zZWxlY3Rvcj1hLnNlbGVjdG9yLHRoaXMuY29udGV4dD1hLmNvbnRleHQpLG4ubWFrZUFycmF5KGEsdGhpcykpfTtDLnByb3RvdHlwZT1uLmZuLEE9bihkKTt2YXIgRD0vXig/OnBhcmVudHN8cHJldig/OlVudGlsfEFsbCkpLyxFPXtjaGlsZHJlbjohMCxjb250ZW50czohMCxuZXh0OiEwLHByZXY6ITB9O24uZm4uZXh0ZW5kKHtoYXM6ZnVuY3Rpb24oYSl7dmFyIGI9bihhLHRoaXMpLGM9Yi5sZW5ndGg7cmV0dXJuIHRoaXMuZmlsdGVyKGZ1bmN0aW9uKCl7Zm9yKHZhciBhPTA7Yz5hO2ErKylpZihuLmNvbnRhaW5zKHRoaXMsYlthXSkpcmV0dXJuITB9KX0sY2xvc2VzdDpmdW5jdGlvbihhLGIpe2Zvcih2YXIgYyxkPTAsZT10aGlzLmxlbmd0aCxmPVtdLGc9dy50ZXN0KGEpfHxcInN0cmluZ1wiIT10eXBlb2YgYT9uKGEsYnx8dGhpcy5jb250ZXh0KTowO2U+ZDtkKyspZm9yKGM9dGhpc1tkXTtjJiZjIT09YjtjPWMucGFyZW50Tm9kZSlpZihjLm5vZGVUeXBlPDExJiYoZz9nLmluZGV4KGMpPi0xOjE9PT1jLm5vZGVUeXBlJiZuLmZpbmQubWF0Y2hlc1NlbGVjdG9yKGMsYSkpKXtmLnB1c2goYyk7YnJlYWt9cmV0dXJuIHRoaXMucHVzaFN0YWNrKGYubGVuZ3RoPjE/bi51bmlxdWVTb3J0KGYpOmYpfSxpbmRleDpmdW5jdGlvbihhKXtyZXR1cm4gYT9cInN0cmluZ1wiPT10eXBlb2YgYT9oLmNhbGwobihhKSx0aGlzWzBdKTpoLmNhbGwodGhpcyxhLmpxdWVyeT9hWzBdOmEpOnRoaXNbMF0mJnRoaXNbMF0ucGFyZW50Tm9kZT90aGlzLmZpcnN0KCkucHJldkFsbCgpLmxlbmd0aDotMX0sYWRkOmZ1bmN0aW9uKGEsYil7cmV0dXJuIHRoaXMucHVzaFN0YWNrKG4udW5pcXVlU29ydChuLm1lcmdlKHRoaXMuZ2V0KCksbihhLGIpKSkpfSxhZGRCYWNrOmZ1bmN0aW9uKGEpe3JldHVybiB0aGlzLmFkZChudWxsPT1hP3RoaXMucHJldk9iamVjdDp0aGlzLnByZXZPYmplY3QuZmlsdGVyKGEpKX19KTtmdW5jdGlvbiBGKGEsYil7d2hpbGUoKGE9YVtiXSkmJjEhPT1hLm5vZGVUeXBlKTtyZXR1cm4gYX1uLmVhY2goe3BhcmVudDpmdW5jdGlvbihhKXt2YXIgYj1hLnBhcmVudE5vZGU7cmV0dXJuIGImJjExIT09Yi5ub2RlVHlwZT9iOm51bGx9LHBhcmVudHM6ZnVuY3Rpb24oYSl7cmV0dXJuIHUoYSxcInBhcmVudE5vZGVcIil9LHBhcmVudHNVbnRpbDpmdW5jdGlvbihhLGIsYyl7cmV0dXJuIHUoYSxcInBhcmVudE5vZGVcIixjKX0sbmV4dDpmdW5jdGlvbihhKXtyZXR1cm4gRihhLFwibmV4dFNpYmxpbmdcIil9LHByZXY6ZnVuY3Rpb24oYSl7cmV0dXJuIEYoYSxcInByZXZpb3VzU2libGluZ1wiKX0sbmV4dEFsbDpmdW5jdGlvbihhKXtyZXR1cm4gdShhLFwibmV4dFNpYmxpbmdcIil9LHByZXZBbGw6ZnVuY3Rpb24oYSl7cmV0dXJuIHUoYSxcInByZXZpb3VzU2libGluZ1wiKX0sbmV4dFVudGlsOmZ1bmN0aW9uKGEsYixjKXtyZXR1cm4gdShhLFwibmV4dFNpYmxpbmdcIixjKX0scHJldlVudGlsOmZ1bmN0aW9uKGEsYixjKXtyZXR1cm4gdShhLFwicHJldmlvdXNTaWJsaW5nXCIsYyl9LHNpYmxpbmdzOmZ1bmN0aW9uKGEpe3JldHVybiB2KChhLnBhcmVudE5vZGV8fHt9KS5maXJzdENoaWxkLGEpfSxjaGlsZHJlbjpmdW5jdGlvbihhKXtyZXR1cm4gdihhLmZpcnN0Q2hpbGQpfSxjb250ZW50czpmdW5jdGlvbihhKXtyZXR1cm4gYS5jb250ZW50RG9jdW1lbnR8fG4ubWVyZ2UoW10sYS5jaGlsZE5vZGVzKX19LGZ1bmN0aW9uKGEsYil7bi5mblthXT1mdW5jdGlvbihjLGQpe3ZhciBlPW4ubWFwKHRoaXMsYixjKTtyZXR1cm5cIlVudGlsXCIhPT1hLnNsaWNlKC01KSYmKGQ9YyksZCYmXCJzdHJpbmdcIj09dHlwZW9mIGQmJihlPW4uZmlsdGVyKGQsZSkpLHRoaXMubGVuZ3RoPjEmJihFW2FdfHxuLnVuaXF1ZVNvcnQoZSksRC50ZXN0KGEpJiZlLnJldmVyc2UoKSksdGhpcy5wdXNoU3RhY2soZSl9fSk7dmFyIEc9L1xcUysvZztmdW5jdGlvbiBIKGEpe3ZhciBiPXt9O3JldHVybiBuLmVhY2goYS5tYXRjaChHKXx8W10sZnVuY3Rpb24oYSxjKXtiW2NdPSEwfSksYn1uLkNhbGxiYWNrcz1mdW5jdGlvbihhKXthPVwic3RyaW5nXCI9PXR5cGVvZiBhP0goYSk6bi5leHRlbmQoe30sYSk7dmFyIGIsYyxkLGUsZj1bXSxnPVtdLGg9LTEsaT1mdW5jdGlvbigpe2ZvcihlPWEub25jZSxkPWI9ITA7Zy5sZW5ndGg7aD0tMSl7Yz1nLnNoaWZ0KCk7d2hpbGUoKytoPGYubGVuZ3RoKWZbaF0uYXBwbHkoY1swXSxjWzFdKT09PSExJiZhLnN0b3BPbkZhbHNlJiYoaD1mLmxlbmd0aCxjPSExKX1hLm1lbW9yeXx8KGM9ITEpLGI9ITEsZSYmKGY9Yz9bXTpcIlwiKX0saj17YWRkOmZ1bmN0aW9uKCl7cmV0dXJuIGYmJihjJiYhYiYmKGg9Zi5sZW5ndGgtMSxnLnB1c2goYykpLGZ1bmN0aW9uIGQoYil7bi5lYWNoKGIsZnVuY3Rpb24oYixjKXtuLmlzRnVuY3Rpb24oYyk/YS51bmlxdWUmJmouaGFzKGMpfHxmLnB1c2goYyk6YyYmYy5sZW5ndGgmJlwic3RyaW5nXCIhPT1uLnR5cGUoYykmJmQoYyl9KX0oYXJndW1lbnRzKSxjJiYhYiYmaSgpKSx0aGlzfSxyZW1vdmU6ZnVuY3Rpb24oKXtyZXR1cm4gbi5lYWNoKGFyZ3VtZW50cyxmdW5jdGlvbihhLGIpe3ZhciBjO3doaWxlKChjPW4uaW5BcnJheShiLGYsYykpPi0xKWYuc3BsaWNlKGMsMSksaD49YyYmaC0tfSksdGhpc30saGFzOmZ1bmN0aW9uKGEpe3JldHVybiBhP24uaW5BcnJheShhLGYpPi0xOmYubGVuZ3RoPjB9LGVtcHR5OmZ1bmN0aW9uKCl7cmV0dXJuIGYmJihmPVtdKSx0aGlzfSxkaXNhYmxlOmZ1bmN0aW9uKCl7cmV0dXJuIGU9Zz1bXSxmPWM9XCJcIix0aGlzfSxkaXNhYmxlZDpmdW5jdGlvbigpe3JldHVybiFmfSxsb2NrOmZ1bmN0aW9uKCl7cmV0dXJuIGU9Zz1bXSxjfHwoZj1jPVwiXCIpLHRoaXN9LGxvY2tlZDpmdW5jdGlvbigpe3JldHVybiEhZX0sZmlyZVdpdGg6ZnVuY3Rpb24oYSxjKXtyZXR1cm4gZXx8KGM9Y3x8W10sYz1bYSxjLnNsaWNlP2Muc2xpY2UoKTpjXSxnLnB1c2goYyksYnx8aSgpKSx0aGlzfSxmaXJlOmZ1bmN0aW9uKCl7cmV0dXJuIGouZmlyZVdpdGgodGhpcyxhcmd1bWVudHMpLHRoaXN9LGZpcmVkOmZ1bmN0aW9uKCl7cmV0dXJuISFkfX07cmV0dXJuIGp9LG4uZXh0ZW5kKHtEZWZlcnJlZDpmdW5jdGlvbihhKXt2YXIgYj1bW1wicmVzb2x2ZVwiLFwiZG9uZVwiLG4uQ2FsbGJhY2tzKFwib25jZSBtZW1vcnlcIiksXCJyZXNvbHZlZFwiXSxbXCJyZWplY3RcIixcImZhaWxcIixuLkNhbGxiYWNrcyhcIm9uY2UgbWVtb3J5XCIpLFwicmVqZWN0ZWRcIl0sW1wibm90aWZ5XCIsXCJwcm9ncmVzc1wiLG4uQ2FsbGJhY2tzKFwibWVtb3J5XCIpXV0sYz1cInBlbmRpbmdcIixkPXtzdGF0ZTpmdW5jdGlvbigpe3JldHVybiBjfSxhbHdheXM6ZnVuY3Rpb24oKXtyZXR1cm4gZS5kb25lKGFyZ3VtZW50cykuZmFpbChhcmd1bWVudHMpLHRoaXN9LHRoZW46ZnVuY3Rpb24oKXt2YXIgYT1hcmd1bWVudHM7cmV0dXJuIG4uRGVmZXJyZWQoZnVuY3Rpb24oYyl7bi5lYWNoKGIsZnVuY3Rpb24oYixmKXt2YXIgZz1uLmlzRnVuY3Rpb24oYVtiXSkmJmFbYl07ZVtmWzFdXShmdW5jdGlvbigpe3ZhciBhPWcmJmcuYXBwbHkodGhpcyxhcmd1bWVudHMpO2EmJm4uaXNGdW5jdGlvbihhLnByb21pc2UpP2EucHJvbWlzZSgpLnByb2dyZXNzKGMubm90aWZ5KS5kb25lKGMucmVzb2x2ZSkuZmFpbChjLnJlamVjdCk6Y1tmWzBdK1wiV2l0aFwiXSh0aGlzPT09ZD9jLnByb21pc2UoKTp0aGlzLGc/W2FdOmFyZ3VtZW50cyl9KX0pLGE9bnVsbH0pLnByb21pc2UoKX0scHJvbWlzZTpmdW5jdGlvbihhKXtyZXR1cm4gbnVsbCE9YT9uLmV4dGVuZChhLGQpOmR9fSxlPXt9O3JldHVybiBkLnBpcGU9ZC50aGVuLG4uZWFjaChiLGZ1bmN0aW9uKGEsZil7dmFyIGc9ZlsyXSxoPWZbM107ZFtmWzFdXT1nLmFkZCxoJiZnLmFkZChmdW5jdGlvbigpe2M9aH0sYlsxXmFdWzJdLmRpc2FibGUsYlsyXVsyXS5sb2NrKSxlW2ZbMF1dPWZ1bmN0aW9uKCl7cmV0dXJuIGVbZlswXStcIldpdGhcIl0odGhpcz09PWU/ZDp0aGlzLGFyZ3VtZW50cyksdGhpc30sZVtmWzBdK1wiV2l0aFwiXT1nLmZpcmVXaXRofSksZC5wcm9taXNlKGUpLGEmJmEuY2FsbChlLGUpLGV9LHdoZW46ZnVuY3Rpb24oYSl7dmFyIGI9MCxjPWUuY2FsbChhcmd1bWVudHMpLGQ9Yy5sZW5ndGgsZj0xIT09ZHx8YSYmbi5pc0Z1bmN0aW9uKGEucHJvbWlzZSk/ZDowLGc9MT09PWY/YTpuLkRlZmVycmVkKCksaD1mdW5jdGlvbihhLGIsYyl7cmV0dXJuIGZ1bmN0aW9uKGQpe2JbYV09dGhpcyxjW2FdPWFyZ3VtZW50cy5sZW5ndGg+MT9lLmNhbGwoYXJndW1lbnRzKTpkLGM9PT1pP2cubm90aWZ5V2l0aChiLGMpOi0tZnx8Zy5yZXNvbHZlV2l0aChiLGMpfX0saSxqLGs7aWYoZD4xKWZvcihpPW5ldyBBcnJheShkKSxqPW5ldyBBcnJheShkKSxrPW5ldyBBcnJheShkKTtkPmI7YisrKWNbYl0mJm4uaXNGdW5jdGlvbihjW2JdLnByb21pc2UpP2NbYl0ucHJvbWlzZSgpLnByb2dyZXNzKGgoYixqLGkpKS5kb25lKGgoYixrLGMpKS5mYWlsKGcucmVqZWN0KTotLWY7cmV0dXJuIGZ8fGcucmVzb2x2ZVdpdGgoayxjKSxnLnByb21pc2UoKX19KTt2YXIgSTtuLmZuLnJlYWR5PWZ1bmN0aW9uKGEpe3JldHVybiBuLnJlYWR5LnByb21pc2UoKS5kb25lKGEpLHRoaXN9LG4uZXh0ZW5kKHtpc1JlYWR5OiExLHJlYWR5V2FpdDoxLGhvbGRSZWFkeTpmdW5jdGlvbihhKXthP24ucmVhZHlXYWl0Kys6bi5yZWFkeSghMCl9LHJlYWR5OmZ1bmN0aW9uKGEpeyhhPT09ITA/LS1uLnJlYWR5V2FpdDpuLmlzUmVhZHkpfHwobi5pc1JlYWR5PSEwLGEhPT0hMCYmLS1uLnJlYWR5V2FpdD4wfHwoSS5yZXNvbHZlV2l0aChkLFtuXSksbi5mbi50cmlnZ2VySGFuZGxlciYmKG4oZCkudHJpZ2dlckhhbmRsZXIoXCJyZWFkeVwiKSxuKGQpLm9mZihcInJlYWR5XCIpKSkpfX0pO2Z1bmN0aW9uIEooKXtkLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsSiksYS5yZW1vdmVFdmVudExpc3RlbmVyKFwibG9hZFwiLEopLG4ucmVhZHkoKX1uLnJlYWR5LnByb21pc2U9ZnVuY3Rpb24oYil7cmV0dXJuIEl8fChJPW4uRGVmZXJyZWQoKSxcImNvbXBsZXRlXCI9PT1kLnJlYWR5U3RhdGV8fFwibG9hZGluZ1wiIT09ZC5yZWFkeVN0YXRlJiYhZC5kb2N1bWVudEVsZW1lbnQuZG9TY3JvbGw/YS5zZXRUaW1lb3V0KG4ucmVhZHkpOihkLmFkZEV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsSiksYS5hZGRFdmVudExpc3RlbmVyKFwibG9hZFwiLEopKSksSS5wcm9taXNlKGIpfSxuLnJlYWR5LnByb21pc2UoKTt2YXIgSz1mdW5jdGlvbihhLGIsYyxkLGUsZixnKXt2YXIgaD0wLGk9YS5sZW5ndGgsaj1udWxsPT1jO2lmKFwib2JqZWN0XCI9PT1uLnR5cGUoYykpe2U9ITA7Zm9yKGggaW4gYylLKGEsYixoLGNbaF0sITAsZixnKX1lbHNlIGlmKHZvaWQgMCE9PWQmJihlPSEwLG4uaXNGdW5jdGlvbihkKXx8KGc9ITApLGomJihnPyhiLmNhbGwoYSxkKSxiPW51bGwpOihqPWIsYj1mdW5jdGlvbihhLGIsYyl7cmV0dXJuIGouY2FsbChuKGEpLGMpfSkpLGIpKWZvcig7aT5oO2grKyliKGFbaF0sYyxnP2Q6ZC5jYWxsKGFbaF0saCxiKGFbaF0sYykpKTtyZXR1cm4gZT9hOmo/Yi5jYWxsKGEpOmk/YihhWzBdLGMpOmZ9LEw9ZnVuY3Rpb24oYSl7cmV0dXJuIDE9PT1hLm5vZGVUeXBlfHw5PT09YS5ub2RlVHlwZXx8ISthLm5vZGVUeXBlfTtmdW5jdGlvbiBNKCl7dGhpcy5leHBhbmRvPW4uZXhwYW5kbytNLnVpZCsrfU0udWlkPTEsTS5wcm90b3R5cGU9e3JlZ2lzdGVyOmZ1bmN0aW9uKGEsYil7dmFyIGM9Ynx8e307cmV0dXJuIGEubm9kZVR5cGU/YVt0aGlzLmV4cGFuZG9dPWM6T2JqZWN0LmRlZmluZVByb3BlcnR5KGEsdGhpcy5leHBhbmRvLHt2YWx1ZTpjLHdyaXRhYmxlOiEwLGNvbmZpZ3VyYWJsZTohMH0pLGFbdGhpcy5leHBhbmRvXX0sY2FjaGU6ZnVuY3Rpb24oYSl7aWYoIUwoYSkpcmV0dXJue307dmFyIGI9YVt0aGlzLmV4cGFuZG9dO3JldHVybiBifHwoYj17fSxMKGEpJiYoYS5ub2RlVHlwZT9hW3RoaXMuZXhwYW5kb109YjpPYmplY3QuZGVmaW5lUHJvcGVydHkoYSx0aGlzLmV4cGFuZG8se3ZhbHVlOmIsY29uZmlndXJhYmxlOiEwfSkpKSxifSxzZXQ6ZnVuY3Rpb24oYSxiLGMpe3ZhciBkLGU9dGhpcy5jYWNoZShhKTtpZihcInN0cmluZ1wiPT10eXBlb2YgYillW2JdPWM7ZWxzZSBmb3IoZCBpbiBiKWVbZF09YltkXTtyZXR1cm4gZX0sZ2V0OmZ1bmN0aW9uKGEsYil7cmV0dXJuIHZvaWQgMD09PWI/dGhpcy5jYWNoZShhKTphW3RoaXMuZXhwYW5kb10mJmFbdGhpcy5leHBhbmRvXVtiXX0sYWNjZXNzOmZ1bmN0aW9uKGEsYixjKXt2YXIgZDtyZXR1cm4gdm9pZCAwPT09Ynx8YiYmXCJzdHJpbmdcIj09dHlwZW9mIGImJnZvaWQgMD09PWM/KGQ9dGhpcy5nZXQoYSxiKSx2b2lkIDAhPT1kP2Q6dGhpcy5nZXQoYSxuLmNhbWVsQ2FzZShiKSkpOih0aGlzLnNldChhLGIsYyksdm9pZCAwIT09Yz9jOmIpfSxyZW1vdmU6ZnVuY3Rpb24oYSxiKXt2YXIgYyxkLGUsZj1hW3RoaXMuZXhwYW5kb107aWYodm9pZCAwIT09Zil7aWYodm9pZCAwPT09Yil0aGlzLnJlZ2lzdGVyKGEpO2Vsc2V7bi5pc0FycmF5KGIpP2Q9Yi5jb25jYXQoYi5tYXAobi5jYW1lbENhc2UpKTooZT1uLmNhbWVsQ2FzZShiKSxiIGluIGY/ZD1bYixlXTooZD1lLGQ9ZCBpbiBmP1tkXTpkLm1hdGNoKEcpfHxbXSkpLGM9ZC5sZW5ndGg7d2hpbGUoYy0tKWRlbGV0ZSBmW2RbY11dfSh2b2lkIDA9PT1ifHxuLmlzRW1wdHlPYmplY3QoZikpJiYoYS5ub2RlVHlwZT9hW3RoaXMuZXhwYW5kb109dm9pZCAwOmRlbGV0ZSBhW3RoaXMuZXhwYW5kb10pfX0saGFzRGF0YTpmdW5jdGlvbihhKXt2YXIgYj1hW3RoaXMuZXhwYW5kb107cmV0dXJuIHZvaWQgMCE9PWImJiFuLmlzRW1wdHlPYmplY3QoYil9fTt2YXIgTj1uZXcgTSxPPW5ldyBNLFA9L14oPzpcXHtbXFx3XFxXXSpcXH18XFxbW1xcd1xcV10qXFxdKSQvLFE9L1tBLVpdL2c7ZnVuY3Rpb24gUihhLGIsYyl7dmFyIGQ7aWYodm9pZCAwPT09YyYmMT09PWEubm9kZVR5cGUpaWYoZD1cImRhdGEtXCIrYi5yZXBsYWNlKFEsXCItJCZcIikudG9Mb3dlckNhc2UoKSxjPWEuZ2V0QXR0cmlidXRlKGQpLFwic3RyaW5nXCI9PXR5cGVvZiBjKXt0cnl7Yz1cInRydWVcIj09PWM/ITA6XCJmYWxzZVwiPT09Yz8hMTpcIm51bGxcIj09PWM/bnVsbDorYytcIlwiPT09Yz8rYzpQLnRlc3QoYyk/bi5wYXJzZUpTT04oYyk6Y31jYXRjaChlKXt9Ty5zZXQoYSxiLGMpO1xyXG59ZWxzZSBjPXZvaWQgMDtyZXR1cm4gY31uLmV4dGVuZCh7aGFzRGF0YTpmdW5jdGlvbihhKXtyZXR1cm4gTy5oYXNEYXRhKGEpfHxOLmhhc0RhdGEoYSl9LGRhdGE6ZnVuY3Rpb24oYSxiLGMpe3JldHVybiBPLmFjY2VzcyhhLGIsYyl9LHJlbW92ZURhdGE6ZnVuY3Rpb24oYSxiKXtPLnJlbW92ZShhLGIpfSxfZGF0YTpmdW5jdGlvbihhLGIsYyl7cmV0dXJuIE4uYWNjZXNzKGEsYixjKX0sX3JlbW92ZURhdGE6ZnVuY3Rpb24oYSxiKXtOLnJlbW92ZShhLGIpfX0pLG4uZm4uZXh0ZW5kKHtkYXRhOmZ1bmN0aW9uKGEsYil7dmFyIGMsZCxlLGY9dGhpc1swXSxnPWYmJmYuYXR0cmlidXRlcztpZih2b2lkIDA9PT1hKXtpZih0aGlzLmxlbmd0aCYmKGU9Ty5nZXQoZiksMT09PWYubm9kZVR5cGUmJiFOLmdldChmLFwiaGFzRGF0YUF0dHJzXCIpKSl7Yz1nLmxlbmd0aDt3aGlsZShjLS0pZ1tjXSYmKGQ9Z1tjXS5uYW1lLDA9PT1kLmluZGV4T2YoXCJkYXRhLVwiKSYmKGQ9bi5jYW1lbENhc2UoZC5zbGljZSg1KSksUihmLGQsZVtkXSkpKTtOLnNldChmLFwiaGFzRGF0YUF0dHJzXCIsITApfXJldHVybiBlfXJldHVyblwib2JqZWN0XCI9PXR5cGVvZiBhP3RoaXMuZWFjaChmdW5jdGlvbigpe08uc2V0KHRoaXMsYSl9KTpLKHRoaXMsZnVuY3Rpb24oYil7dmFyIGMsZDtpZihmJiZ2b2lkIDA9PT1iKXtpZihjPU8uZ2V0KGYsYSl8fE8uZ2V0KGYsYS5yZXBsYWNlKFEsXCItJCZcIikudG9Mb3dlckNhc2UoKSksdm9pZCAwIT09YylyZXR1cm4gYztpZihkPW4uY2FtZWxDYXNlKGEpLGM9Ty5nZXQoZixkKSx2b2lkIDAhPT1jKXJldHVybiBjO2lmKGM9UihmLGQsdm9pZCAwKSx2b2lkIDAhPT1jKXJldHVybiBjfWVsc2UgZD1uLmNhbWVsQ2FzZShhKSx0aGlzLmVhY2goZnVuY3Rpb24oKXt2YXIgYz1PLmdldCh0aGlzLGQpO08uc2V0KHRoaXMsZCxiKSxhLmluZGV4T2YoXCItXCIpPi0xJiZ2b2lkIDAhPT1jJiZPLnNldCh0aGlzLGEsYil9KX0sbnVsbCxiLGFyZ3VtZW50cy5sZW5ndGg+MSxudWxsLCEwKX0scmVtb3ZlRGF0YTpmdW5jdGlvbihhKXtyZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uKCl7Ty5yZW1vdmUodGhpcyxhKX0pfX0pLG4uZXh0ZW5kKHtxdWV1ZTpmdW5jdGlvbihhLGIsYyl7dmFyIGQ7cmV0dXJuIGE/KGI9KGJ8fFwiZnhcIikrXCJxdWV1ZVwiLGQ9Ti5nZXQoYSxiKSxjJiYoIWR8fG4uaXNBcnJheShjKT9kPU4uYWNjZXNzKGEsYixuLm1ha2VBcnJheShjKSk6ZC5wdXNoKGMpKSxkfHxbXSk6dm9pZCAwfSxkZXF1ZXVlOmZ1bmN0aW9uKGEsYil7Yj1ifHxcImZ4XCI7dmFyIGM9bi5xdWV1ZShhLGIpLGQ9Yy5sZW5ndGgsZT1jLnNoaWZ0KCksZj1uLl9xdWV1ZUhvb2tzKGEsYiksZz1mdW5jdGlvbigpe24uZGVxdWV1ZShhLGIpfTtcImlucHJvZ3Jlc3NcIj09PWUmJihlPWMuc2hpZnQoKSxkLS0pLGUmJihcImZ4XCI9PT1iJiZjLnVuc2hpZnQoXCJpbnByb2dyZXNzXCIpLGRlbGV0ZSBmLnN0b3AsZS5jYWxsKGEsZyxmKSksIWQmJmYmJmYuZW1wdHkuZmlyZSgpfSxfcXVldWVIb29rczpmdW5jdGlvbihhLGIpe3ZhciBjPWIrXCJxdWV1ZUhvb2tzXCI7cmV0dXJuIE4uZ2V0KGEsYyl8fE4uYWNjZXNzKGEsYyx7ZW1wdHk6bi5DYWxsYmFja3MoXCJvbmNlIG1lbW9yeVwiKS5hZGQoZnVuY3Rpb24oKXtOLnJlbW92ZShhLFtiK1wicXVldWVcIixjXSl9KX0pfX0pLG4uZm4uZXh0ZW5kKHtxdWV1ZTpmdW5jdGlvbihhLGIpe3ZhciBjPTI7cmV0dXJuXCJzdHJpbmdcIiE9dHlwZW9mIGEmJihiPWEsYT1cImZ4XCIsYy0tKSxhcmd1bWVudHMubGVuZ3RoPGM/bi5xdWV1ZSh0aGlzWzBdLGEpOnZvaWQgMD09PWI/dGhpczp0aGlzLmVhY2goZnVuY3Rpb24oKXt2YXIgYz1uLnF1ZXVlKHRoaXMsYSxiKTtuLl9xdWV1ZUhvb2tzKHRoaXMsYSksXCJmeFwiPT09YSYmXCJpbnByb2dyZXNzXCIhPT1jWzBdJiZuLmRlcXVldWUodGhpcyxhKX0pfSxkZXF1ZXVlOmZ1bmN0aW9uKGEpe3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXtuLmRlcXVldWUodGhpcyxhKX0pfSxjbGVhclF1ZXVlOmZ1bmN0aW9uKGEpe3JldHVybiB0aGlzLnF1ZXVlKGF8fFwiZnhcIixbXSl9LHByb21pc2U6ZnVuY3Rpb24oYSxiKXt2YXIgYyxkPTEsZT1uLkRlZmVycmVkKCksZj10aGlzLGc9dGhpcy5sZW5ndGgsaD1mdW5jdGlvbigpey0tZHx8ZS5yZXNvbHZlV2l0aChmLFtmXSl9O1wic3RyaW5nXCIhPXR5cGVvZiBhJiYoYj1hLGE9dm9pZCAwKSxhPWF8fFwiZnhcIjt3aGlsZShnLS0pYz1OLmdldChmW2ddLGErXCJxdWV1ZUhvb2tzXCIpLGMmJmMuZW1wdHkmJihkKyssYy5lbXB0eS5hZGQoaCkpO3JldHVybiBoKCksZS5wcm9taXNlKGIpfX0pO3ZhciBTPS9bKy1dPyg/OlxcZCpcXC58KVxcZCsoPzpbZUVdWystXT9cXGQrfCkvLnNvdXJjZSxUPW5ldyBSZWdFeHAoXCJeKD86KFsrLV0pPXwpKFwiK1MrXCIpKFthLXolXSopJFwiLFwiaVwiKSxVPVtcIlRvcFwiLFwiUmlnaHRcIixcIkJvdHRvbVwiLFwiTGVmdFwiXSxWPWZ1bmN0aW9uKGEsYil7cmV0dXJuIGE9Ynx8YSxcIm5vbmVcIj09PW4uY3NzKGEsXCJkaXNwbGF5XCIpfHwhbi5jb250YWlucyhhLm93bmVyRG9jdW1lbnQsYSl9O2Z1bmN0aW9uIFcoYSxiLGMsZCl7dmFyIGUsZj0xLGc9MjAsaD1kP2Z1bmN0aW9uKCl7cmV0dXJuIGQuY3VyKCl9OmZ1bmN0aW9uKCl7cmV0dXJuIG4uY3NzKGEsYixcIlwiKX0saT1oKCksaj1jJiZjWzNdfHwobi5jc3NOdW1iZXJbYl0/XCJcIjpcInB4XCIpLGs9KG4uY3NzTnVtYmVyW2JdfHxcInB4XCIhPT1qJiYraSkmJlQuZXhlYyhuLmNzcyhhLGIpKTtpZihrJiZrWzNdIT09ail7aj1qfHxrWzNdLGM9Y3x8W10saz0raXx8MTtkbyBmPWZ8fFwiLjVcIixrLz1mLG4uc3R5bGUoYSxiLGsraik7d2hpbGUoZiE9PShmPWgoKS9pKSYmMSE9PWYmJi0tZyl9cmV0dXJuIGMmJihrPStrfHwraXx8MCxlPWNbMV0/aysoY1sxXSsxKSpjWzJdOitjWzJdLGQmJihkLnVuaXQ9aixkLnN0YXJ0PWssZC5lbmQ9ZSkpLGV9dmFyIFg9L14oPzpjaGVja2JveHxyYWRpbykkL2ksWT0vPChbXFx3Oi1dKykvLFo9L14kfFxcLyg/OmphdmF8ZWNtYSlzY3JpcHQvaSwkPXtvcHRpb246WzEsXCI8c2VsZWN0IG11bHRpcGxlPSdtdWx0aXBsZSc+XCIsXCI8L3NlbGVjdD5cIl0sdGhlYWQ6WzEsXCI8dGFibGU+XCIsXCI8L3RhYmxlPlwiXSxjb2w6WzIsXCI8dGFibGU+PGNvbGdyb3VwPlwiLFwiPC9jb2xncm91cD48L3RhYmxlPlwiXSx0cjpbMixcIjx0YWJsZT48dGJvZHk+XCIsXCI8L3Rib2R5PjwvdGFibGU+XCJdLHRkOlszLFwiPHRhYmxlPjx0Ym9keT48dHI+XCIsXCI8L3RyPjwvdGJvZHk+PC90YWJsZT5cIl0sX2RlZmF1bHQ6WzAsXCJcIixcIlwiXX07JC5vcHRncm91cD0kLm9wdGlvbiwkLnRib2R5PSQudGZvb3Q9JC5jb2xncm91cD0kLmNhcHRpb249JC50aGVhZCwkLnRoPSQudGQ7ZnVuY3Rpb24gXyhhLGIpe3ZhciBjPVwidW5kZWZpbmVkXCIhPXR5cGVvZiBhLmdldEVsZW1lbnRzQnlUYWdOYW1lP2EuZ2V0RWxlbWVudHNCeVRhZ05hbWUoYnx8XCIqXCIpOlwidW5kZWZpbmVkXCIhPXR5cGVvZiBhLnF1ZXJ5U2VsZWN0b3JBbGw/YS5xdWVyeVNlbGVjdG9yQWxsKGJ8fFwiKlwiKTpbXTtyZXR1cm4gdm9pZCAwPT09Ynx8YiYmbi5ub2RlTmFtZShhLGIpP24ubWVyZ2UoW2FdLGMpOmN9ZnVuY3Rpb24gYWEoYSxiKXtmb3IodmFyIGM9MCxkPWEubGVuZ3RoO2Q+YztjKyspTi5zZXQoYVtjXSxcImdsb2JhbEV2YWxcIiwhYnx8Ti5nZXQoYltjXSxcImdsb2JhbEV2YWxcIikpfXZhciBiYT0vPHwmIz9cXHcrOy87ZnVuY3Rpb24gY2EoYSxiLGMsZCxlKXtmb3IodmFyIGYsZyxoLGksaixrLGw9Yi5jcmVhdGVEb2N1bWVudEZyYWdtZW50KCksbT1bXSxvPTAscD1hLmxlbmd0aDtwPm87bysrKWlmKGY9YVtvXSxmfHwwPT09ZilpZihcIm9iamVjdFwiPT09bi50eXBlKGYpKW4ubWVyZ2UobSxmLm5vZGVUeXBlP1tmXTpmKTtlbHNlIGlmKGJhLnRlc3QoZikpe2c9Z3x8bC5hcHBlbmRDaGlsZChiLmNyZWF0ZUVsZW1lbnQoXCJkaXZcIikpLGg9KFkuZXhlYyhmKXx8W1wiXCIsXCJcIl0pWzFdLnRvTG93ZXJDYXNlKCksaT0kW2hdfHwkLl9kZWZhdWx0LGcuaW5uZXJIVE1MPWlbMV0rbi5odG1sUHJlZmlsdGVyKGYpK2lbMl0saz1pWzBdO3doaWxlKGstLSlnPWcubGFzdENoaWxkO24ubWVyZ2UobSxnLmNoaWxkTm9kZXMpLGc9bC5maXJzdENoaWxkLGcudGV4dENvbnRlbnQ9XCJcIn1lbHNlIG0ucHVzaChiLmNyZWF0ZVRleHROb2RlKGYpKTtsLnRleHRDb250ZW50PVwiXCIsbz0wO3doaWxlKGY9bVtvKytdKWlmKGQmJm4uaW5BcnJheShmLGQpPi0xKWUmJmUucHVzaChmKTtlbHNlIGlmKGo9bi5jb250YWlucyhmLm93bmVyRG9jdW1lbnQsZiksZz1fKGwuYXBwZW5kQ2hpbGQoZiksXCJzY3JpcHRcIiksaiYmYWEoZyksYyl7az0wO3doaWxlKGY9Z1trKytdKVoudGVzdChmLnR5cGV8fFwiXCIpJiZjLnB1c2goZil9cmV0dXJuIGx9IWZ1bmN0aW9uKCl7dmFyIGE9ZC5jcmVhdGVEb2N1bWVudEZyYWdtZW50KCksYj1hLmFwcGVuZENoaWxkKGQuY3JlYXRlRWxlbWVudChcImRpdlwiKSksYz1kLmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiKTtjLnNldEF0dHJpYnV0ZShcInR5cGVcIixcInJhZGlvXCIpLGMuc2V0QXR0cmlidXRlKFwiY2hlY2tlZFwiLFwiY2hlY2tlZFwiKSxjLnNldEF0dHJpYnV0ZShcIm5hbWVcIixcInRcIiksYi5hcHBlbmRDaGlsZChjKSxsLmNoZWNrQ2xvbmU9Yi5jbG9uZU5vZGUoITApLmNsb25lTm9kZSghMCkubGFzdENoaWxkLmNoZWNrZWQsYi5pbm5lckhUTUw9XCI8dGV4dGFyZWE+eDwvdGV4dGFyZWE+XCIsbC5ub0Nsb25lQ2hlY2tlZD0hIWIuY2xvbmVOb2RlKCEwKS5sYXN0Q2hpbGQuZGVmYXVsdFZhbHVlfSgpO3ZhciBkYT0vXmtleS8sZWE9L14oPzptb3VzZXxwb2ludGVyfGNvbnRleHRtZW51fGRyYWd8ZHJvcCl8Y2xpY2svLGZhPS9eKFteLl0qKSg/OlxcLiguKyl8KS87ZnVuY3Rpb24gZ2EoKXtyZXR1cm4hMH1mdW5jdGlvbiBoYSgpe3JldHVybiExfWZ1bmN0aW9uIGlhKCl7dHJ5e3JldHVybiBkLmFjdGl2ZUVsZW1lbnR9Y2F0Y2goYSl7fX1mdW5jdGlvbiBqYShhLGIsYyxkLGUsZil7dmFyIGcsaDtpZihcIm9iamVjdFwiPT10eXBlb2YgYil7XCJzdHJpbmdcIiE9dHlwZW9mIGMmJihkPWR8fGMsYz12b2lkIDApO2ZvcihoIGluIGIpamEoYSxoLGMsZCxiW2hdLGYpO3JldHVybiBhfWlmKG51bGw9PWQmJm51bGw9PWU/KGU9YyxkPWM9dm9pZCAwKTpudWxsPT1lJiYoXCJzdHJpbmdcIj09dHlwZW9mIGM/KGU9ZCxkPXZvaWQgMCk6KGU9ZCxkPWMsYz12b2lkIDApKSxlPT09ITEpZT1oYTtlbHNlIGlmKCFlKXJldHVybiBhO3JldHVybiAxPT09ZiYmKGc9ZSxlPWZ1bmN0aW9uKGEpe3JldHVybiBuKCkub2ZmKGEpLGcuYXBwbHkodGhpcyxhcmd1bWVudHMpfSxlLmd1aWQ9Zy5ndWlkfHwoZy5ndWlkPW4uZ3VpZCsrKSksYS5lYWNoKGZ1bmN0aW9uKCl7bi5ldmVudC5hZGQodGhpcyxiLGUsZCxjKX0pfW4uZXZlbnQ9e2dsb2JhbDp7fSxhZGQ6ZnVuY3Rpb24oYSxiLGMsZCxlKXt2YXIgZixnLGgsaSxqLGssbCxtLG8scCxxLHI9Ti5nZXQoYSk7aWYocil7Yy5oYW5kbGVyJiYoZj1jLGM9Zi5oYW5kbGVyLGU9Zi5zZWxlY3RvciksYy5ndWlkfHwoYy5ndWlkPW4uZ3VpZCsrKSwoaT1yLmV2ZW50cyl8fChpPXIuZXZlbnRzPXt9KSwoZz1yLmhhbmRsZSl8fChnPXIuaGFuZGxlPWZ1bmN0aW9uKGIpe3JldHVyblwidW5kZWZpbmVkXCIhPXR5cGVvZiBuJiZuLmV2ZW50LnRyaWdnZXJlZCE9PWIudHlwZT9uLmV2ZW50LmRpc3BhdGNoLmFwcGx5KGEsYXJndW1lbnRzKTp2b2lkIDB9KSxiPShifHxcIlwiKS5tYXRjaChHKXx8W1wiXCJdLGo9Yi5sZW5ndGg7d2hpbGUoai0tKWg9ZmEuZXhlYyhiW2pdKXx8W10sbz1xPWhbMV0scD0oaFsyXXx8XCJcIikuc3BsaXQoXCIuXCIpLnNvcnQoKSxvJiYobD1uLmV2ZW50LnNwZWNpYWxbb118fHt9LG89KGU/bC5kZWxlZ2F0ZVR5cGU6bC5iaW5kVHlwZSl8fG8sbD1uLmV2ZW50LnNwZWNpYWxbb118fHt9LGs9bi5leHRlbmQoe3R5cGU6byxvcmlnVHlwZTpxLGRhdGE6ZCxoYW5kbGVyOmMsZ3VpZDpjLmd1aWQsc2VsZWN0b3I6ZSxuZWVkc0NvbnRleHQ6ZSYmbi5leHByLm1hdGNoLm5lZWRzQ29udGV4dC50ZXN0KGUpLG5hbWVzcGFjZTpwLmpvaW4oXCIuXCIpfSxmKSwobT1pW29dKXx8KG09aVtvXT1bXSxtLmRlbGVnYXRlQ291bnQ9MCxsLnNldHVwJiZsLnNldHVwLmNhbGwoYSxkLHAsZykhPT0hMXx8YS5hZGRFdmVudExpc3RlbmVyJiZhLmFkZEV2ZW50TGlzdGVuZXIobyxnKSksbC5hZGQmJihsLmFkZC5jYWxsKGEsayksay5oYW5kbGVyLmd1aWR8fChrLmhhbmRsZXIuZ3VpZD1jLmd1aWQpKSxlP20uc3BsaWNlKG0uZGVsZWdhdGVDb3VudCsrLDAsayk6bS5wdXNoKGspLG4uZXZlbnQuZ2xvYmFsW29dPSEwKX19LHJlbW92ZTpmdW5jdGlvbihhLGIsYyxkLGUpe3ZhciBmLGcsaCxpLGosayxsLG0sbyxwLHEscj1OLmhhc0RhdGEoYSkmJk4uZ2V0KGEpO2lmKHImJihpPXIuZXZlbnRzKSl7Yj0oYnx8XCJcIikubWF0Y2goRyl8fFtcIlwiXSxqPWIubGVuZ3RoO3doaWxlKGotLSlpZihoPWZhLmV4ZWMoYltqXSl8fFtdLG89cT1oWzFdLHA9KGhbMl18fFwiXCIpLnNwbGl0KFwiLlwiKS5zb3J0KCksbyl7bD1uLmV2ZW50LnNwZWNpYWxbb118fHt9LG89KGQ/bC5kZWxlZ2F0ZVR5cGU6bC5iaW5kVHlwZSl8fG8sbT1pW29dfHxbXSxoPWhbMl0mJm5ldyBSZWdFeHAoXCIoXnxcXFxcLilcIitwLmpvaW4oXCJcXFxcLig/Oi4qXFxcXC58KVwiKStcIihcXFxcLnwkKVwiKSxnPWY9bS5sZW5ndGg7d2hpbGUoZi0tKWs9bVtmXSwhZSYmcSE9PWsub3JpZ1R5cGV8fGMmJmMuZ3VpZCE9PWsuZ3VpZHx8aCYmIWgudGVzdChrLm5hbWVzcGFjZSl8fGQmJmQhPT1rLnNlbGVjdG9yJiYoXCIqKlwiIT09ZHx8IWsuc2VsZWN0b3IpfHwobS5zcGxpY2UoZiwxKSxrLnNlbGVjdG9yJiZtLmRlbGVnYXRlQ291bnQtLSxsLnJlbW92ZSYmbC5yZW1vdmUuY2FsbChhLGspKTtnJiYhbS5sZW5ndGgmJihsLnRlYXJkb3duJiZsLnRlYXJkb3duLmNhbGwoYSxwLHIuaGFuZGxlKSE9PSExfHxuLnJlbW92ZUV2ZW50KGEsbyxyLmhhbmRsZSksZGVsZXRlIGlbb10pfWVsc2UgZm9yKG8gaW4gaSluLmV2ZW50LnJlbW92ZShhLG8rYltqXSxjLGQsITApO24uaXNFbXB0eU9iamVjdChpKSYmTi5yZW1vdmUoYSxcImhhbmRsZSBldmVudHNcIil9fSxkaXNwYXRjaDpmdW5jdGlvbihhKXthPW4uZXZlbnQuZml4KGEpO3ZhciBiLGMsZCxmLGcsaD1bXSxpPWUuY2FsbChhcmd1bWVudHMpLGo9KE4uZ2V0KHRoaXMsXCJldmVudHNcIil8fHt9KVthLnR5cGVdfHxbXSxrPW4uZXZlbnQuc3BlY2lhbFthLnR5cGVdfHx7fTtpZihpWzBdPWEsYS5kZWxlZ2F0ZVRhcmdldD10aGlzLCFrLnByZURpc3BhdGNofHxrLnByZURpc3BhdGNoLmNhbGwodGhpcyxhKSE9PSExKXtoPW4uZXZlbnQuaGFuZGxlcnMuY2FsbCh0aGlzLGEsaiksYj0wO3doaWxlKChmPWhbYisrXSkmJiFhLmlzUHJvcGFnYXRpb25TdG9wcGVkKCkpe2EuY3VycmVudFRhcmdldD1mLmVsZW0sYz0wO3doaWxlKChnPWYuaGFuZGxlcnNbYysrXSkmJiFhLmlzSW1tZWRpYXRlUHJvcGFnYXRpb25TdG9wcGVkKCkpKCFhLnJuYW1lc3BhY2V8fGEucm5hbWVzcGFjZS50ZXN0KGcubmFtZXNwYWNlKSkmJihhLmhhbmRsZU9iaj1nLGEuZGF0YT1nLmRhdGEsZD0oKG4uZXZlbnQuc3BlY2lhbFtnLm9yaWdUeXBlXXx8e30pLmhhbmRsZXx8Zy5oYW5kbGVyKS5hcHBseShmLmVsZW0saSksdm9pZCAwIT09ZCYmKGEucmVzdWx0PWQpPT09ITEmJihhLnByZXZlbnREZWZhdWx0KCksYS5zdG9wUHJvcGFnYXRpb24oKSkpfXJldHVybiBrLnBvc3REaXNwYXRjaCYmay5wb3N0RGlzcGF0Y2guY2FsbCh0aGlzLGEpLGEucmVzdWx0fX0saGFuZGxlcnM6ZnVuY3Rpb24oYSxiKXt2YXIgYyxkLGUsZixnPVtdLGg9Yi5kZWxlZ2F0ZUNvdW50LGk9YS50YXJnZXQ7aWYoaCYmaS5ub2RlVHlwZSYmKFwiY2xpY2tcIiE9PWEudHlwZXx8aXNOYU4oYS5idXR0b24pfHxhLmJ1dHRvbjwxKSlmb3IoO2khPT10aGlzO2k9aS5wYXJlbnROb2RlfHx0aGlzKWlmKDE9PT1pLm5vZGVUeXBlJiYoaS5kaXNhYmxlZCE9PSEwfHxcImNsaWNrXCIhPT1hLnR5cGUpKXtmb3IoZD1bXSxjPTA7aD5jO2MrKylmPWJbY10sZT1mLnNlbGVjdG9yK1wiIFwiLHZvaWQgMD09PWRbZV0mJihkW2VdPWYubmVlZHNDb250ZXh0P24oZSx0aGlzKS5pbmRleChpKT4tMTpuLmZpbmQoZSx0aGlzLG51bGwsW2ldKS5sZW5ndGgpLGRbZV0mJmQucHVzaChmKTtkLmxlbmd0aCYmZy5wdXNoKHtlbGVtOmksaGFuZGxlcnM6ZH0pfXJldHVybiBoPGIubGVuZ3RoJiZnLnB1c2goe2VsZW06dGhpcyxoYW5kbGVyczpiLnNsaWNlKGgpfSksZ30scHJvcHM6XCJhbHRLZXkgYnViYmxlcyBjYW5jZWxhYmxlIGN0cmxLZXkgY3VycmVudFRhcmdldCBkZXRhaWwgZXZlbnRQaGFzZSBtZXRhS2V5IHJlbGF0ZWRUYXJnZXQgc2hpZnRLZXkgdGFyZ2V0IHRpbWVTdGFtcCB2aWV3IHdoaWNoXCIuc3BsaXQoXCIgXCIpLGZpeEhvb2tzOnt9LGtleUhvb2tzOntwcm9wczpcImNoYXIgY2hhckNvZGUga2V5IGtleUNvZGVcIi5zcGxpdChcIiBcIiksZmlsdGVyOmZ1bmN0aW9uKGEsYil7cmV0dXJuIG51bGw9PWEud2hpY2gmJihhLndoaWNoPW51bGwhPWIuY2hhckNvZGU/Yi5jaGFyQ29kZTpiLmtleUNvZGUpLGF9fSxtb3VzZUhvb2tzOntwcm9wczpcImJ1dHRvbiBidXR0b25zIGNsaWVudFggY2xpZW50WSBvZmZzZXRYIG9mZnNldFkgcGFnZVggcGFnZVkgc2NyZWVuWCBzY3JlZW5ZIHRvRWxlbWVudFwiLnNwbGl0KFwiIFwiKSxmaWx0ZXI6ZnVuY3Rpb24oYSxiKXt2YXIgYyxlLGYsZz1iLmJ1dHRvbjtyZXR1cm4gbnVsbD09YS5wYWdlWCYmbnVsbCE9Yi5jbGllbnRYJiYoYz1hLnRhcmdldC5vd25lckRvY3VtZW50fHxkLGU9Yy5kb2N1bWVudEVsZW1lbnQsZj1jLmJvZHksYS5wYWdlWD1iLmNsaWVudFgrKGUmJmUuc2Nyb2xsTGVmdHx8ZiYmZi5zY3JvbGxMZWZ0fHwwKS0oZSYmZS5jbGllbnRMZWZ0fHxmJiZmLmNsaWVudExlZnR8fDApLGEucGFnZVk9Yi5jbGllbnRZKyhlJiZlLnNjcm9sbFRvcHx8ZiYmZi5zY3JvbGxUb3B8fDApLShlJiZlLmNsaWVudFRvcHx8ZiYmZi5jbGllbnRUb3B8fDApKSxhLndoaWNofHx2b2lkIDA9PT1nfHwoYS53aGljaD0xJmc/MToyJmc/Mzo0Jmc/MjowKSxhfX0sZml4OmZ1bmN0aW9uKGEpe2lmKGFbbi5leHBhbmRvXSlyZXR1cm4gYTt2YXIgYixjLGUsZj1hLnR5cGUsZz1hLGg9dGhpcy5maXhIb29rc1tmXTtofHwodGhpcy5maXhIb29rc1tmXT1oPWVhLnRlc3QoZik/dGhpcy5tb3VzZUhvb2tzOmRhLnRlc3QoZik/dGhpcy5rZXlIb29rczp7fSksZT1oLnByb3BzP3RoaXMucHJvcHMuY29uY2F0KGgucHJvcHMpOnRoaXMucHJvcHMsYT1uZXcgbi5FdmVudChnKSxiPWUubGVuZ3RoO3doaWxlKGItLSljPWVbYl0sYVtjXT1nW2NdO3JldHVybiBhLnRhcmdldHx8KGEudGFyZ2V0PWQpLDM9PT1hLnRhcmdldC5ub2RlVHlwZSYmKGEudGFyZ2V0PWEudGFyZ2V0LnBhcmVudE5vZGUpLGguZmlsdGVyP2guZmlsdGVyKGEsZyk6YX0sc3BlY2lhbDp7bG9hZDp7bm9CdWJibGU6ITB9LGZvY3VzOnt0cmlnZ2VyOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMhPT1pYSgpJiZ0aGlzLmZvY3VzPyh0aGlzLmZvY3VzKCksITEpOnZvaWQgMH0sZGVsZWdhdGVUeXBlOlwiZm9jdXNpblwifSxibHVyOnt0cmlnZ2VyOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXM9PT1pYSgpJiZ0aGlzLmJsdXI/KHRoaXMuYmx1cigpLCExKTp2b2lkIDB9LGRlbGVnYXRlVHlwZTpcImZvY3Vzb3V0XCJ9LGNsaWNrOnt0cmlnZ2VyOmZ1bmN0aW9uKCl7cmV0dXJuXCJjaGVja2JveFwiPT09dGhpcy50eXBlJiZ0aGlzLmNsaWNrJiZuLm5vZGVOYW1lKHRoaXMsXCJpbnB1dFwiKT8odGhpcy5jbGljaygpLCExKTp2b2lkIDB9LF9kZWZhdWx0OmZ1bmN0aW9uKGEpe3JldHVybiBuLm5vZGVOYW1lKGEudGFyZ2V0LFwiYVwiKX19LGJlZm9yZXVubG9hZDp7cG9zdERpc3BhdGNoOmZ1bmN0aW9uKGEpe3ZvaWQgMCE9PWEucmVzdWx0JiZhLm9yaWdpbmFsRXZlbnQmJihhLm9yaWdpbmFsRXZlbnQucmV0dXJuVmFsdWU9YS5yZXN1bHQpfX19fSxuLnJlbW92ZUV2ZW50PWZ1bmN0aW9uKGEsYixjKXthLnJlbW92ZUV2ZW50TGlzdGVuZXImJmEucmVtb3ZlRXZlbnRMaXN0ZW5lcihiLGMpfSxuLkV2ZW50PWZ1bmN0aW9uKGEsYil7cmV0dXJuIHRoaXMgaW5zdGFuY2VvZiBuLkV2ZW50PyhhJiZhLnR5cGU/KHRoaXMub3JpZ2luYWxFdmVudD1hLHRoaXMudHlwZT1hLnR5cGUsdGhpcy5pc0RlZmF1bHRQcmV2ZW50ZWQ9YS5kZWZhdWx0UHJldmVudGVkfHx2b2lkIDA9PT1hLmRlZmF1bHRQcmV2ZW50ZWQmJmEucmV0dXJuVmFsdWU9PT0hMT9nYTpoYSk6dGhpcy50eXBlPWEsYiYmbi5leHRlbmQodGhpcyxiKSx0aGlzLnRpbWVTdGFtcD1hJiZhLnRpbWVTdGFtcHx8bi5ub3coKSx2b2lkKHRoaXNbbi5leHBhbmRvXT0hMCkpOm5ldyBuLkV2ZW50KGEsYil9LG4uRXZlbnQucHJvdG90eXBlPXtjb25zdHJ1Y3RvcjpuLkV2ZW50LGlzRGVmYXVsdFByZXZlbnRlZDpoYSxpc1Byb3BhZ2F0aW9uU3RvcHBlZDpoYSxpc0ltbWVkaWF0ZVByb3BhZ2F0aW9uU3RvcHBlZDpoYSxwcmV2ZW50RGVmYXVsdDpmdW5jdGlvbigpe3ZhciBhPXRoaXMub3JpZ2luYWxFdmVudDt0aGlzLmlzRGVmYXVsdFByZXZlbnRlZD1nYSxhJiZhLnByZXZlbnREZWZhdWx0KCl9LHN0b3BQcm9wYWdhdGlvbjpmdW5jdGlvbigpe3ZhciBhPXRoaXMub3JpZ2luYWxFdmVudDt0aGlzLmlzUHJvcGFnYXRpb25TdG9wcGVkPWdhLGEmJmEuc3RvcFByb3BhZ2F0aW9uKCl9LHN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbjpmdW5jdGlvbigpe3ZhciBhPXRoaXMub3JpZ2luYWxFdmVudDt0aGlzLmlzSW1tZWRpYXRlUHJvcGFnYXRpb25TdG9wcGVkPWdhLGEmJmEuc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uKCksdGhpcy5zdG9wUHJvcGFnYXRpb24oKX19LG4uZWFjaCh7bW91c2VlbnRlcjpcIm1vdXNlb3ZlclwiLG1vdXNlbGVhdmU6XCJtb3VzZW91dFwiLHBvaW50ZXJlbnRlcjpcInBvaW50ZXJvdmVyXCIscG9pbnRlcmxlYXZlOlwicG9pbnRlcm91dFwifSxmdW5jdGlvbihhLGIpe24uZXZlbnQuc3BlY2lhbFthXT17ZGVsZWdhdGVUeXBlOmIsYmluZFR5cGU6YixoYW5kbGU6ZnVuY3Rpb24oYSl7dmFyIGMsZD10aGlzLGU9YS5yZWxhdGVkVGFyZ2V0LGY9YS5oYW5kbGVPYmo7cmV0dXJuKCFlfHxlIT09ZCYmIW4uY29udGFpbnMoZCxlKSkmJihhLnR5cGU9Zi5vcmlnVHlwZSxjPWYuaGFuZGxlci5hcHBseSh0aGlzLGFyZ3VtZW50cyksYS50eXBlPWIpLGN9fX0pLG4uZm4uZXh0ZW5kKHtvbjpmdW5jdGlvbihhLGIsYyxkKXtyZXR1cm4gamEodGhpcyxhLGIsYyxkKX0sb25lOmZ1bmN0aW9uKGEsYixjLGQpe3JldHVybiBqYSh0aGlzLGEsYixjLGQsMSl9LG9mZjpmdW5jdGlvbihhLGIsYyl7dmFyIGQsZTtpZihhJiZhLnByZXZlbnREZWZhdWx0JiZhLmhhbmRsZU9iailyZXR1cm4gZD1hLmhhbmRsZU9iaixuKGEuZGVsZWdhdGVUYXJnZXQpLm9mZihkLm5hbWVzcGFjZT9kLm9yaWdUeXBlK1wiLlwiK2QubmFtZXNwYWNlOmQub3JpZ1R5cGUsZC5zZWxlY3RvcixkLmhhbmRsZXIpLHRoaXM7aWYoXCJvYmplY3RcIj09dHlwZW9mIGEpe2ZvcihlIGluIGEpdGhpcy5vZmYoZSxiLGFbZV0pO3JldHVybiB0aGlzfXJldHVybihiPT09ITF8fFwiZnVuY3Rpb25cIj09dHlwZW9mIGIpJiYoYz1iLGI9dm9pZCAwKSxjPT09ITEmJihjPWhhKSx0aGlzLmVhY2goZnVuY3Rpb24oKXtuLmV2ZW50LnJlbW92ZSh0aGlzLGEsYyxiKX0pfX0pO3ZhciBrYT0vPCg/IWFyZWF8YnJ8Y29sfGVtYmVkfGhyfGltZ3xpbnB1dHxsaW5rfG1ldGF8cGFyYW0pKChbXFx3Oi1dKylbXj5dKilcXC8+L2dpLGxhPS88c2NyaXB0fDxzdHlsZXw8bGluay9pLG1hPS9jaGVja2VkXFxzKig/OltePV18PVxccyouY2hlY2tlZC4pL2ksbmE9L150cnVlXFwvKC4qKS8sb2E9L15cXHMqPCEoPzpcXFtDREFUQVxcW3wtLSl8KD86XFxdXFxdfC0tKT5cXHMqJC9nO2Z1bmN0aW9uIHBhKGEsYil7cmV0dXJuIG4ubm9kZU5hbWUoYSxcInRhYmxlXCIpJiZuLm5vZGVOYW1lKDExIT09Yi5ub2RlVHlwZT9iOmIuZmlyc3RDaGlsZCxcInRyXCIpP2EuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJ0Ym9keVwiKVswXXx8YS5hcHBlbmRDaGlsZChhLm93bmVyRG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInRib2R5XCIpKTphfWZ1bmN0aW9uIHFhKGEpe3JldHVybiBhLnR5cGU9KG51bGwhPT1hLmdldEF0dHJpYnV0ZShcInR5cGVcIikpK1wiL1wiK2EudHlwZSxhfWZ1bmN0aW9uIHJhKGEpe3ZhciBiPW5hLmV4ZWMoYS50eXBlKTtyZXR1cm4gYj9hLnR5cGU9YlsxXTphLnJlbW92ZUF0dHJpYnV0ZShcInR5cGVcIiksYX1mdW5jdGlvbiBzYShhLGIpe3ZhciBjLGQsZSxmLGcsaCxpLGo7aWYoMT09PWIubm9kZVR5cGUpe2lmKE4uaGFzRGF0YShhKSYmKGY9Ti5hY2Nlc3MoYSksZz1OLnNldChiLGYpLGo9Zi5ldmVudHMpKXtkZWxldGUgZy5oYW5kbGUsZy5ldmVudHM9e307Zm9yKGUgaW4gailmb3IoYz0wLGQ9altlXS5sZW5ndGg7ZD5jO2MrKyluLmV2ZW50LmFkZChiLGUsaltlXVtjXSl9Ty5oYXNEYXRhKGEpJiYoaD1PLmFjY2VzcyhhKSxpPW4uZXh0ZW5kKHt9LGgpLE8uc2V0KGIsaSkpfX1mdW5jdGlvbiB0YShhLGIpe3ZhciBjPWIubm9kZU5hbWUudG9Mb3dlckNhc2UoKTtcImlucHV0XCI9PT1jJiZYLnRlc3QoYS50eXBlKT9iLmNoZWNrZWQ9YS5jaGVja2VkOihcImlucHV0XCI9PT1jfHxcInRleHRhcmVhXCI9PT1jKSYmKGIuZGVmYXVsdFZhbHVlPWEuZGVmYXVsdFZhbHVlKX1mdW5jdGlvbiB1YShhLGIsYyxkKXtiPWYuYXBwbHkoW10sYik7dmFyIGUsZyxoLGksaixrLG09MCxvPWEubGVuZ3RoLHA9by0xLHE9YlswXSxyPW4uaXNGdW5jdGlvbihxKTtpZihyfHxvPjEmJlwic3RyaW5nXCI9PXR5cGVvZiBxJiYhbC5jaGVja0Nsb25lJiZtYS50ZXN0KHEpKXJldHVybiBhLmVhY2goZnVuY3Rpb24oZSl7dmFyIGY9YS5lcShlKTtyJiYoYlswXT1xLmNhbGwodGhpcyxlLGYuaHRtbCgpKSksdWEoZixiLGMsZCl9KTtpZihvJiYoZT1jYShiLGFbMF0ub3duZXJEb2N1bWVudCwhMSxhLGQpLGc9ZS5maXJzdENoaWxkLDE9PT1lLmNoaWxkTm9kZXMubGVuZ3RoJiYoZT1nKSxnfHxkKSl7Zm9yKGg9bi5tYXAoXyhlLFwic2NyaXB0XCIpLHFhKSxpPWgubGVuZ3RoO28+bTttKyspaj1lLG0hPT1wJiYoaj1uLmNsb25lKGosITAsITApLGkmJm4ubWVyZ2UoaCxfKGosXCJzY3JpcHRcIikpKSxjLmNhbGwoYVttXSxqLG0pO2lmKGkpZm9yKGs9aFtoLmxlbmd0aC0xXS5vd25lckRvY3VtZW50LG4ubWFwKGgscmEpLG09MDtpPm07bSsrKWo9aFttXSxaLnRlc3Qoai50eXBlfHxcIlwiKSYmIU4uYWNjZXNzKGosXCJnbG9iYWxFdmFsXCIpJiZuLmNvbnRhaW5zKGssaikmJihqLnNyYz9uLl9ldmFsVXJsJiZuLl9ldmFsVXJsKGouc3JjKTpuLmdsb2JhbEV2YWwoai50ZXh0Q29udGVudC5yZXBsYWNlKG9hLFwiXCIpKSl9cmV0dXJuIGF9ZnVuY3Rpb24gdmEoYSxiLGMpe2Zvcih2YXIgZCxlPWI/bi5maWx0ZXIoYixhKTphLGY9MDtudWxsIT0oZD1lW2ZdKTtmKyspY3x8MSE9PWQubm9kZVR5cGV8fG4uY2xlYW5EYXRhKF8oZCkpLGQucGFyZW50Tm9kZSYmKGMmJm4uY29udGFpbnMoZC5vd25lckRvY3VtZW50LGQpJiZhYShfKGQsXCJzY3JpcHRcIikpLGQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChkKSk7cmV0dXJuIGF9bi5leHRlbmQoe2h0bWxQcmVmaWx0ZXI6ZnVuY3Rpb24oYSl7cmV0dXJuIGEucmVwbGFjZShrYSxcIjwkMT48LyQyPlwiKX0sY2xvbmU6ZnVuY3Rpb24oYSxiLGMpe3ZhciBkLGUsZixnLGg9YS5jbG9uZU5vZGUoITApLGk9bi5jb250YWlucyhhLm93bmVyRG9jdW1lbnQsYSk7aWYoIShsLm5vQ2xvbmVDaGVja2VkfHwxIT09YS5ub2RlVHlwZSYmMTEhPT1hLm5vZGVUeXBlfHxuLmlzWE1MRG9jKGEpKSlmb3IoZz1fKGgpLGY9XyhhKSxkPTAsZT1mLmxlbmd0aDtlPmQ7ZCsrKXRhKGZbZF0sZ1tkXSk7aWYoYilpZihjKWZvcihmPWZ8fF8oYSksZz1nfHxfKGgpLGQ9MCxlPWYubGVuZ3RoO2U+ZDtkKyspc2EoZltkXSxnW2RdKTtlbHNlIHNhKGEsaCk7cmV0dXJuIGc9XyhoLFwic2NyaXB0XCIpLGcubGVuZ3RoPjAmJmFhKGcsIWkmJl8oYSxcInNjcmlwdFwiKSksaH0sY2xlYW5EYXRhOmZ1bmN0aW9uKGEpe2Zvcih2YXIgYixjLGQsZT1uLmV2ZW50LnNwZWNpYWwsZj0wO3ZvaWQgMCE9PShjPWFbZl0pO2YrKylpZihMKGMpKXtpZihiPWNbTi5leHBhbmRvXSl7aWYoYi5ldmVudHMpZm9yKGQgaW4gYi5ldmVudHMpZVtkXT9uLmV2ZW50LnJlbW92ZShjLGQpOm4ucmVtb3ZlRXZlbnQoYyxkLGIuaGFuZGxlKTtjW04uZXhwYW5kb109dm9pZCAwfWNbTy5leHBhbmRvXSYmKGNbTy5leHBhbmRvXT12b2lkIDApfX19KSxuLmZuLmV4dGVuZCh7ZG9tTWFuaXA6dWEsZGV0YWNoOmZ1bmN0aW9uKGEpe3JldHVybiB2YSh0aGlzLGEsITApfSxyZW1vdmU6ZnVuY3Rpb24oYSl7cmV0dXJuIHZhKHRoaXMsYSl9LHRleHQ6ZnVuY3Rpb24oYSl7cmV0dXJuIEsodGhpcyxmdW5jdGlvbihhKXtyZXR1cm4gdm9pZCAwPT09YT9uLnRleHQodGhpcyk6dGhpcy5lbXB0eSgpLmVhY2goZnVuY3Rpb24oKXsoMT09PXRoaXMubm9kZVR5cGV8fDExPT09dGhpcy5ub2RlVHlwZXx8OT09PXRoaXMubm9kZVR5cGUpJiYodGhpcy50ZXh0Q29udGVudD1hKX0pfSxudWxsLGEsYXJndW1lbnRzLmxlbmd0aCl9LGFwcGVuZDpmdW5jdGlvbigpe3JldHVybiB1YSh0aGlzLGFyZ3VtZW50cyxmdW5jdGlvbihhKXtpZigxPT09dGhpcy5ub2RlVHlwZXx8MTE9PT10aGlzLm5vZGVUeXBlfHw5PT09dGhpcy5ub2RlVHlwZSl7dmFyIGI9cGEodGhpcyxhKTtiLmFwcGVuZENoaWxkKGEpfX0pfSxwcmVwZW5kOmZ1bmN0aW9uKCl7cmV0dXJuIHVhKHRoaXMsYXJndW1lbnRzLGZ1bmN0aW9uKGEpe2lmKDE9PT10aGlzLm5vZGVUeXBlfHwxMT09PXRoaXMubm9kZVR5cGV8fDk9PT10aGlzLm5vZGVUeXBlKXt2YXIgYj1wYSh0aGlzLGEpO2IuaW5zZXJ0QmVmb3JlKGEsYi5maXJzdENoaWxkKX19KX0sYmVmb3JlOmZ1bmN0aW9uKCl7cmV0dXJuIHVhKHRoaXMsYXJndW1lbnRzLGZ1bmN0aW9uKGEpe3RoaXMucGFyZW50Tm9kZSYmdGhpcy5wYXJlbnROb2RlLmluc2VydEJlZm9yZShhLHRoaXMpfSl9LGFmdGVyOmZ1bmN0aW9uKCl7cmV0dXJuIHVhKHRoaXMsYXJndW1lbnRzLGZ1bmN0aW9uKGEpe3RoaXMucGFyZW50Tm9kZSYmdGhpcy5wYXJlbnROb2RlLmluc2VydEJlZm9yZShhLHRoaXMubmV4dFNpYmxpbmcpfSl9LGVtcHR5OmZ1bmN0aW9uKCl7Zm9yKHZhciBhLGI9MDtudWxsIT0oYT10aGlzW2JdKTtiKyspMT09PWEubm9kZVR5cGUmJihuLmNsZWFuRGF0YShfKGEsITEpKSxhLnRleHRDb250ZW50PVwiXCIpO3JldHVybiB0aGlzfSxjbG9uZTpmdW5jdGlvbihhLGIpe3JldHVybiBhPW51bGw9PWE/ITE6YSxiPW51bGw9PWI/YTpiLHRoaXMubWFwKGZ1bmN0aW9uKCl7cmV0dXJuIG4uY2xvbmUodGhpcyxhLGIpfSl9LGh0bWw6ZnVuY3Rpb24oYSl7cmV0dXJuIEsodGhpcyxmdW5jdGlvbihhKXt2YXIgYj10aGlzWzBdfHx7fSxjPTAsZD10aGlzLmxlbmd0aDtpZih2b2lkIDA9PT1hJiYxPT09Yi5ub2RlVHlwZSlyZXR1cm4gYi5pbm5lckhUTUw7aWYoXCJzdHJpbmdcIj09dHlwZW9mIGEmJiFsYS50ZXN0KGEpJiYhJFsoWS5leGVjKGEpfHxbXCJcIixcIlwiXSlbMV0udG9Mb3dlckNhc2UoKV0pe2E9bi5odG1sUHJlZmlsdGVyKGEpO3RyeXtmb3IoO2Q+YztjKyspYj10aGlzW2NdfHx7fSwxPT09Yi5ub2RlVHlwZSYmKG4uY2xlYW5EYXRhKF8oYiwhMSkpLGIuaW5uZXJIVE1MPWEpO2I9MH1jYXRjaChlKXt9fWImJnRoaXMuZW1wdHkoKS5hcHBlbmQoYSl9LG51bGwsYSxhcmd1bWVudHMubGVuZ3RoKX0scmVwbGFjZVdpdGg6ZnVuY3Rpb24oKXt2YXIgYT1bXTtyZXR1cm4gdWEodGhpcyxhcmd1bWVudHMsZnVuY3Rpb24oYil7dmFyIGM9dGhpcy5wYXJlbnROb2RlO24uaW5BcnJheSh0aGlzLGEpPDAmJihuLmNsZWFuRGF0YShfKHRoaXMpKSxjJiZjLnJlcGxhY2VDaGlsZChiLHRoaXMpKX0sYSl9fSksbi5lYWNoKHthcHBlbmRUbzpcImFwcGVuZFwiLHByZXBlbmRUbzpcInByZXBlbmRcIixpbnNlcnRCZWZvcmU6XCJiZWZvcmVcIixpbnNlcnRBZnRlcjpcImFmdGVyXCIscmVwbGFjZUFsbDpcInJlcGxhY2VXaXRoXCJ9LGZ1bmN0aW9uKGEsYil7bi5mblthXT1mdW5jdGlvbihhKXtmb3IodmFyIGMsZD1bXSxlPW4oYSksZj1lLmxlbmd0aC0xLGg9MDtmPj1oO2grKyljPWg9PT1mP3RoaXM6dGhpcy5jbG9uZSghMCksbihlW2hdKVtiXShjKSxnLmFwcGx5KGQsYy5nZXQoKSk7cmV0dXJuIHRoaXMucHVzaFN0YWNrKGQpfX0pO3ZhciB3YSx4YT17SFRNTDpcImJsb2NrXCIsQk9EWTpcImJsb2NrXCJ9O2Z1bmN0aW9uIHlhKGEsYil7dmFyIGM9bihiLmNyZWF0ZUVsZW1lbnQoYSkpLmFwcGVuZFRvKGIuYm9keSksZD1uLmNzcyhjWzBdLFwiZGlzcGxheVwiKTtyZXR1cm4gYy5kZXRhY2goKSxkfWZ1bmN0aW9uIHphKGEpe3ZhciBiPWQsYz14YVthXTtyZXR1cm4gY3x8KGM9eWEoYSxiKSxcIm5vbmVcIiE9PWMmJmN8fCh3YT0od2F8fG4oXCI8aWZyYW1lIGZyYW1lYm9yZGVyPScwJyB3aWR0aD0nMCcgaGVpZ2h0PScwJy8+XCIpKS5hcHBlbmRUbyhiLmRvY3VtZW50RWxlbWVudCksYj13YVswXS5jb250ZW50RG9jdW1lbnQsYi53cml0ZSgpLGIuY2xvc2UoKSxjPXlhKGEsYiksd2EuZGV0YWNoKCkpLHhhW2FdPWMpLGN9dmFyIEFhPS9ebWFyZ2luLyxCYT1uZXcgUmVnRXhwKFwiXihcIitTK1wiKSg/IXB4KVthLXolXSskXCIsXCJpXCIpLENhPWZ1bmN0aW9uKGIpe3ZhciBjPWIub3duZXJEb2N1bWVudC5kZWZhdWx0VmlldztyZXR1cm4gYyYmYy5vcGVuZXJ8fChjPWEpLGMuZ2V0Q29tcHV0ZWRTdHlsZShiKX0sRGE9ZnVuY3Rpb24oYSxiLGMsZCl7dmFyIGUsZixnPXt9O2ZvcihmIGluIGIpZ1tmXT1hLnN0eWxlW2ZdLGEuc3R5bGVbZl09YltmXTtlPWMuYXBwbHkoYSxkfHxbXSk7Zm9yKGYgaW4gYilhLnN0eWxlW2ZdPWdbZl07cmV0dXJuIGV9LEVhPWQuZG9jdW1lbnRFbGVtZW50OyFmdW5jdGlvbigpe3ZhciBiLGMsZSxmLGc9ZC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpLGg9ZC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO2lmKGguc3R5bGUpe2guc3R5bGUuYmFja2dyb3VuZENsaXA9XCJjb250ZW50LWJveFwiLGguY2xvbmVOb2RlKCEwKS5zdHlsZS5iYWNrZ3JvdW5kQ2xpcD1cIlwiLGwuY2xlYXJDbG9uZVN0eWxlPVwiY29udGVudC1ib3hcIj09PWguc3R5bGUuYmFja2dyb3VuZENsaXAsZy5zdHlsZS5jc3NUZXh0PVwiYm9yZGVyOjA7d2lkdGg6OHB4O2hlaWdodDowO3RvcDowO2xlZnQ6LTk5OTlweDtwYWRkaW5nOjA7bWFyZ2luLXRvcDoxcHg7cG9zaXRpb246YWJzb2x1dGVcIixnLmFwcGVuZENoaWxkKGgpO2Z1bmN0aW9uIGkoKXtoLnN0eWxlLmNzc1RleHQ9XCItd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDtib3gtc2l6aW5nOmJvcmRlci1ib3g7cG9zaXRpb246cmVsYXRpdmU7ZGlzcGxheTpibG9jazttYXJnaW46YXV0bztib3JkZXI6MXB4O3BhZGRpbmc6MXB4O3RvcDoxJTt3aWR0aDo1MCVcIixoLmlubmVySFRNTD1cIlwiLEVhLmFwcGVuZENoaWxkKGcpO3ZhciBkPWEuZ2V0Q29tcHV0ZWRTdHlsZShoKTtiPVwiMSVcIiE9PWQudG9wLGY9XCIycHhcIj09PWQubWFyZ2luTGVmdCxjPVwiNHB4XCI9PT1kLndpZHRoLGguc3R5bGUubWFyZ2luUmlnaHQ9XCI1MCVcIixlPVwiNHB4XCI9PT1kLm1hcmdpblJpZ2h0LEVhLnJlbW92ZUNoaWxkKGcpfW4uZXh0ZW5kKGwse3BpeGVsUG9zaXRpb246ZnVuY3Rpb24oKXtyZXR1cm4gaSgpLGJ9LGJveFNpemluZ1JlbGlhYmxlOmZ1bmN0aW9uKCl7cmV0dXJuIG51bGw9PWMmJmkoKSxjfSxwaXhlbE1hcmdpblJpZ2h0OmZ1bmN0aW9uKCl7cmV0dXJuIG51bGw9PWMmJmkoKSxlfSxyZWxpYWJsZU1hcmdpbkxlZnQ6ZnVuY3Rpb24oKXtyZXR1cm4gbnVsbD09YyYmaSgpLGZ9LHJlbGlhYmxlTWFyZ2luUmlnaHQ6ZnVuY3Rpb24oKXt2YXIgYixjPWguYXBwZW5kQ2hpbGQoZC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpKTtyZXR1cm4gYy5zdHlsZS5jc3NUZXh0PWguc3R5bGUuY3NzVGV4dD1cIi13ZWJraXQtYm94LXNpemluZzpjb250ZW50LWJveDtib3gtc2l6aW5nOmNvbnRlbnQtYm94O2Rpc3BsYXk6YmxvY2s7bWFyZ2luOjA7Ym9yZGVyOjA7cGFkZGluZzowXCIsYy5zdHlsZS5tYXJnaW5SaWdodD1jLnN0eWxlLndpZHRoPVwiMFwiLGguc3R5bGUud2lkdGg9XCIxcHhcIixFYS5hcHBlbmRDaGlsZChnKSxiPSFwYXJzZUZsb2F0KGEuZ2V0Q29tcHV0ZWRTdHlsZShjKS5tYXJnaW5SaWdodCksRWEucmVtb3ZlQ2hpbGQoZyksaC5yZW1vdmVDaGlsZChjKSxifX0pfX0oKTtmdW5jdGlvbiBGYShhLGIsYyl7dmFyIGQsZSxmLGcsaD1hLnN0eWxlO3JldHVybiBjPWN8fENhKGEpLGc9Yz9jLmdldFByb3BlcnR5VmFsdWUoYil8fGNbYl06dm9pZCAwLFwiXCIhPT1nJiZ2b2lkIDAhPT1nfHxuLmNvbnRhaW5zKGEub3duZXJEb2N1bWVudCxhKXx8KGc9bi5zdHlsZShhLGIpKSxjJiYhbC5waXhlbE1hcmdpblJpZ2h0KCkmJkJhLnRlc3QoZykmJkFhLnRlc3QoYikmJihkPWgud2lkdGgsZT1oLm1pbldpZHRoLGY9aC5tYXhXaWR0aCxoLm1pbldpZHRoPWgubWF4V2lkdGg9aC53aWR0aD1nLGc9Yy53aWR0aCxoLndpZHRoPWQsaC5taW5XaWR0aD1lLGgubWF4V2lkdGg9Ziksdm9pZCAwIT09Zz9nK1wiXCI6Z31mdW5jdGlvbiBHYShhLGIpe3JldHVybntnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gYSgpP3ZvaWQgZGVsZXRlIHRoaXMuZ2V0Oih0aGlzLmdldD1iKS5hcHBseSh0aGlzLGFyZ3VtZW50cyl9fX12YXIgSGE9L14obm9uZXx0YWJsZSg/IS1jW2VhXSkuKykvLElhPXtwb3NpdGlvbjpcImFic29sdXRlXCIsdmlzaWJpbGl0eTpcImhpZGRlblwiLGRpc3BsYXk6XCJibG9ja1wifSxKYT17bGV0dGVyU3BhY2luZzpcIjBcIixmb250V2VpZ2h0OlwiNDAwXCJ9LEthPVtcIldlYmtpdFwiLFwiT1wiLFwiTW96XCIsXCJtc1wiXSxMYT1kLmNyZWF0ZUVsZW1lbnQoXCJkaXZcIikuc3R5bGU7ZnVuY3Rpb24gTWEoYSl7aWYoYSBpbiBMYSlyZXR1cm4gYTt2YXIgYj1hWzBdLnRvVXBwZXJDYXNlKCkrYS5zbGljZSgxKSxjPUthLmxlbmd0aDt3aGlsZShjLS0paWYoYT1LYVtjXStiLGEgaW4gTGEpcmV0dXJuIGF9ZnVuY3Rpb24gTmEoYSxiLGMpe3ZhciBkPVQuZXhlYyhiKTtyZXR1cm4gZD9NYXRoLm1heCgwLGRbMl0tKGN8fDApKSsoZFszXXx8XCJweFwiKTpifWZ1bmN0aW9uIE9hKGEsYixjLGQsZSl7Zm9yKHZhciBmPWM9PT0oZD9cImJvcmRlclwiOlwiY29udGVudFwiKT80Olwid2lkdGhcIj09PWI/MTowLGc9MDs0PmY7Zis9MilcIm1hcmdpblwiPT09YyYmKGcrPW4uY3NzKGEsYytVW2ZdLCEwLGUpKSxkPyhcImNvbnRlbnRcIj09PWMmJihnLT1uLmNzcyhhLFwicGFkZGluZ1wiK1VbZl0sITAsZSkpLFwibWFyZ2luXCIhPT1jJiYoZy09bi5jc3MoYSxcImJvcmRlclwiK1VbZl0rXCJXaWR0aFwiLCEwLGUpKSk6KGcrPW4uY3NzKGEsXCJwYWRkaW5nXCIrVVtmXSwhMCxlKSxcInBhZGRpbmdcIiE9PWMmJihnKz1uLmNzcyhhLFwiYm9yZGVyXCIrVVtmXStcIldpZHRoXCIsITAsZSkpKTtyZXR1cm4gZ31mdW5jdGlvbiBQYShiLGMsZSl7dmFyIGY9ITAsZz1cIndpZHRoXCI9PT1jP2Iub2Zmc2V0V2lkdGg6Yi5vZmZzZXRIZWlnaHQsaD1DYShiKSxpPVwiYm9yZGVyLWJveFwiPT09bi5jc3MoYixcImJveFNpemluZ1wiLCExLGgpO2lmKGQubXNGdWxsc2NyZWVuRWxlbWVudCYmYS50b3AhPT1hJiZiLmdldENsaWVudFJlY3RzKCkubGVuZ3RoJiYoZz1NYXRoLnJvdW5kKDEwMCpiLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpW2NdKSksMD49Z3x8bnVsbD09Zyl7aWYoZz1GYShiLGMsaCksKDA+Z3x8bnVsbD09ZykmJihnPWIuc3R5bGVbY10pLEJhLnRlc3QoZykpcmV0dXJuIGc7Zj1pJiYobC5ib3hTaXppbmdSZWxpYWJsZSgpfHxnPT09Yi5zdHlsZVtjXSksZz1wYXJzZUZsb2F0KGcpfHwwfXJldHVybiBnK09hKGIsYyxlfHwoaT9cImJvcmRlclwiOlwiY29udGVudFwiKSxmLGgpK1wicHhcIn1mdW5jdGlvbiBRYShhLGIpe2Zvcih2YXIgYyxkLGUsZj1bXSxnPTAsaD1hLmxlbmd0aDtoPmc7ZysrKWQ9YVtnXSxkLnN0eWxlJiYoZltnXT1OLmdldChkLFwib2xkZGlzcGxheVwiKSxjPWQuc3R5bGUuZGlzcGxheSxiPyhmW2ddfHxcIm5vbmVcIiE9PWN8fChkLnN0eWxlLmRpc3BsYXk9XCJcIiksXCJcIj09PWQuc3R5bGUuZGlzcGxheSYmVihkKSYmKGZbZ109Ti5hY2Nlc3MoZCxcIm9sZGRpc3BsYXlcIix6YShkLm5vZGVOYW1lKSkpKTooZT1WKGQpLFwibm9uZVwiPT09YyYmZXx8Ti5zZXQoZCxcIm9sZGRpc3BsYXlcIixlP2M6bi5jc3MoZCxcImRpc3BsYXlcIikpKSk7Zm9yKGc9MDtoPmc7ZysrKWQ9YVtnXSxkLnN0eWxlJiYoYiYmXCJub25lXCIhPT1kLnN0eWxlLmRpc3BsYXkmJlwiXCIhPT1kLnN0eWxlLmRpc3BsYXl8fChkLnN0eWxlLmRpc3BsYXk9Yj9mW2ddfHxcIlwiOlwibm9uZVwiKSk7cmV0dXJuIGF9bi5leHRlbmQoe2Nzc0hvb2tzOntvcGFjaXR5OntnZXQ6ZnVuY3Rpb24oYSxiKXtpZihiKXt2YXIgYz1GYShhLFwib3BhY2l0eVwiKTtyZXR1cm5cIlwiPT09Yz9cIjFcIjpjfX19fSxjc3NOdW1iZXI6e2FuaW1hdGlvbkl0ZXJhdGlvbkNvdW50OiEwLGNvbHVtbkNvdW50OiEwLGZpbGxPcGFjaXR5OiEwLGZsZXhHcm93OiEwLGZsZXhTaHJpbms6ITAsZm9udFdlaWdodDohMCxsaW5lSGVpZ2h0OiEwLG9wYWNpdHk6ITAsb3JkZXI6ITAsb3JwaGFuczohMCx3aWRvd3M6ITAsekluZGV4OiEwLHpvb206ITB9LGNzc1Byb3BzOntcImZsb2F0XCI6XCJjc3NGbG9hdFwifSxzdHlsZTpmdW5jdGlvbihhLGIsYyxkKXtpZihhJiYzIT09YS5ub2RlVHlwZSYmOCE9PWEubm9kZVR5cGUmJmEuc3R5bGUpe3ZhciBlLGYsZyxoPW4uY2FtZWxDYXNlKGIpLGk9YS5zdHlsZTtyZXR1cm4gYj1uLmNzc1Byb3BzW2hdfHwobi5jc3NQcm9wc1toXT1NYShoKXx8aCksZz1uLmNzc0hvb2tzW2JdfHxuLmNzc0hvb2tzW2hdLHZvaWQgMD09PWM/ZyYmXCJnZXRcImluIGcmJnZvaWQgMCE9PShlPWcuZ2V0KGEsITEsZCkpP2U6aVtiXTooZj10eXBlb2YgYyxcInN0cmluZ1wiPT09ZiYmKGU9VC5leGVjKGMpKSYmZVsxXSYmKGM9VyhhLGIsZSksZj1cIm51bWJlclwiKSxudWxsIT1jJiZjPT09YyYmKFwibnVtYmVyXCI9PT1mJiYoYys9ZSYmZVszXXx8KG4uY3NzTnVtYmVyW2hdP1wiXCI6XCJweFwiKSksbC5jbGVhckNsb25lU3R5bGV8fFwiXCIhPT1jfHwwIT09Yi5pbmRleE9mKFwiYmFja2dyb3VuZFwiKXx8KGlbYl09XCJpbmhlcml0XCIpLGcmJlwic2V0XCJpbiBnJiZ2b2lkIDA9PT0oYz1nLnNldChhLGMsZCkpfHwoaVtiXT1jKSksdm9pZCAwKX19LGNzczpmdW5jdGlvbihhLGIsYyxkKXt2YXIgZSxmLGcsaD1uLmNhbWVsQ2FzZShiKTtyZXR1cm4gYj1uLmNzc1Byb3BzW2hdfHwobi5jc3NQcm9wc1toXT1NYShoKXx8aCksZz1uLmNzc0hvb2tzW2JdfHxuLmNzc0hvb2tzW2hdLGcmJlwiZ2V0XCJpbiBnJiYoZT1nLmdldChhLCEwLGMpKSx2b2lkIDA9PT1lJiYoZT1GYShhLGIsZCkpLFwibm9ybWFsXCI9PT1lJiZiIGluIEphJiYoZT1KYVtiXSksXCJcIj09PWN8fGM/KGY9cGFyc2VGbG9hdChlKSxjPT09ITB8fGlzRmluaXRlKGYpP2Z8fDA6ZSk6ZX19KSxuLmVhY2goW1wiaGVpZ2h0XCIsXCJ3aWR0aFwiXSxmdW5jdGlvbihhLGIpe24uY3NzSG9va3NbYl09e2dldDpmdW5jdGlvbihhLGMsZCl7cmV0dXJuIGM/SGEudGVzdChuLmNzcyhhLFwiZGlzcGxheVwiKSkmJjA9PT1hLm9mZnNldFdpZHRoP0RhKGEsSWEsZnVuY3Rpb24oKXtyZXR1cm4gUGEoYSxiLGQpfSk6UGEoYSxiLGQpOnZvaWQgMH0sc2V0OmZ1bmN0aW9uKGEsYyxkKXt2YXIgZSxmPWQmJkNhKGEpLGc9ZCYmT2EoYSxiLGQsXCJib3JkZXItYm94XCI9PT1uLmNzcyhhLFwiYm94U2l6aW5nXCIsITEsZiksZik7cmV0dXJuIGcmJihlPVQuZXhlYyhjKSkmJlwicHhcIiE9PShlWzNdfHxcInB4XCIpJiYoYS5zdHlsZVtiXT1jLGM9bi5jc3MoYSxiKSksTmEoYSxjLGcpfX19KSxuLmNzc0hvb2tzLm1hcmdpbkxlZnQ9R2EobC5yZWxpYWJsZU1hcmdpbkxlZnQsZnVuY3Rpb24oYSxiKXtyZXR1cm4gYj8ocGFyc2VGbG9hdChGYShhLFwibWFyZ2luTGVmdFwiKSl8fGEuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdC1EYShhLHttYXJnaW5MZWZ0OjB9LGZ1bmN0aW9uKCl7cmV0dXJuIGEuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdH0pKStcInB4XCI6dm9pZCAwfSksbi5jc3NIb29rcy5tYXJnaW5SaWdodD1HYShsLnJlbGlhYmxlTWFyZ2luUmlnaHQsZnVuY3Rpb24oYSxiKXtyZXR1cm4gYj9EYShhLHtkaXNwbGF5OlwiaW5saW5lLWJsb2NrXCJ9LEZhLFthLFwibWFyZ2luUmlnaHRcIl0pOnZvaWQgMH0pLG4uZWFjaCh7bWFyZ2luOlwiXCIscGFkZGluZzpcIlwiLGJvcmRlcjpcIldpZHRoXCJ9LGZ1bmN0aW9uKGEsYil7bi5jc3NIb29rc1thK2JdPXtleHBhbmQ6ZnVuY3Rpb24oYyl7Zm9yKHZhciBkPTAsZT17fSxmPVwic3RyaW5nXCI9PXR5cGVvZiBjP2Muc3BsaXQoXCIgXCIpOltjXTs0PmQ7ZCsrKWVbYStVW2RdK2JdPWZbZF18fGZbZC0yXXx8ZlswXTtyZXR1cm4gZX19LEFhLnRlc3QoYSl8fChuLmNzc0hvb2tzW2ErYl0uc2V0PU5hKX0pLG4uZm4uZXh0ZW5kKHtjc3M6ZnVuY3Rpb24oYSxiKXtyZXR1cm4gSyh0aGlzLGZ1bmN0aW9uKGEsYixjKXt2YXIgZCxlLGY9e30sZz0wO2lmKG4uaXNBcnJheShiKSl7Zm9yKGQ9Q2EoYSksZT1iLmxlbmd0aDtlPmc7ZysrKWZbYltnXV09bi5jc3MoYSxiW2ddLCExLGQpO3JldHVybiBmfXJldHVybiB2b2lkIDAhPT1jP24uc3R5bGUoYSxiLGMpOm4uY3NzKGEsYil9LGEsYixhcmd1bWVudHMubGVuZ3RoPjEpfSxzaG93OmZ1bmN0aW9uKCl7cmV0dXJuIFFhKHRoaXMsITApfSxoaWRlOmZ1bmN0aW9uKCl7cmV0dXJuIFFhKHRoaXMpfSx0b2dnbGU6ZnVuY3Rpb24oYSl7cmV0dXJuXCJib29sZWFuXCI9PXR5cGVvZiBhP2E/dGhpcy5zaG93KCk6dGhpcy5oaWRlKCk6dGhpcy5lYWNoKGZ1bmN0aW9uKCl7Vih0aGlzKT9uKHRoaXMpLnNob3coKTpuKHRoaXMpLmhpZGUoKX0pfX0pO2Z1bmN0aW9uIFJhKGEsYixjLGQsZSl7cmV0dXJuIG5ldyBSYS5wcm90b3R5cGUuaW5pdChhLGIsYyxkLGUpfW4uVHdlZW49UmEsUmEucHJvdG90eXBlPXtjb25zdHJ1Y3RvcjpSYSxpbml0OmZ1bmN0aW9uKGEsYixjLGQsZSxmKXt0aGlzLmVsZW09YSx0aGlzLnByb3A9Yyx0aGlzLmVhc2luZz1lfHxuLmVhc2luZy5fZGVmYXVsdCx0aGlzLm9wdGlvbnM9Yix0aGlzLnN0YXJ0PXRoaXMubm93PXRoaXMuY3VyKCksdGhpcy5lbmQ9ZCx0aGlzLnVuaXQ9Znx8KG4uY3NzTnVtYmVyW2NdP1wiXCI6XCJweFwiKX0sY3VyOmZ1bmN0aW9uKCl7dmFyIGE9UmEucHJvcEhvb2tzW3RoaXMucHJvcF07cmV0dXJuIGEmJmEuZ2V0P2EuZ2V0KHRoaXMpOlJhLnByb3BIb29rcy5fZGVmYXVsdC5nZXQodGhpcyl9LHJ1bjpmdW5jdGlvbihhKXt2YXIgYixjPVJhLnByb3BIb29rc1t0aGlzLnByb3BdO3JldHVybiB0aGlzLm9wdGlvbnMuZHVyYXRpb24/dGhpcy5wb3M9Yj1uLmVhc2luZ1t0aGlzLmVhc2luZ10oYSx0aGlzLm9wdGlvbnMuZHVyYXRpb24qYSwwLDEsdGhpcy5vcHRpb25zLmR1cmF0aW9uKTp0aGlzLnBvcz1iPWEsdGhpcy5ub3c9KHRoaXMuZW5kLXRoaXMuc3RhcnQpKmIrdGhpcy5zdGFydCx0aGlzLm9wdGlvbnMuc3RlcCYmdGhpcy5vcHRpb25zLnN0ZXAuY2FsbCh0aGlzLmVsZW0sdGhpcy5ub3csdGhpcyksYyYmYy5zZXQ/Yy5zZXQodGhpcyk6UmEucHJvcEhvb2tzLl9kZWZhdWx0LnNldCh0aGlzKSx0aGlzfX0sUmEucHJvdG90eXBlLmluaXQucHJvdG90eXBlPVJhLnByb3RvdHlwZSxSYS5wcm9wSG9va3M9e19kZWZhdWx0OntnZXQ6ZnVuY3Rpb24oYSl7dmFyIGI7cmV0dXJuIDEhPT1hLmVsZW0ubm9kZVR5cGV8fG51bGwhPWEuZWxlbVthLnByb3BdJiZudWxsPT1hLmVsZW0uc3R5bGVbYS5wcm9wXT9hLmVsZW1bYS5wcm9wXTooYj1uLmNzcyhhLmVsZW0sYS5wcm9wLFwiXCIpLGImJlwiYXV0b1wiIT09Yj9iOjApfSxzZXQ6ZnVuY3Rpb24oYSl7bi5meC5zdGVwW2EucHJvcF0/bi5meC5zdGVwW2EucHJvcF0oYSk6MSE9PWEuZWxlbS5ub2RlVHlwZXx8bnVsbD09YS5lbGVtLnN0eWxlW24uY3NzUHJvcHNbYS5wcm9wXV0mJiFuLmNzc0hvb2tzW2EucHJvcF0/YS5lbGVtW2EucHJvcF09YS5ub3c6bi5zdHlsZShhLmVsZW0sYS5wcm9wLGEubm93K2EudW5pdCl9fX0sUmEucHJvcEhvb2tzLnNjcm9sbFRvcD1SYS5wcm9wSG9va3Muc2Nyb2xsTGVmdD17c2V0OmZ1bmN0aW9uKGEpe2EuZWxlbS5ub2RlVHlwZSYmYS5lbGVtLnBhcmVudE5vZGUmJihhLmVsZW1bYS5wcm9wXT1hLm5vdyl9fSxuLmVhc2luZz17bGluZWFyOmZ1bmN0aW9uKGEpe3JldHVybiBhfSxzd2luZzpmdW5jdGlvbihhKXtyZXR1cm4uNS1NYXRoLmNvcyhhKk1hdGguUEkpLzJ9LF9kZWZhdWx0Olwic3dpbmdcIn0sbi5meD1SYS5wcm90b3R5cGUuaW5pdCxuLmZ4LnN0ZXA9e307dmFyIFNhLFRhLFVhPS9eKD86dG9nZ2xlfHNob3d8aGlkZSkkLyxWYT0vcXVldWVIb29rcyQvO2Z1bmN0aW9uIFdhKCl7cmV0dXJuIGEuc2V0VGltZW91dChmdW5jdGlvbigpe1NhPXZvaWQgMH0pLFNhPW4ubm93KCl9ZnVuY3Rpb24gWGEoYSxiKXt2YXIgYyxkPTAsZT17aGVpZ2h0OmF9O2ZvcihiPWI/MTowOzQ+ZDtkKz0yLWIpYz1VW2RdLGVbXCJtYXJnaW5cIitjXT1lW1wicGFkZGluZ1wiK2NdPWE7cmV0dXJuIGImJihlLm9wYWNpdHk9ZS53aWR0aD1hKSxlfWZ1bmN0aW9uIFlhKGEsYixjKXtmb3IodmFyIGQsZT0oX2EudHdlZW5lcnNbYl18fFtdKS5jb25jYXQoX2EudHdlZW5lcnNbXCIqXCJdKSxmPTAsZz1lLmxlbmd0aDtnPmY7ZisrKWlmKGQ9ZVtmXS5jYWxsKGMsYixhKSlyZXR1cm4gZH1mdW5jdGlvbiBaYShhLGIsYyl7dmFyIGQsZSxmLGcsaCxpLGosayxsPXRoaXMsbT17fSxvPWEuc3R5bGUscD1hLm5vZGVUeXBlJiZWKGEpLHE9Ti5nZXQoYSxcImZ4c2hvd1wiKTtjLnF1ZXVlfHwoaD1uLl9xdWV1ZUhvb2tzKGEsXCJmeFwiKSxudWxsPT1oLnVucXVldWVkJiYoaC51bnF1ZXVlZD0wLGk9aC5lbXB0eS5maXJlLGguZW1wdHkuZmlyZT1mdW5jdGlvbigpe2gudW5xdWV1ZWR8fGkoKX0pLGgudW5xdWV1ZWQrKyxsLmFsd2F5cyhmdW5jdGlvbigpe2wuYWx3YXlzKGZ1bmN0aW9uKCl7aC51bnF1ZXVlZC0tLG4ucXVldWUoYSxcImZ4XCIpLmxlbmd0aHx8aC5lbXB0eS5maXJlKCl9KX0pKSwxPT09YS5ub2RlVHlwZSYmKFwiaGVpZ2h0XCJpbiBifHxcIndpZHRoXCJpbiBiKSYmKGMub3ZlcmZsb3c9W28ub3ZlcmZsb3csby5vdmVyZmxvd1gsby5vdmVyZmxvd1ldLGo9bi5jc3MoYSxcImRpc3BsYXlcIiksaz1cIm5vbmVcIj09PWo/Ti5nZXQoYSxcIm9sZGRpc3BsYXlcIil8fHphKGEubm9kZU5hbWUpOmosXCJpbmxpbmVcIj09PWsmJlwibm9uZVwiPT09bi5jc3MoYSxcImZsb2F0XCIpJiYoby5kaXNwbGF5PVwiaW5saW5lLWJsb2NrXCIpKSxjLm92ZXJmbG93JiYoby5vdmVyZmxvdz1cImhpZGRlblwiLGwuYWx3YXlzKGZ1bmN0aW9uKCl7by5vdmVyZmxvdz1jLm92ZXJmbG93WzBdLG8ub3ZlcmZsb3dYPWMub3ZlcmZsb3dbMV0sby5vdmVyZmxvd1k9Yy5vdmVyZmxvd1syXX0pKTtmb3IoZCBpbiBiKWlmKGU9YltkXSxVYS5leGVjKGUpKXtpZihkZWxldGUgYltkXSxmPWZ8fFwidG9nZ2xlXCI9PT1lLGU9PT0ocD9cImhpZGVcIjpcInNob3dcIikpe2lmKFwic2hvd1wiIT09ZXx8IXF8fHZvaWQgMD09PXFbZF0pY29udGludWU7cD0hMH1tW2RdPXEmJnFbZF18fG4uc3R5bGUoYSxkKX1lbHNlIGo9dm9pZCAwO2lmKG4uaXNFbXB0eU9iamVjdChtKSlcImlubGluZVwiPT09KFwibm9uZVwiPT09aj96YShhLm5vZGVOYW1lKTpqKSYmKG8uZGlzcGxheT1qKTtlbHNle3E/XCJoaWRkZW5cImluIHEmJihwPXEuaGlkZGVuKTpxPU4uYWNjZXNzKGEsXCJmeHNob3dcIix7fSksZiYmKHEuaGlkZGVuPSFwKSxwP24oYSkuc2hvdygpOmwuZG9uZShmdW5jdGlvbigpe24oYSkuaGlkZSgpfSksbC5kb25lKGZ1bmN0aW9uKCl7dmFyIGI7Ti5yZW1vdmUoYSxcImZ4c2hvd1wiKTtmb3IoYiBpbiBtKW4uc3R5bGUoYSxiLG1bYl0pfSk7Zm9yKGQgaW4gbSlnPVlhKHA/cVtkXTowLGQsbCksZCBpbiBxfHwocVtkXT1nLnN0YXJ0LHAmJihnLmVuZD1nLnN0YXJ0LGcuc3RhcnQ9XCJ3aWR0aFwiPT09ZHx8XCJoZWlnaHRcIj09PWQ/MTowKSl9fWZ1bmN0aW9uICRhKGEsYil7dmFyIGMsZCxlLGYsZztmb3IoYyBpbiBhKWlmKGQ9bi5jYW1lbENhc2UoYyksZT1iW2RdLGY9YVtjXSxuLmlzQXJyYXkoZikmJihlPWZbMV0sZj1hW2NdPWZbMF0pLGMhPT1kJiYoYVtkXT1mLGRlbGV0ZSBhW2NdKSxnPW4uY3NzSG9va3NbZF0sZyYmXCJleHBhbmRcImluIGcpe2Y9Zy5leHBhbmQoZiksZGVsZXRlIGFbZF07Zm9yKGMgaW4gZiljIGluIGF8fChhW2NdPWZbY10sYltjXT1lKX1lbHNlIGJbZF09ZX1mdW5jdGlvbiBfYShhLGIsYyl7dmFyIGQsZSxmPTAsZz1fYS5wcmVmaWx0ZXJzLmxlbmd0aCxoPW4uRGVmZXJyZWQoKS5hbHdheXMoZnVuY3Rpb24oKXtkZWxldGUgaS5lbGVtfSksaT1mdW5jdGlvbigpe2lmKGUpcmV0dXJuITE7Zm9yKHZhciBiPVNhfHxXYSgpLGM9TWF0aC5tYXgoMCxqLnN0YXJ0VGltZStqLmR1cmF0aW9uLWIpLGQ9Yy9qLmR1cmF0aW9ufHwwLGY9MS1kLGc9MCxpPWoudHdlZW5zLmxlbmd0aDtpPmc7ZysrKWoudHdlZW5zW2ddLnJ1bihmKTtyZXR1cm4gaC5ub3RpZnlXaXRoKGEsW2osZixjXSksMT5mJiZpP2M6KGgucmVzb2x2ZVdpdGgoYSxbal0pLCExKX0saj1oLnByb21pc2Uoe2VsZW06YSxwcm9wczpuLmV4dGVuZCh7fSxiKSxvcHRzOm4uZXh0ZW5kKCEwLHtzcGVjaWFsRWFzaW5nOnt9LGVhc2luZzpuLmVhc2luZy5fZGVmYXVsdH0sYyksb3JpZ2luYWxQcm9wZXJ0aWVzOmIsb3JpZ2luYWxPcHRpb25zOmMsc3RhcnRUaW1lOlNhfHxXYSgpLGR1cmF0aW9uOmMuZHVyYXRpb24sdHdlZW5zOltdLGNyZWF0ZVR3ZWVuOmZ1bmN0aW9uKGIsYyl7dmFyIGQ9bi5Ud2VlbihhLGoub3B0cyxiLGMsai5vcHRzLnNwZWNpYWxFYXNpbmdbYl18fGoub3B0cy5lYXNpbmcpO3JldHVybiBqLnR3ZWVucy5wdXNoKGQpLGR9LHN0b3A6ZnVuY3Rpb24oYil7dmFyIGM9MCxkPWI/ai50d2VlbnMubGVuZ3RoOjA7aWYoZSlyZXR1cm4gdGhpcztmb3IoZT0hMDtkPmM7YysrKWoudHdlZW5zW2NdLnJ1bigxKTtyZXR1cm4gYj8oaC5ub3RpZnlXaXRoKGEsW2osMSwwXSksaC5yZXNvbHZlV2l0aChhLFtqLGJdKSk6aC5yZWplY3RXaXRoKGEsW2osYl0pLHRoaXN9fSksaz1qLnByb3BzO2ZvcigkYShrLGoub3B0cy5zcGVjaWFsRWFzaW5nKTtnPmY7ZisrKWlmKGQ9X2EucHJlZmlsdGVyc1tmXS5jYWxsKGosYSxrLGoub3B0cykpcmV0dXJuIG4uaXNGdW5jdGlvbihkLnN0b3ApJiYobi5fcXVldWVIb29rcyhqLmVsZW0sai5vcHRzLnF1ZXVlKS5zdG9wPW4ucHJveHkoZC5zdG9wLGQpKSxkO3JldHVybiBuLm1hcChrLFlhLGopLG4uaXNGdW5jdGlvbihqLm9wdHMuc3RhcnQpJiZqLm9wdHMuc3RhcnQuY2FsbChhLGopLG4uZngudGltZXIobi5leHRlbmQoaSx7ZWxlbTphLGFuaW06aixxdWV1ZTpqLm9wdHMucXVldWV9KSksai5wcm9ncmVzcyhqLm9wdHMucHJvZ3Jlc3MpLmRvbmUoai5vcHRzLmRvbmUsai5vcHRzLmNvbXBsZXRlKS5mYWlsKGoub3B0cy5mYWlsKS5hbHdheXMoai5vcHRzLmFsd2F5cyl9bi5BbmltYXRpb249bi5leHRlbmQoX2Ese3R3ZWVuZXJzOntcIipcIjpbZnVuY3Rpb24oYSxiKXt2YXIgYz10aGlzLmNyZWF0ZVR3ZWVuKGEsYik7cmV0dXJuIFcoYy5lbGVtLGEsVC5leGVjKGIpLGMpLGN9XX0sdHdlZW5lcjpmdW5jdGlvbihhLGIpe24uaXNGdW5jdGlvbihhKT8oYj1hLGE9W1wiKlwiXSk6YT1hLm1hdGNoKEcpO2Zvcih2YXIgYyxkPTAsZT1hLmxlbmd0aDtlPmQ7ZCsrKWM9YVtkXSxfYS50d2VlbmVyc1tjXT1fYS50d2VlbmVyc1tjXXx8W10sX2EudHdlZW5lcnNbY10udW5zaGlmdChiKX0scHJlZmlsdGVyczpbWmFdLHByZWZpbHRlcjpmdW5jdGlvbihhLGIpe2I/X2EucHJlZmlsdGVycy51bnNoaWZ0KGEpOl9hLnByZWZpbHRlcnMucHVzaChhKX19KSxuLnNwZWVkPWZ1bmN0aW9uKGEsYixjKXt2YXIgZD1hJiZcIm9iamVjdFwiPT10eXBlb2YgYT9uLmV4dGVuZCh7fSxhKTp7Y29tcGxldGU6Y3x8IWMmJmJ8fG4uaXNGdW5jdGlvbihhKSYmYSxkdXJhdGlvbjphLGVhc2luZzpjJiZifHxiJiYhbi5pc0Z1bmN0aW9uKGIpJiZifTtyZXR1cm4gZC5kdXJhdGlvbj1uLmZ4Lm9mZj8wOlwibnVtYmVyXCI9PXR5cGVvZiBkLmR1cmF0aW9uP2QuZHVyYXRpb246ZC5kdXJhdGlvbiBpbiBuLmZ4LnNwZWVkcz9uLmZ4LnNwZWVkc1tkLmR1cmF0aW9uXTpuLmZ4LnNwZWVkcy5fZGVmYXVsdCwobnVsbD09ZC5xdWV1ZXx8ZC5xdWV1ZT09PSEwKSYmKGQucXVldWU9XCJmeFwiKSxkLm9sZD1kLmNvbXBsZXRlLGQuY29tcGxldGU9ZnVuY3Rpb24oKXtuLmlzRnVuY3Rpb24oZC5vbGQpJiZkLm9sZC5jYWxsKHRoaXMpLGQucXVldWUmJm4uZGVxdWV1ZSh0aGlzLGQucXVldWUpfSxkfSxuLmZuLmV4dGVuZCh7ZmFkZVRvOmZ1bmN0aW9uKGEsYixjLGQpe3JldHVybiB0aGlzLmZpbHRlcihWKS5jc3MoXCJvcGFjaXR5XCIsMCkuc2hvdygpLmVuZCgpLmFuaW1hdGUoe29wYWNpdHk6Yn0sYSxjLGQpfSxhbmltYXRlOmZ1bmN0aW9uKGEsYixjLGQpe3ZhciBlPW4uaXNFbXB0eU9iamVjdChhKSxmPW4uc3BlZWQoYixjLGQpLGc9ZnVuY3Rpb24oKXt2YXIgYj1fYSh0aGlzLG4uZXh0ZW5kKHt9LGEpLGYpOyhlfHxOLmdldCh0aGlzLFwiZmluaXNoXCIpKSYmYi5zdG9wKCEwKX07cmV0dXJuIGcuZmluaXNoPWcsZXx8Zi5xdWV1ZT09PSExP3RoaXMuZWFjaChnKTp0aGlzLnF1ZXVlKGYucXVldWUsZyl9LHN0b3A6ZnVuY3Rpb24oYSxiLGMpe3ZhciBkPWZ1bmN0aW9uKGEpe3ZhciBiPWEuc3RvcDtkZWxldGUgYS5zdG9wLGIoYyl9O3JldHVyblwic3RyaW5nXCIhPXR5cGVvZiBhJiYoYz1iLGI9YSxhPXZvaWQgMCksYiYmYSE9PSExJiZ0aGlzLnF1ZXVlKGF8fFwiZnhcIixbXSksdGhpcy5lYWNoKGZ1bmN0aW9uKCl7dmFyIGI9ITAsZT1udWxsIT1hJiZhK1wicXVldWVIb29rc1wiLGY9bi50aW1lcnMsZz1OLmdldCh0aGlzKTtpZihlKWdbZV0mJmdbZV0uc3RvcCYmZChnW2VdKTtlbHNlIGZvcihlIGluIGcpZ1tlXSYmZ1tlXS5zdG9wJiZWYS50ZXN0KGUpJiZkKGdbZV0pO2ZvcihlPWYubGVuZ3RoO2UtLTspZltlXS5lbGVtIT09dGhpc3x8bnVsbCE9YSYmZltlXS5xdWV1ZSE9PWF8fChmW2VdLmFuaW0uc3RvcChjKSxiPSExLGYuc3BsaWNlKGUsMSkpOyhifHwhYykmJm4uZGVxdWV1ZSh0aGlzLGEpfSl9LGZpbmlzaDpmdW5jdGlvbihhKXtyZXR1cm4gYSE9PSExJiYoYT1hfHxcImZ4XCIpLHRoaXMuZWFjaChmdW5jdGlvbigpe3ZhciBiLGM9Ti5nZXQodGhpcyksZD1jW2ErXCJxdWV1ZVwiXSxlPWNbYStcInF1ZXVlSG9va3NcIl0sZj1uLnRpbWVycyxnPWQ/ZC5sZW5ndGg6MDtmb3IoYy5maW5pc2g9ITAsbi5xdWV1ZSh0aGlzLGEsW10pLGUmJmUuc3RvcCYmZS5zdG9wLmNhbGwodGhpcywhMCksYj1mLmxlbmd0aDtiLS07KWZbYl0uZWxlbT09PXRoaXMmJmZbYl0ucXVldWU9PT1hJiYoZltiXS5hbmltLnN0b3AoITApLGYuc3BsaWNlKGIsMSkpO2ZvcihiPTA7Zz5iO2IrKylkW2JdJiZkW2JdLmZpbmlzaCYmZFtiXS5maW5pc2guY2FsbCh0aGlzKTtkZWxldGUgYy5maW5pc2h9KX19KSxuLmVhY2goW1widG9nZ2xlXCIsXCJzaG93XCIsXCJoaWRlXCJdLGZ1bmN0aW9uKGEsYil7dmFyIGM9bi5mbltiXTtuLmZuW2JdPWZ1bmN0aW9uKGEsZCxlKXtyZXR1cm4gbnVsbD09YXx8XCJib29sZWFuXCI9PXR5cGVvZiBhP2MuYXBwbHkodGhpcyxhcmd1bWVudHMpOnRoaXMuYW5pbWF0ZShYYShiLCEwKSxhLGQsZSl9fSksbi5lYWNoKHtzbGlkZURvd246WGEoXCJzaG93XCIpLHNsaWRlVXA6WGEoXCJoaWRlXCIpLHNsaWRlVG9nZ2xlOlhhKFwidG9nZ2xlXCIpLGZhZGVJbjp7b3BhY2l0eTpcInNob3dcIn0sZmFkZU91dDp7b3BhY2l0eTpcImhpZGVcIn0sZmFkZVRvZ2dsZTp7b3BhY2l0eTpcInRvZ2dsZVwifX0sZnVuY3Rpb24oYSxiKXtuLmZuW2FdPWZ1bmN0aW9uKGEsYyxkKXtyZXR1cm4gdGhpcy5hbmltYXRlKGIsYSxjLGQpfX0pLG4udGltZXJzPVtdLG4uZngudGljaz1mdW5jdGlvbigpe3ZhciBhLGI9MCxjPW4udGltZXJzO2ZvcihTYT1uLm5vdygpO2I8Yy5sZW5ndGg7YisrKWE9Y1tiXSxhKCl8fGNbYl0hPT1hfHxjLnNwbGljZShiLS0sMSk7Yy5sZW5ndGh8fG4uZnguc3RvcCgpLFNhPXZvaWQgMH0sbi5meC50aW1lcj1mdW5jdGlvbihhKXtuLnRpbWVycy5wdXNoKGEpLGEoKT9uLmZ4LnN0YXJ0KCk6bi50aW1lcnMucG9wKCl9LG4uZnguaW50ZXJ2YWw9MTMsbi5meC5zdGFydD1mdW5jdGlvbigpe1RhfHwoVGE9YS5zZXRJbnRlcnZhbChuLmZ4LnRpY2ssbi5meC5pbnRlcnZhbCkpfSxuLmZ4LnN0b3A9ZnVuY3Rpb24oKXthLmNsZWFySW50ZXJ2YWwoVGEpLFRhPW51bGx9LG4uZnguc3BlZWRzPXtzbG93OjYwMCxmYXN0OjIwMCxfZGVmYXVsdDo0MDB9LG4uZm4uZGVsYXk9ZnVuY3Rpb24oYixjKXtyZXR1cm4gYj1uLmZ4P24uZnguc3BlZWRzW2JdfHxiOmIsYz1jfHxcImZ4XCIsdGhpcy5xdWV1ZShjLGZ1bmN0aW9uKGMsZCl7dmFyIGU9YS5zZXRUaW1lb3V0KGMsYik7ZC5zdG9wPWZ1bmN0aW9uKCl7YS5jbGVhclRpbWVvdXQoZSl9fSl9LGZ1bmN0aW9uKCl7dmFyIGE9ZC5jcmVhdGVFbGVtZW50KFwiaW5wdXRcIiksYj1kLmNyZWF0ZUVsZW1lbnQoXCJzZWxlY3RcIiksYz1iLmFwcGVuZENoaWxkKGQuY3JlYXRlRWxlbWVudChcIm9wdGlvblwiKSk7YS50eXBlPVwiY2hlY2tib3hcIixsLmNoZWNrT249XCJcIiE9PWEudmFsdWUsbC5vcHRTZWxlY3RlZD1jLnNlbGVjdGVkLGIuZGlzYWJsZWQ9ITAsbC5vcHREaXNhYmxlZD0hYy5kaXNhYmxlZCxhPWQuY3JlYXRlRWxlbWVudChcImlucHV0XCIpLGEudmFsdWU9XCJ0XCIsYS50eXBlPVwicmFkaW9cIixsLnJhZGlvVmFsdWU9XCJ0XCI9PT1hLnZhbHVlfSgpO3ZhciBhYixiYj1uLmV4cHIuYXR0ckhhbmRsZTtuLmZuLmV4dGVuZCh7YXR0cjpmdW5jdGlvbihhLGIpe3JldHVybiBLKHRoaXMsbi5hdHRyLGEsYixhcmd1bWVudHMubGVuZ3RoPjEpfSxyZW1vdmVBdHRyOmZ1bmN0aW9uKGEpe3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXtuLnJlbW92ZUF0dHIodGhpcyxhKX0pfX0pLG4uZXh0ZW5kKHthdHRyOmZ1bmN0aW9uKGEsYixjKXt2YXIgZCxlLGY9YS5ub2RlVHlwZTtpZigzIT09ZiYmOCE9PWYmJjIhPT1mKXJldHVyblwidW5kZWZpbmVkXCI9PXR5cGVvZiBhLmdldEF0dHJpYnV0ZT9uLnByb3AoYSxiLGMpOigxPT09ZiYmbi5pc1hNTERvYyhhKXx8KGI9Yi50b0xvd2VyQ2FzZSgpLGU9bi5hdHRySG9va3NbYl18fChuLmV4cHIubWF0Y2guYm9vbC50ZXN0KGIpP2FiOnZvaWQgMCkpLHZvaWQgMCE9PWM/bnVsbD09PWM/dm9pZCBuLnJlbW92ZUF0dHIoYSxiKTplJiZcInNldFwiaW4gZSYmdm9pZCAwIT09KGQ9ZS5zZXQoYSxjLGIpKT9kOihhLnNldEF0dHJpYnV0ZShiLGMrXCJcIiksYyk6ZSYmXCJnZXRcImluIGUmJm51bGwhPT0oZD1lLmdldChhLGIpKT9kOihkPW4uZmluZC5hdHRyKGEsYiksbnVsbD09ZD92b2lkIDA6ZCkpfSxhdHRySG9va3M6e3R5cGU6e3NldDpmdW5jdGlvbihhLGIpe2lmKCFsLnJhZGlvVmFsdWUmJlwicmFkaW9cIj09PWImJm4ubm9kZU5hbWUoYSxcImlucHV0XCIpKXt2YXIgYz1hLnZhbHVlO3JldHVybiBhLnNldEF0dHJpYnV0ZShcInR5cGVcIixiKSxjJiYoYS52YWx1ZT1jKSxifX19fSxyZW1vdmVBdHRyOmZ1bmN0aW9uKGEsYil7dmFyIGMsZCxlPTAsZj1iJiZiLm1hdGNoKEcpO2lmKGYmJjE9PT1hLm5vZGVUeXBlKXdoaWxlKGM9ZltlKytdKWQ9bi5wcm9wRml4W2NdfHxjLG4uZXhwci5tYXRjaC5ib29sLnRlc3QoYykmJihhW2RdPSExKSxhLnJlbW92ZUF0dHJpYnV0ZShjKX19KSxhYj17c2V0OmZ1bmN0aW9uKGEsYixjKXtyZXR1cm4gYj09PSExP24ucmVtb3ZlQXR0cihhLGMpOmEuc2V0QXR0cmlidXRlKGMsYyksY319LG4uZWFjaChuLmV4cHIubWF0Y2guYm9vbC5zb3VyY2UubWF0Y2goL1xcdysvZyksZnVuY3Rpb24oYSxiKXt2YXIgYz1iYltiXXx8bi5maW5kLmF0dHI7YmJbYl09ZnVuY3Rpb24oYSxiLGQpe3ZhciBlLGY7cmV0dXJuIGR8fChmPWJiW2JdLGJiW2JdPWUsZT1udWxsIT1jKGEsYixkKT9iLnRvTG93ZXJDYXNlKCk6bnVsbCxiYltiXT1mKSxlfX0pO3ZhciBjYj0vXig/OmlucHV0fHNlbGVjdHx0ZXh0YXJlYXxidXR0b24pJC9pLGRiPS9eKD86YXxhcmVhKSQvaTtuLmZuLmV4dGVuZCh7cHJvcDpmdW5jdGlvbihhLGIpe3JldHVybiBLKHRoaXMsbi5wcm9wLGEsYixhcmd1bWVudHMubGVuZ3RoPjEpfSxyZW1vdmVQcm9wOmZ1bmN0aW9uKGEpe3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXtkZWxldGUgdGhpc1tuLnByb3BGaXhbYV18fGFdfSl9fSksbi5leHRlbmQoe3Byb3A6ZnVuY3Rpb24oYSxiLGMpe3ZhciBkLGUsZj1hLm5vZGVUeXBlO2lmKDMhPT1mJiY4IT09ZiYmMiE9PWYpcmV0dXJuIDE9PT1mJiZuLmlzWE1MRG9jKGEpfHwoYj1uLnByb3BGaXhbYl18fGIsXHJcbmU9bi5wcm9wSG9va3NbYl0pLHZvaWQgMCE9PWM/ZSYmXCJzZXRcImluIGUmJnZvaWQgMCE9PShkPWUuc2V0KGEsYyxiKSk/ZDphW2JdPWM6ZSYmXCJnZXRcImluIGUmJm51bGwhPT0oZD1lLmdldChhLGIpKT9kOmFbYl19LHByb3BIb29rczp7dGFiSW5kZXg6e2dldDpmdW5jdGlvbihhKXt2YXIgYj1uLmZpbmQuYXR0cihhLFwidGFiaW5kZXhcIik7cmV0dXJuIGI/cGFyc2VJbnQoYiwxMCk6Y2IudGVzdChhLm5vZGVOYW1lKXx8ZGIudGVzdChhLm5vZGVOYW1lKSYmYS5ocmVmPzA6LTF9fX0scHJvcEZpeDp7XCJmb3JcIjpcImh0bWxGb3JcIixcImNsYXNzXCI6XCJjbGFzc05hbWVcIn19KSxsLm9wdFNlbGVjdGVkfHwobi5wcm9wSG9va3Muc2VsZWN0ZWQ9e2dldDpmdW5jdGlvbihhKXt2YXIgYj1hLnBhcmVudE5vZGU7cmV0dXJuIGImJmIucGFyZW50Tm9kZSYmYi5wYXJlbnROb2RlLnNlbGVjdGVkSW5kZXgsbnVsbH19KSxuLmVhY2goW1widGFiSW5kZXhcIixcInJlYWRPbmx5XCIsXCJtYXhMZW5ndGhcIixcImNlbGxTcGFjaW5nXCIsXCJjZWxsUGFkZGluZ1wiLFwicm93U3BhblwiLFwiY29sU3BhblwiLFwidXNlTWFwXCIsXCJmcmFtZUJvcmRlclwiLFwiY29udGVudEVkaXRhYmxlXCJdLGZ1bmN0aW9uKCl7bi5wcm9wRml4W3RoaXMudG9Mb3dlckNhc2UoKV09dGhpc30pO3ZhciBlYj0vW1xcdFxcclxcblxcZl0vZztmdW5jdGlvbiBmYihhKXtyZXR1cm4gYS5nZXRBdHRyaWJ1dGUmJmEuZ2V0QXR0cmlidXRlKFwiY2xhc3NcIil8fFwiXCJ9bi5mbi5leHRlbmQoe2FkZENsYXNzOmZ1bmN0aW9uKGEpe3ZhciBiLGMsZCxlLGYsZyxoLGk9MDtpZihuLmlzRnVuY3Rpb24oYSkpcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbihiKXtuKHRoaXMpLmFkZENsYXNzKGEuY2FsbCh0aGlzLGIsZmIodGhpcykpKX0pO2lmKFwic3RyaW5nXCI9PXR5cGVvZiBhJiZhKXtiPWEubWF0Y2goRyl8fFtdO3doaWxlKGM9dGhpc1tpKytdKWlmKGU9ZmIoYyksZD0xPT09Yy5ub2RlVHlwZSYmKFwiIFwiK2UrXCIgXCIpLnJlcGxhY2UoZWIsXCIgXCIpKXtnPTA7d2hpbGUoZj1iW2crK10pZC5pbmRleE9mKFwiIFwiK2YrXCIgXCIpPDAmJihkKz1mK1wiIFwiKTtoPW4udHJpbShkKSxlIT09aCYmYy5zZXRBdHRyaWJ1dGUoXCJjbGFzc1wiLGgpfX1yZXR1cm4gdGhpc30scmVtb3ZlQ2xhc3M6ZnVuY3Rpb24oYSl7dmFyIGIsYyxkLGUsZixnLGgsaT0wO2lmKG4uaXNGdW5jdGlvbihhKSlyZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uKGIpe24odGhpcykucmVtb3ZlQ2xhc3MoYS5jYWxsKHRoaXMsYixmYih0aGlzKSkpfSk7aWYoIWFyZ3VtZW50cy5sZW5ndGgpcmV0dXJuIHRoaXMuYXR0cihcImNsYXNzXCIsXCJcIik7aWYoXCJzdHJpbmdcIj09dHlwZW9mIGEmJmEpe2I9YS5tYXRjaChHKXx8W107d2hpbGUoYz10aGlzW2krK10paWYoZT1mYihjKSxkPTE9PT1jLm5vZGVUeXBlJiYoXCIgXCIrZStcIiBcIikucmVwbGFjZShlYixcIiBcIikpe2c9MDt3aGlsZShmPWJbZysrXSl3aGlsZShkLmluZGV4T2YoXCIgXCIrZitcIiBcIik+LTEpZD1kLnJlcGxhY2UoXCIgXCIrZitcIiBcIixcIiBcIik7aD1uLnRyaW0oZCksZSE9PWgmJmMuc2V0QXR0cmlidXRlKFwiY2xhc3NcIixoKX19cmV0dXJuIHRoaXN9LHRvZ2dsZUNsYXNzOmZ1bmN0aW9uKGEsYil7dmFyIGM9dHlwZW9mIGE7cmV0dXJuXCJib29sZWFuXCI9PXR5cGVvZiBiJiZcInN0cmluZ1wiPT09Yz9iP3RoaXMuYWRkQ2xhc3MoYSk6dGhpcy5yZW1vdmVDbGFzcyhhKTpuLmlzRnVuY3Rpb24oYSk/dGhpcy5lYWNoKGZ1bmN0aW9uKGMpe24odGhpcykudG9nZ2xlQ2xhc3MoYS5jYWxsKHRoaXMsYyxmYih0aGlzKSxiKSxiKX0pOnRoaXMuZWFjaChmdW5jdGlvbigpe3ZhciBiLGQsZSxmO2lmKFwic3RyaW5nXCI9PT1jKXtkPTAsZT1uKHRoaXMpLGY9YS5tYXRjaChHKXx8W107d2hpbGUoYj1mW2QrK10pZS5oYXNDbGFzcyhiKT9lLnJlbW92ZUNsYXNzKGIpOmUuYWRkQ2xhc3MoYil9ZWxzZSh2b2lkIDA9PT1hfHxcImJvb2xlYW5cIj09PWMpJiYoYj1mYih0aGlzKSxiJiZOLnNldCh0aGlzLFwiX19jbGFzc05hbWVfX1wiLGIpLHRoaXMuc2V0QXR0cmlidXRlJiZ0aGlzLnNldEF0dHJpYnV0ZShcImNsYXNzXCIsYnx8YT09PSExP1wiXCI6Ti5nZXQodGhpcyxcIl9fY2xhc3NOYW1lX19cIil8fFwiXCIpKX0pfSxoYXNDbGFzczpmdW5jdGlvbihhKXt2YXIgYixjLGQ9MDtiPVwiIFwiK2ErXCIgXCI7d2hpbGUoYz10aGlzW2QrK10paWYoMT09PWMubm9kZVR5cGUmJihcIiBcIitmYihjKStcIiBcIikucmVwbGFjZShlYixcIiBcIikuaW5kZXhPZihiKT4tMSlyZXR1cm4hMDtyZXR1cm4hMX19KTt2YXIgZ2I9L1xcci9nO24uZm4uZXh0ZW5kKHt2YWw6ZnVuY3Rpb24oYSl7dmFyIGIsYyxkLGU9dGhpc1swXTt7aWYoYXJndW1lbnRzLmxlbmd0aClyZXR1cm4gZD1uLmlzRnVuY3Rpb24oYSksdGhpcy5lYWNoKGZ1bmN0aW9uKGMpe3ZhciBlOzE9PT10aGlzLm5vZGVUeXBlJiYoZT1kP2EuY2FsbCh0aGlzLGMsbih0aGlzKS52YWwoKSk6YSxudWxsPT1lP2U9XCJcIjpcIm51bWJlclwiPT10eXBlb2YgZT9lKz1cIlwiOm4uaXNBcnJheShlKSYmKGU9bi5tYXAoZSxmdW5jdGlvbihhKXtyZXR1cm4gbnVsbD09YT9cIlwiOmErXCJcIn0pKSxiPW4udmFsSG9va3NbdGhpcy50eXBlXXx8bi52YWxIb29rc1t0aGlzLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCldLGImJlwic2V0XCJpbiBiJiZ2b2lkIDAhPT1iLnNldCh0aGlzLGUsXCJ2YWx1ZVwiKXx8KHRoaXMudmFsdWU9ZSkpfSk7aWYoZSlyZXR1cm4gYj1uLnZhbEhvb2tzW2UudHlwZV18fG4udmFsSG9va3NbZS5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpXSxiJiZcImdldFwiaW4gYiYmdm9pZCAwIT09KGM9Yi5nZXQoZSxcInZhbHVlXCIpKT9jOihjPWUudmFsdWUsXCJzdHJpbmdcIj09dHlwZW9mIGM/Yy5yZXBsYWNlKGdiLFwiXCIpOm51bGw9PWM/XCJcIjpjKX19fSksbi5leHRlbmQoe3ZhbEhvb2tzOntvcHRpb246e2dldDpmdW5jdGlvbihhKXtyZXR1cm4gbi50cmltKGEudmFsdWUpfX0sc2VsZWN0OntnZXQ6ZnVuY3Rpb24oYSl7Zm9yKHZhciBiLGMsZD1hLm9wdGlvbnMsZT1hLnNlbGVjdGVkSW5kZXgsZj1cInNlbGVjdC1vbmVcIj09PWEudHlwZXx8MD5lLGc9Zj9udWxsOltdLGg9Zj9lKzE6ZC5sZW5ndGgsaT0wPmU/aDpmP2U6MDtoPmk7aSsrKWlmKGM9ZFtpXSwoYy5zZWxlY3RlZHx8aT09PWUpJiYobC5vcHREaXNhYmxlZD8hYy5kaXNhYmxlZDpudWxsPT09Yy5nZXRBdHRyaWJ1dGUoXCJkaXNhYmxlZFwiKSkmJighYy5wYXJlbnROb2RlLmRpc2FibGVkfHwhbi5ub2RlTmFtZShjLnBhcmVudE5vZGUsXCJvcHRncm91cFwiKSkpe2lmKGI9bihjKS52YWwoKSxmKXJldHVybiBiO2cucHVzaChiKX1yZXR1cm4gZ30sc2V0OmZ1bmN0aW9uKGEsYil7dmFyIGMsZCxlPWEub3B0aW9ucyxmPW4ubWFrZUFycmF5KGIpLGc9ZS5sZW5ndGg7d2hpbGUoZy0tKWQ9ZVtnXSwoZC5zZWxlY3RlZD1uLmluQXJyYXkobi52YWxIb29rcy5vcHRpb24uZ2V0KGQpLGYpPi0xKSYmKGM9ITApO3JldHVybiBjfHwoYS5zZWxlY3RlZEluZGV4PS0xKSxmfX19fSksbi5lYWNoKFtcInJhZGlvXCIsXCJjaGVja2JveFwiXSxmdW5jdGlvbigpe24udmFsSG9va3NbdGhpc109e3NldDpmdW5jdGlvbihhLGIpe3JldHVybiBuLmlzQXJyYXkoYik/YS5jaGVja2VkPW4uaW5BcnJheShuKGEpLnZhbCgpLGIpPi0xOnZvaWQgMH19LGwuY2hlY2tPbnx8KG4udmFsSG9va3NbdGhpc10uZ2V0PWZ1bmN0aW9uKGEpe3JldHVybiBudWxsPT09YS5nZXRBdHRyaWJ1dGUoXCJ2YWx1ZVwiKT9cIm9uXCI6YS52YWx1ZX0pfSk7dmFyIGhiPS9eKD86Zm9jdXNpbmZvY3VzfGZvY3Vzb3V0Ymx1cikkLztuLmV4dGVuZChuLmV2ZW50LHt0cmlnZ2VyOmZ1bmN0aW9uKGIsYyxlLGYpe3ZhciBnLGgsaSxqLGwsbSxvLHA9W2V8fGRdLHE9ay5jYWxsKGIsXCJ0eXBlXCIpP2IudHlwZTpiLHI9ay5jYWxsKGIsXCJuYW1lc3BhY2VcIik/Yi5uYW1lc3BhY2Uuc3BsaXQoXCIuXCIpOltdO2lmKGg9aT1lPWV8fGQsMyE9PWUubm9kZVR5cGUmJjghPT1lLm5vZGVUeXBlJiYhaGIudGVzdChxK24uZXZlbnQudHJpZ2dlcmVkKSYmKHEuaW5kZXhPZihcIi5cIik+LTEmJihyPXEuc3BsaXQoXCIuXCIpLHE9ci5zaGlmdCgpLHIuc29ydCgpKSxsPXEuaW5kZXhPZihcIjpcIik8MCYmXCJvblwiK3EsYj1iW24uZXhwYW5kb10/YjpuZXcgbi5FdmVudChxLFwib2JqZWN0XCI9PXR5cGVvZiBiJiZiKSxiLmlzVHJpZ2dlcj1mPzI6MyxiLm5hbWVzcGFjZT1yLmpvaW4oXCIuXCIpLGIucm5hbWVzcGFjZT1iLm5hbWVzcGFjZT9uZXcgUmVnRXhwKFwiKF58XFxcXC4pXCIrci5qb2luKFwiXFxcXC4oPzouKlxcXFwufClcIikrXCIoXFxcXC58JClcIik6bnVsbCxiLnJlc3VsdD12b2lkIDAsYi50YXJnZXR8fChiLnRhcmdldD1lKSxjPW51bGw9PWM/W2JdOm4ubWFrZUFycmF5KGMsW2JdKSxvPW4uZXZlbnQuc3BlY2lhbFtxXXx8e30sZnx8IW8udHJpZ2dlcnx8by50cmlnZ2VyLmFwcGx5KGUsYykhPT0hMSkpe2lmKCFmJiYhby5ub0J1YmJsZSYmIW4uaXNXaW5kb3coZSkpe2ZvcihqPW8uZGVsZWdhdGVUeXBlfHxxLGhiLnRlc3QoaitxKXx8KGg9aC5wYXJlbnROb2RlKTtoO2g9aC5wYXJlbnROb2RlKXAucHVzaChoKSxpPWg7aT09PShlLm93bmVyRG9jdW1lbnR8fGQpJiZwLnB1c2goaS5kZWZhdWx0Vmlld3x8aS5wYXJlbnRXaW5kb3d8fGEpfWc9MDt3aGlsZSgoaD1wW2crK10pJiYhYi5pc1Byb3BhZ2F0aW9uU3RvcHBlZCgpKWIudHlwZT1nPjE/ajpvLmJpbmRUeXBlfHxxLG09KE4uZ2V0KGgsXCJldmVudHNcIil8fHt9KVtiLnR5cGVdJiZOLmdldChoLFwiaGFuZGxlXCIpLG0mJm0uYXBwbHkoaCxjKSxtPWwmJmhbbF0sbSYmbS5hcHBseSYmTChoKSYmKGIucmVzdWx0PW0uYXBwbHkoaCxjKSxiLnJlc3VsdD09PSExJiZiLnByZXZlbnREZWZhdWx0KCkpO3JldHVybiBiLnR5cGU9cSxmfHxiLmlzRGVmYXVsdFByZXZlbnRlZCgpfHxvLl9kZWZhdWx0JiZvLl9kZWZhdWx0LmFwcGx5KHAucG9wKCksYykhPT0hMXx8IUwoZSl8fGwmJm4uaXNGdW5jdGlvbihlW3FdKSYmIW4uaXNXaW5kb3coZSkmJihpPWVbbF0saSYmKGVbbF09bnVsbCksbi5ldmVudC50cmlnZ2VyZWQ9cSxlW3FdKCksbi5ldmVudC50cmlnZ2VyZWQ9dm9pZCAwLGkmJihlW2xdPWkpKSxiLnJlc3VsdH19LHNpbXVsYXRlOmZ1bmN0aW9uKGEsYixjKXt2YXIgZD1uLmV4dGVuZChuZXcgbi5FdmVudCxjLHt0eXBlOmEsaXNTaW11bGF0ZWQ6ITB9KTtuLmV2ZW50LnRyaWdnZXIoZCxudWxsLGIpLGQuaXNEZWZhdWx0UHJldmVudGVkKCkmJmMucHJldmVudERlZmF1bHQoKX19KSxuLmZuLmV4dGVuZCh7dHJpZ2dlcjpmdW5jdGlvbihhLGIpe3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXtuLmV2ZW50LnRyaWdnZXIoYSxiLHRoaXMpfSl9LHRyaWdnZXJIYW5kbGVyOmZ1bmN0aW9uKGEsYil7dmFyIGM9dGhpc1swXTtyZXR1cm4gYz9uLmV2ZW50LnRyaWdnZXIoYSxiLGMsITApOnZvaWQgMH19KSxuLmVhY2goXCJibHVyIGZvY3VzIGZvY3VzaW4gZm9jdXNvdXQgbG9hZCByZXNpemUgc2Nyb2xsIHVubG9hZCBjbGljayBkYmxjbGljayBtb3VzZWRvd24gbW91c2V1cCBtb3VzZW1vdmUgbW91c2VvdmVyIG1vdXNlb3V0IG1vdXNlZW50ZXIgbW91c2VsZWF2ZSBjaGFuZ2Ugc2VsZWN0IHN1Ym1pdCBrZXlkb3duIGtleXByZXNzIGtleXVwIGVycm9yIGNvbnRleHRtZW51XCIuc3BsaXQoXCIgXCIpLGZ1bmN0aW9uKGEsYil7bi5mbltiXT1mdW5jdGlvbihhLGMpe3JldHVybiBhcmd1bWVudHMubGVuZ3RoPjA/dGhpcy5vbihiLG51bGwsYSxjKTp0aGlzLnRyaWdnZXIoYil9fSksbi5mbi5leHRlbmQoe2hvdmVyOmZ1bmN0aW9uKGEsYil7cmV0dXJuIHRoaXMubW91c2VlbnRlcihhKS5tb3VzZWxlYXZlKGJ8fGEpfX0pLGwuZm9jdXNpbj1cIm9uZm9jdXNpblwiaW4gYSxsLmZvY3VzaW58fG4uZWFjaCh7Zm9jdXM6XCJmb2N1c2luXCIsYmx1cjpcImZvY3Vzb3V0XCJ9LGZ1bmN0aW9uKGEsYil7dmFyIGM9ZnVuY3Rpb24oYSl7bi5ldmVudC5zaW11bGF0ZShiLGEudGFyZ2V0LG4uZXZlbnQuZml4KGEpKX07bi5ldmVudC5zcGVjaWFsW2JdPXtzZXR1cDpmdW5jdGlvbigpe3ZhciBkPXRoaXMub3duZXJEb2N1bWVudHx8dGhpcyxlPU4uYWNjZXNzKGQsYik7ZXx8ZC5hZGRFdmVudExpc3RlbmVyKGEsYywhMCksTi5hY2Nlc3MoZCxiLChlfHwwKSsxKX0sdGVhcmRvd246ZnVuY3Rpb24oKXt2YXIgZD10aGlzLm93bmVyRG9jdW1lbnR8fHRoaXMsZT1OLmFjY2VzcyhkLGIpLTE7ZT9OLmFjY2VzcyhkLGIsZSk6KGQucmVtb3ZlRXZlbnRMaXN0ZW5lcihhLGMsITApLE4ucmVtb3ZlKGQsYikpfX19KTt2YXIgaWI9YS5sb2NhdGlvbixqYj1uLm5vdygpLGtiPS9cXD8vO24ucGFyc2VKU09OPWZ1bmN0aW9uKGEpe3JldHVybiBKU09OLnBhcnNlKGErXCJcIil9LG4ucGFyc2VYTUw9ZnVuY3Rpb24oYil7dmFyIGM7aWYoIWJ8fFwic3RyaW5nXCIhPXR5cGVvZiBiKXJldHVybiBudWxsO3RyeXtjPShuZXcgYS5ET01QYXJzZXIpLnBhcnNlRnJvbVN0cmluZyhiLFwidGV4dC94bWxcIil9Y2F0Y2goZCl7Yz12b2lkIDB9cmV0dXJuKCFjfHxjLmdldEVsZW1lbnRzQnlUYWdOYW1lKFwicGFyc2VyZXJyb3JcIikubGVuZ3RoKSYmbi5lcnJvcihcIkludmFsaWQgWE1MOiBcIitiKSxjfTt2YXIgbGI9LyMuKiQvLG1iPS8oWz8mXSlfPVteJl0qLyxuYj0vXiguKj8pOlsgXFx0XSooW15cXHJcXG5dKikkL2dtLG9iPS9eKD86YWJvdXR8YXBwfGFwcC1zdG9yYWdlfC4rLWV4dGVuc2lvbnxmaWxlfHJlc3x3aWRnZXQpOiQvLHBiPS9eKD86R0VUfEhFQUQpJC8scWI9L15cXC9cXC8vLHJiPXt9LHNiPXt9LHRiPVwiKi9cIi5jb25jYXQoXCIqXCIpLHViPWQuY3JlYXRlRWxlbWVudChcImFcIik7dWIuaHJlZj1pYi5ocmVmO2Z1bmN0aW9uIHZiKGEpe3JldHVybiBmdW5jdGlvbihiLGMpe1wic3RyaW5nXCIhPXR5cGVvZiBiJiYoYz1iLGI9XCIqXCIpO3ZhciBkLGU9MCxmPWIudG9Mb3dlckNhc2UoKS5tYXRjaChHKXx8W107aWYobi5pc0Z1bmN0aW9uKGMpKXdoaWxlKGQ9ZltlKytdKVwiK1wiPT09ZFswXT8oZD1kLnNsaWNlKDEpfHxcIipcIiwoYVtkXT1hW2RdfHxbXSkudW5zaGlmdChjKSk6KGFbZF09YVtkXXx8W10pLnB1c2goYyl9fWZ1bmN0aW9uIHdiKGEsYixjLGQpe3ZhciBlPXt9LGY9YT09PXNiO2Z1bmN0aW9uIGcoaCl7dmFyIGk7cmV0dXJuIGVbaF09ITAsbi5lYWNoKGFbaF18fFtdLGZ1bmN0aW9uKGEsaCl7dmFyIGo9aChiLGMsZCk7cmV0dXJuXCJzdHJpbmdcIiE9dHlwZW9mIGp8fGZ8fGVbal0/Zj8hKGk9aik6dm9pZCAwOihiLmRhdGFUeXBlcy51bnNoaWZ0KGopLGcoaiksITEpfSksaX1yZXR1cm4gZyhiLmRhdGFUeXBlc1swXSl8fCFlW1wiKlwiXSYmZyhcIipcIil9ZnVuY3Rpb24geGIoYSxiKXt2YXIgYyxkLGU9bi5hamF4U2V0dGluZ3MuZmxhdE9wdGlvbnN8fHt9O2ZvcihjIGluIGIpdm9pZCAwIT09YltjXSYmKChlW2NdP2E6ZHx8KGQ9e30pKVtjXT1iW2NdKTtyZXR1cm4gZCYmbi5leHRlbmQoITAsYSxkKSxhfWZ1bmN0aW9uIHliKGEsYixjKXt2YXIgZCxlLGYsZyxoPWEuY29udGVudHMsaT1hLmRhdGFUeXBlczt3aGlsZShcIipcIj09PWlbMF0paS5zaGlmdCgpLHZvaWQgMD09PWQmJihkPWEubWltZVR5cGV8fGIuZ2V0UmVzcG9uc2VIZWFkZXIoXCJDb250ZW50LVR5cGVcIikpO2lmKGQpZm9yKGUgaW4gaClpZihoW2VdJiZoW2VdLnRlc3QoZCkpe2kudW5zaGlmdChlKTticmVha31pZihpWzBdaW4gYylmPWlbMF07ZWxzZXtmb3IoZSBpbiBjKXtpZighaVswXXx8YS5jb252ZXJ0ZXJzW2UrXCIgXCIraVswXV0pe2Y9ZTticmVha31nfHwoZz1lKX1mPWZ8fGd9cmV0dXJuIGY/KGYhPT1pWzBdJiZpLnVuc2hpZnQoZiksY1tmXSk6dm9pZCAwfWZ1bmN0aW9uIHpiKGEsYixjLGQpe3ZhciBlLGYsZyxoLGksaj17fSxrPWEuZGF0YVR5cGVzLnNsaWNlKCk7aWYoa1sxXSlmb3IoZyBpbiBhLmNvbnZlcnRlcnMpaltnLnRvTG93ZXJDYXNlKCldPWEuY29udmVydGVyc1tnXTtmPWsuc2hpZnQoKTt3aGlsZShmKWlmKGEucmVzcG9uc2VGaWVsZHNbZl0mJihjW2EucmVzcG9uc2VGaWVsZHNbZl1dPWIpLCFpJiZkJiZhLmRhdGFGaWx0ZXImJihiPWEuZGF0YUZpbHRlcihiLGEuZGF0YVR5cGUpKSxpPWYsZj1rLnNoaWZ0KCkpaWYoXCIqXCI9PT1mKWY9aTtlbHNlIGlmKFwiKlwiIT09aSYmaSE9PWYpe2lmKGc9altpK1wiIFwiK2ZdfHxqW1wiKiBcIitmXSwhZylmb3IoZSBpbiBqKWlmKGg9ZS5zcGxpdChcIiBcIiksaFsxXT09PWYmJihnPWpbaStcIiBcIitoWzBdXXx8altcIiogXCIraFswXV0pKXtnPT09ITA/Zz1qW2VdOmpbZV0hPT0hMCYmKGY9aFswXSxrLnVuc2hpZnQoaFsxXSkpO2JyZWFrfWlmKGchPT0hMClpZihnJiZhW1widGhyb3dzXCJdKWI9ZyhiKTtlbHNlIHRyeXtiPWcoYil9Y2F0Y2gobCl7cmV0dXJue3N0YXRlOlwicGFyc2VyZXJyb3JcIixlcnJvcjpnP2w6XCJObyBjb252ZXJzaW9uIGZyb20gXCIraStcIiB0byBcIitmfX19cmV0dXJue3N0YXRlOlwic3VjY2Vzc1wiLGRhdGE6Yn19bi5leHRlbmQoe2FjdGl2ZTowLGxhc3RNb2RpZmllZDp7fSxldGFnOnt9LGFqYXhTZXR0aW5nczp7dXJsOmliLmhyZWYsdHlwZTpcIkdFVFwiLGlzTG9jYWw6b2IudGVzdChpYi5wcm90b2NvbCksZ2xvYmFsOiEwLHByb2Nlc3NEYXRhOiEwLGFzeW5jOiEwLGNvbnRlbnRUeXBlOlwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkOyBjaGFyc2V0PVVURi04XCIsYWNjZXB0czp7XCIqXCI6dGIsdGV4dDpcInRleHQvcGxhaW5cIixodG1sOlwidGV4dC9odG1sXCIseG1sOlwiYXBwbGljYXRpb24veG1sLCB0ZXh0L3htbFwiLGpzb246XCJhcHBsaWNhdGlvbi9qc29uLCB0ZXh0L2phdmFzY3JpcHRcIn0sY29udGVudHM6e3htbDovXFxieG1sXFxiLyxodG1sOi9cXGJodG1sLyxqc29uOi9cXGJqc29uXFxiL30scmVzcG9uc2VGaWVsZHM6e3htbDpcInJlc3BvbnNlWE1MXCIsdGV4dDpcInJlc3BvbnNlVGV4dFwiLGpzb246XCJyZXNwb25zZUpTT05cIn0sY29udmVydGVyczp7XCIqIHRleHRcIjpTdHJpbmcsXCJ0ZXh0IGh0bWxcIjohMCxcInRleHQganNvblwiOm4ucGFyc2VKU09OLFwidGV4dCB4bWxcIjpuLnBhcnNlWE1MfSxmbGF0T3B0aW9uczp7dXJsOiEwLGNvbnRleHQ6ITB9fSxhamF4U2V0dXA6ZnVuY3Rpb24oYSxiKXtyZXR1cm4gYj94Yih4YihhLG4uYWpheFNldHRpbmdzKSxiKTp4YihuLmFqYXhTZXR0aW5ncyxhKX0sYWpheFByZWZpbHRlcjp2YihyYiksYWpheFRyYW5zcG9ydDp2YihzYiksYWpheDpmdW5jdGlvbihiLGMpe1wib2JqZWN0XCI9PXR5cGVvZiBiJiYoYz1iLGI9dm9pZCAwKSxjPWN8fHt9O3ZhciBlLGYsZyxoLGksaixrLGwsbT1uLmFqYXhTZXR1cCh7fSxjKSxvPW0uY29udGV4dHx8bSxwPW0uY29udGV4dCYmKG8ubm9kZVR5cGV8fG8uanF1ZXJ5KT9uKG8pOm4uZXZlbnQscT1uLkRlZmVycmVkKCkscj1uLkNhbGxiYWNrcyhcIm9uY2UgbWVtb3J5XCIpLHM9bS5zdGF0dXNDb2RlfHx7fSx0PXt9LHU9e30sdj0wLHc9XCJjYW5jZWxlZFwiLHg9e3JlYWR5U3RhdGU6MCxnZXRSZXNwb25zZUhlYWRlcjpmdW5jdGlvbihhKXt2YXIgYjtpZigyPT09dil7aWYoIWgpe2g9e307d2hpbGUoYj1uYi5leGVjKGcpKWhbYlsxXS50b0xvd2VyQ2FzZSgpXT1iWzJdfWI9aFthLnRvTG93ZXJDYXNlKCldfXJldHVybiBudWxsPT1iP251bGw6Yn0sZ2V0QWxsUmVzcG9uc2VIZWFkZXJzOmZ1bmN0aW9uKCl7cmV0dXJuIDI9PT12P2c6bnVsbH0sc2V0UmVxdWVzdEhlYWRlcjpmdW5jdGlvbihhLGIpe3ZhciBjPWEudG9Mb3dlckNhc2UoKTtyZXR1cm4gdnx8KGE9dVtjXT11W2NdfHxhLHRbYV09YiksdGhpc30sb3ZlcnJpZGVNaW1lVHlwZTpmdW5jdGlvbihhKXtyZXR1cm4gdnx8KG0ubWltZVR5cGU9YSksdGhpc30sc3RhdHVzQ29kZTpmdW5jdGlvbihhKXt2YXIgYjtpZihhKWlmKDI+dilmb3IoYiBpbiBhKXNbYl09W3NbYl0sYVtiXV07ZWxzZSB4LmFsd2F5cyhhW3guc3RhdHVzXSk7cmV0dXJuIHRoaXN9LGFib3J0OmZ1bmN0aW9uKGEpe3ZhciBiPWF8fHc7cmV0dXJuIGUmJmUuYWJvcnQoYikseigwLGIpLHRoaXN9fTtpZihxLnByb21pc2UoeCkuY29tcGxldGU9ci5hZGQseC5zdWNjZXNzPXguZG9uZSx4LmVycm9yPXguZmFpbCxtLnVybD0oKGJ8fG0udXJsfHxpYi5ocmVmKStcIlwiKS5yZXBsYWNlKGxiLFwiXCIpLnJlcGxhY2UocWIsaWIucHJvdG9jb2wrXCIvL1wiKSxtLnR5cGU9Yy5tZXRob2R8fGMudHlwZXx8bS5tZXRob2R8fG0udHlwZSxtLmRhdGFUeXBlcz1uLnRyaW0obS5kYXRhVHlwZXx8XCIqXCIpLnRvTG93ZXJDYXNlKCkubWF0Y2goRyl8fFtcIlwiXSxudWxsPT1tLmNyb3NzRG9tYWluKXtqPWQuY3JlYXRlRWxlbWVudChcImFcIik7dHJ5e2ouaHJlZj1tLnVybCxqLmhyZWY9ai5ocmVmLG0uY3Jvc3NEb21haW49dWIucHJvdG9jb2wrXCIvL1wiK3ViLmhvc3QhPWoucHJvdG9jb2wrXCIvL1wiK2ouaG9zdH1jYXRjaCh5KXttLmNyb3NzRG9tYWluPSEwfX1pZihtLmRhdGEmJm0ucHJvY2Vzc0RhdGEmJlwic3RyaW5nXCIhPXR5cGVvZiBtLmRhdGEmJihtLmRhdGE9bi5wYXJhbShtLmRhdGEsbS50cmFkaXRpb25hbCkpLHdiKHJiLG0sYyx4KSwyPT09dilyZXR1cm4geDtrPW4uZXZlbnQmJm0uZ2xvYmFsLGsmJjA9PT1uLmFjdGl2ZSsrJiZuLmV2ZW50LnRyaWdnZXIoXCJhamF4U3RhcnRcIiksbS50eXBlPW0udHlwZS50b1VwcGVyQ2FzZSgpLG0uaGFzQ29udGVudD0hcGIudGVzdChtLnR5cGUpLGY9bS51cmwsbS5oYXNDb250ZW50fHwobS5kYXRhJiYoZj1tLnVybCs9KGtiLnRlc3QoZik/XCImXCI6XCI/XCIpK20uZGF0YSxkZWxldGUgbS5kYXRhKSxtLmNhY2hlPT09ITEmJihtLnVybD1tYi50ZXN0KGYpP2YucmVwbGFjZShtYixcIiQxXz1cIitqYisrKTpmKyhrYi50ZXN0KGYpP1wiJlwiOlwiP1wiKStcIl89XCIramIrKykpLG0uaWZNb2RpZmllZCYmKG4ubGFzdE1vZGlmaWVkW2ZdJiZ4LnNldFJlcXVlc3RIZWFkZXIoXCJJZi1Nb2RpZmllZC1TaW5jZVwiLG4ubGFzdE1vZGlmaWVkW2ZdKSxuLmV0YWdbZl0mJnguc2V0UmVxdWVzdEhlYWRlcihcIklmLU5vbmUtTWF0Y2hcIixuLmV0YWdbZl0pKSwobS5kYXRhJiZtLmhhc0NvbnRlbnQmJm0uY29udGVudFR5cGUhPT0hMXx8Yy5jb250ZW50VHlwZSkmJnguc2V0UmVxdWVzdEhlYWRlcihcIkNvbnRlbnQtVHlwZVwiLG0uY29udGVudFR5cGUpLHguc2V0UmVxdWVzdEhlYWRlcihcIkFjY2VwdFwiLG0uZGF0YVR5cGVzWzBdJiZtLmFjY2VwdHNbbS5kYXRhVHlwZXNbMF1dP20uYWNjZXB0c1ttLmRhdGFUeXBlc1swXV0rKFwiKlwiIT09bS5kYXRhVHlwZXNbMF0/XCIsIFwiK3RiK1wiOyBxPTAuMDFcIjpcIlwiKTptLmFjY2VwdHNbXCIqXCJdKTtmb3IobCBpbiBtLmhlYWRlcnMpeC5zZXRSZXF1ZXN0SGVhZGVyKGwsbS5oZWFkZXJzW2xdKTtpZihtLmJlZm9yZVNlbmQmJihtLmJlZm9yZVNlbmQuY2FsbChvLHgsbSk9PT0hMXx8Mj09PXYpKXJldHVybiB4LmFib3J0KCk7dz1cImFib3J0XCI7Zm9yKGwgaW57c3VjY2VzczoxLGVycm9yOjEsY29tcGxldGU6MX0peFtsXShtW2xdKTtpZihlPXdiKHNiLG0sYyx4KSl7aWYoeC5yZWFkeVN0YXRlPTEsayYmcC50cmlnZ2VyKFwiYWpheFNlbmRcIixbeCxtXSksMj09PXYpcmV0dXJuIHg7bS5hc3luYyYmbS50aW1lb3V0PjAmJihpPWEuc2V0VGltZW91dChmdW5jdGlvbigpe3guYWJvcnQoXCJ0aW1lb3V0XCIpfSxtLnRpbWVvdXQpKTt0cnl7dj0xLGUuc2VuZCh0LHopfWNhdGNoKHkpe2lmKCEoMj52KSl0aHJvdyB5O3ooLTEseSl9fWVsc2UgeigtMSxcIk5vIFRyYW5zcG9ydFwiKTtmdW5jdGlvbiB6KGIsYyxkLGgpe3ZhciBqLGwsdCx1LHcseT1jOzIhPT12JiYodj0yLGkmJmEuY2xlYXJUaW1lb3V0KGkpLGU9dm9pZCAwLGc9aHx8XCJcIix4LnJlYWR5U3RhdGU9Yj4wPzQ6MCxqPWI+PTIwMCYmMzAwPmJ8fDMwND09PWIsZCYmKHU9eWIobSx4LGQpKSx1PXpiKG0sdSx4LGopLGo/KG0uaWZNb2RpZmllZCYmKHc9eC5nZXRSZXNwb25zZUhlYWRlcihcIkxhc3QtTW9kaWZpZWRcIiksdyYmKG4ubGFzdE1vZGlmaWVkW2ZdPXcpLHc9eC5nZXRSZXNwb25zZUhlYWRlcihcImV0YWdcIiksdyYmKG4uZXRhZ1tmXT13KSksMjA0PT09Ynx8XCJIRUFEXCI9PT1tLnR5cGU/eT1cIm5vY29udGVudFwiOjMwND09PWI/eT1cIm5vdG1vZGlmaWVkXCI6KHk9dS5zdGF0ZSxsPXUuZGF0YSx0PXUuZXJyb3Isaj0hdCkpOih0PXksKGJ8fCF5KSYmKHk9XCJlcnJvclwiLDA+YiYmKGI9MCkpKSx4LnN0YXR1cz1iLHguc3RhdHVzVGV4dD0oY3x8eSkrXCJcIixqP3EucmVzb2x2ZVdpdGgobyxbbCx5LHhdKTpxLnJlamVjdFdpdGgobyxbeCx5LHRdKSx4LnN0YXR1c0NvZGUocykscz12b2lkIDAsayYmcC50cmlnZ2VyKGo/XCJhamF4U3VjY2Vzc1wiOlwiYWpheEVycm9yXCIsW3gsbSxqP2w6dF0pLHIuZmlyZVdpdGgobyxbeCx5XSksayYmKHAudHJpZ2dlcihcImFqYXhDb21wbGV0ZVwiLFt4LG1dKSwtLW4uYWN0aXZlfHxuLmV2ZW50LnRyaWdnZXIoXCJhamF4U3RvcFwiKSkpfXJldHVybiB4fSxnZXRKU09OOmZ1bmN0aW9uKGEsYixjKXtyZXR1cm4gbi5nZXQoYSxiLGMsXCJqc29uXCIpfSxnZXRTY3JpcHQ6ZnVuY3Rpb24oYSxiKXtyZXR1cm4gbi5nZXQoYSx2b2lkIDAsYixcInNjcmlwdFwiKX19KSxuLmVhY2goW1wiZ2V0XCIsXCJwb3N0XCJdLGZ1bmN0aW9uKGEsYil7bltiXT1mdW5jdGlvbihhLGMsZCxlKXtyZXR1cm4gbi5pc0Z1bmN0aW9uKGMpJiYoZT1lfHxkLGQ9YyxjPXZvaWQgMCksbi5hamF4KG4uZXh0ZW5kKHt1cmw6YSx0eXBlOmIsZGF0YVR5cGU6ZSxkYXRhOmMsc3VjY2VzczpkfSxuLmlzUGxhaW5PYmplY3QoYSkmJmEpKX19KSxuLl9ldmFsVXJsPWZ1bmN0aW9uKGEpe3JldHVybiBuLmFqYXgoe3VybDphLHR5cGU6XCJHRVRcIixkYXRhVHlwZTpcInNjcmlwdFwiLGFzeW5jOiExLGdsb2JhbDohMSxcInRocm93c1wiOiEwfSl9LG4uZm4uZXh0ZW5kKHt3cmFwQWxsOmZ1bmN0aW9uKGEpe3ZhciBiO3JldHVybiBuLmlzRnVuY3Rpb24oYSk/dGhpcy5lYWNoKGZ1bmN0aW9uKGIpe24odGhpcykud3JhcEFsbChhLmNhbGwodGhpcyxiKSl9KToodGhpc1swXSYmKGI9bihhLHRoaXNbMF0ub3duZXJEb2N1bWVudCkuZXEoMCkuY2xvbmUoITApLHRoaXNbMF0ucGFyZW50Tm9kZSYmYi5pbnNlcnRCZWZvcmUodGhpc1swXSksYi5tYXAoZnVuY3Rpb24oKXt2YXIgYT10aGlzO3doaWxlKGEuZmlyc3RFbGVtZW50Q2hpbGQpYT1hLmZpcnN0RWxlbWVudENoaWxkO3JldHVybiBhfSkuYXBwZW5kKHRoaXMpKSx0aGlzKX0sd3JhcElubmVyOmZ1bmN0aW9uKGEpe3JldHVybiBuLmlzRnVuY3Rpb24oYSk/dGhpcy5lYWNoKGZ1bmN0aW9uKGIpe24odGhpcykud3JhcElubmVyKGEuY2FsbCh0aGlzLGIpKX0pOnRoaXMuZWFjaChmdW5jdGlvbigpe3ZhciBiPW4odGhpcyksYz1iLmNvbnRlbnRzKCk7Yy5sZW5ndGg/Yy53cmFwQWxsKGEpOmIuYXBwZW5kKGEpfSl9LHdyYXA6ZnVuY3Rpb24oYSl7dmFyIGI9bi5pc0Z1bmN0aW9uKGEpO3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oYyl7bih0aGlzKS53cmFwQWxsKGI/YS5jYWxsKHRoaXMsYyk6YSl9KX0sdW53cmFwOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMucGFyZW50KCkuZWFjaChmdW5jdGlvbigpe24ubm9kZU5hbWUodGhpcyxcImJvZHlcIil8fG4odGhpcykucmVwbGFjZVdpdGgodGhpcy5jaGlsZE5vZGVzKX0pLmVuZCgpfX0pLG4uZXhwci5maWx0ZXJzLmhpZGRlbj1mdW5jdGlvbihhKXtyZXR1cm4hbi5leHByLmZpbHRlcnMudmlzaWJsZShhKX0sbi5leHByLmZpbHRlcnMudmlzaWJsZT1mdW5jdGlvbihhKXtyZXR1cm4gYS5vZmZzZXRXaWR0aD4wfHxhLm9mZnNldEhlaWdodD4wfHxhLmdldENsaWVudFJlY3RzKCkubGVuZ3RoPjB9O3ZhciBBYj0vJTIwL2csQmI9L1xcW1xcXSQvLENiPS9cXHI/XFxuL2csRGI9L14oPzpzdWJtaXR8YnV0dG9ufGltYWdlfHJlc2V0fGZpbGUpJC9pLEViPS9eKD86aW5wdXR8c2VsZWN0fHRleHRhcmVhfGtleWdlbikvaTtmdW5jdGlvbiBGYihhLGIsYyxkKXt2YXIgZTtpZihuLmlzQXJyYXkoYikpbi5lYWNoKGIsZnVuY3Rpb24oYixlKXtjfHxCYi50ZXN0KGEpP2QoYSxlKTpGYihhK1wiW1wiKyhcIm9iamVjdFwiPT10eXBlb2YgZSYmbnVsbCE9ZT9iOlwiXCIpK1wiXVwiLGUsYyxkKX0pO2Vsc2UgaWYoY3x8XCJvYmplY3RcIiE9PW4udHlwZShiKSlkKGEsYik7ZWxzZSBmb3IoZSBpbiBiKUZiKGErXCJbXCIrZStcIl1cIixiW2VdLGMsZCl9bi5wYXJhbT1mdW5jdGlvbihhLGIpe3ZhciBjLGQ9W10sZT1mdW5jdGlvbihhLGIpe2I9bi5pc0Z1bmN0aW9uKGIpP2IoKTpudWxsPT1iP1wiXCI6YixkW2QubGVuZ3RoXT1lbmNvZGVVUklDb21wb25lbnQoYSkrXCI9XCIrZW5jb2RlVVJJQ29tcG9uZW50KGIpfTtpZih2b2lkIDA9PT1iJiYoYj1uLmFqYXhTZXR0aW5ncyYmbi5hamF4U2V0dGluZ3MudHJhZGl0aW9uYWwpLG4uaXNBcnJheShhKXx8YS5qcXVlcnkmJiFuLmlzUGxhaW5PYmplY3QoYSkpbi5lYWNoKGEsZnVuY3Rpb24oKXtlKHRoaXMubmFtZSx0aGlzLnZhbHVlKX0pO2Vsc2UgZm9yKGMgaW4gYSlGYihjLGFbY10sYixlKTtyZXR1cm4gZC5qb2luKFwiJlwiKS5yZXBsYWNlKEFiLFwiK1wiKX0sbi5mbi5leHRlbmQoe3NlcmlhbGl6ZTpmdW5jdGlvbigpe3JldHVybiBuLnBhcmFtKHRoaXMuc2VyaWFsaXplQXJyYXkoKSl9LHNlcmlhbGl6ZUFycmF5OmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMubWFwKGZ1bmN0aW9uKCl7dmFyIGE9bi5wcm9wKHRoaXMsXCJlbGVtZW50c1wiKTtyZXR1cm4gYT9uLm1ha2VBcnJheShhKTp0aGlzfSkuZmlsdGVyKGZ1bmN0aW9uKCl7dmFyIGE9dGhpcy50eXBlO3JldHVybiB0aGlzLm5hbWUmJiFuKHRoaXMpLmlzKFwiOmRpc2FibGVkXCIpJiZFYi50ZXN0KHRoaXMubm9kZU5hbWUpJiYhRGIudGVzdChhKSYmKHRoaXMuY2hlY2tlZHx8IVgudGVzdChhKSl9KS5tYXAoZnVuY3Rpb24oYSxiKXt2YXIgYz1uKHRoaXMpLnZhbCgpO3JldHVybiBudWxsPT1jP251bGw6bi5pc0FycmF5KGMpP24ubWFwKGMsZnVuY3Rpb24oYSl7cmV0dXJue25hbWU6Yi5uYW1lLHZhbHVlOmEucmVwbGFjZShDYixcIlxcclxcblwiKX19KTp7bmFtZTpiLm5hbWUsdmFsdWU6Yy5yZXBsYWNlKENiLFwiXFxyXFxuXCIpfX0pLmdldCgpfX0pLG4uYWpheFNldHRpbmdzLnhocj1mdW5jdGlvbigpe3RyeXtyZXR1cm4gbmV3IGEuWE1MSHR0cFJlcXVlc3R9Y2F0Y2goYil7fX07dmFyIEdiPXswOjIwMCwxMjIzOjIwNH0sSGI9bi5hamF4U2V0dGluZ3MueGhyKCk7bC5jb3JzPSEhSGImJlwid2l0aENyZWRlbnRpYWxzXCJpbiBIYixsLmFqYXg9SGI9ISFIYixuLmFqYXhUcmFuc3BvcnQoZnVuY3Rpb24oYil7dmFyIGMsZDtyZXR1cm4gbC5jb3JzfHxIYiYmIWIuY3Jvc3NEb21haW4/e3NlbmQ6ZnVuY3Rpb24oZSxmKXt2YXIgZyxoPWIueGhyKCk7aWYoaC5vcGVuKGIudHlwZSxiLnVybCxiLmFzeW5jLGIudXNlcm5hbWUsYi5wYXNzd29yZCksYi54aHJGaWVsZHMpZm9yKGcgaW4gYi54aHJGaWVsZHMpaFtnXT1iLnhockZpZWxkc1tnXTtiLm1pbWVUeXBlJiZoLm92ZXJyaWRlTWltZVR5cGUmJmgub3ZlcnJpZGVNaW1lVHlwZShiLm1pbWVUeXBlKSxiLmNyb3NzRG9tYWlufHxlW1wiWC1SZXF1ZXN0ZWQtV2l0aFwiXXx8KGVbXCJYLVJlcXVlc3RlZC1XaXRoXCJdPVwiWE1MSHR0cFJlcXVlc3RcIik7Zm9yKGcgaW4gZSloLnNldFJlcXVlc3RIZWFkZXIoZyxlW2ddKTtjPWZ1bmN0aW9uKGEpe3JldHVybiBmdW5jdGlvbigpe2MmJihjPWQ9aC5vbmxvYWQ9aC5vbmVycm9yPWgub25hYm9ydD1oLm9ucmVhZHlzdGF0ZWNoYW5nZT1udWxsLFwiYWJvcnRcIj09PWE/aC5hYm9ydCgpOlwiZXJyb3JcIj09PWE/XCJudW1iZXJcIiE9dHlwZW9mIGguc3RhdHVzP2YoMCxcImVycm9yXCIpOmYoaC5zdGF0dXMsaC5zdGF0dXNUZXh0KTpmKEdiW2guc3RhdHVzXXx8aC5zdGF0dXMsaC5zdGF0dXNUZXh0LFwidGV4dFwiIT09KGgucmVzcG9uc2VUeXBlfHxcInRleHRcIil8fFwic3RyaW5nXCIhPXR5cGVvZiBoLnJlc3BvbnNlVGV4dD97YmluYXJ5OmgucmVzcG9uc2V9Ont0ZXh0OmgucmVzcG9uc2VUZXh0fSxoLmdldEFsbFJlc3BvbnNlSGVhZGVycygpKSl9fSxoLm9ubG9hZD1jKCksZD1oLm9uZXJyb3I9YyhcImVycm9yXCIpLHZvaWQgMCE9PWgub25hYm9ydD9oLm9uYWJvcnQ9ZDpoLm9ucmVhZHlzdGF0ZWNoYW5nZT1mdW5jdGlvbigpezQ9PT1oLnJlYWR5U3RhdGUmJmEuc2V0VGltZW91dChmdW5jdGlvbigpe2MmJmQoKX0pfSxjPWMoXCJhYm9ydFwiKTt0cnl7aC5zZW5kKGIuaGFzQ29udGVudCYmYi5kYXRhfHxudWxsKX1jYXRjaChpKXtpZihjKXRocm93IGl9fSxhYm9ydDpmdW5jdGlvbigpe2MmJmMoKX19OnZvaWQgMH0pLG4uYWpheFNldHVwKHthY2NlcHRzOntzY3JpcHQ6XCJ0ZXh0L2phdmFzY3JpcHQsIGFwcGxpY2F0aW9uL2phdmFzY3JpcHQsIGFwcGxpY2F0aW9uL2VjbWFzY3JpcHQsIGFwcGxpY2F0aW9uL3gtZWNtYXNjcmlwdFwifSxjb250ZW50czp7c2NyaXB0Oi9cXGIoPzpqYXZhfGVjbWEpc2NyaXB0XFxiL30sY29udmVydGVyczp7XCJ0ZXh0IHNjcmlwdFwiOmZ1bmN0aW9uKGEpe3JldHVybiBuLmdsb2JhbEV2YWwoYSksYX19fSksbi5hamF4UHJlZmlsdGVyKFwic2NyaXB0XCIsZnVuY3Rpb24oYSl7dm9pZCAwPT09YS5jYWNoZSYmKGEuY2FjaGU9ITEpLGEuY3Jvc3NEb21haW4mJihhLnR5cGU9XCJHRVRcIil9KSxuLmFqYXhUcmFuc3BvcnQoXCJzY3JpcHRcIixmdW5jdGlvbihhKXtpZihhLmNyb3NzRG9tYWluKXt2YXIgYixjO3JldHVybntzZW5kOmZ1bmN0aW9uKGUsZil7Yj1uKFwiPHNjcmlwdD5cIikucHJvcCh7Y2hhcnNldDphLnNjcmlwdENoYXJzZXQsc3JjOmEudXJsfSkub24oXCJsb2FkIGVycm9yXCIsYz1mdW5jdGlvbihhKXtiLnJlbW92ZSgpLGM9bnVsbCxhJiZmKFwiZXJyb3JcIj09PWEudHlwZT80MDQ6MjAwLGEudHlwZSl9KSxkLmhlYWQuYXBwZW5kQ2hpbGQoYlswXSl9LGFib3J0OmZ1bmN0aW9uKCl7YyYmYygpfX19fSk7dmFyIEliPVtdLEpiPS8oPSlcXD8oPz0mfCQpfFxcP1xcPy87bi5hamF4U2V0dXAoe2pzb25wOlwiY2FsbGJhY2tcIixqc29ucENhbGxiYWNrOmZ1bmN0aW9uKCl7dmFyIGE9SWIucG9wKCl8fG4uZXhwYW5kbytcIl9cIitqYisrO3JldHVybiB0aGlzW2FdPSEwLGF9fSksbi5hamF4UHJlZmlsdGVyKFwianNvbiBqc29ucFwiLGZ1bmN0aW9uKGIsYyxkKXt2YXIgZSxmLGcsaD1iLmpzb25wIT09ITEmJihKYi50ZXN0KGIudXJsKT9cInVybFwiOlwic3RyaW5nXCI9PXR5cGVvZiBiLmRhdGEmJjA9PT0oYi5jb250ZW50VHlwZXx8XCJcIikuaW5kZXhPZihcImFwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZFwiKSYmSmIudGVzdChiLmRhdGEpJiZcImRhdGFcIik7cmV0dXJuIGh8fFwianNvbnBcIj09PWIuZGF0YVR5cGVzWzBdPyhlPWIuanNvbnBDYWxsYmFjaz1uLmlzRnVuY3Rpb24oYi5qc29ucENhbGxiYWNrKT9iLmpzb25wQ2FsbGJhY2soKTpiLmpzb25wQ2FsbGJhY2ssaD9iW2hdPWJbaF0ucmVwbGFjZShKYixcIiQxXCIrZSk6Yi5qc29ucCE9PSExJiYoYi51cmwrPShrYi50ZXN0KGIudXJsKT9cIiZcIjpcIj9cIikrYi5qc29ucCtcIj1cIitlKSxiLmNvbnZlcnRlcnNbXCJzY3JpcHQganNvblwiXT1mdW5jdGlvbigpe3JldHVybiBnfHxuLmVycm9yKGUrXCIgd2FzIG5vdCBjYWxsZWRcIiksZ1swXX0sYi5kYXRhVHlwZXNbMF09XCJqc29uXCIsZj1hW2VdLGFbZV09ZnVuY3Rpb24oKXtnPWFyZ3VtZW50c30sZC5hbHdheXMoZnVuY3Rpb24oKXt2b2lkIDA9PT1mP24oYSkucmVtb3ZlUHJvcChlKTphW2VdPWYsYltlXSYmKGIuanNvbnBDYWxsYmFjaz1jLmpzb25wQ2FsbGJhY2ssSWIucHVzaChlKSksZyYmbi5pc0Z1bmN0aW9uKGYpJiZmKGdbMF0pLGc9Zj12b2lkIDB9KSxcInNjcmlwdFwiKTp2b2lkIDB9KSxsLmNyZWF0ZUhUTUxEb2N1bWVudD1mdW5jdGlvbigpe3ZhciBhPWQuaW1wbGVtZW50YXRpb24uY3JlYXRlSFRNTERvY3VtZW50KFwiXCIpLmJvZHk7cmV0dXJuIGEuaW5uZXJIVE1MPVwiPGZvcm0+PC9mb3JtPjxmb3JtPjwvZm9ybT5cIiwyPT09YS5jaGlsZE5vZGVzLmxlbmd0aH0oKSxuLnBhcnNlSFRNTD1mdW5jdGlvbihhLGIsYyl7aWYoIWF8fFwic3RyaW5nXCIhPXR5cGVvZiBhKXJldHVybiBudWxsO1wiYm9vbGVhblwiPT10eXBlb2YgYiYmKGM9YixiPSExKSxiPWJ8fChsLmNyZWF0ZUhUTUxEb2N1bWVudD9kLmltcGxlbWVudGF0aW9uLmNyZWF0ZUhUTUxEb2N1bWVudChcIlwiKTpkKTt2YXIgZT14LmV4ZWMoYSksZj0hYyYmW107cmV0dXJuIGU/W2IuY3JlYXRlRWxlbWVudChlWzFdKV06KGU9Y2EoW2FdLGIsZiksZiYmZi5sZW5ndGgmJm4oZikucmVtb3ZlKCksbi5tZXJnZShbXSxlLmNoaWxkTm9kZXMpKX07dmFyIEtiPW4uZm4ubG9hZDtuLmZuLmxvYWQ9ZnVuY3Rpb24oYSxiLGMpe2lmKFwic3RyaW5nXCIhPXR5cGVvZiBhJiZLYilyZXR1cm4gS2IuYXBwbHkodGhpcyxhcmd1bWVudHMpO3ZhciBkLGUsZixnPXRoaXMsaD1hLmluZGV4T2YoXCIgXCIpO3JldHVybiBoPi0xJiYoZD1uLnRyaW0oYS5zbGljZShoKSksYT1hLnNsaWNlKDAsaCkpLG4uaXNGdW5jdGlvbihiKT8oYz1iLGI9dm9pZCAwKTpiJiZcIm9iamVjdFwiPT10eXBlb2YgYiYmKGU9XCJQT1NUXCIpLGcubGVuZ3RoPjAmJm4uYWpheCh7dXJsOmEsdHlwZTplfHxcIkdFVFwiLGRhdGFUeXBlOlwiaHRtbFwiLGRhdGE6Yn0pLmRvbmUoZnVuY3Rpb24oYSl7Zj1hcmd1bWVudHMsZy5odG1sKGQ/bihcIjxkaXY+XCIpLmFwcGVuZChuLnBhcnNlSFRNTChhKSkuZmluZChkKTphKX0pLmFsd2F5cyhjJiZmdW5jdGlvbihhLGIpe2cuZWFjaChmdW5jdGlvbigpe2MuYXBwbHkoZyxmfHxbYS5yZXNwb25zZVRleHQsYixhXSl9KX0pLHRoaXN9LG4uZWFjaChbXCJhamF4U3RhcnRcIixcImFqYXhTdG9wXCIsXCJhamF4Q29tcGxldGVcIixcImFqYXhFcnJvclwiLFwiYWpheFN1Y2Nlc3NcIixcImFqYXhTZW5kXCJdLGZ1bmN0aW9uKGEsYil7bi5mbltiXT1mdW5jdGlvbihhKXtyZXR1cm4gdGhpcy5vbihiLGEpfX0pLG4uZXhwci5maWx0ZXJzLmFuaW1hdGVkPWZ1bmN0aW9uKGEpe3JldHVybiBuLmdyZXAobi50aW1lcnMsZnVuY3Rpb24oYil7cmV0dXJuIGE9PT1iLmVsZW19KS5sZW5ndGh9O2Z1bmN0aW9uIExiKGEpe3JldHVybiBuLmlzV2luZG93KGEpP2E6OT09PWEubm9kZVR5cGUmJmEuZGVmYXVsdFZpZXd9bi5vZmZzZXQ9e3NldE9mZnNldDpmdW5jdGlvbihhLGIsYyl7dmFyIGQsZSxmLGcsaCxpLGosaz1uLmNzcyhhLFwicG9zaXRpb25cIiksbD1uKGEpLG09e307XCJzdGF0aWNcIj09PWsmJihhLnN0eWxlLnBvc2l0aW9uPVwicmVsYXRpdmVcIiksaD1sLm9mZnNldCgpLGY9bi5jc3MoYSxcInRvcFwiKSxpPW4uY3NzKGEsXCJsZWZ0XCIpLGo9KFwiYWJzb2x1dGVcIj09PWt8fFwiZml4ZWRcIj09PWspJiYoZitpKS5pbmRleE9mKFwiYXV0b1wiKT4tMSxqPyhkPWwucG9zaXRpb24oKSxnPWQudG9wLGU9ZC5sZWZ0KTooZz1wYXJzZUZsb2F0KGYpfHwwLGU9cGFyc2VGbG9hdChpKXx8MCksbi5pc0Z1bmN0aW9uKGIpJiYoYj1iLmNhbGwoYSxjLG4uZXh0ZW5kKHt9LGgpKSksbnVsbCE9Yi50b3AmJihtLnRvcD1iLnRvcC1oLnRvcCtnKSxudWxsIT1iLmxlZnQmJihtLmxlZnQ9Yi5sZWZ0LWgubGVmdCtlKSxcInVzaW5nXCJpbiBiP2IudXNpbmcuY2FsbChhLG0pOmwuY3NzKG0pfX0sbi5mbi5leHRlbmQoe29mZnNldDpmdW5jdGlvbihhKXtpZihhcmd1bWVudHMubGVuZ3RoKXJldHVybiB2b2lkIDA9PT1hP3RoaXM6dGhpcy5lYWNoKGZ1bmN0aW9uKGIpe24ub2Zmc2V0LnNldE9mZnNldCh0aGlzLGEsYil9KTt2YXIgYixjLGQ9dGhpc1swXSxlPXt0b3A6MCxsZWZ0OjB9LGY9ZCYmZC5vd25lckRvY3VtZW50O2lmKGYpcmV0dXJuIGI9Zi5kb2N1bWVudEVsZW1lbnQsbi5jb250YWlucyhiLGQpPyhlPWQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksYz1MYihmKSx7dG9wOmUudG9wK2MucGFnZVlPZmZzZXQtYi5jbGllbnRUb3AsbGVmdDplLmxlZnQrYy5wYWdlWE9mZnNldC1iLmNsaWVudExlZnR9KTplfSxwb3NpdGlvbjpmdW5jdGlvbigpe2lmKHRoaXNbMF0pe3ZhciBhLGIsYz10aGlzWzBdLGQ9e3RvcDowLGxlZnQ6MH07cmV0dXJuXCJmaXhlZFwiPT09bi5jc3MoYyxcInBvc2l0aW9uXCIpP2I9Yy5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTooYT10aGlzLm9mZnNldFBhcmVudCgpLGI9dGhpcy5vZmZzZXQoKSxuLm5vZGVOYW1lKGFbMF0sXCJodG1sXCIpfHwoZD1hLm9mZnNldCgpKSxkLnRvcCs9bi5jc3MoYVswXSxcImJvcmRlclRvcFdpZHRoXCIsITApLGQubGVmdCs9bi5jc3MoYVswXSxcImJvcmRlckxlZnRXaWR0aFwiLCEwKSkse3RvcDpiLnRvcC1kLnRvcC1uLmNzcyhjLFwibWFyZ2luVG9wXCIsITApLGxlZnQ6Yi5sZWZ0LWQubGVmdC1uLmNzcyhjLFwibWFyZ2luTGVmdFwiLCEwKX19fSxvZmZzZXRQYXJlbnQ6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5tYXAoZnVuY3Rpb24oKXt2YXIgYT10aGlzLm9mZnNldFBhcmVudDt3aGlsZShhJiZcInN0YXRpY1wiPT09bi5jc3MoYSxcInBvc2l0aW9uXCIpKWE9YS5vZmZzZXRQYXJlbnQ7cmV0dXJuIGF8fEVhfSl9fSksbi5lYWNoKHtzY3JvbGxMZWZ0OlwicGFnZVhPZmZzZXRcIixzY3JvbGxUb3A6XCJwYWdlWU9mZnNldFwifSxmdW5jdGlvbihhLGIpe3ZhciBjPVwicGFnZVlPZmZzZXRcIj09PWI7bi5mblthXT1mdW5jdGlvbihkKXtyZXR1cm4gSyh0aGlzLGZ1bmN0aW9uKGEsZCxlKXt2YXIgZj1MYihhKTtyZXR1cm4gdm9pZCAwPT09ZT9mP2ZbYl06YVtkXTp2b2lkKGY/Zi5zY3JvbGxUbyhjP2YucGFnZVhPZmZzZXQ6ZSxjP2U6Zi5wYWdlWU9mZnNldCk6YVtkXT1lKX0sYSxkLGFyZ3VtZW50cy5sZW5ndGgpfX0pLG4uZWFjaChbXCJ0b3BcIixcImxlZnRcIl0sZnVuY3Rpb24oYSxiKXtuLmNzc0hvb2tzW2JdPUdhKGwucGl4ZWxQb3NpdGlvbixmdW5jdGlvbihhLGMpe3JldHVybiBjPyhjPUZhKGEsYiksQmEudGVzdChjKT9uKGEpLnBvc2l0aW9uKClbYl0rXCJweFwiOmMpOnZvaWQgMH0pfSksbi5lYWNoKHtIZWlnaHQ6XCJoZWlnaHRcIixXaWR0aDpcIndpZHRoXCJ9LGZ1bmN0aW9uKGEsYil7bi5lYWNoKHtwYWRkaW5nOlwiaW5uZXJcIithLGNvbnRlbnQ6YixcIlwiOlwib3V0ZXJcIithfSxmdW5jdGlvbihjLGQpe24uZm5bZF09ZnVuY3Rpb24oZCxlKXt2YXIgZj1hcmd1bWVudHMubGVuZ3RoJiYoY3x8XCJib29sZWFuXCIhPXR5cGVvZiBkKSxnPWN8fChkPT09ITB8fGU9PT0hMD9cIm1hcmdpblwiOlwiYm9yZGVyXCIpO3JldHVybiBLKHRoaXMsZnVuY3Rpb24oYixjLGQpe3ZhciBlO3JldHVybiBuLmlzV2luZG93KGIpP2IuZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50W1wiY2xpZW50XCIrYV06OT09PWIubm9kZVR5cGU/KGU9Yi5kb2N1bWVudEVsZW1lbnQsTWF0aC5tYXgoYi5ib2R5W1wic2Nyb2xsXCIrYV0sZVtcInNjcm9sbFwiK2FdLGIuYm9keVtcIm9mZnNldFwiK2FdLGVbXCJvZmZzZXRcIithXSxlW1wiY2xpZW50XCIrYV0pKTp2b2lkIDA9PT1kP24uY3NzKGIsYyxnKTpuLnN0eWxlKGIsYyxkLGcpfSxiLGY/ZDp2b2lkIDAsZixudWxsKX19KX0pLG4uZm4uZXh0ZW5kKHtiaW5kOmZ1bmN0aW9uKGEsYixjKXtyZXR1cm4gdGhpcy5vbihhLG51bGwsYixjKX0sdW5iaW5kOmZ1bmN0aW9uKGEsYil7cmV0dXJuIHRoaXMub2ZmKGEsbnVsbCxiKX0sZGVsZWdhdGU6ZnVuY3Rpb24oYSxiLGMsZCl7cmV0dXJuIHRoaXMub24oYixhLGMsZCl9LHVuZGVsZWdhdGU6ZnVuY3Rpb24oYSxiLGMpe3JldHVybiAxPT09YXJndW1lbnRzLmxlbmd0aD90aGlzLm9mZihhLFwiKipcIik6dGhpcy5vZmYoYixhfHxcIioqXCIsYyl9LHNpemU6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5sZW5ndGh9fSksbi5mbi5hbmRTZWxmPW4uZm4uYWRkQmFjayxcImZ1bmN0aW9uXCI9PXR5cGVvZiBkZWZpbmUmJmRlZmluZS5hbWQmJmRlZmluZShcImpxdWVyeVwiLFtdLGZ1bmN0aW9uKCl7cmV0dXJuIG59KTt2YXIgTWI9YS5qUXVlcnksTmI9YS4kO3JldHVybiBuLm5vQ29uZmxpY3Q9ZnVuY3Rpb24oYil7cmV0dXJuIGEuJD09PW4mJihhLiQ9TmIpLGImJmEualF1ZXJ5PT09biYmKGEualF1ZXJ5PU1iKSxufSxifHwoYS5qUXVlcnk9YS4kPW4pLG59KTtcclxuLypcclxuICogalF1ZXJ5IE9yYml0IFBsdWdpbiAxLjIuM1xyXG4gKiB3d3cuWlVSQi5jb20vcGxheWdyb3VuZFxyXG4gKiBDb3B5cmlnaHQgMjAxMCwgWlVSQlxyXG4gKiBGcmVlIHRvIHVzZSB1bmRlciB0aGUgTUlUIGxpY2Vuc2UuXHJcbiAqIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXHJcbiAqL1xyXG5cclxuXHJcbihmdW5jdGlvbigkKSB7XHJcblxyXG4gICAgJC5mbi5vcmJpdCA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcclxuXHJcbiAgICAgICAgLy9EZWZhdWx0cyB0byBleHRlbmQgb3B0aW9uc1xyXG4gICAgICAgIHZhciBkZWZhdWx0cyA9IHtcclxuICAgICAgICAgICAgYW5pbWF0aW9uOiAnaG9yaXpvbnRhbC1wdXNoJywgXHRcdC8vIGZhZGUsIGhvcml6b250YWwtc2xpZGUsIHZlcnRpY2FsLXNsaWRlLCBob3Jpem9udGFsLXB1c2hcclxuICAgICAgICAgICAgYW5pbWF0aW9uU3BlZWQ6IDYwMCwgXHRcdFx0XHQvLyBob3cgZmFzdCBhbmltdGlvbnMgYXJlXHJcbiAgICAgICAgICAgIHRpbWVyOiB0cnVlLCBcdFx0XHRcdFx0XHQvLyB0cnVlIG9yIGZhbHNlIHRvIGhhdmUgdGhlIHRpbWVyXHJcbiAgICAgICAgICAgIGFkdmFuY2VTcGVlZDogNDAwMCwgXHRcdFx0XHQvLyBpZiB0aW1lciBpcyBlbmFibGVkLCB0aW1lIGJldHdlZW4gdHJhbnNpdGlvbnNcclxuICAgICAgICAgICAgcGF1c2VPbkhvdmVyOiBmYWxzZSwgXHRcdFx0XHQvLyBpZiB5b3UgaG92ZXIgcGF1c2VzIHRoZSBzbGlkZXJcclxuICAgICAgICAgICAgc3RhcnRDbG9ja09uTW91c2VPdXQ6IGZhbHNlLCBcdFx0Ly8gaWYgY2xvY2sgc2hvdWxkIHN0YXJ0IG9uIE1vdXNlT3V0XHJcbiAgICAgICAgICAgIHN0YXJ0Q2xvY2tPbk1vdXNlT3V0QWZ0ZXI6IDEwMDAsIFx0Ly8gaG93IGxvbmcgYWZ0ZXIgTW91c2VPdXQgc2hvdWxkIHRoZSB0aW1lciBzdGFydCBhZ2FpblxyXG4gICAgICAgICAgICBkaXJlY3Rpb25hbE5hdjogdHJ1ZSwgXHRcdFx0XHQvLyBtYW51YWwgYWR2YW5jaW5nIGRpcmVjdGlvbmFsIG5hdnNcclxuICAgICAgICAgICAgY2FwdGlvbnM6IHRydWUsIFx0XHRcdFx0XHQvLyBkbyB5b3Ugd2FudCBjYXB0aW9ucz9cclxuICAgICAgICAgICAgY2FwdGlvbkFuaW1hdGlvbjogJ2ZhZGUnLCBcdFx0XHQvLyBmYWRlLCBzbGlkZU9wZW4sIG5vbmVcclxuICAgICAgICAgICAgY2FwdGlvbkFuaW1hdGlvblNwZWVkOiA2MDAsIFx0XHQvLyBpZiBzbyBob3cgcXVpY2tseSBzaG91bGQgdGhleSBhbmltYXRlIGluXHJcbiAgICAgICAgICAgIGJ1bGxldHM6IGZhbHNlLFx0XHRcdFx0XHRcdC8vIHRydWUgb3IgZmFsc2UgdG8gYWN0aXZhdGUgdGhlIGJ1bGxldCBuYXZpZ2F0aW9uXHJcbiAgICAgICAgICAgIGJ1bGxldFRodW1iczogZmFsc2UsXHRcdFx0XHQvLyB0aHVtYm5haWxzIGZvciB0aGUgYnVsbGV0c1xyXG4gICAgICAgICAgICBidWxsZXRUaHVtYkxvY2F0aW9uOiAnJyxcdFx0XHQvLyBsb2NhdGlvbiBmcm9tIHRoaXMgZmlsZSB3aGVyZSB0aHVtYnMgd2lsbCBiZVxyXG4gICAgICAgICAgICBhZnRlclNsaWRlQ2hhbmdlOiBmdW5jdGlvbigpe30sICAgICAvLyBlbXB0eSBmdW5jdGlvblxyXG4gICAgICAgICAgICBudW1iZXJzOiBmYWxzZVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIC8vRXh0ZW5kIHRob3NlIG9wdGlvbnNcclxuICAgICAgICB2YXIgb3B0aW9ucyA9ICQuZXh0ZW5kKGRlZmF1bHRzLCBvcHRpb25zKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbigpIHtcclxuXHJcbi8vID09PT09PT09PT09PT09XHJcbi8vICEgU0VUVVBcclxuLy8gPT09PT09PT09PT09PT1cclxuXHJcbiAgICAgICAgICAgIC8vR2xvYmFsIFZhcmlhYmxlc1xyXG4gICAgICAgICAgICB2YXIgYWN0aXZlU2xpZGUgPSAwLFxyXG4gICAgICAgICAgICAgICAgbnVtYmVyU2xpZGVzID0gMCxcclxuICAgICAgICAgICAgICAgIG9yYml0V2lkdGgsXHJcbiAgICAgICAgICAgICAgICBvcmJpdEhlaWdodCxcclxuICAgICAgICAgICAgICAgIGxvY2tlZDtcclxuXHJcbiAgICAgICAgICAgIC8vSW5pdGlhbGl6ZVxyXG4gICAgICAgICAgICB2YXIgb3JiaXQgPSAkKHRoaXMpLmFkZENsYXNzKCdvcmJpdCcpLFxyXG4gICAgICAgICAgICAgICAgb3JiaXRXcmFwcGVyID0gb3JiaXQud3JhcCgnPGRpdiBjbGFzcz1cIm9yYml0LXdyYXBwZXJcIiAvPicpLnBhcmVudCgpO1xyXG4gICAgICAgICAgICBvcmJpdC5hZGQob3JiaXRXaWR0aCkud2lkdGgoJzFweCcpLmhlaWdodCgnMXB4Jyk7XHJcblxyXG4gICAgICAgICAgICAvL0NvbGxlY3QgYWxsIHNsaWRlcyBhbmQgc2V0IHNsaWRlciBzaXplIG9mIGxhcmdlc3QgaW1hZ2VcclxuICAgICAgICAgICAgdmFyIHNsaWRlcyA9IG9yYml0LmNoaWxkcmVuKCdpbWcsIGEsIGRpdicpO1xyXG4gICAgICAgICAgICBzbGlkZXMuZWFjaChmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIHZhciBfc2xpZGUgPSAkKHRoaXMpLFxyXG4gICAgICAgICAgICAgICAgICAgIF9zbGlkZVdpZHRoID0gX3NsaWRlLndpZHRoKCksXHJcbiAgICAgICAgICAgICAgICAgICAgX3NsaWRlSGVpZ2h0ID0gX3NsaWRlLmhlaWdodCgpO1xyXG4gICAgICAgICAgICAgICAgaWYoX3NsaWRlV2lkdGggPiBvcmJpdC53aWR0aCgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb3JiaXQuYWRkKG9yYml0V3JhcHBlcikud2lkdGgoX3NsaWRlV2lkdGgpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9yYml0V2lkdGggPSBvcmJpdC53aWR0aCgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYoX3NsaWRlSGVpZ2h0ID4gb3JiaXQuaGVpZ2h0KCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBvcmJpdC5hZGQob3JiaXRXcmFwcGVyKS5oZWlnaHQoX3NsaWRlSGVpZ2h0KTtcclxuICAgICAgICAgICAgICAgICAgICBvcmJpdEhlaWdodCA9IG9yYml0LmhlaWdodCgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgbnVtYmVyU2xpZGVzKys7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy9BbmltYXRpb24gbG9ja2luZyBmdW5jdGlvbnNcclxuICAgICAgICAgICAgZnVuY3Rpb24gdW5sb2NrKCkge1xyXG4gICAgICAgICAgICAgICAgbG9ja2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZnVuY3Rpb24gbG9jaygpIHtcclxuICAgICAgICAgICAgICAgIGxvY2tlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vSWYgdGhlcmUgaXMgb25seSBhIHNpbmdsZSBzbGlkZSByZW1vdmUgbmF2LCB0aW1lciBhbmQgYnVsbGV0c1xyXG4gICAgICAgICAgICBpZihzbGlkZXMubGVuZ3RoID09IDEpIHtcclxuICAgICAgICAgICAgICAgIG9wdGlvbnMuZGlyZWN0aW9uYWxOYXYgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIG9wdGlvbnMudGltZXIgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIG9wdGlvbnMuYnVsbGV0cyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgb3B0aW9ucy5udW1iZXJzID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vU2V0IGluaXRpYWwgZnJvbnQgcGhvdG8gei1pbmRleCBhbmQgZmFkZXMgaXQgaW5cclxuICAgICAgICAgICAgc2xpZGVzLmVxKGFjdGl2ZVNsaWRlKVxyXG4gICAgICAgICAgICAgICAgLmNzcyh7XCJ6LWluZGV4XCIgOiAzfSlcclxuICAgICAgICAgICAgICAgIC5mYWRlSW4oZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy9icmluZ3MgaW4gYWxsIG90aGVyIHNsaWRlcyBJRiBjc3MgZGVjbGFyZXMgYSBkaXNwbGF5OiBub25lXHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzLmNzcyh7XCJkaXNwbGF5XCI6XCJpbmxpbmUtYmxvY2tcIn0pXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbi8vID09PT09PT09PT09PT09XHJcbi8vICEgVElNRVJcclxuLy8gPT09PT09PT09PT09PT1cclxuXHJcbiAgICAgICAgICAgIC8vVGltZXIgRXhlY3V0aW9uXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIHN0YXJ0Q2xvY2soKSB7XHJcbiAgICAgICAgICAgICAgICBpZighb3B0aW9ucy50aW1lciAgfHwgb3B0aW9ucy50aW1lciA9PSAnZmFsc2UnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vaWYgdGltZXIgaXMgaGlkZGVuLCBkb24ndCBuZWVkIHRvIGRvIGNyYXp5IGNhbGN1bGF0aW9uc1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmKHRpbWVyLmlzKCc6aGlkZGVuJykpIHtcclxuICAgICAgICAgICAgICAgICAgICBjbG9jayA9IHNldEludGVydmFsKGZ1bmN0aW9uKGUpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaGlmdChcIm5leHRcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgb3B0aW9ucy5hZHZhbmNlU3BlZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vaWYgdGltZXIgaXMgdmlzaWJsZSBhbmQgd29ya2luZywgbGV0J3MgZG8gc29tZSBtYXRoXHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpbWVyUnVubmluZyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgcGF1c2UucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNsb2NrID0gc2V0SW50ZXJ2YWwoZnVuY3Rpb24oZSl7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvKiAgc2hpZnQoXCJuZXh0XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgdGltZXJMaW5lTGVmdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgLmNzcyAoe1wid2lkdGhcIjogMH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAuYW5pbWF0ZSh7XCJ3aWR0aFwiOiA0MH0sIG9wdGlvbnMuYW5pbWF0aW9uU3BlZWQsIHJlc2V0QW5kVW5sb2NrKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIHRpbWVyTGluZVJpZ2h0LmNzcyAoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgXCJ3aWR0aFwiOiB3aWR0aFRpbWVyQ3NzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICB9KTsqL1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLyp2YXIgcG9zaXRpb25CYWNrZ3JvdW5kQ3NzID0gXCItXCIgKyBwb3NpdGlvbkJhY2tncm91bmQgKydweCc7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb25CYWNrZ3JvdW5kICs9IDAuNDsqL1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLyogc2xpZGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAuZXEoYWN0aXZlU2xpZGUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAuY3NzKHtcImJhY2tncm91bmRQb3NpdGlvblwiOnBvc2l0aW9uQmFja2dyb3VuZENzc30pOyovXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvKiAgaWYocG9zaXRpb25CYWNrZ3JvdW5kID4gNDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uQmFja2dyb3VuZCAgPSAwO1xyXG5cclxuXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgfSovXHJcblxyXG5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoVGltZXJsaW5lICs9IHdpZHRoVGltZXJsaW5lSztcclxuXHJcblxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZXEoYWN0aXZlU2xpZGUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuY3NzKHtcImJhY2tncm91bmRQb3NpdGlvblwiOndpZHRoVGltZXJsaW5lKi0yKydweCd9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpbWVyTGluZUxlZnQuY3NzICh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIndpZHRoXCI6IHdpZHRoVGltZXJsaW5lKyclJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGltZXJMaW5lUmlnaHQuY3NzICh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIndpZHRoXCI6IHdpZHRoVGltZXJsaW5lKyclJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3JuYW1lbnQuY3NzICh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ3aWR0aFwiOiB3aWR0aFRpbWVybGluZSsnJSdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYod2lkdGhUaW1lcmxpbmUgPiAxMDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qICB3aWR0aFRpbWVybGluZSAgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpbWVyTGluZUxlZnQuYW5pbWF0ZSh7XCJ3aWR0aFwiOjB9LCBvcHRpb25zLmFuaW1hdGlvblNwZWVkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aW1lckxpbmVSaWdodC5hbmltYXRlKHtcIndpZHRoXCI6MH0sIG9wdGlvbnMuYW5pbWF0aW9uU3BlZWQpOyovXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2hpZnQoXCJuZXh0XCIpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvKiAgICB2YXIgZGVncmVlQ1NTID0gXCJyb3RhdGUoXCIrZGVncmVlcytcImRlZylcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgZGVncmVlcyArPSAyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICByb3RhdG9yLmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBcIi13ZWJraXQtdHJhbnNmb3JtXCI6IGRlZ3JlZUNTUyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgIFwiLW1vei10cmFuc2Zvcm1cIjogZGVncmVlQ1NTLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgXCItby10cmFuc2Zvcm1cIjogZGVncmVlQ1NTXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBpZihkZWdyZWVzID4gb3B0aW9ucy5hZHZhbmNlU3BlZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIHJvdGF0b3IucmVtb3ZlQ2xhc3MoJ21vdmUnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIG1hc2sucmVtb3ZlQ2xhc3MoJ21vdmUnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIGRlZ3JlZXMgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgc2hpZnQoXCJuZXh0XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgfSovXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgb3B0aW9ucy5hZHZhbmNlU3BlZWQvMTgwKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBmdW5jdGlvbiBzdG9wQ2xvY2soKSB7XHJcbiAgICAgICAgICAgICAgICBpZighb3B0aW9ucy50aW1lciB8fCBvcHRpb25zLnRpbWVyID09ICdmYWxzZScpIHsgcmV0dXJuIGZhbHNlOyB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpbWVyUnVubmluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwoY2xvY2spO1xyXG4gICAgICAgICAgICAgICAgICAgIHBhdXNlLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy9UaW1lciBTZXR1cFxyXG4gICAgICAgICAgICBpZihvcHRpb25zLnRpbWVyKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgdGltZXJIVE1MID0gJzxkaXYgY2xhc3M9XCJ0aW1lclwiPicgK1xyXG4gICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwib3JiaXQtb3JuYW1lbnRcIj48ZGl2IGNsYXNzPVwib3JiaXQtb3JuYW1lbnQyXCI+PC9kaXY+PC9kaXY+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJ0aW1lcl9fbGVmdFwiPiAnICtcclxuICAgICAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cInRpbWVyLWxpbmUgdGltZXItbGluZS1sZWZ0XCI+PC9kaXY+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJ0aW1lci1saW5lMiB0aW1lci1saW5lMi1sZWZ0XCI+PC9kaXY+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgJzwvZGl2PicgK1xyXG4gICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwidGltZXJfX3JpZ2h0XCI+ICcgK1xyXG4gICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwidGltZXItbGluZSB0aW1lci1saW5lLXJpZ2h0XCI+PC9kaXY+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJ0aW1lci1saW5lMiB0aW1lci1saW5lMi1yaWdodFwiPjwvZGl2PicgK1xyXG4gICAgICAgICAgICAgICAgICAgICc8L2Rpdj4nK1xyXG4gICAgICAgICAgICAgICAgICAgICc8c3BhbiBjbGFzcz1cIm1hc2tcIj48c3BhbiBjbGFzcz1cInJvdGF0b3JcIj48L3NwYW4+PC9zcGFuPjxzcGFuIGNsYXNzPVwicGF1c2VcIj48L3NwYW4+PC9kaXY+J1xyXG4gICAgICAgICAgICAgICAgLy92YXIgdGltZXJIVE1MID0gJzxkaXYgY2xhc3M9XCJ0aW1lclwiPjxzcGFuIGNsYXNzPVwibWFza1wiPjxzcGFuIGNsYXNzPVwicm90YXRvclwiPjwvc3Bhbj48L3NwYW4+PHNwYW4gY2xhc3M9XCJwYXVzZVwiPjwvc3Bhbj48L2Rpdj4nXHJcbiAgICAgICAgICAgICAgICBvcmJpdFdyYXBwZXIuYXBwZW5kKHRpbWVySFRNTCk7XHJcbiAgICAgICAgICAgICAgICB2YXIgdGltZXIgPSBvcmJpdFdyYXBwZXIuY2hpbGRyZW4oJ2Rpdi50aW1lcicpLFxyXG4gICAgICAgICAgICAgICAgICAgIHRpbWVyUnVubmluZztcclxuICAgICAgICAgICAgICAgIGlmKHRpbWVyLmxlbmd0aCAhPSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIHJvdGF0b3IgPSAkKCdkaXYudGltZXIgc3Bhbi5yb3RhdG9yJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ybmFtZW50ID0gJCgnLm9yYml0LW9ybmFtZW50MicpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbkJhY2tncm91bmQgPSAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aW1lckxpbmVMZWZ0ID0gJCgnZGl2LnRpbWVyLWxpbmUtbGVmdCcpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aW1lckxpbmVSaWdodCA9ICQoJ2Rpdi50aW1lci1saW5lLXJpZ2h0JyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoVGltZXJsaW5lID0gMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFzayA9ICQoJ2Rpdi50aW1lciBzcGFuLm1hc2snKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGF1c2UgPSAkKCdkaXYudGltZXIgc3Bhbi5wYXVzZScpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWdyZWVzID0gMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGhUaW1lcmxpbmVLID0gb3B0aW9ucy5hZHZhbmNlU3BlZWQvMTgwLzEwMCoxLjUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uQmFja2dyb3VuZEsgPSBvcHRpb25zLmFkdmFuY2VTcGVlZC8xODAvMTAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbG9jaztcclxuICAgICAgICAgICAgICAgICAgICBzdGFydENsb2NrKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGltZXIuY2xpY2soZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKCF0aW1lclJ1bm5pbmcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0Q2xvY2soKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0b3BDbG9jaygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYob3B0aW9ucy5zdGFydENsb2NrT25Nb3VzZU91dCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBvdXRUaW1lcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3JiaXRXcmFwcGVyLm1vdXNlbGVhdmUoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdXRUaW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYoIXRpbWVyUnVubmluZyl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0Q2xvY2soKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBvcHRpb25zLnN0YXJ0Q2xvY2tPbk1vdXNlT3V0QWZ0ZXIpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9yYml0V3JhcHBlci5tb3VzZWVudGVyKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG91dFRpbWVyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vUGF1c2UgVGltZXIgb24gaG92ZXJcclxuICAgICAgICAgICAgaWYob3B0aW9ucy5wYXVzZU9uSG92ZXIpIHtcclxuICAgICAgICAgICAgICAgIG9yYml0V3JhcHBlci5tb3VzZWVudGVyKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHN0b3BDbG9jaygpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbi8vID09PT09PT09PT09PT09XHJcbi8vICEgQ0FQVElPTlNcclxuLy8gPT09PT09PT09PT09PT1cclxuXHJcbiAgICAgICAgICAgIC8vQ2FwdGlvbiBTZXR1cFxyXG4gICAgICAgICAgICBpZihvcHRpb25zLmNhcHRpb25zKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgY2FwdGlvbkhUTUwgPSAnPGRpdiBjbGFzcz1cIm9yYml0LWNhcHRpb25cIj48L2Rpdj4nO1xyXG4gICAgICAgICAgICAgICAgb3JiaXRXcmFwcGVyLmFwcGVuZChjYXB0aW9uSFRNTCk7XHJcbiAgICAgICAgICAgICAgICB2YXIgY2FwdGlvbiA9IG9yYml0V3JhcHBlci5jaGlsZHJlbignLm9yYml0LWNhcHRpb24nKTtcclxuICAgICAgICAgICAgICAgIHNldENhcHRpb24oKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy9DYXB0aW9uIEV4ZWN1dGlvblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBzZXRDYXB0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgaWYoIW9wdGlvbnMuY2FwdGlvbnMgfHwgb3B0aW9ucy5jYXB0aW9ucyA9PVwiZmFsc2VcIikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2NhcHRpb24nKTtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgX2NhcHRpb25Mb2NhdGlvbiA9IHNsaWRlcy5lcShhY3RpdmVTbGlkZSkuZGF0YSgnY2FwdGlvbicpOyAvL2dldCBJRCBmcm9tIHJlbCB0YWcgb24gaW1hZ2VcclxuICAgICAgICAgICAgICAgICAgICBfY2FwdGlvbkhUTUwgPSAkKF9jYXB0aW9uTG9jYXRpb24pLmh0bWwoKTsgLy9nZXQgSFRNTCBmcm9tIHRoZSBtYXRjaGluZyBIVE1MIGVudGl0eVxyXG4gICAgICAgICAgICAgICAgICAgIC8vU2V0IEhUTUwgZm9yIHRoZSBjYXB0aW9uIGlmIGl0IGV4aXN0c1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKF9jYXB0aW9uSFRNTCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXB0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuYXR0cignaWQnLF9jYXB0aW9uTG9jYXRpb24pIC8vIEFkZCBJRCBjYXB0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuaHRtbChfY2FwdGlvbkhUTUwpOyAvLyBDaGFuZ2UgSFRNTCBpbiBDYXB0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vQW5pbWF0aW9ucyBmb3IgQ2FwdGlvbiBlbnRyYW5jZXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYob3B0aW9ucy5jYXB0aW9uQW5pbWF0aW9uID09ICdub25lJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FwdGlvbi5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYob3B0aW9ucy5jYXB0aW9uQW5pbWF0aW9uID09ICdmYWRlJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FwdGlvbi5mYWRlSW4ob3B0aW9ucy5jYXB0aW9uQW5pbWF0aW9uU3BlZWQpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIub3JiaXQtY2FwdGlvbl9fZmFkZVwiKS5mYWRlVG9nZ2xlKDApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIi5vcmJpdC1jYXB0aW9uX19mYWRlXCIpLmZhZGVUb2dnbGUoNDAwKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYob3B0aW9ucy5jYXB0aW9uQW5pbWF0aW9uID09ICdzbGlkZU9wZW4nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXB0aW9uLnNsaWRlRG93bihvcHRpb25zLmNhcHRpb25BbmltYXRpb25TcGVlZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL0FuaW1hdGlvbnMgZm9yIENhcHRpb24gZXhpdHNcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYob3B0aW9ucy5jYXB0aW9uQW5pbWF0aW9uID09ICdub25lJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FwdGlvbi5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYob3B0aW9ucy5jYXB0aW9uQW5pbWF0aW9uID09ICdmYWRlJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FwdGlvbi5mYWRlT3V0KG9wdGlvbnMuY2FwdGlvbkFuaW1hdGlvblNwZWVkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihvcHRpb25zLmNhcHRpb25BbmltYXRpb24gPT0gJ3NsaWRlT3BlbicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhcHRpb24uc2xpZGVVcChvcHRpb25zLmNhcHRpb25BbmltYXRpb25TcGVlZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbi8vID09PT09PT09PT09PT09PT09PVxyXG4vLyAhIERJUkVDVElPTkFMIE5BVlxyXG4vLyA9PT09PT09PT09PT09PT09PT1cclxuXHJcbiAgICAgICAgICAgIC8vRGlyZWN0aW9uYWxOYXYgeyByaWdodEJ1dHRvbiAtLT4gc2hpZnQoXCJuZXh0XCIpLCBsZWZ0QnV0dG9uIC0tPiBzaGlmdChcInByZXZcIik7XHJcbiAgICAgICAgICAgIGlmKG9wdGlvbnMuZGlyZWN0aW9uYWxOYXYpIHtcclxuICAgICAgICAgICAgICAgIGlmKG9wdGlvbnMuZGlyZWN0aW9uYWxOYXYgPT0gXCJmYWxzZVwiKSB7IHJldHVybiBmYWxzZTsgfVxyXG4gICAgICAgICAgICAgICAgdmFyIGRpcmVjdGlvbmFsTmF2SFRNTCA9ICc8ZGl2IGNsYXNzPVwic2xpZGVyLW5hdlwiPjxzcGFuIGNsYXNzPVwicmlnaHRcIj5SaWdodDwvc3Bhbj48c3BhbiBjbGFzcz1cImxlZnRcIj5MZWZ0PC9zcGFuPjwvZGl2Pic7XHJcbiAgICAgICAgICAgICAgICBvcmJpdFdyYXBwZXIuYXBwZW5kKGRpcmVjdGlvbmFsTmF2SFRNTCk7XHJcbiAgICAgICAgICAgICAgICB2YXIgbGVmdEJ0biA9IG9yYml0V3JhcHBlci5jaGlsZHJlbignZGl2LnNsaWRlci1uYXYnKS5jaGlsZHJlbignc3Bhbi5sZWZ0JyksXHJcbiAgICAgICAgICAgICAgICAgICAgcmlnaHRCdG4gPSBvcmJpdFdyYXBwZXIuY2hpbGRyZW4oJ2Rpdi5zbGlkZXItbmF2JykuY2hpbGRyZW4oJ3NwYW4ucmlnaHQnKTtcclxuICAgICAgICAgICAgIC8qICAgdmFyICBkaXZPcmJpdCA9ICQoJy5vcmJpdCcpLmFwcGVuZChkaXJlY3Rpb25hbE5hdkhUTUwpLFxyXG4gICAgICAgICAgICAgICAgICAgIGxlZnRCdG4gPSBkaXZPcmJpdC5jaGlsZHJlbignZGl2LnNsaWRlci1uYXYnKS5jaGlsZHJlbignc3Bhbi5sZWZ0JyksXHJcbiAgICAgICAgICAgICAgICAgICAgcmlnaHRCdG4gPSBkaXZPcmJpdC5jaGlsZHJlbignZGl2LnNsaWRlci1uYXYnKS5jaGlsZHJlbignc3Bhbi5yaWdodCcpOyovXHJcblxyXG4gICAgICAgICAgICAgICAgbGVmdEJ0bi5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICAvKiBzdG9wQ2xvY2soKTsqL1xyXG4gICAgICAgICAgICAgICAgICAgIHNoaWZ0KFwicHJldlwiKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgcmlnaHRCdG4uY2xpY2soZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLyogIHN0b3BDbG9jaygpOyovXHJcbiAgICAgICAgICAgICAgICAgICAgc2hpZnQoXCJuZXh0XCIpXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4vLyA9PT09PT09PT09PT09PT09PT1cclxuLy8gISBTTElERVIgTlVNQkVSU1xyXG4vLyA9PT09PT09PT09PT09PT09PT1cclxuICAgICAgICAgICAgaWYob3B0aW9ucy5udW1iZXJzID09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgIHZhciBzbGlkZU51bWJlcnNIVE1MID0gJzxkaXYgY2xhc3M9XCJvcmJpdC1udW1iZXJzXCI+PHNwYW4gY2xhc3M9XCJvcmJpdC1udW1iZXJzX19jdXJyZW50XCI+MTwvc3Bhbj4vPHNwYW4gY2xhc3M9XCJvcmJpdC1udW1iZXJzX19sYXN0XCI+JytudW1iZXJTbGlkZXMrJzwvc3Bhbj48L2Rpdj4nO1xyXG4gICAgICAgICAgICAgICAgb3JiaXRXcmFwcGVyLmFwcGVuZChzbGlkZU51bWJlcnNIVE1MKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuXHJcblxyXG4gICAgICAgICAgICBmdW5jdGlvbiBzbGlkZXJWaWV3KCl7XHJcbiAgICAgICAgICAgICAgICAkKCcuY29udGVudC1zbGlkZXInKS5jc3Moe3Bvc2l0aW9uOidzdGF0aWMnfSx7XCJ3aWR0aFwiOjUwMH0pO1xyXG5cclxuICAgICAgICAgICAgfVxyXG5cclxuLy8gPT09PT09PT09PT09PT09PT09XHJcbi8vICEgQlVMTEVUIE5BVlxyXG4vLyA9PT09PT09PT09PT09PT09PT1cclxuXHJcbiAgICAgICAgICAgIC8vQnVsbGV0IE5hdiBTZXR1cFxyXG4gICAgICAgICAgICBpZihvcHRpb25zLmJ1bGxldHMpIHtcclxuICAgICAgICAgICAgICAgIHZhciBidWxsZXRIVE1MID0gJzx1bCBjbGFzcz1cIm9yYml0LWJ1bGxldHNcIj48L3VsPic7XHJcbiAgICAgICAgICAgICAgICBvcmJpdFdyYXBwZXIuYXBwZW5kKGJ1bGxldEhUTUwpO1xyXG4gICAgICAgICAgICAgICAgdmFyIGJ1bGxldHMgPSBvcmJpdFdyYXBwZXIuY2hpbGRyZW4oJ3VsLm9yYml0LWJ1bGxldHMnKTtcclxuXHJcbiAgICAgICAgICAgICAgICBmb3IoaT0wOyBpPG51bWJlclNsaWRlczsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGxpTWFya3VwID0gJCgnPGxpPicrKGkrMSkrJzwvbGk+Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYob3B0aW9ucy5idWxsZXRUaHVtYnMpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhclx0dGh1bWJOYW1lID0gc2xpZGVzLmVxKGkpLmRhdGEoJ3RodW1iJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHRodW1iTmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGxpTWFya3VwID0gJCgnPGxpIGNsYXNzPVwiaGFzLXRodW1iXCI+JytpKyc8L2xpPicpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpTWFya3VwLmNzcyh7XCJiYWNrZ3JvdW5kXCIgOiBcInVybChcIitvcHRpb25zLmJ1bGxldFRodW1iTG9jYXRpb24rdGh1bWJOYW1lK1wiKSBuby1yZXBlYXRcIn0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIG9yYml0V3JhcHBlci5jaGlsZHJlbigndWwub3JiaXQtYnVsbGV0cycpLmFwcGVuZChsaU1hcmt1cCk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGlNYXJrdXAuZGF0YSgnaW5kZXgnLGkpO1xyXG4gICAgICAgICAgICAgICAgICAgIGxpTWFya3VwLmNsaWNrKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdG9wQ2xvY2soKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2hpZnQoJCh0aGlzKS5kYXRhKCdpbmRleCcpKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgc2V0QWN0aXZlQnVsbGV0KCk7XHJcblxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvL0J1bGxldCBOYXYgRXhlY3V0aW9uXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIHNldEFjdGl2ZUJ1bGxldCgpIHtcclxuICAgICAgICAgICAgICAgIGlmKCFvcHRpb25zLmJ1bGxldHMpIHsgcmV0dXJuIGZhbHNlOyB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGJ1bGxldHMuY2hpbGRyZW4oJ2xpJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpLmVxKGFjdGl2ZVNsaWRlKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbi8vID09PT09PT09PT09PT09PT09PT09XHJcbi8vICEgU0hJRlQgQU5JTUFUSU9OU1xyXG4vLyA9PT09PT09PT09PT09PT09PT09PVxyXG5cclxuICAgICAgICAgICAgLy9BbmltYXRpbmcgdGhlIHNoaWZ0IVxyXG4gICAgICAgICAgICBmdW5jdGlvbiBzaGlmdChkaXJlY3Rpb24pIHtcclxuICAgICAgICAgICAgICAgIC8vcmVtZW1iZXIgcHJldmlvdXMgYWN0aXZlU2xpZGVcclxuICAgICAgICAgICAgICAgIHZhciBwcmV2QWN0aXZlU2xpZGUgPSBhY3RpdmVTbGlkZSxcclxuICAgICAgICAgICAgICAgICAgICBzbGlkZURpcmVjdGlvbiA9IGRpcmVjdGlvbjtcclxuICAgICAgICAgICAgICAgIC8vZXhpdCBmdW5jdGlvbiBpZiBidWxsZXQgY2xpY2tlZCBpcyBzYW1lIGFzIHRoZSBjdXJyZW50IGltYWdlXHJcbiAgICAgICAgICAgICAgICBpZihwcmV2QWN0aXZlU2xpZGUgPT0gc2xpZGVEaXJlY3Rpb24pIHsgcmV0dXJuIGZhbHNlOyB9XHJcbiAgICAgICAgICAgICAgICAvL3Jlc2V0IFogJiBVbmxvY2tcclxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIHJlc2V0QW5kVW5sb2NrKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAuZXEocHJldkFjdGl2ZVNsaWRlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuY3NzKHtcInotaW5kZXhcIiA6IDF9KTtcclxuICAgICAgICAgICAgICAgICAgICB1bmxvY2soKTtcclxuICAgICAgICAgICAgICAgICAgICBvcHRpb25zLmFmdGVyU2xpZGVDaGFuZ2UuY2FsbCh0aGlzKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmKHNsaWRlcy5sZW5ndGggPT0gXCIxXCIpIHsgcmV0dXJuIGZhbHNlOyB9XHJcbiAgICAgICAgICAgICAgICBpZighbG9ja2VkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbG9jaygpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vZGVkdWNlIHRoZSBwcm9wZXIgYWN0aXZlSW1hZ2VcclxuICAgICAgICAgICAgICAgICAgICBpZihkaXJlY3Rpb24gPT0gXCJuZXh0XCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aXZlU2xpZGUrK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihhY3RpdmVTbGlkZSA9PSBudW1iZXJTbGlkZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFjdGl2ZVNsaWRlID0gMDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZihkaXJlY3Rpb24gPT0gXCJwcmV2XCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aXZlU2xpZGUtLVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihhY3RpdmVTbGlkZSA8IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFjdGl2ZVNsaWRlID0gbnVtYmVyU2xpZGVzLTE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3RpdmVTbGlkZSA9IGRpcmVjdGlvbjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHByZXZBY3RpdmVTbGlkZSA8IGFjdGl2ZVNsaWRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzbGlkZURpcmVjdGlvbiA9IFwibmV4dFwiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHByZXZBY3RpdmVTbGlkZSA+IGFjdGl2ZVNsaWRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzbGlkZURpcmVjdGlvbiA9IFwicHJldlwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmKG9wdGlvbnMudGltZXI9PSB0cnVlKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGhUaW1lcmxpbmUgID0gMDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGltZXJMaW5lTGVmdC5hbmltYXRlKHtcIndpZHRoXCI6MH0sIG9wdGlvbnMuYW5pbWF0aW9uU3BlZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aW1lckxpbmVSaWdodC5hbmltYXRlKHtcIndpZHRoXCI6MH0sIG9wdGlvbnMuYW5pbWF0aW9uU3BlZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBpZihvcHRpb25zLm51bWJlcnMgPT0gdHJ1ZSApe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcmJpdFdyYXBwZXIuZmluZChcIi5vcmJpdC1udW1iZXJzX19jdXJyZW50XCIpLnRleHQoYWN0aXZlU2xpZGUrMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vc2V0IHRvIGNvcnJlY3QgYnVsbGV0XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0QWN0aXZlQnVsbGV0KCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vc2V0IHByZXZpb3VzIHNsaWRlIHotaW5kZXggdG8gb25lIGJlbG93IHdoYXQgbmV3IGFjdGl2ZVNsaWRlIHdpbGwgYmVcclxuICAgICAgICAgICAgICAgICAgICBzbGlkZXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmVxKHByZXZBY3RpdmVTbGlkZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmNzcyh7XCJ6LWluZGV4XCIgOiAyfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vZmFkZVxyXG4gICAgICAgICAgICAgICAgICAgIGlmKG9wdGlvbnMuYW5pbWF0aW9uID09IFwiZmFkZVwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmVxKGFjdGl2ZVNsaWRlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmNzcyh7XCJvcGFjaXR5XCIgOiAwLCBcInotaW5kZXhcIiA6IDN9KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmFuaW1hdGUoe1wib3BhY2l0eVwiIDogMX0sIG9wdGlvbnMuYW5pbWF0aW9uU3BlZWQsIHJlc2V0QW5kVW5sb2NrKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLy9ob3Jpem9udGFsLXNsaWRlXHJcbiAgICAgICAgICAgICAgICAgICAgaWYob3B0aW9ucy5hbmltYXRpb24gPT0gXCJob3Jpem9udGFsLXNsaWRlXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoc2xpZGVEaXJlY3Rpb24gPT0gXCJuZXh0XCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5lcShhY3RpdmVTbGlkZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuY3NzKHtcImxlZnRcIjogb3JiaXRXaWR0aCwgXCJ6LWluZGV4XCIgOiAzfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuYW5pbWF0ZSh7XCJsZWZ0XCIgOiAwfSwgb3B0aW9ucy5hbmltYXRpb25TcGVlZCwgcmVzZXRBbmRVbmxvY2spO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHNsaWRlRGlyZWN0aW9uID09IFwicHJldlwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZXEoYWN0aXZlU2xpZGUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmNzcyh7XCJsZWZ0XCI6IC1vcmJpdFdpZHRoLCBcInotaW5kZXhcIiA6IDN9KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hbmltYXRlKHtcImxlZnRcIiA6IDB9LCBvcHRpb25zLmFuaW1hdGlvblNwZWVkLCByZXNldEFuZFVubG9jayk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLy9ob3Jpem9udGFsLXJvbVxyXG4gICAgICAgICAgICAgICAgICAgIGlmKG9wdGlvbnMuYW5pbWF0aW9uID09IFwiaG9yaXpvbnRhbC1yb21cIikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihzbGlkZURpcmVjdGlvbiA9PSBcIm5leHRcIikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmVxKGFjdGl2ZVNsaWRlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5jc3Moe1wiei1pbmRleFwiIDogMn0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmVxKHByZXZBY3RpdmVTbGlkZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuY3NzKHtcIndpZHRoXCI6b3JiaXRXaWR0aCwgXCJ6LWluZGV4XCIgOiAzfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuYW5pbWF0ZSh7XCJ3aWR0aFwiIDowIH0sIG9wdGlvbnMuYW5pbWF0aW9uU3BlZWQsIGZ1bmN0aW9uKCl7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5lcShhY3RpdmVTbGlkZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5jc3Moe1wiei1pbmRleFwiIDogM30pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuYW5pbWF0ZSh7fSwgb3B0aW9ucy5hbmltYXRpb25TcGVlZCwgcmVzZXRBbmRVbmxvY2spO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZXEocHJldkFjdGl2ZVNsaWRlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmNzcyh7XCJ3aWR0aFwiIDogb3JiaXRXaWR0aH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoc2xpZGVEaXJlY3Rpb24gPT0gXCJwcmV2XCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5lcShhY3RpdmVTbGlkZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuY3NzKHtcIndpZHRoXCI6MCxcImxlZnRcIjowLCBcInotaW5kZXhcIiA6IDN9KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hbmltYXRlKHtcIndpZHRoXCIgOiBvcmJpdFdpZHRofSwgb3B0aW9ucy5hbmltYXRpb25TcGVlZCwgcmVzZXRBbmRVbmxvY2spO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIC8vdmVydGljYWwtc2xpZGVcclxuICAgICAgICAgICAgICAgICAgICBpZihvcHRpb25zLmFuaW1hdGlvbiA9PSBcInZlcnRpY2FsLXNsaWRlXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoc2xpZGVEaXJlY3Rpb24gPT0gXCJwcmV2XCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5lcShhY3RpdmVTbGlkZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuY3NzKHtcInRvcFwiOiBvcmJpdEhlaWdodCwgXCJ6LWluZGV4XCIgOiAzfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuYW5pbWF0ZSh7XCJ0b3BcIiA6IDB9LCBvcHRpb25zLmFuaW1hdGlvblNwZWVkLCByZXNldEFuZFVubG9jayk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoc2xpZGVEaXJlY3Rpb24gPT0gXCJuZXh0XCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5lcShhY3RpdmVTbGlkZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuY3NzKHtcInRvcFwiOiAtb3JiaXRIZWlnaHQsIFwiei1pbmRleFwiIDogM30pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmFuaW1hdGUoe1widG9wXCIgOiAwfSwgb3B0aW9ucy5hbmltYXRpb25TcGVlZCwgcmVzZXRBbmRVbmxvY2spO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIC8vcHVzaC1vdmVyXHJcbiAgICAgICAgICAgICAgICAgICAgaWYob3B0aW9ucy5hbmltYXRpb24gPT0gXCJob3Jpem9udGFsLXB1c2hcIikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihzbGlkZURpcmVjdGlvbiA9PSBcIm5leHRcIikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmVxKGFjdGl2ZVNsaWRlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5jc3Moe1wibGVmdFwiOiBvcmJpdFdpZHRoLCBcInotaW5kZXhcIiA6IDN9KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hbmltYXRlKHtcImxlZnRcIiA6IDB9LCBvcHRpb25zLmFuaW1hdGlvblNwZWVkLCByZXNldEFuZFVubG9jayk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZXEocHJldkFjdGl2ZVNsaWRlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hbmltYXRlKHtcImxlZnRcIiA6IC1vcmJpdFdpZHRofSwgb3B0aW9ucy5hbmltYXRpb25TcGVlZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoc2xpZGVEaXJlY3Rpb24gPT0gXCJwcmV2XCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5lcShhY3RpdmVTbGlkZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuY3NzKHtcImxlZnRcIjogLW9yYml0V2lkdGgsIFwiei1pbmRleFwiIDogM30pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmFuaW1hdGUoe1wibGVmdFwiIDogMH0sIG9wdGlvbnMuYW5pbWF0aW9uU3BlZWQsIHJlc2V0QW5kVW5sb2NrKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5lcShwcmV2QWN0aXZlU2xpZGUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmFuaW1hdGUoe1wibGVmdFwiIDogb3JiaXRXaWR0aH0sIG9wdGlvbnMuYW5pbWF0aW9uU3BlZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHNldENhcHRpb24oKTtcclxuICAgICAgICAgICAgICAgIH0gLy9sb2NrXHJcbiAgICAgICAgICAgIH0vL29yYml0IGZ1bmN0aW9uXHJcbiAgICAgICAgfSk7Ly9lYWNoIGNhbGxcclxuICAgIH0vL29yYml0IHBsdWdpbiBjYWxsXHJcbn0pKGpRdWVyeSk7XHJcbi8qIGpRdWVyeSBGb3JtIFN0eWxlciB2MS43LjQgfCAoYykgRGltb3ggfCBodHRwczovL2dpdGh1Yi5jb20vRGltb3gvalF1ZXJ5Rm9ybVN0eWxlciAqL1xyXG4oZnVuY3Rpb24oYil7XCJmdW5jdGlvblwiPT09dHlwZW9mIGRlZmluZSYmZGVmaW5lLmFtZD9kZWZpbmUoW1wiLi4vYm93ZXJfY29tcG9uZW50cy9qcXVlcnktZm9ybS1zdHlsZXIvanF1ZXJ5LmZvcm1zdHlsZXJcIl0sYik6XCJvYmplY3RcIj09PXR5cGVvZiBleHBvcnRzP21vZHVsZS5leHBvcnRzPWIocmVxdWlyZShcImpxdWVyeVwiKSk6YihqUXVlcnkpfSkoZnVuY3Rpb24oYil7ZnVuY3Rpb24geihjLCBhKXt0aGlzLmVsZW1lbnQ9Yzt0aGlzLm9wdGlvbnM9Yi5leHRlbmQoe30sTixhKTt0aGlzLmluaXQoKX1mdW5jdGlvbiBHKGMpe2lmKCFiKGMudGFyZ2V0KS5wYXJlbnRzKCkuaGFzQ2xhc3MoXCJqcS1zZWxlY3Rib3hcIikmJlwiT1BUSU9OXCIhPWMudGFyZ2V0Lm5vZGVOYW1lJiZiKFwiZGl2LmpxLXNlbGVjdGJveC5vcGVuZWRcIikubGVuZ3RoKXtjPWIoXCJkaXYuanEtc2VsZWN0Ym94Lm9wZW5lZFwiKTt2YXIgYT1iKFwiZGl2LmpxLXNlbGVjdGJveF9fc2VhcmNoIGlucHV0XCIsYyksZj1iKFwiZGl2LmpxLXNlbGVjdGJveF9fZHJvcGRvd25cIixjKTtjLmZpbmQoXCJzZWxlY3RcIikuZGF0YShcIl9cIitcclxuaCkub3B0aW9ucy5vblNlbGVjdENsb3NlZC5jYWxsKGMpO2EubGVuZ3RoJiZhLnZhbChcIlwiKS5rZXl1cCgpO2YuaGlkZSgpLmZpbmQoXCJsaS5zZWxcIikuYWRkQ2xhc3MoXCJzZWxlY3RlZFwiKTtjLnJlbW92ZUNsYXNzKFwiZm9jdXNlZCBvcGVuZWQgZHJvcHVwIGRyb3Bkb3duXCIpfX12YXIgaD1cInN0eWxlclwiLE49e3dyYXBwZXI6XCJmb3JtXCIsaWRTdWZmaXg6XCItc3R5bGVyXCIsZmlsZVBsYWNlaG9sZGVyOlwiXFx1MDQyNFxcdTA0MzBcXHUwNDM5XFx1MDQzYiBcXHUwNDNkXFx1MDQzNSBcXHUwNDMyXFx1MDQ0YlxcdTA0MzFcXHUwNDQwXFx1MDQzMFxcdTA0M2RcIixmaWxlQnJvd3NlOlwiXFx1MDQxZVxcdTA0MzFcXHUwNDM3XFx1MDQzZVxcdTA0NDAuLi5cIixmaWxlTnVtYmVyOlwiXFx1MDQxMlxcdTA0NGJcXHUwNDMxXFx1MDQ0MFxcdTA0MzBcXHUwNDNkXFx1MDQzZSBcXHUwNDQ0XFx1MDQzMFxcdTA0MzlcXHUwNDNiXFx1MDQzZVxcdTA0MzI6ICVzXCIsc2VsZWN0UGxhY2Vob2xkZXI6XCJcXHUwNDEyXFx1MDQ0YlxcdTA0MzFcXHUwNDM1XFx1MDQ0MFxcdTA0MzhcXHUwNDQyXFx1MDQzNS4uLlwiLFxyXG5zZWxlY3RTZWFyY2g6ITEsc2VsZWN0U2VhcmNoTGltaXQ6MTAsc2VsZWN0U2VhcmNoTm90Rm91bmQ6XCJcXHUwNDIxXFx1MDQzZVxcdTA0MzJcXHUwNDNmXFx1MDQzMFxcdTA0MzRcXHUwNDM1XFx1MDQzZFxcdTA0MzhcXHUwNDM5IFxcdTA0M2RcXHUwNDM1IFxcdTA0M2RcXHUwNDMwXFx1MDQzOVxcdTA0MzRcXHUwNDM1XFx1MDQzZFxcdTA0M2VcIixzZWxlY3RTZWFyY2hQbGFjZWhvbGRlcjpcIlxcdTA0MWZcXHUwNDNlXFx1MDQzOFxcdTA0NDFcXHUwNDNhLi4uXCIsc2VsZWN0VmlzaWJsZU9wdGlvbnM6MCxzaW5nbGVTZWxlY3R6SW5kZXg6XCIxMDBcIixzZWxlY3RTbWFydFBvc2l0aW9uaW5nOiEwLG9uU2VsZWN0T3BlbmVkOmZ1bmN0aW9uKCl7fSxvblNlbGVjdENsb3NlZDpmdW5jdGlvbigpe30sb25Gb3JtU3R5bGVkOmZ1bmN0aW9uKCl7fX07ei5wcm90b3R5cGU9e2luaXQ6ZnVuY3Rpb24oKXtmdW5jdGlvbiBjKCl7dmFyIGI9XCJcIixkPVwiXCIsYz1cIlwiLGU9XCJcIjt2b2lkIDAhPT1hLmF0dHIoXCJpZFwiKSYmXCJcIiE9PWEuYXR0cihcImlkXCIpJiZcclxuKGI9JyBpZD1cIicrYS5hdHRyKFwiaWRcIikrZi5pZFN1ZmZpeCsnXCInKTt2b2lkIDAhPT1hLmF0dHIoXCJ0aXRsZVwiKSYmXCJcIiE9PWEuYXR0cihcInRpdGxlXCIpJiYoZD0nIHRpdGxlPVwiJythLmF0dHIoXCJ0aXRsZVwiKSsnXCInKTt2b2lkIDAhPT1hLmF0dHIoXCJjbGFzc1wiKSYmXCJcIiE9PWEuYXR0cihcImNsYXNzXCIpJiYoYz1cIiBcIithLmF0dHIoXCJjbGFzc1wiKSk7dmFyIGw9YS5kYXRhKCksdDtmb3IodCBpbiBsKVwiXCIhPT1sW3RdJiZcIl9zdHlsZXJcIiE9PXQmJihlKz1cIiBkYXRhLVwiK3QrJz1cIicrbFt0XSsnXCInKTt0aGlzLmlkPWIrZTt0aGlzLnRpdGxlPWQ7dGhpcy5jbGFzc2VzPWN9dmFyIGE9Yih0aGlzLmVsZW1lbnQpLGY9dGhpcy5vcHRpb25zLHk9bmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvKGlQYWR8aVBob25lfGlQb2QpL2kpJiYhbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvKFdpbmRvd3NcXHNQaG9uZSkvaSk/ITA6ITEsaD1uYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKC9BbmRyb2lkL2kpJiZcclxuIW5hdmlnYXRvci51c2VyQWdlbnQubWF0Y2goLyhXaW5kb3dzXFxzUGhvbmUpL2kpPyEwOiExO2lmKGEuaXMoXCI6Y2hlY2tib3hcIikpe3ZhciB6PWZ1bmN0aW9uKCl7dmFyIGY9bmV3IGMsZD1iKFwiPGRpdlwiK2YuaWQrJyBjbGFzcz1cImpxLWNoZWNrYm94JytmLmNsYXNzZXMrJ1wiJytmLnRpdGxlKyc+PGRpdiBjbGFzcz1cImpxLWNoZWNrYm94X19kaXZcIj48L2Rpdj48L2Rpdj4nKTthLmNzcyh7cG9zaXRpb246XCJhYnNvbHV0ZVwiLHpJbmRleDpcIi0xXCIsb3BhY2l0eTowLG1hcmdpbjowLHBhZGRpbmc6MH0pLmFmdGVyKGQpLnByZXBlbmRUbyhkKTtkLmF0dHIoXCJ1bnNlbGVjdGFibGVcIixcIm9uXCIpLmNzcyh7XCItd2Via2l0LXVzZXItc2VsZWN0XCI6XCJub25lXCIsXCItbW96LXVzZXItc2VsZWN0XCI6XCJub25lXCIsXCItbXMtdXNlci1zZWxlY3RcIjpcIm5vbmVcIixcIi1vLXVzZXItc2VsZWN0XCI6XCJub25lXCIsXCJ1c2VyLXNlbGVjdFwiOlwibm9uZVwiLGRpc3BsYXk6XCJpbmxpbmUtYmxvY2tcIixwb3NpdGlvbjpcInJlbGF0aXZlXCIsXHJcbm92ZXJmbG93OlwiaGlkZGVuXCJ9KTthLmlzKFwiOmNoZWNrZWRcIikmJmQuYWRkQ2xhc3MoXCJjaGVja2VkXCIpO2EuaXMoXCI6ZGlzYWJsZWRcIikmJmQuYWRkQ2xhc3MoXCJkaXNhYmxlZFwiKTtkLmNsaWNrKGZ1bmN0aW9uKGIpe2IucHJldmVudERlZmF1bHQoKTtkLmlzKFwiLmRpc2FibGVkXCIpfHwoYS5pcyhcIjpjaGVja2VkXCIpPyhhLnByb3AoXCJjaGVja2VkXCIsITEpLGQucmVtb3ZlQ2xhc3MoXCJjaGVja2VkXCIpKTooYS5wcm9wKFwiY2hlY2tlZFwiLCEwKSxkLmFkZENsYXNzKFwiY2hlY2tlZFwiKSksYS5mb2N1cygpLmNoYW5nZSgpKX0pO2EuY2xvc2VzdChcImxhYmVsXCIpLmFkZCgnbGFiZWxbZm9yPVwiJythLmF0dHIoXCJpZFwiKSsnXCJdJykub24oXCJjbGljay5zdHlsZXJcIixmdW5jdGlvbihhKXtiKGEudGFyZ2V0KS5pcyhcImFcIil8fGIoYS50YXJnZXQpLmNsb3Nlc3QoZCkubGVuZ3RofHwoZC50cmlnZ2VySGFuZGxlcihcImNsaWNrXCIpLGEucHJldmVudERlZmF1bHQoKSl9KTthLm9uKFwiY2hhbmdlLnN0eWxlclwiLFxyXG5mdW5jdGlvbigpe2EuaXMoXCI6Y2hlY2tlZFwiKT9kLmFkZENsYXNzKFwiY2hlY2tlZFwiKTpkLnJlbW92ZUNsYXNzKFwiY2hlY2tlZFwiKX0pLm9uKFwia2V5ZG93bi5zdHlsZXJcIixmdW5jdGlvbihhKXszMj09YS53aGljaCYmZC5jbGljaygpfSkub24oXCJmb2N1cy5zdHlsZXJcIixmdW5jdGlvbigpe2QuaXMoXCIuZGlzYWJsZWRcIil8fGQuYWRkQ2xhc3MoXCJmb2N1c2VkXCIpfSkub24oXCJibHVyLnN0eWxlclwiLGZ1bmN0aW9uKCl7ZC5yZW1vdmVDbGFzcyhcImZvY3VzZWRcIil9KX07eigpO2Eub24oXCJyZWZyZXNoXCIsZnVuY3Rpb24oKXthLmNsb3Nlc3QoXCJsYWJlbFwiKS5hZGQoJ2xhYmVsW2Zvcj1cIicrYS5hdHRyKFwiaWRcIikrJ1wiXScpLm9mZihcIi5zdHlsZXJcIik7YS5vZmYoXCIuc3R5bGVyXCIpLnBhcmVudCgpLmJlZm9yZShhKS5yZW1vdmUoKTt6KCl9KX1lbHNlIGlmKGEuaXMoXCI6cmFkaW9cIikpe3ZhciBCPWZ1bmN0aW9uKCl7dmFyIHg9bmV3IGMsZD1iKFwiPGRpdlwiK3guaWQrJyBjbGFzcz1cImpxLXJhZGlvJytcclxueC5jbGFzc2VzKydcIicreC50aXRsZSsnPjxkaXYgY2xhc3M9XCJqcS1yYWRpb19fZGl2XCI+PC9kaXY+PC9kaXY+Jyk7YS5jc3Moe3Bvc2l0aW9uOlwiYWJzb2x1dGVcIix6SW5kZXg6XCItMVwiLG9wYWNpdHk6MCxtYXJnaW46MCxwYWRkaW5nOjB9KS5hZnRlcihkKS5wcmVwZW5kVG8oZCk7ZC5hdHRyKFwidW5zZWxlY3RhYmxlXCIsXCJvblwiKS5jc3Moe1wiLXdlYmtpdC11c2VyLXNlbGVjdFwiOlwibm9uZVwiLFwiLW1vei11c2VyLXNlbGVjdFwiOlwibm9uZVwiLFwiLW1zLXVzZXItc2VsZWN0XCI6XCJub25lXCIsXCItby11c2VyLXNlbGVjdFwiOlwibm9uZVwiLFwidXNlci1zZWxlY3RcIjpcIm5vbmVcIixkaXNwbGF5OlwiaW5saW5lLWJsb2NrXCIscG9zaXRpb246XCJyZWxhdGl2ZVwifSk7YS5pcyhcIjpjaGVja2VkXCIpJiZkLmFkZENsYXNzKFwiY2hlY2tlZFwiKTthLmlzKFwiOmRpc2FibGVkXCIpJiZkLmFkZENsYXNzKFwiZGlzYWJsZWRcIik7ZC5jbGljayhmdW5jdGlvbihiKXtiLnByZXZlbnREZWZhdWx0KCk7ZC5pcyhcIi5kaXNhYmxlZFwiKXx8XHJcbihkLmNsb3Nlc3QoZi53cmFwcGVyKS5maW5kKCdpbnB1dFtuYW1lPVwiJythLmF0dHIoXCJuYW1lXCIpKydcIl0nKS5wcm9wKFwiY2hlY2tlZFwiLCExKS5wYXJlbnQoKS5yZW1vdmVDbGFzcyhcImNoZWNrZWRcIiksYS5wcm9wKFwiY2hlY2tlZFwiLCEwKS5wYXJlbnQoKS5hZGRDbGFzcyhcImNoZWNrZWRcIiksYS5mb2N1cygpLmNoYW5nZSgpKX0pO2EuY2xvc2VzdChcImxhYmVsXCIpLmFkZCgnbGFiZWxbZm9yPVwiJythLmF0dHIoXCJpZFwiKSsnXCJdJykub24oXCJjbGljay5zdHlsZXJcIixmdW5jdGlvbihhKXtiKGEudGFyZ2V0KS5pcyhcImFcIil8fGIoYS50YXJnZXQpLmNsb3Nlc3QoZCkubGVuZ3RofHwoZC50cmlnZ2VySGFuZGxlcihcImNsaWNrXCIpLGEucHJldmVudERlZmF1bHQoKSl9KTthLm9uKFwiY2hhbmdlLnN0eWxlclwiLGZ1bmN0aW9uKCl7YS5wYXJlbnQoKS5hZGRDbGFzcyhcImNoZWNrZWRcIil9KS5vbihcImZvY3VzLnN0eWxlclwiLGZ1bmN0aW9uKCl7ZC5pcyhcIi5kaXNhYmxlZFwiKXx8ZC5hZGRDbGFzcyhcImZvY3VzZWRcIil9KS5vbihcImJsdXIuc3R5bGVyXCIsXHJcbmZ1bmN0aW9uKCl7ZC5yZW1vdmVDbGFzcyhcImZvY3VzZWRcIil9KX07QigpO2Eub24oXCJyZWZyZXNoXCIsZnVuY3Rpb24oKXthLmNsb3Nlc3QoXCJsYWJlbFwiKS5hZGQoJ2xhYmVsW2Zvcj1cIicrYS5hdHRyKFwiaWRcIikrJ1wiXScpLm9mZihcIi5zdHlsZXJcIik7YS5vZmYoXCIuc3R5bGVyXCIpLnBhcmVudCgpLmJlZm9yZShhKS5yZW1vdmUoKTtCKCl9KX1lbHNlIGlmKGEuaXMoXCI6ZmlsZVwiKSl7YS5jc3Moe3Bvc2l0aW9uOlwiYWJzb2x1dGVcIix0b3A6MCxyaWdodDowLHdpZHRoOlwiMTAwJVwiLGhlaWdodDpcIjEwMCVcIixvcGFjaXR5OjAsbWFyZ2luOjAscGFkZGluZzowfSk7dmFyIEM9ZnVuY3Rpb24oKXt2YXIgeD1uZXcgYyxkPWEuZGF0YShcInBsYWNlaG9sZGVyXCIpO3ZvaWQgMD09PWQmJihkPWYuZmlsZVBsYWNlaG9sZGVyKTt2YXIgQT1hLmRhdGEoXCJicm93c2VcIik7aWYodm9pZCAwPT09QXx8XCJcIj09PUEpQT1mLmZpbGVCcm93c2U7dmFyIGU9YihcIjxkaXZcIit4LmlkKycgY2xhc3M9XCJqcS1maWxlJyt4LmNsYXNzZXMrXHJcbidcIicreC50aXRsZSsnIHN0eWxlPVwiZGlzcGxheTogaW5saW5lLWJsb2NrOyBwb3NpdGlvbjogcmVsYXRpdmU7IG92ZXJmbG93OiBoaWRkZW5cIj48L2Rpdj4nKSxsPWIoJzxkaXYgY2xhc3M9XCJqcS1maWxlX19uYW1lXCI+JytkK1wiPC9kaXY+XCIpLmFwcGVuZFRvKGUpO2IoJzxkaXYgY2xhc3M9XCJqcS1maWxlX19icm93c2VcIj4nK0ErXCI8L2Rpdj5cIikuYXBwZW5kVG8oZSk7YS5hZnRlcihlKS5hcHBlbmRUbyhlKTthLmlzKFwiOmRpc2FibGVkXCIpJiZlLmFkZENsYXNzKFwiZGlzYWJsZWRcIik7YS5vbihcImNoYW5nZS5zdHlsZXJcIixmdW5jdGlvbigpe3ZhciBiPWEudmFsKCk7aWYoYS5pcyhcIlttdWx0aXBsZV1cIikpe3ZhciBiPVwiXCIsYz1hWzBdLmZpbGVzLmxlbmd0aDswPGMmJihiPWEuZGF0YShcIm51bWJlclwiKSx2b2lkIDA9PT1iJiYoYj1mLmZpbGVOdW1iZXIpLGI9Yi5yZXBsYWNlKFwiJXNcIixjKSl9bC50ZXh0KGIucmVwbGFjZSgvLitbXFxcXFxcL10vLFwiXCIpKTtcIlwiPT09Yj8obC50ZXh0KGQpLGUucmVtb3ZlQ2xhc3MoXCJjaGFuZ2VkXCIpKTpcclxuZS5hZGRDbGFzcyhcImNoYW5nZWRcIil9KS5vbihcImZvY3VzLnN0eWxlclwiLGZ1bmN0aW9uKCl7ZS5hZGRDbGFzcyhcImZvY3VzZWRcIil9KS5vbihcImJsdXIuc3R5bGVyXCIsZnVuY3Rpb24oKXtlLnJlbW92ZUNsYXNzKFwiZm9jdXNlZFwiKX0pLm9uKFwiY2xpY2suc3R5bGVyXCIsZnVuY3Rpb24oKXtlLnJlbW92ZUNsYXNzKFwiZm9jdXNlZFwiKX0pfTtDKCk7YS5vbihcInJlZnJlc2hcIixmdW5jdGlvbigpe2Eub2ZmKFwiLnN0eWxlclwiKS5wYXJlbnQoKS5iZWZvcmUoYSkucmVtb3ZlKCk7QygpfSl9ZWxzZSBpZihhLmlzKCdpbnB1dFt0eXBlPVwibnVtYmVyXCJdJykpe3ZhciBEPWZ1bmN0aW9uKCl7dmFyIGM9YignPGRpdiBjbGFzcz1cImpxLW51bWJlclwiPjxkaXYgY2xhc3M9XCJqcS1udW1iZXJfX3NwaW4gbWludXNcIj48L2Rpdj48ZGl2IGNsYXNzPVwianEtbnVtYmVyX19zcGluIHBsdXNcIj48L2Rpdj48L2Rpdj4nKTthLmFmdGVyKGMpLnByZXBlbmRUbyhjKS53cmFwKCc8ZGl2IGNsYXNzPVwianEtbnVtYmVyX19maWVsZFwiPjwvZGl2PicpO1xyXG5hLmlzKFwiOmRpc2FibGVkXCIpJiZjLmFkZENsYXNzKFwiZGlzYWJsZWRcIik7dmFyIGQsZixlLGw9bnVsbCx0PW51bGw7dm9pZCAwIT09YS5hdHRyKFwibWluXCIpJiYoZD1hLmF0dHIoXCJtaW5cIikpO3ZvaWQgMCE9PWEuYXR0cihcIm1heFwiKSYmKGY9YS5hdHRyKFwibWF4XCIpKTtlPXZvaWQgMCE9PWEuYXR0cihcInN0ZXBcIikmJmIuaXNOdW1lcmljKGEuYXR0cihcInN0ZXBcIikpP051bWJlcihhLmF0dHIoXCJzdGVwXCIpKTpOdW1iZXIoMSk7dmFyIEs9ZnVuY3Rpb24ocyl7dmFyIGM9YS52YWwoKSxrO2IuaXNOdW1lcmljKGMpfHwoYz0wLGEudmFsKFwiMFwiKSk7cy5pcyhcIi5taW51c1wiKT8oaz1wYXJzZUludChjLDEwKS1lLDA8ZSYmKGs9TWF0aC5jZWlsKGsvZSkqZSkpOnMuaXMoXCIucGx1c1wiKSYmKGs9cGFyc2VJbnQoYywxMCkrZSwwPGUmJihrPU1hdGguZmxvb3Ioay9lKSplKSk7Yi5pc051bWVyaWMoZCkmJmIuaXNOdW1lcmljKGYpP2s+PWQmJms8PWYmJmEudmFsKGspOmIuaXNOdW1lcmljKGQpJiYhYi5pc051bWVyaWMoZik/XHJcbms+PWQmJmEudmFsKGspOiFiLmlzTnVtZXJpYyhkKSYmYi5pc051bWVyaWMoZik/azw9ZiYmYS52YWwoayk6YS52YWwoayl9O2MuaXMoXCIuZGlzYWJsZWRcIil8fChjLm9uKFwibW91c2Vkb3duXCIsXCJkaXYuanEtbnVtYmVyX19zcGluXCIsZnVuY3Rpb24oKXt2YXIgYT1iKHRoaXMpO0soYSk7bD1zZXRUaW1lb3V0KGZ1bmN0aW9uKCl7dD1zZXRJbnRlcnZhbChmdW5jdGlvbigpe0soYSl9LDQwKX0sMzUwKX0pLm9uKFwibW91c2V1cCBtb3VzZW91dFwiLFwiZGl2LmpxLW51bWJlcl9fc3BpblwiLGZ1bmN0aW9uKCl7Y2xlYXJUaW1lb3V0KGwpO2NsZWFySW50ZXJ2YWwodCl9KSxhLm9uKFwiZm9jdXMuc3R5bGVyXCIsZnVuY3Rpb24oKXtjLmFkZENsYXNzKFwiZm9jdXNlZFwiKX0pLm9uKFwiYmx1ci5zdHlsZXJcIixmdW5jdGlvbigpe2MucmVtb3ZlQ2xhc3MoXCJmb2N1c2VkXCIpfSkpfTtEKCk7YS5vbihcInJlZnJlc2hcIixmdW5jdGlvbigpe2Eub2ZmKFwiLnN0eWxlclwiKS5jbG9zZXN0KFwiLmpxLW51bWJlclwiKS5iZWZvcmUoYSkucmVtb3ZlKCk7XHJcbkQoKX0pfWVsc2UgaWYoYS5pcyhcInNlbGVjdFwiKSl7dmFyIE09ZnVuY3Rpb24oKXtmdW5jdGlvbiB4KGEpe2Eub2ZmKFwibW91c2V3aGVlbCBET01Nb3VzZVNjcm9sbFwiKS5vbihcIm1vdXNld2hlZWwgRE9NTW91c2VTY3JvbGxcIixmdW5jdGlvbihhKXt2YXIgYz1udWxsO1wibW91c2V3aGVlbFwiPT1hLnR5cGU/Yz0tMSphLm9yaWdpbmFsRXZlbnQud2hlZWxEZWx0YTpcIkRPTU1vdXNlU2Nyb2xsXCI9PWEudHlwZSYmKGM9NDAqYS5vcmlnaW5hbEV2ZW50LmRldGFpbCk7YyYmKGEuc3RvcFByb3BhZ2F0aW9uKCksYS5wcmV2ZW50RGVmYXVsdCgpLGIodGhpcykuc2Nyb2xsVG9wKGMrYih0aGlzKS5zY3JvbGxUb3AoKSkpfSl9ZnVuY3Rpb24gZCgpe2Zvcih2YXIgYT0wO2E8bC5sZW5ndGg7YSsrKXt2YXIgYj1sLmVxKGEpLGM9XCJcIixkPVwiXCIsZT1jPVwiXCIsdT1cIlwiLHA9XCJcIix2PVwiXCIsdz1cIlwiLGc9XCJcIjtiLnByb3AoXCJzZWxlY3RlZFwiKSYmKGQ9XCJzZWxlY3RlZCBzZWxcIik7Yi5pcyhcIjpkaXNhYmxlZFwiKSYmXHJcbihkPVwiZGlzYWJsZWRcIik7Yi5pcyhcIjpzZWxlY3RlZDpkaXNhYmxlZFwiKSYmKGQ9XCJzZWxlY3RlZCBzZWwgZGlzYWJsZWRcIik7dm9pZCAwIT09Yi5hdHRyKFwiaWRcIikmJlwiXCIhPT1iLmF0dHIoXCJpZFwiKSYmKGU9JyBpZD1cIicrYi5hdHRyKFwiaWRcIikrZi5pZFN1ZmZpeCsnXCInKTt2b2lkIDAhPT1iLmF0dHIoXCJ0aXRsZVwiKSYmXCJcIiE9PWwuYXR0cihcInRpdGxlXCIpJiYodT0nIHRpdGxlPVwiJytiLmF0dHIoXCJ0aXRsZVwiKSsnXCInKTt2b2lkIDAhPT1iLmF0dHIoXCJjbGFzc1wiKSYmKHY9XCIgXCIrYi5hdHRyKFwiY2xhc3NcIiksZz0nIGRhdGEtanFmcy1jbGFzcz1cIicrYi5hdHRyKFwiY2xhc3NcIikrJ1wiJyk7dmFyIGg9Yi5kYXRhKCkscjtmb3IociBpbiBoKVwiXCIhPT1oW3JdJiYocCs9XCIgZGF0YS1cIityKyc9XCInK2hbcl0rJ1wiJyk7XCJcIiE9PWQrdiYmKGM9JyBjbGFzcz1cIicrZCt2KydcIicpO2M9XCI8bGlcIitnK3ArYyt1K2UrXCI+XCIrYi5odG1sKCkrXCI8L2xpPlwiO2IucGFyZW50KCkuaXMoXCJvcHRncm91cFwiKSYmKHZvaWQgMCE9PVxyXG5iLnBhcmVudCgpLmF0dHIoXCJjbGFzc1wiKSYmKHc9XCIgXCIrYi5wYXJlbnQoKS5hdHRyKFwiY2xhc3NcIikpLGM9XCI8bGlcIitnK3ArJyBjbGFzcz1cIicrZCt2K1wiIG9wdGlvblwiK3crJ1wiJyt1K2UrXCI+XCIrYi5odG1sKCkrXCI8L2xpPlwiLGIuaXMoXCI6Zmlyc3QtY2hpbGRcIikmJihjPSc8bGkgY2xhc3M9XCJvcHRncm91cCcrdysnXCI+JytiLnBhcmVudCgpLmF0dHIoXCJsYWJlbFwiKStcIjwvbGk+XCIrYykpO3QrPWN9fWZ1bmN0aW9uIHooKXt2YXIgZT1uZXcgYyxzPVwiXCIsSD1hLmRhdGEoXCJwbGFjZWhvbGRlclwiKSxrPWEuZGF0YShcInNlYXJjaFwiKSxoPWEuZGF0YShcInNlYXJjaC1saW1pdFwiKSx1PWEuZGF0YShcInNlYXJjaC1ub3QtZm91bmRcIikscD1hLmRhdGEoXCJzZWFyY2gtcGxhY2Vob2xkZXJcIiksdj1hLmRhdGEoXCJ6LWluZGV4XCIpLHc9YS5kYXRhKFwic21hcnQtcG9zaXRpb25pbmdcIik7dm9pZCAwPT09SCYmKEg9Zi5zZWxlY3RQbGFjZWhvbGRlcik7aWYodm9pZCAwPT09a3x8XCJcIj09PWspaz1mLnNlbGVjdFNlYXJjaDtcclxuaWYodm9pZCAwPT09aHx8XCJcIj09PWgpaD1mLnNlbGVjdFNlYXJjaExpbWl0O2lmKHZvaWQgMD09PXV8fFwiXCI9PT11KXU9Zi5zZWxlY3RTZWFyY2hOb3RGb3VuZDt2b2lkIDA9PT1wJiYocD1mLnNlbGVjdFNlYXJjaFBsYWNlaG9sZGVyKTtpZih2b2lkIDA9PT12fHxcIlwiPT09dil2PWYuc2luZ2xlU2VsZWN0ekluZGV4O2lmKHZvaWQgMD09PXd8fFwiXCI9PT13KXc9Zi5zZWxlY3RTbWFydFBvc2l0aW9uaW5nO3ZhciBnPWIoXCI8ZGl2XCIrZS5pZCsnIGNsYXNzPVwianEtc2VsZWN0Ym94IGpxc2VsZWN0JytlLmNsYXNzZXMrJ1wiIHN0eWxlPVwiZGlzcGxheTogaW5saW5lLWJsb2NrOyBwb3NpdGlvbjogcmVsYXRpdmU7IHotaW5kZXg6Jyt2KydcIj48ZGl2IGNsYXNzPVwianEtc2VsZWN0Ym94X19zZWxlY3RcIicrZS50aXRsZSsnIHN0eWxlPVwicG9zaXRpb246IHJlbGF0aXZlXCI+PGRpdiBjbGFzcz1cImpxLXNlbGVjdGJveF9fc2VsZWN0LXRleHRcIj48L2Rpdj48ZGl2IGNsYXNzPVwianEtc2VsZWN0Ym94X190cmlnZ2VyXCI+PGRpdiBjbGFzcz1cImpxLXNlbGVjdGJveF9fdHJpZ2dlci1hcnJvd1wiPjwvZGl2PjwvZGl2PjwvZGl2PjwvZGl2PicpO1xyXG5hLmNzcyh7bWFyZ2luOjAscGFkZGluZzowfSkuYWZ0ZXIoZykucHJlcGVuZFRvKGcpO3ZhciBMPWIoXCJkaXYuanEtc2VsZWN0Ym94X19zZWxlY3RcIixnKSxyPWIoXCJkaXYuanEtc2VsZWN0Ym94X19zZWxlY3QtdGV4dFwiLGcpLGU9bC5maWx0ZXIoXCI6c2VsZWN0ZWRcIik7ZCgpO2smJihzPSc8ZGl2IGNsYXNzPVwianEtc2VsZWN0Ym94X19zZWFyY2hcIj48aW5wdXQgdHlwZT1cInNlYXJjaFwiIGF1dG9jb21wbGV0ZT1cIm9mZlwiIHBsYWNlaG9sZGVyPVwiJytwKydcIj48L2Rpdj48ZGl2IGNsYXNzPVwianEtc2VsZWN0Ym94X19ub3QtZm91bmRcIj4nK3UrXCI8L2Rpdj5cIik7dmFyIG09YignPGRpdiBjbGFzcz1cImpxLXNlbGVjdGJveF9fZHJvcGRvd25cIiBzdHlsZT1cInBvc2l0aW9uOiBhYnNvbHV0ZVwiPicrcysnPHVsIHN0eWxlPVwicG9zaXRpb246IHJlbGF0aXZlOyBsaXN0LXN0eWxlOiBub25lOyBvdmVyZmxvdzogYXV0bzsgb3ZlcmZsb3cteDogaGlkZGVuXCI+Jyt0K1wiPC91bD48L2Rpdj5cIik7Zy5hcHBlbmQobSk7XHJcbnZhciBxPWIoXCJ1bFwiLG0pLG49YihcImxpXCIsbSksRT1iKFwiaW5wdXRcIixtKSxBPWIoXCJkaXYuanEtc2VsZWN0Ym94X19ub3QtZm91bmRcIixtKS5oaWRlKCk7bi5sZW5ndGg8aCYmRS5wYXJlbnQoKS5oaWRlKCk7XCJcIj09PWEudmFsKCk/ci50ZXh0KEgpLmFkZENsYXNzKFwicGxhY2Vob2xkZXJcIik6ci50ZXh0KGUudGV4dCgpKTt2YXIgRj0wLEI9MDtuLmVhY2goZnVuY3Rpb24oKXt2YXIgYT1iKHRoaXMpO2EuY3NzKHtkaXNwbGF5OlwiaW5saW5lLWJsb2NrXCJ9KTthLmlubmVyV2lkdGgoKT5GJiYoRj1hLmlubmVyV2lkdGgoKSxCPWEud2lkdGgoKSk7YS5jc3Moe2Rpc3BsYXk6XCJcIn0pfSk7ci5pcyhcIi5wbGFjZWhvbGRlclwiKSYmci53aWR0aCgpPkY/ci53aWR0aChyLndpZHRoKCkpOihzPWcuY2xvbmUoKS5hcHBlbmRUbyhcImJvZHlcIikud2lkdGgoXCJhdXRvXCIpLGs9cy5vdXRlcldpZHRoKCkscy5yZW1vdmUoKSxrPT1nLm91dGVyV2lkdGgoKSYmci53aWR0aChCKSk7Rj5nLndpZHRoKCkmJm0ud2lkdGgoRik7XHJcblwiXCI9PT1sLmZpcnN0KCkudGV4dCgpJiZcIlwiIT09YS5kYXRhKFwicGxhY2Vob2xkZXJcIikmJm4uZmlyc3QoKS5oaWRlKCk7YS5jc3Moe3Bvc2l0aW9uOlwiYWJzb2x1dGVcIixsZWZ0OjAsdG9wOjAsd2lkdGg6XCIxMDAlXCIsaGVpZ2h0OlwiMTAwJVwiLG9wYWNpdHk6MH0pO3ZhciBDPWcub3V0ZXJIZWlnaHQoKSxJPUUub3V0ZXJIZWlnaHQoKSxKPXEuY3NzKFwibWF4LWhlaWdodFwiKSxzPW4uZmlsdGVyKFwiLnNlbGVjdGVkXCIpOzE+cy5sZW5ndGgmJm4uZmlyc3QoKS5hZGRDbGFzcyhcInNlbGVjdGVkIHNlbFwiKTt2b2lkIDA9PT1uLmRhdGEoXCJsaS1oZWlnaHRcIikmJm4uZGF0YShcImxpLWhlaWdodFwiLG4ub3V0ZXJIZWlnaHQoKSk7dmFyIEQ9bS5jc3MoXCJ0b3BcIik7XCJhdXRvXCI9PW0uY3NzKFwibGVmdFwiKSYmbS5jc3Moe2xlZnQ6MH0pO1wiYXV0b1wiPT1tLmNzcyhcInRvcFwiKSYmbS5jc3Moe3RvcDpDfSk7bS5oaWRlKCk7cy5sZW5ndGgmJihsLmZpcnN0KCkudGV4dCgpIT1lLnRleHQoKSYmZy5hZGRDbGFzcyhcImNoYW5nZWRcIiksXHJcbmcuZGF0YShcImpxZnMtY2xhc3NcIixzLmRhdGEoXCJqcWZzLWNsYXNzXCIpKSxnLmFkZENsYXNzKHMuZGF0YShcImpxZnMtY2xhc3NcIikpKTtpZihhLmlzKFwiOmRpc2FibGVkXCIpKXJldHVybiBnLmFkZENsYXNzKFwiZGlzYWJsZWRcIiksITE7TC5jbGljayhmdW5jdGlvbigpe2IoXCJkaXYuanEtc2VsZWN0Ym94XCIpLmZpbHRlcihcIi5vcGVuZWRcIikubGVuZ3RoJiZmLm9uU2VsZWN0Q2xvc2VkLmNhbGwoYihcImRpdi5qcS1zZWxlY3Rib3hcIikuZmlsdGVyKFwiLm9wZW5lZFwiKSk7YS5mb2N1cygpO2lmKCF5KXt2YXIgYz1iKHdpbmRvdyksZD1uLmRhdGEoXCJsaS1oZWlnaHRcIiksZT1nLm9mZnNldCgpLnRvcCxrPWMuaGVpZ2h0KCktQy0oZS1jLnNjcm9sbFRvcCgpKSxwPWEuZGF0YShcInZpc2libGUtb3B0aW9uc1wiKTtpZih2b2lkIDA9PT1wfHxcIlwiPT09cClwPWYuc2VsZWN0VmlzaWJsZU9wdGlvbnM7dmFyIHM9NSpkLGg9ZCpwOzA8cCYmNj5wJiYocz1oKTswPT09cCYmKGg9XCJhdXRvXCIpO3ZhciBwPWZ1bmN0aW9uKCl7bS5oZWlnaHQoXCJhdXRvXCIpLmNzcyh7Ym90dG9tOlwiYXV0b1wiLFxyXG50b3A6RH0pO3ZhciBhPWZ1bmN0aW9uKCl7cS5jc3MoXCJtYXgtaGVpZ2h0XCIsTWF0aC5mbG9vcigoay0yMC1JKS9kKSpkKX07YSgpO3EuY3NzKFwibWF4LWhlaWdodFwiLGgpO1wibm9uZVwiIT1KJiZxLmNzcyhcIm1heC1oZWlnaHRcIixKKTtrPG0ub3V0ZXJIZWlnaHQoKSsyMCYmYSgpfSxyPWZ1bmN0aW9uKCl7bS5oZWlnaHQoXCJhdXRvXCIpLmNzcyh7dG9wOlwiYXV0b1wiLGJvdHRvbTpEfSk7dmFyIGE9ZnVuY3Rpb24oKXtxLmNzcyhcIm1heC1oZWlnaHRcIixNYXRoLmZsb29yKChlLWMuc2Nyb2xsVG9wKCktMjAtSSkvZCkqZCl9O2EoKTtxLmNzcyhcIm1heC1oZWlnaHRcIixoKTtcIm5vbmVcIiE9SiYmcS5jc3MoXCJtYXgtaGVpZ2h0XCIsSik7ZS1jLnNjcm9sbFRvcCgpLTIwPG0ub3V0ZXJIZWlnaHQoKSsyMCYmYSgpfTshMD09PXd8fDE9PT13P2s+cytJKzIwPyhwKCksZy5yZW1vdmVDbGFzcyhcImRyb3B1cFwiKS5hZGRDbGFzcyhcImRyb3Bkb3duXCIpKToocigpLGcucmVtb3ZlQ2xhc3MoXCJkcm9wZG93blwiKS5hZGRDbGFzcyhcImRyb3B1cFwiKSk6XHJcbighMT09PXd8fDA9PT13KSYmaz5zK0krMjAmJihwKCksZy5yZW1vdmVDbGFzcyhcImRyb3B1cFwiKS5hZGRDbGFzcyhcImRyb3Bkb3duXCIpKTtnLm9mZnNldCgpLmxlZnQrbS5vdXRlcldpZHRoKCk+Yy53aWR0aCgpJiZtLmNzcyh7bGVmdDpcImF1dG9cIixyaWdodDowfSk7YihcImRpdi5qcXNlbGVjdFwiKS5jc3Moe3pJbmRleDp2LTF9KS5yZW1vdmVDbGFzcyhcIm9wZW5lZFwiKTtnLmNzcyh7ekluZGV4OnZ9KTttLmlzKFwiOmhpZGRlblwiKT8oYihcImRpdi5qcS1zZWxlY3Rib3hfX2Ryb3Bkb3duOnZpc2libGVcIikuaGlkZSgpLG0uc2hvdygpLGcuYWRkQ2xhc3MoXCJvcGVuZWQgZm9jdXNlZFwiKSxmLm9uU2VsZWN0T3BlbmVkLmNhbGwoZykpOihtLmhpZGUoKSxnLnJlbW92ZUNsYXNzKFwib3BlbmVkIGRyb3B1cCBkcm9wZG93blwiKSxiKFwiZGl2LmpxLXNlbGVjdGJveFwiKS5maWx0ZXIoXCIub3BlbmVkXCIpLmxlbmd0aCYmZi5vblNlbGVjdENsb3NlZC5jYWxsKGcpKTtFLmxlbmd0aCYmKEUudmFsKFwiXCIpLmtleXVwKCksXHJcbkEuaGlkZSgpLEUua2V5dXAoZnVuY3Rpb24oKXt2YXIgYz1iKHRoaXMpLnZhbCgpO24uZWFjaChmdW5jdGlvbigpe2IodGhpcykuaHRtbCgpLm1hdGNoKFJlZ0V4cChcIi4qP1wiK2MrXCIuKj9cIixcImlcIikpP2IodGhpcykuc2hvdygpOmIodGhpcykuaGlkZSgpfSk7XCJcIj09PWwuZmlyc3QoKS50ZXh0KCkmJlwiXCIhPT1hLmRhdGEoXCJwbGFjZWhvbGRlclwiKSYmbi5maXJzdCgpLmhpZGUoKTsxPm4uZmlsdGVyKFwiOnZpc2libGVcIikubGVuZ3RoP0Euc2hvdygpOkEuaGlkZSgpfSkpO24uZmlsdGVyKFwiLnNlbGVjdGVkXCIpLmxlbmd0aCYmKFwiXCI9PT1hLnZhbCgpP3Euc2Nyb2xsVG9wKDApOigwIT09cS5pbm5lckhlaWdodCgpL2QlMiYmKGQvPTIpLHEuc2Nyb2xsVG9wKHEuc2Nyb2xsVG9wKCkrbi5maWx0ZXIoXCIuc2VsZWN0ZWRcIikucG9zaXRpb24oKS50b3AtcS5pbm5lckhlaWdodCgpLzIrZCkpKTt4KHEpfX0pO24uaG92ZXIoZnVuY3Rpb24oKXtiKHRoaXMpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3MoXCJzZWxlY3RlZFwiKX0pO1xyXG5uLmZpbHRlcihcIi5zZWxlY3RlZFwiKS50ZXh0KCk7bi5maWx0ZXIoXCI6bm90KC5kaXNhYmxlZCk6bm90KC5vcHRncm91cClcIikuY2xpY2soZnVuY3Rpb24oKXthLmZvY3VzKCk7dmFyIGM9Yih0aGlzKSxkPWMudGV4dCgpO2lmKCFjLmlzKFwiLnNlbGVjdGVkXCIpKXt2YXIgZT1jLmluZGV4KCksZT1lLWMucHJldkFsbChcIi5vcHRncm91cFwiKS5sZW5ndGg7Yy5hZGRDbGFzcyhcInNlbGVjdGVkIHNlbFwiKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKFwic2VsZWN0ZWQgc2VsXCIpO2wucHJvcChcInNlbGVjdGVkXCIsITEpLmVxKGUpLnByb3AoXCJzZWxlY3RlZFwiLCEwKTtyLnRleHQoZCk7Zy5kYXRhKFwianFmcy1jbGFzc1wiKSYmZy5yZW1vdmVDbGFzcyhnLmRhdGEoXCJqcWZzLWNsYXNzXCIpKTtnLmRhdGEoXCJqcWZzLWNsYXNzXCIsYy5kYXRhKFwianFmcy1jbGFzc1wiKSk7Zy5hZGRDbGFzcyhjLmRhdGEoXCJqcWZzLWNsYXNzXCIpKTthLmNoYW5nZSgpfW0uaGlkZSgpO2cucmVtb3ZlQ2xhc3MoXCJvcGVuZWQgZHJvcHVwIGRyb3Bkb3duXCIpO1xyXG5mLm9uU2VsZWN0Q2xvc2VkLmNhbGwoZyl9KTttLm1vdXNlb3V0KGZ1bmN0aW9uKCl7YihcImxpLnNlbFwiLG0pLmFkZENsYXNzKFwic2VsZWN0ZWRcIil9KTthLm9uKFwiY2hhbmdlLnN0eWxlclwiLGZ1bmN0aW9uKCl7ci50ZXh0KGwuZmlsdGVyKFwiOnNlbGVjdGVkXCIpLnRleHQoKSkucmVtb3ZlQ2xhc3MoXCJwbGFjZWhvbGRlclwiKTtuLnJlbW92ZUNsYXNzKFwic2VsZWN0ZWQgc2VsXCIpLm5vdChcIi5vcHRncm91cFwiKS5lcShhWzBdLnNlbGVjdGVkSW5kZXgpLmFkZENsYXNzKFwic2VsZWN0ZWQgc2VsXCIpO2wuZmlyc3QoKS50ZXh0KCkhPW4uZmlsdGVyKFwiLnNlbGVjdGVkXCIpLnRleHQoKT9nLmFkZENsYXNzKFwiY2hhbmdlZFwiKTpnLnJlbW92ZUNsYXNzKFwiY2hhbmdlZFwiKX0pLm9uKFwiZm9jdXMuc3R5bGVyXCIsZnVuY3Rpb24oKXtnLmFkZENsYXNzKFwiZm9jdXNlZFwiKTtiKFwiZGl2Lmpxc2VsZWN0XCIpLm5vdChcIi5mb2N1c2VkXCIpLnJlbW92ZUNsYXNzKFwib3BlbmVkIGRyb3B1cCBkcm9wZG93blwiKS5maW5kKFwiZGl2LmpxLXNlbGVjdGJveF9fZHJvcGRvd25cIikuaGlkZSgpfSkub24oXCJibHVyLnN0eWxlclwiLFxyXG5mdW5jdGlvbigpe2cucmVtb3ZlQ2xhc3MoXCJmb2N1c2VkXCIpfSkub24oXCJrZXlkb3duLnN0eWxlciBrZXl1cC5zdHlsZXJcIixmdW5jdGlvbihiKXt2YXIgYz1uLmRhdGEoXCJsaS1oZWlnaHRcIik7XCJcIj09PWEudmFsKCk/ci50ZXh0KEgpLmFkZENsYXNzKFwicGxhY2Vob2xkZXJcIik6ci50ZXh0KGwuZmlsdGVyKFwiOnNlbGVjdGVkXCIpLnRleHQoKSk7bi5yZW1vdmVDbGFzcyhcInNlbGVjdGVkIHNlbFwiKS5ub3QoXCIub3B0Z3JvdXBcIikuZXEoYVswXS5zZWxlY3RlZEluZGV4KS5hZGRDbGFzcyhcInNlbGVjdGVkIHNlbFwiKTtpZigzOD09Yi53aGljaHx8Mzc9PWIud2hpY2h8fDMzPT1iLndoaWNofHwzNj09Yi53aGljaClcIlwiPT09YS52YWwoKT9xLnNjcm9sbFRvcCgwKTpxLnNjcm9sbFRvcChxLnNjcm9sbFRvcCgpK24uZmlsdGVyKFwiLnNlbGVjdGVkXCIpLnBvc2l0aW9uKCkudG9wKTs0MCE9Yi53aGljaCYmMzkhPWIud2hpY2gmJjM0IT1iLndoaWNoJiYzNSE9Yi53aGljaHx8cS5zY3JvbGxUb3AocS5zY3JvbGxUb3AoKStcclxubi5maWx0ZXIoXCIuc2VsZWN0ZWRcIikucG9zaXRpb24oKS50b3AtcS5pbm5lckhlaWdodCgpK2MpOzEzPT1iLndoaWNoJiYoYi5wcmV2ZW50RGVmYXVsdCgpLG0uaGlkZSgpLGcucmVtb3ZlQ2xhc3MoXCJvcGVuZWQgZHJvcHVwIGRyb3Bkb3duXCIpLGYub25TZWxlY3RDbG9zZWQuY2FsbChnKSl9KS5vbihcImtleWRvd24uc3R5bGVyXCIsZnVuY3Rpb24oYSl7MzI9PWEud2hpY2gmJihhLnByZXZlbnREZWZhdWx0KCksTC5jbGljaygpKX0pO0cucmVnaXN0ZXJlZHx8KGIoZG9jdW1lbnQpLm9uKFwiY2xpY2tcIixHKSxHLnJlZ2lzdGVyZWQ9ITApfWZ1bmN0aW9uIGUoKXt2YXIgZT1uZXcgYyxmPWIoXCI8ZGl2XCIrZS5pZCsnIGNsYXNzPVwianEtc2VsZWN0LW11bHRpcGxlIGpxc2VsZWN0JytlLmNsYXNzZXMrJ1wiJytlLnRpdGxlKycgc3R5bGU9XCJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IHBvc2l0aW9uOiByZWxhdGl2ZVwiPjwvZGl2PicpO2EuY3NzKHttYXJnaW46MCxwYWRkaW5nOjB9KS5hZnRlcihmKTtcclxuZCgpO2YuYXBwZW5kKFwiPHVsPlwiK3QrXCI8L3VsPlwiKTt2YXIgaD1iKFwidWxcIixmKS5jc3Moe3Bvc2l0aW9uOlwicmVsYXRpdmVcIixcIm92ZXJmbG93LXhcIjpcImhpZGRlblwiLFwiLXdlYmtpdC1vdmVyZmxvdy1zY3JvbGxpbmdcIjpcInRvdWNoXCJ9KSxrPWIoXCJsaVwiLGYpLmF0dHIoXCJ1bnNlbGVjdGFibGVcIixcIm9uXCIpLGU9YS5hdHRyKFwic2l6ZVwiKSx5PWgub3V0ZXJIZWlnaHQoKSx1PWsub3V0ZXJIZWlnaHQoKTt2b2lkIDAhPT1lJiYwPGU/aC5jc3Moe2hlaWdodDp1KmV9KTpoLmNzcyh7aGVpZ2h0OjQqdX0pO3k+Zi5oZWlnaHQoKSYmKGguY3NzKFwib3ZlcmZsb3dZXCIsXCJzY3JvbGxcIikseChoKSxrLmZpbHRlcihcIi5zZWxlY3RlZFwiKS5sZW5ndGgmJmguc2Nyb2xsVG9wKGguc2Nyb2xsVG9wKCkray5maWx0ZXIoXCIuc2VsZWN0ZWRcIikucG9zaXRpb24oKS50b3ApKTthLnByZXBlbmRUbyhmKS5jc3Moe3Bvc2l0aW9uOlwiYWJzb2x1dGVcIixsZWZ0OjAsdG9wOjAsd2lkdGg6XCIxMDAlXCIsaGVpZ2h0OlwiMTAwJVwiLFxyXG5vcGFjaXR5OjB9KTtpZihhLmlzKFwiOmRpc2FibGVkXCIpKWYuYWRkQ2xhc3MoXCJkaXNhYmxlZFwiKSxsLmVhY2goZnVuY3Rpb24oKXtiKHRoaXMpLmlzKFwiOnNlbGVjdGVkXCIpJiZrLmVxKGIodGhpcykuaW5kZXgoKSkuYWRkQ2xhc3MoXCJzZWxlY3RlZFwiKX0pO2Vsc2UgaWYoay5maWx0ZXIoXCI6bm90KC5kaXNhYmxlZCk6bm90KC5vcHRncm91cClcIikuY2xpY2soZnVuY3Rpb24oYyl7YS5mb2N1cygpO3ZhciBkPWIodGhpcyk7Yy5jdHJsS2V5fHxjLm1ldGFLZXl8fGQuYWRkQ2xhc3MoXCJzZWxlY3RlZFwiKTtjLnNoaWZ0S2V5fHxkLmFkZENsYXNzKFwiZmlyc3RcIik7Yy5jdHJsS2V5fHwoYy5tZXRhS2V5fHxjLnNoaWZ0S2V5KXx8ZC5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKFwic2VsZWN0ZWQgZmlyc3RcIik7aWYoYy5jdHJsS2V5fHxjLm1ldGFLZXkpZC5pcyhcIi5zZWxlY3RlZFwiKT9kLnJlbW92ZUNsYXNzKFwic2VsZWN0ZWQgZmlyc3RcIik6ZC5hZGRDbGFzcyhcInNlbGVjdGVkIGZpcnN0XCIpLGQuc2libGluZ3MoKS5yZW1vdmVDbGFzcyhcImZpcnN0XCIpO1xyXG5pZihjLnNoaWZ0S2V5KXt2YXIgZT0hMSxmPSExO2Quc2libGluZ3MoKS5yZW1vdmVDbGFzcyhcInNlbGVjdGVkXCIpLnNpYmxpbmdzKFwiLmZpcnN0XCIpLmFkZENsYXNzKFwic2VsZWN0ZWRcIik7ZC5wcmV2QWxsKCkuZWFjaChmdW5jdGlvbigpe2IodGhpcykuaXMoXCIuZmlyc3RcIikmJihlPSEwKX0pO2QubmV4dEFsbCgpLmVhY2goZnVuY3Rpb24oKXtiKHRoaXMpLmlzKFwiLmZpcnN0XCIpJiYoZj0hMCl9KTtlJiZkLnByZXZBbGwoKS5lYWNoKGZ1bmN0aW9uKCl7aWYoYih0aGlzKS5pcyhcIi5zZWxlY3RlZFwiKSlyZXR1cm4hMTtiKHRoaXMpLm5vdChcIi5kaXNhYmxlZCwgLm9wdGdyb3VwXCIpLmFkZENsYXNzKFwic2VsZWN0ZWRcIil9KTtmJiZkLm5leHRBbGwoKS5lYWNoKGZ1bmN0aW9uKCl7aWYoYih0aGlzKS5pcyhcIi5zZWxlY3RlZFwiKSlyZXR1cm4hMTtiKHRoaXMpLm5vdChcIi5kaXNhYmxlZCwgLm9wdGdyb3VwXCIpLmFkZENsYXNzKFwic2VsZWN0ZWRcIil9KTsxPT1rLmZpbHRlcihcIi5zZWxlY3RlZFwiKS5sZW5ndGgmJlxyXG5kLmFkZENsYXNzKFwiZmlyc3RcIil9bC5wcm9wKFwic2VsZWN0ZWRcIiwhMSk7ay5maWx0ZXIoXCIuc2VsZWN0ZWRcIikuZWFjaChmdW5jdGlvbigpe3ZhciBhPWIodGhpcyksYz1hLmluZGV4KCk7YS5pcyhcIi5vcHRpb25cIikmJihjLT1hLnByZXZBbGwoXCIub3B0Z3JvdXBcIikubGVuZ3RoKTtsLmVxKGMpLnByb3AoXCJzZWxlY3RlZFwiLCEwKX0pO2EuY2hhbmdlKCl9KSxsLmVhY2goZnVuY3Rpb24oYSl7Yih0aGlzKS5kYXRhKFwib3B0aW9uSW5kZXhcIixhKX0pLGEub24oXCJjaGFuZ2Uuc3R5bGVyXCIsZnVuY3Rpb24oKXtrLnJlbW92ZUNsYXNzKFwic2VsZWN0ZWRcIik7dmFyIGE9W107bC5maWx0ZXIoXCI6c2VsZWN0ZWRcIikuZWFjaChmdW5jdGlvbigpe2EucHVzaChiKHRoaXMpLmRhdGEoXCJvcHRpb25JbmRleFwiKSl9KTtrLm5vdChcIi5vcHRncm91cFwiKS5maWx0ZXIoZnVuY3Rpb24oYyl7cmV0dXJuLTE8Yi5pbkFycmF5KGMsYSl9KS5hZGRDbGFzcyhcInNlbGVjdGVkXCIpfSkub24oXCJmb2N1cy5zdHlsZXJcIixmdW5jdGlvbigpe2YuYWRkQ2xhc3MoXCJmb2N1c2VkXCIpfSkub24oXCJibHVyLnN0eWxlclwiLFxyXG5mdW5jdGlvbigpe2YucmVtb3ZlQ2xhc3MoXCJmb2N1c2VkXCIpfSkseT5mLmhlaWdodCgpKWEub24oXCJrZXlkb3duLnN0eWxlclwiLGZ1bmN0aW9uKGEpezM4IT1hLndoaWNoJiYzNyE9YS53aGljaCYmMzMhPWEud2hpY2h8fGguc2Nyb2xsVG9wKGguc2Nyb2xsVG9wKCkray5maWx0ZXIoXCIuc2VsZWN0ZWRcIikucG9zaXRpb24oKS50b3AtdSk7NDAhPWEud2hpY2gmJjM5IT1hLndoaWNoJiYzNCE9YS53aGljaHx8aC5zY3JvbGxUb3AoaC5zY3JvbGxUb3AoKStrLmZpbHRlcihcIi5zZWxlY3RlZDpsYXN0XCIpLnBvc2l0aW9uKCkudG9wLWguaW5uZXJIZWlnaHQoKSsyKnUpfSl9dmFyIGw9YihcIm9wdGlvblwiLGEpLHQ9XCJcIjthLmlzKFwiW211bHRpcGxlXVwiKT9ofHx5fHxlKCk6eigpfTtNKCk7YS5vbihcInJlZnJlc2hcIixmdW5jdGlvbigpe2Eub2ZmKFwiLnN0eWxlclwiKS5wYXJlbnQoKS5iZWZvcmUoYSkucmVtb3ZlKCk7TSgpfSl9ZWxzZSBpZihhLmlzKFwiOnJlc2V0XCIpKWEub24oXCJjbGlja1wiLGZ1bmN0aW9uKCl7c2V0VGltZW91dChmdW5jdGlvbigpe2EuY2xvc2VzdChmLndyYXBwZXIpLmZpbmQoXCJpbnB1dCwgc2VsZWN0XCIpLnRyaWdnZXIoXCJyZWZyZXNoXCIpfSxcclxuMSl9KX0sZGVzdHJveTpmdW5jdGlvbigpe3ZhciBjPWIodGhpcy5lbGVtZW50KTtjLmlzKFwiOmNoZWNrYm94XCIpfHxjLmlzKFwiOnJhZGlvXCIpPyhjLnJlbW92ZURhdGEoXCJfXCIraCkub2ZmKFwiLnN0eWxlciByZWZyZXNoXCIpLnJlbW92ZUF0dHIoXCJzdHlsZVwiKS5wYXJlbnQoKS5iZWZvcmUoYykucmVtb3ZlKCksYy5jbG9zZXN0KFwibGFiZWxcIikuYWRkKCdsYWJlbFtmb3I9XCInK2MuYXR0cihcImlkXCIpKydcIl0nKS5vZmYoXCIuc3R5bGVyXCIpKTpjLmlzKCdpbnB1dFt0eXBlPVwibnVtYmVyXCJdJyk/Yy5yZW1vdmVEYXRhKFwiX1wiK2gpLm9mZihcIi5zdHlsZXIgcmVmcmVzaFwiKS5jbG9zZXN0KFwiLmpxLW51bWJlclwiKS5iZWZvcmUoYykucmVtb3ZlKCk6KGMuaXMoXCI6ZmlsZVwiKXx8Yy5pcyhcInNlbGVjdFwiKSkmJmMucmVtb3ZlRGF0YShcIl9cIitoKS5vZmYoXCIuc3R5bGVyIHJlZnJlc2hcIikucmVtb3ZlQXR0cihcInN0eWxlXCIpLnBhcmVudCgpLmJlZm9yZShjKS5yZW1vdmUoKX19O2IuZm5baF09ZnVuY3Rpb24oYyl7dmFyIGE9XHJcbmFyZ3VtZW50cztpZih2b2lkIDA9PT1jfHxcIm9iamVjdFwiPT09dHlwZW9mIGMpcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbigpe2IuZGF0YSh0aGlzLFwiX1wiK2gpfHxiLmRhdGEodGhpcyxcIl9cIitoLG5ldyB6KHRoaXMsYykpfSkucHJvbWlzZSgpLmRvbmUoZnVuY3Rpb24oKXt2YXIgYT1iKHRoaXNbMF0pLmRhdGEoXCJfXCIraCk7YSYmYS5vcHRpb25zLm9uRm9ybVN0eWxlZC5jYWxsKCl9KSx0aGlzO2lmKFwic3RyaW5nXCI9PT10eXBlb2YgYyYmXCJfXCIhPT1jWzBdJiZcImluaXRcIiE9PWMpe3ZhciBmO3RoaXMuZWFjaChmdW5jdGlvbigpe3ZhciB5PWIuZGF0YSh0aGlzLFwiX1wiK2gpO3kgaW5zdGFuY2VvZiB6JiZcImZ1bmN0aW9uXCI9PT10eXBlb2YgeVtjXSYmKGY9eVtjXS5hcHBseSh5LEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGEsMSkpKX0pO3JldHVybiB2b2lkIDAhPT1mP2Y6dGhpc319O0cucmVnaXN0ZXJlZD0hMX0pO1xyXG4vKipcclxuICogdml2dXMgLSBKYXZhU2NyaXB0IGxpYnJhcnkgdG8gbWFrZSBkcmF3aW5nIGFuaW1hdGlvbiBvbiBTVkdcclxuICogQHZlcnNpb24gdjAuMy4wXHJcbiAqIEBsaW5rIGh0dHBzOi8vZ2l0aHViLmNvbS9tYXh3ZWxsaXRvL3ZpdnVzXHJcbiAqIEBsaWNlbnNlIE1JVFxyXG4gKi9cclxuXHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbihmdW5jdGlvbiAod2luZG93LCBkb2N1bWVudCkge1xyXG5cclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICogUGF0aGZvcm1lclxyXG4gKiBCZXRhIHZlcnNpb25cclxuICpcclxuICogVGFrZSBhbnkgU1ZHIHZlcnNpb24gMS4xIGFuZCB0cmFuc2Zvcm1cclxuICogY2hpbGQgZWxlbWVudHMgdG8gJ3BhdGgnIGVsZW1lbnRzXHJcbiAqXHJcbiAqIFRoaXMgY29kZSBpcyBwdXJlbHkgZm9ya2VkIGZyb21cclxuICogaHR0cHM6Ly9naXRodWIuY29tL1dhZXN0L1NWR1BhdGhDb252ZXJ0ZXJcclxuICovXHJcblxyXG4vKipcclxuICogQ2xhc3MgY29uc3RydWN0b3JcclxuICpcclxuICogQHBhcmFtIHtET018U3RyaW5nfSBlbGVtZW50IERvbSBlbGVtZW50IG9mIHRoZSBTVkcgb3IgaWQgb2YgaXRcclxuICovXHJcbmZ1bmN0aW9uIFBhdGhmb3JtZXIoZWxlbWVudCkge1xyXG4gIC8vIFRlc3QgcGFyYW1zXHJcbiAgaWYgKHR5cGVvZiBlbGVtZW50ID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgdGhyb3cgbmV3IEVycm9yKCdQYXRoZm9ybWVyIFtjb25zdHJ1Y3Rvcl06IFwiZWxlbWVudFwiIHBhcmFtZXRlciBpcyByZXF1aXJlZCcpO1xyXG4gIH1cclxuXHJcbiAgLy8gU2V0IHRoZSBlbGVtZW50XHJcbiAgaWYgKGVsZW1lbnQuY29uc3RydWN0b3IgPT09IFN0cmluZykge1xyXG4gICAgZWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGVsZW1lbnQpO1xyXG4gICAgaWYgKCFlbGVtZW50KSB7XHJcbiAgICAgIHRocm93IG5ldyBFcnJvcignUGF0aGZvcm1lciBbY29uc3RydWN0b3JdOiBcImVsZW1lbnRcIiBwYXJhbWV0ZXIgaXMgbm90IHJlbGF0ZWQgdG8gYW4gZXhpc3RpbmcgSUQnKTtcclxuICAgIH1cclxuICB9XHJcbiAgaWYgKGVsZW1lbnQuY29uc3RydWN0b3IgaW5zdGFuY2VvZiB3aW5kb3cuU1ZHRWxlbWVudCB8fCAvXnN2ZyQvaS50ZXN0KGVsZW1lbnQubm9kZU5hbWUpKSB7XHJcbiAgICB0aGlzLmVsID0gZWxlbWVudDtcclxuICB9IGVsc2Uge1xyXG4gICAgdGhyb3cgbmV3IEVycm9yKCdQYXRoZm9ybWVyIFtjb25zdHJ1Y3Rvcl06IFwiZWxlbWVudFwiIHBhcmFtZXRlciBtdXN0IGJlIGEgc3RyaW5nIG9yIGEgU1ZHZWxlbWVudCcpO1xyXG4gIH1cclxuXHJcbiAgLy8gU3RhcnRcclxuICB0aGlzLnNjYW4oZWxlbWVudCk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBMaXN0IG9mIHRhZ3Mgd2hpY2ggY2FuIGJlIHRyYW5zZm9ybWVkXHJcbiAqIHRvIHBhdGggZWxlbWVudHNcclxuICpcclxuICogQHR5cGUge0FycmF5fVxyXG4gKi9cclxuUGF0aGZvcm1lci5wcm90b3R5cGUuVFlQRVMgPSBbJ2xpbmUnLCAnZWxsaXBzZScsICdjaXJjbGUnLCAncG9seWdvbicsICdwb2x5bGluZScsICdyZWN0J107XHJcblxyXG4vKipcclxuICogTGlzdCBvZiBhdHRyaWJ1dGUgbmFtZXMgd2hpY2ggY29udGFpblxyXG4gKiBkYXRhLiBUaGlzIGFycmF5IGxpc3QgdGhlbSB0byBjaGVjayBpZlxyXG4gKiB0aGV5IGNvbnRhaW4gYmFkIHZhbHVlcywgbGlrZSBwZXJjZW50YWdlLiBcclxuICpcclxuICogQHR5cGUge0FycmF5fVxyXG4gKi9cclxuUGF0aGZvcm1lci5wcm90b3R5cGUuQVRUUl9XQVRDSCA9IFsnY3gnLCAnY3knLCAncG9pbnRzJywgJ3InLCAncngnLCAncnknLCAneCcsICd4MScsICd4MicsICd5JywgJ3kxJywgJ3kyJ107XHJcblxyXG4vKipcclxuICogRmluZHMgdGhlIGVsZW1lbnRzIGNvbXBhdGlibGUgZm9yIHRyYW5zZm9ybVxyXG4gKiBhbmQgYXBwbHkgdGhlIGxpa2VkIG1ldGhvZFxyXG4gKlxyXG4gKiBAcGFyYW0gIHtvYmplY3R9IG9wdGlvbnMgT2JqZWN0IGZyb20gdGhlIGNvbnN0cnVjdG9yXHJcbiAqL1xyXG5QYXRoZm9ybWVyLnByb3RvdHlwZS5zY2FuID0gZnVuY3Rpb24gKHN2Zykge1xyXG4gIHZhciBmbiwgZWxlbWVudCwgcGF0aERhdGEsIHBhdGhEb20sXHJcbiAgICBlbGVtZW50cyA9IHN2Zy5xdWVyeVNlbGVjdG9yQWxsKHRoaXMuVFlQRVMuam9pbignLCcpKTtcclxuICBmb3IgKHZhciBpID0gMDsgaSA8IGVsZW1lbnRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICBlbGVtZW50ID0gZWxlbWVudHNbaV07XHJcbiAgICBmbiA9IHRoaXNbZWxlbWVudC50YWdOYW1lLnRvTG93ZXJDYXNlKCkgKyAnVG9QYXRoJ107XHJcbiAgICBwYXRoRGF0YSA9IGZuKHRoaXMucGFyc2VBdHRyKGVsZW1lbnQuYXR0cmlidXRlcykpO1xyXG4gICAgcGF0aERvbSA9IHRoaXMucGF0aE1ha2VyKGVsZW1lbnQsIHBhdGhEYXRhKTtcclxuICAgIGVsZW1lbnQucGFyZW50Tm9kZS5yZXBsYWNlQ2hpbGQocGF0aERvbSwgZWxlbWVudCk7XHJcbiAgfVxyXG59O1xyXG5cclxuXHJcbi8qKlxyXG4gKiBSZWFkIGBsaW5lYCBlbGVtZW50IHRvIGV4dHJhY3QgYW5kIHRyYW5zZm9ybVxyXG4gKiBkYXRhLCB0byBtYWtlIGl0IHJlYWR5IGZvciBhIGBwYXRoYCBvYmplY3QuXHJcbiAqXHJcbiAqIEBwYXJhbSAge0RPTWVsZW1lbnR9IGVsZW1lbnQgTGluZSBlbGVtZW50IHRvIHRyYW5zZm9ybVxyXG4gKiBAcmV0dXJuIHtvYmplY3R9ICAgICAgICAgICAgIERhdGEgZm9yIGEgYHBhdGhgIGVsZW1lbnRcclxuICovXHJcblBhdGhmb3JtZXIucHJvdG90eXBlLmxpbmVUb1BhdGggPSBmdW5jdGlvbiAoZWxlbWVudCkge1xyXG4gIHZhciBuZXdFbGVtZW50ID0ge307XHJcbiAgbmV3RWxlbWVudC5kID0gJ00nICsgZWxlbWVudC54MSArICcsJyArIGVsZW1lbnQueTEgKyAnTCcgKyBlbGVtZW50LngyICsgJywnICsgZWxlbWVudC55MjtcclxuICByZXR1cm4gbmV3RWxlbWVudDtcclxufTtcclxuXHJcbi8qKlxyXG4gKiBSZWFkIGByZWN0YCBlbGVtZW50IHRvIGV4dHJhY3QgYW5kIHRyYW5zZm9ybVxyXG4gKiBkYXRhLCB0byBtYWtlIGl0IHJlYWR5IGZvciBhIGBwYXRoYCBvYmplY3QuXHJcbiAqIFRoZSByYWRpdXMtYm9yZGVyIGlzIG5vdCB0YWtlbiBpbiBjaGFyZ2UgeWV0LlxyXG4gKiAoeW91ciBoZWxwIGlzIG1vcmUgdGhhbiB3ZWxjb21lZClcclxuICpcclxuICogQHBhcmFtICB7RE9NZWxlbWVudH0gZWxlbWVudCBSZWN0IGVsZW1lbnQgdG8gdHJhbnNmb3JtXHJcbiAqIEByZXR1cm4ge29iamVjdH0gICAgICAgICAgICAgRGF0YSBmb3IgYSBgcGF0aGAgZWxlbWVudFxyXG4gKi9cclxuUGF0aGZvcm1lci5wcm90b3R5cGUucmVjdFRvUGF0aCA9IGZ1bmN0aW9uIChlbGVtZW50KSB7XHJcbiAgdmFyIG5ld0VsZW1lbnQgPSB7fSxcclxuICAgIHggPSBwYXJzZUZsb2F0KGVsZW1lbnQueCkgfHwgMCxcclxuICAgIHkgPSBwYXJzZUZsb2F0KGVsZW1lbnQueSkgfHwgMCxcclxuICAgIHdpZHRoID0gcGFyc2VGbG9hdChlbGVtZW50LndpZHRoKSB8fCAwLFxyXG4gICAgaGVpZ2h0ID0gcGFyc2VGbG9hdChlbGVtZW50LmhlaWdodCkgfHwgMDtcclxuICBuZXdFbGVtZW50LmQgID0gJ00nICsgeCArICcgJyArIHkgKyAnICc7XHJcbiAgbmV3RWxlbWVudC5kICs9ICdMJyArICh4ICsgd2lkdGgpICsgJyAnICsgeSArICcgJztcclxuICBuZXdFbGVtZW50LmQgKz0gJ0wnICsgKHggKyB3aWR0aCkgKyAnICcgKyAoeSArIGhlaWdodCkgKyAnICc7XHJcbiAgbmV3RWxlbWVudC5kICs9ICdMJyArIHggKyAnICcgKyAoeSArIGhlaWdodCkgKyAnIFonO1xyXG4gIHJldHVybiBuZXdFbGVtZW50O1xyXG59O1xyXG5cclxuLyoqXHJcbiAqIFJlYWQgYHBvbHlsaW5lYCBlbGVtZW50IHRvIGV4dHJhY3QgYW5kIHRyYW5zZm9ybVxyXG4gKiBkYXRhLCB0byBtYWtlIGl0IHJlYWR5IGZvciBhIGBwYXRoYCBvYmplY3QuXHJcbiAqXHJcbiAqIEBwYXJhbSAge0RPTWVsZW1lbnR9IGVsZW1lbnQgUG9seWxpbmUgZWxlbWVudCB0byB0cmFuc2Zvcm1cclxuICogQHJldHVybiB7b2JqZWN0fSAgICAgICAgICAgICBEYXRhIGZvciBhIGBwYXRoYCBlbGVtZW50XHJcbiAqL1xyXG5QYXRoZm9ybWVyLnByb3RvdHlwZS5wb2x5bGluZVRvUGF0aCA9IGZ1bmN0aW9uIChlbGVtZW50KSB7XHJcbiAgdmFyIGksIHBhdGg7XHJcbiAgdmFyIG5ld0VsZW1lbnQgPSB7fTtcclxuICB2YXIgcG9pbnRzID0gZWxlbWVudC5wb2ludHMudHJpbSgpLnNwbGl0KCcgJyk7XHJcbiAgXHJcbiAgLy8gUmVmb3JtYXR0aW5nIGlmIHBvaW50cyBhcmUgZGVmaW5lZCB3aXRob3V0IGNvbW1hc1xyXG4gIGlmIChlbGVtZW50LnBvaW50cy5pbmRleE9mKCcsJykgPT09IC0xKSB7XHJcbiAgICB2YXIgZm9ybWF0dGVkUG9pbnRzID0gW107XHJcbiAgICBmb3IgKGkgPSAwOyBpIDwgcG9pbnRzLmxlbmd0aDsgaSs9Mikge1xyXG4gICAgICBmb3JtYXR0ZWRQb2ludHMucHVzaChwb2ludHNbaV0gKyAnLCcgKyBwb2ludHNbaSsxXSk7XHJcbiAgICB9XHJcbiAgICBwb2ludHMgPSBmb3JtYXR0ZWRQb2ludHM7XHJcbiAgfVxyXG5cclxuICAvLyBHZW5lcmF0ZSB0aGUgcGF0aC5kIHZhbHVlXHJcbiAgcGF0aCA9ICdNJyArIHBvaW50c1swXTtcclxuICBmb3IoaSA9IDE7IGkgPCBwb2ludHMubGVuZ3RoOyBpKyspIHtcclxuICAgIGlmIChwb2ludHNbaV0uaW5kZXhPZignLCcpICE9PSAtMSkge1xyXG4gICAgICBwYXRoICs9ICdMJyArIHBvaW50c1tpXTtcclxuICAgIH1cclxuICB9XHJcbiAgbmV3RWxlbWVudC5kID0gcGF0aDtcclxuICByZXR1cm4gbmV3RWxlbWVudDtcclxufTtcclxuXHJcbi8qKlxyXG4gKiBSZWFkIGBwb2x5Z29uYCBlbGVtZW50IHRvIGV4dHJhY3QgYW5kIHRyYW5zZm9ybVxyXG4gKiBkYXRhLCB0byBtYWtlIGl0IHJlYWR5IGZvciBhIGBwYXRoYCBvYmplY3QuXHJcbiAqIFRoaXMgbWV0aG9kIHJlbHkgb24gcG9seWxpbmVUb1BhdGgsIGJlY2F1c2UgdGhlXHJcbiAqIGxvZ2ljIGlzIHNpbWlsYXIuIFRoZSBwYXRoIGNyZWF0ZWQgaXMganVzdCBjbG9zZWQsXHJcbiAqIHNvIGl0IG5lZWRzIGFuICdaJyBhdCB0aGUgZW5kLlxyXG4gKlxyXG4gKiBAcGFyYW0gIHtET01lbGVtZW50fSBlbGVtZW50IFBvbHlnb24gZWxlbWVudCB0byB0cmFuc2Zvcm1cclxuICogQHJldHVybiB7b2JqZWN0fSAgICAgICAgICAgICBEYXRhIGZvciBhIGBwYXRoYCBlbGVtZW50XHJcbiAqL1xyXG5QYXRoZm9ybWVyLnByb3RvdHlwZS5wb2x5Z29uVG9QYXRoID0gZnVuY3Rpb24gKGVsZW1lbnQpIHtcclxuICB2YXIgbmV3RWxlbWVudCA9IFBhdGhmb3JtZXIucHJvdG90eXBlLnBvbHlsaW5lVG9QYXRoKGVsZW1lbnQpO1xyXG4gIG5ld0VsZW1lbnQuZCArPSAnWic7XHJcbiAgcmV0dXJuIG5ld0VsZW1lbnQ7XHJcbn07XHJcblxyXG4vKipcclxuICogUmVhZCBgZWxsaXBzZWAgZWxlbWVudCB0byBleHRyYWN0IGFuZCB0cmFuc2Zvcm1cclxuICogZGF0YSwgdG8gbWFrZSBpdCByZWFkeSBmb3IgYSBgcGF0aGAgb2JqZWN0LlxyXG4gKlxyXG4gKiBAcGFyYW0gIHtET01lbGVtZW50fSBlbGVtZW50IGVsbGlwc2UgZWxlbWVudCB0byB0cmFuc2Zvcm1cclxuICogQHJldHVybiB7b2JqZWN0fSAgICAgICAgICAgICBEYXRhIGZvciBhIGBwYXRoYCBlbGVtZW50XHJcbiAqL1xyXG5QYXRoZm9ybWVyLnByb3RvdHlwZS5lbGxpcHNlVG9QYXRoID0gZnVuY3Rpb24gKGVsZW1lbnQpIHtcclxuICB2YXIgc3RhcnRYID0gZWxlbWVudC5jeCAtIGVsZW1lbnQucngsXHJcbiAgICAgIHN0YXJ0WSA9IGVsZW1lbnQuY3k7XHJcbiAgdmFyIGVuZFggPSBwYXJzZUZsb2F0KGVsZW1lbnQuY3gpICsgcGFyc2VGbG9hdChlbGVtZW50LnJ4KSxcclxuICAgICAgZW5kWSA9IGVsZW1lbnQuY3k7XHJcblxyXG4gIHZhciBuZXdFbGVtZW50ID0ge307XHJcbiAgbmV3RWxlbWVudC5kID0gJ00nICsgc3RhcnRYICsgJywnICsgc3RhcnRZICtcclxuICAgICAgICAgICAgICAgICAnQScgKyBlbGVtZW50LnJ4ICsgJywnICsgZWxlbWVudC5yeSArICcgMCwxLDEgJyArIGVuZFggKyAnLCcgKyBlbmRZICtcclxuICAgICAgICAgICAgICAgICAnQScgKyBlbGVtZW50LnJ4ICsgJywnICsgZWxlbWVudC5yeSArICcgMCwxLDEgJyArIHN0YXJ0WCArICcsJyArIGVuZFk7XHJcbiAgcmV0dXJuIG5ld0VsZW1lbnQ7XHJcbn07XHJcblxyXG4vKipcclxuICogUmVhZCBgY2lyY2xlYCBlbGVtZW50IHRvIGV4dHJhY3QgYW5kIHRyYW5zZm9ybVxyXG4gKiBkYXRhLCB0byBtYWtlIGl0IHJlYWR5IGZvciBhIGBwYXRoYCBvYmplY3QuXHJcbiAqXHJcbiAqIEBwYXJhbSAge0RPTWVsZW1lbnR9IGVsZW1lbnQgQ2lyY2xlIGVsZW1lbnQgdG8gdHJhbnNmb3JtXHJcbiAqIEByZXR1cm4ge29iamVjdH0gICAgICAgICAgICAgRGF0YSBmb3IgYSBgcGF0aGAgZWxlbWVudFxyXG4gKi9cclxuUGF0aGZvcm1lci5wcm90b3R5cGUuY2lyY2xlVG9QYXRoID0gZnVuY3Rpb24gKGVsZW1lbnQpIHtcclxuICB2YXIgbmV3RWxlbWVudCA9IHt9O1xyXG4gIHZhciBzdGFydFggPSBlbGVtZW50LmN4IC0gZWxlbWVudC5yLFxyXG4gICAgICBzdGFydFkgPSBlbGVtZW50LmN5O1xyXG4gIHZhciBlbmRYID0gcGFyc2VGbG9hdChlbGVtZW50LmN4KSArIHBhcnNlRmxvYXQoZWxlbWVudC5yKSxcclxuICAgICAgZW5kWSA9IGVsZW1lbnQuY3k7XHJcbiAgbmV3RWxlbWVudC5kID0gICdNJyArIHN0YXJ0WCArICcsJyArIHN0YXJ0WSArXHJcbiAgICAgICAgICAgICAgICAgICdBJyArIGVsZW1lbnQuciArICcsJyArIGVsZW1lbnQuciArICcgMCwxLDEgJyArIGVuZFggKyAnLCcgKyBlbmRZICtcclxuICAgICAgICAgICAgICAgICAgJ0EnICsgZWxlbWVudC5yICsgJywnICsgZWxlbWVudC5yICsgJyAwLDEsMSAnICsgc3RhcnRYICsgJywnICsgZW5kWTtcclxuICByZXR1cm4gbmV3RWxlbWVudDtcclxufTtcclxuXHJcbi8qKlxyXG4gKiBDcmVhdGUgYHBhdGhgIGVsZW1lbnRzIGZvcm0gb3JpZ2luYWwgZWxlbWVudFxyXG4gKiBhbmQgcHJlcGFyZWQgb2JqZWN0c1xyXG4gKlxyXG4gKiBAcGFyYW0gIHtET01lbGVtZW50fSBlbGVtZW50ICBPcmlnaW5hbCBlbGVtZW50IHRvIHRyYW5zZm9ybVxyXG4gKiBAcGFyYW0gIHtvYmplY3R9IHBhdGhEYXRhICAgICBQYXRoIGRhdGEgKGZyb20gYHRvUGF0aGAgbWV0aG9kcylcclxuICogQHJldHVybiB7RE9NZWxlbWVudH0gICAgICAgICAgUGF0aCBlbGVtZW50XHJcbiAqL1xyXG5QYXRoZm9ybWVyLnByb3RvdHlwZS5wYXRoTWFrZXIgPSBmdW5jdGlvbiAoZWxlbWVudCwgcGF0aERhdGEpIHtcclxuICB2YXIgaSwgYXR0ciwgcGF0aFRhZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnROUygnaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnLCdwYXRoJyk7XHJcbiAgZm9yKGkgPSAwOyBpIDwgZWxlbWVudC5hdHRyaWJ1dGVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICBhdHRyID0gZWxlbWVudC5hdHRyaWJ1dGVzW2ldO1xyXG4gICAgaWYgKHRoaXMuQVRUUl9XQVRDSC5pbmRleE9mKGF0dHIubmFtZSkgPT09IC0xKSB7XHJcbiAgICAgIHBhdGhUYWcuc2V0QXR0cmlidXRlKGF0dHIubmFtZSwgYXR0ci52YWx1ZSk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGZvcihpIGluIHBhdGhEYXRhKSB7XHJcbiAgICBwYXRoVGFnLnNldEF0dHJpYnV0ZShpLCBwYXRoRGF0YVtpXSk7XHJcbiAgfVxyXG4gIHJldHVybiBwYXRoVGFnO1xyXG59O1xyXG5cclxuLyoqXHJcbiAqIFBhcnNlIGF0dHJpYnV0ZXMgb2YgYSBET00gZWxlbWVudCB0b1xyXG4gKiBnZXQgYW4gb2JqZWN0IG9mIGF0dHJpYnV0ZSA9PiB2YWx1ZVxyXG4gKlxyXG4gKiBAcGFyYW0gIHtOYW1lZE5vZGVNYXB9IGF0dHJpYnV0ZXMgQXR0cmlidXRlcyBvYmplY3QgZnJvbSBET00gZWxlbWVudCB0byBwYXJzZVxyXG4gKiBAcmV0dXJuIHtvYmplY3R9ICAgICAgICAgICAgICAgICAgT2JqZWN0IG9mIGF0dHJpYnV0ZXNcclxuICovXHJcblBhdGhmb3JtZXIucHJvdG90eXBlLnBhcnNlQXR0ciA9IGZ1bmN0aW9uIChlbGVtZW50KSB7XHJcbiAgdmFyIGF0dHIsIG91dHB1dCA9IHt9O1xyXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgZWxlbWVudC5sZW5ndGg7IGkrKykge1xyXG4gICAgYXR0ciA9IGVsZW1lbnRbaV07XHJcbiAgICAvLyBDaGVjayBpZiBubyBkYXRhIGF0dHJpYnV0ZSBjb250YWlucyAnJScsIG9yIHRoZSB0cmFuc2Zvcm1hdGlvbiBpcyBpbXBvc3NpYmxlXHJcbiAgICBpZiAodGhpcy5BVFRSX1dBVENILmluZGV4T2YoYXR0ci5uYW1lKSAhPT0gLTEgJiYgYXR0ci52YWx1ZS5pbmRleE9mKCclJykgIT09IC0xKSB7XHJcbiAgICAgIHRocm93IG5ldyBFcnJvcignUGF0aGZvcm1lciBbcGFyc2VBdHRyXTogYSBTVkcgc2hhcGUgZ290IHZhbHVlcyBpbiBwZXJjZW50YWdlLiBUaGlzIGNhbm5vdCBiZSB0cmFuc2Zvcm1lZCBpbnRvIFxcJ3BhdGhcXCcgdGFncy4gUGxlYXNlIHVzZSBcXCd2aWV3Qm94XFwnLicpO1xyXG4gICAgfVxyXG4gICAgb3V0cHV0W2F0dHIubmFtZV0gPSBhdHRyLnZhbHVlO1xyXG4gIH1cclxuICByZXR1cm4gb3V0cHV0O1xyXG59O1xyXG5cclxuICAndXNlIHN0cmljdCc7XHJcblxyXG52YXIgcmVxdWVzdEFuaW1GcmFtZSwgY2FuY2VsQW5pbUZyYW1lLCBwYXJzZVBvc2l0aXZlSW50O1xyXG5cclxuLyoqXHJcbiAqIFZpdnVzXHJcbiAqIEJldGEgdmVyc2lvblxyXG4gKlxyXG4gKiBUYWtlIGFueSBTVkcgYW5kIG1ha2UgdGhlIGFuaW1hdGlvblxyXG4gKiB0byBnaXZlIGdpdmUgdGhlIGltcHJlc3Npb24gb2YgbGl2ZSBkcmF3aW5nXHJcbiAqXHJcbiAqIFRoaXMgaW4gbW9yZSB0aGFuIGp1c3QgaW5zcGlyZWQgZnJvbSBjb2Ryb3BzXHJcbiAqIEF0IHRoYXQgcG9pbnQsIGl0J3MgYSBwdXJlIGZvcmsuXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIENsYXNzIGNvbnN0cnVjdG9yXHJcbiAqIG9wdGlvbiBzdHJ1Y3R1cmVcclxuICogICB0eXBlOiAnZGVsYXllZCd8J2FzeW5jJ3wnb25lQnlPbmUnfCdzY3JpcHQnICh0byBrbm93IGlmIHRoZSBpdGVtIG11c3QgYmUgZHJhd24gYXN5bmNocm9ub3VzbHkgb3Igbm90LCBkZWZhdWx0OiBkZWxheWVkKVxyXG4gKiAgIGR1cmF0aW9uOiA8aW50PiAoaW4gZnJhbWVzKVxyXG4gKiAgIHN0YXJ0OiAnaW5WaWV3cG9ydCd8J21hbnVhbCd8J2F1dG9zdGFydCcgKHN0YXJ0IGF1dG9tYXRpY2FsbHkgdGhlIGFuaW1hdGlvbiwgZGVmYXVsdDogaW5WaWV3cG9ydClcclxuICogICBkZWxheTogPGludD4gKGRlbGF5IGJldHdlZW4gdGhlIGRyYXdpbmcgb2YgZmlyc3QgYW5kIGxhc3QgcGF0aClcclxuICogICBkYXNoR2FwIDxpbnRlZ2VyPiB3aGl0ZXNwYWNlIGV4dHJhIG1hcmdpbiBiZXR3ZWVuIGRhc2hlc1xyXG4gKiAgIHBhdGhUaW1pbmdGdW5jdGlvbiA8ZnVuY3Rpb24+IHRpbWluZyBhbmltYXRpb24gZnVuY3Rpb24gZm9yIGVhY2ggcGF0aCBlbGVtZW50IG9mIHRoZSBTVkdcclxuICogICBhbmltVGltaW5nRnVuY3Rpb24gPGZ1bmN0aW9uPiB0aW1pbmcgYW5pbWF0aW9uIGZ1bmN0aW9uIGZvciB0aGUgY29tcGxldGUgU1ZHXHJcbiAqICAgZm9yY2VSZW5kZXIgPGJvb2xlYW4+IGZvcmNlIHRoZSBicm93c2VyIHRvIHJlLXJlbmRlciBhbGwgdXBkYXRlZCBwYXRoIGl0ZW1zXHJcbiAqICAgc2VsZkRlc3Ryb3kgPGJvb2xlYW4+IHJlbW92ZXMgYWxsIGV4dHJhIHN0eWxpbmcgb24gdGhlIFNWRywgYW5kIGxlYXZlcyBpdCBhcyBvcmlnaW5hbFxyXG4gKlxyXG4gKiBUaGUgYXR0cmlidXRlICd0eXBlJyBpcyBieSBkZWZhdWx0IG9uICdkZWxheWVkJy5cclxuICogIC0gJ2RlbGF5ZWQnXHJcbiAqICAgIGFsbCBwYXRocyBhcmUgZHJhdyBhdCB0aGUgc2FtZSB0aW1lIGJ1dCB3aXRoIGFcclxuICogICAgbGl0dGxlIGRlbGF5IGJldHdlZW4gdGhlbSBiZWZvcmUgc3RhcnRcclxuICogIC0gJ2FzeW5jJ1xyXG4gKiAgICBhbGwgcGF0aCBhcmUgc3RhcnQgYW5kIGZpbmlzaCBhdCB0aGUgc2FtZSB0aW1lXHJcbiAqICAtICdvbmVCeU9uZSdcclxuICogICAgb25seSBvbmUgcGF0aCBpcyBkcmF3IGF0IHRoZSB0aW1lXHJcbiAqICAgIHRoZSBlbmQgb2YgdGhlIGZpcnN0IG9uZSB3aWxsIHRyaWdnZXIgdGhlIGRyYXdcclxuICogICAgb2YgdGhlIG5leHQgb25lXHJcbiAqXHJcbiAqIEFsbCB0aGVzZSB2YWx1ZXMgY2FuIGJlIG92ZXJ3cml0dGVuIGluZGl2aWR1YWxseVxyXG4gKiBmb3IgZWFjaCBwYXRoIGl0ZW0gaW4gdGhlIFNWR1xyXG4gKiBUaGUgdmFsdWUgb2YgZnJhbWVzIHdpbGwgYWx3YXlzIHRha2UgdGhlIGFkdmFudGFnZSBvZlxyXG4gKiB0aGUgZHVyYXRpb24gdmFsdWUuXHJcbiAqIElmIHlvdSBmYWlsIHNvbWV3aGVyZSwgYW4gZXJyb3Igd2lsbCBiZSB0aHJvd24uXHJcbiAqIEdvb2QgbHVjay5cclxuICpcclxuICogQGNvbnN0cnVjdG9yXHJcbiAqIEB0aGlzIHtWaXZ1c31cclxuICogQHBhcmFtIHtET018U3RyaW5nfSAgIGVsZW1lbnQgIERvbSBlbGVtZW50IG9mIHRoZSBTVkcgb3IgaWQgb2YgaXRcclxuICogQHBhcmFtIHtPYmplY3R9ICAgICAgIG9wdGlvbnMgIE9wdGlvbnMgYWJvdXQgdGhlIGFuaW1hdGlvblxyXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSAgICAgY2FsbGJhY2sgQ2FsbGJhY2sgZm9yIHRoZSBlbmQgb2YgdGhlIGFuaW1hdGlvblxyXG4gKi9cclxuZnVuY3Rpb24gVml2dXMgKGVsZW1lbnQsIG9wdGlvbnMsIGNhbGxiYWNrKSB7XHJcblxyXG4gIC8vIFNldHVwXHJcbiAgdGhpcy5pc1JlYWR5ID0gZmFsc2U7XHJcbiAgdGhpcy5zZXRFbGVtZW50KGVsZW1lbnQsIG9wdGlvbnMpO1xyXG4gIHRoaXMuc2V0T3B0aW9ucyhvcHRpb25zKTtcclxuICB0aGlzLnNldENhbGxiYWNrKGNhbGxiYWNrKTtcclxuXHJcbiAgaWYgKHRoaXMuaXNSZWFkeSkge1xyXG4gICAgdGhpcy5pbml0KCk7XHJcbiAgfVxyXG59XHJcblxyXG4vKipcclxuICogVGltaW5nIGZ1bmN0aW9uc1xyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiogXHJcbiAqIFxyXG4gKiBEZWZhdWx0IGZ1bmN0aW9ucyB0byBoZWxwIGRldmVsb3BlcnMuXHJcbiAqIEl0IGFsd2F5cyB0YWtlIGEgbnVtYmVyIGFzIHBhcmFtZXRlciAoYmV0d2VlbiAwIHRvIDEpIHRoZW5cclxuICogcmV0dXJuIGEgbnVtYmVyIChiZXR3ZWVuIDAgYW5kIDEpXHJcbiAqL1xyXG5WaXZ1cy5MSU5FQVIgICAgICAgICAgPSBmdW5jdGlvbiAoeCkge3JldHVybiB4O307XHJcblZpdnVzLkVBU0UgICAgICAgICAgICA9IGZ1bmN0aW9uICh4KSB7cmV0dXJuIC1NYXRoLmNvcyh4ICogTWF0aC5QSSkgLyAyICsgMC41O307XHJcblZpdnVzLkVBU0VfT1VUICAgICAgICA9IGZ1bmN0aW9uICh4KSB7cmV0dXJuIDEgLSBNYXRoLnBvdygxLXgsIDMpO307XHJcblZpdnVzLkVBU0VfSU4gICAgICAgICA9IGZ1bmN0aW9uICh4KSB7cmV0dXJuIE1hdGgucG93KHgsIDMpO307XHJcblZpdnVzLkVBU0VfT1VUX0JPVU5DRSA9IGZ1bmN0aW9uICh4KSB7XHJcbiAgdmFyIGJhc2UgPSAtTWF0aC5jb3MoeCAqICgwLjUgKiBNYXRoLlBJKSkgKyAxLFxyXG4gICAgcmF0ZSA9IE1hdGgucG93KGJhc2UsMS41KSxcclxuICAgIHJhdGVSID0gTWF0aC5wb3coMSAtIHgsIDIpLFxyXG4gICAgcHJvZ3Jlc3MgPSAtTWF0aC5hYnMoTWF0aC5jb3MocmF0ZSAqICgyLjUgKiBNYXRoLlBJKSApKSArIDE7XHJcbiAgcmV0dXJuICgxLSByYXRlUikgKyAocHJvZ3Jlc3MgKiByYXRlUik7XHJcbn07XHJcblxyXG5cclxuLyoqXHJcbiAqIFNldHRlcnNcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIENoZWNrIGFuZCBzZXQgdGhlIGVsZW1lbnQgaW4gdGhlIGluc3RhbmNlXHJcbiAqIFRoZSBtZXRob2Qgd2lsbCBub3QgcmV0dXJuIGFueXRoaW5nLCBidXQgd2lsbCB0aHJvdyBhblxyXG4gKiBlcnJvciBpZiB0aGUgcGFyYW1ldGVyIGlzIGludmFsaWRcclxuICpcclxuICogQHBhcmFtIHtET018U3RyaW5nfSAgIGVsZW1lbnQgIFNWRyBEb20gZWxlbWVudCBvciBpZCBvZiBpdFxyXG4gKi9cclxuVml2dXMucHJvdG90eXBlLnNldEVsZW1lbnQgPSBmdW5jdGlvbiAoZWxlbWVudCwgb3B0aW9ucykge1xyXG4gIC8vIEJhc2ljIGNoZWNrXHJcbiAgaWYgKHR5cGVvZiBlbGVtZW50ID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgdGhyb3cgbmV3IEVycm9yKCdWaXZ1cyBbY29uc3RydWN0b3JdOiBcImVsZW1lbnRcIiBwYXJhbWV0ZXIgaXMgcmVxdWlyZWQnKTtcclxuICB9XHJcblxyXG4gIC8vIFNldCB0aGUgZWxlbWVudFxyXG4gIGlmIChlbGVtZW50LmNvbnN0cnVjdG9yID09PSBTdHJpbmcpIHtcclxuICAgIGVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChlbGVtZW50KTtcclxuICAgIGlmICghZWxlbWVudCkge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ1ZpdnVzIFtjb25zdHJ1Y3Rvcl06IFwiZWxlbWVudFwiIHBhcmFtZXRlciBpcyBub3QgcmVsYXRlZCB0byBhbiBleGlzdGluZyBJRCcpO1xyXG4gICAgfVxyXG4gIH1cclxuICB0aGlzLnBhcmVudEVsID0gZWxlbWVudDtcclxuXHJcbiAgLy8gQ3JlYXRlIHRoZSBvYmplY3QgZWxlbWVudCBpZiB0aGUgcHJvcGVydHkgYGZpbGVgIGV4aXN0cyBpbiB0aGUgb3B0aW9ucyBvYmplY3RcclxuICBpZiAob3B0aW9ucyAmJiBvcHRpb25zLmZpbGUpIHtcclxuICAgIHZhciBvYmpFbG0gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdvYmplY3QnKTtcclxuICAgIG9iakVsbS5zZXRBdHRyaWJ1dGUoJ3R5cGUnLCAnaW1hZ2Uvc3ZnK3htbCcpO1xyXG4gICAgb2JqRWxtLnNldEF0dHJpYnV0ZSgnZGF0YScsIG9wdGlvbnMuZmlsZSk7XHJcbiAgICBvYmpFbG0uc2V0QXR0cmlidXRlKCdidWlsdC1ieS12aXZ1cycsICd0cnVlJyk7XHJcbiAgICBlbGVtZW50LmFwcGVuZENoaWxkKG9iakVsbSk7XHJcbiAgICBlbGVtZW50ID0gb2JqRWxtO1xyXG4gIH1cclxuXHJcbiAgc3dpdGNoIChlbGVtZW50LmNvbnN0cnVjdG9yKSB7XHJcbiAgY2FzZSB3aW5kb3cuU1ZHU1ZHRWxlbWVudDpcclxuICBjYXNlIHdpbmRvdy5TVkdFbGVtZW50OlxyXG4gICAgdGhpcy5lbCA9IGVsZW1lbnQ7XHJcbiAgICB0aGlzLmlzUmVhZHkgPSB0cnVlO1xyXG4gICAgYnJlYWs7XHJcblxyXG4gIGNhc2Ugd2luZG93LkhUTUxPYmplY3RFbGVtZW50OlxyXG4gICAgLy8gSWYgd2UgaGF2ZSB0byB3YWl0IGZvciBpdFxyXG4gICAgdmFyIG9uTG9hZCwgc2VsZjtcclxuICAgIFxyXG4gICAgc2VsZiA9IHRoaXM7XHJcbiAgICBvbkxvYWQgPSBmdW5jdGlvbiAoZSkge1xyXG4gICAgICBpZiAoc2VsZi5pc1JlYWR5KSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICAgIHNlbGYuZWwgPSBlbGVtZW50LmNvbnRlbnREb2N1bWVudCAmJiBlbGVtZW50LmNvbnRlbnREb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdzdmcnKTtcclxuICAgICAgaWYgKCFzZWxmLmVsICYmIGUpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1ZpdnVzIFtjb25zdHJ1Y3Rvcl06IG9iamVjdCBsb2FkZWQgZG9lcyBub3QgY29udGFpbiBhbnkgU1ZHJyk7XHJcbiAgICAgIH1cclxuICAgICAgZWxzZSBpZiAoc2VsZi5lbCkge1xyXG4gICAgICAgIGlmIChlbGVtZW50LmdldEF0dHJpYnV0ZSgnYnVpbHQtYnktdml2dXMnKSkge1xyXG4gICAgICAgICAgc2VsZi5wYXJlbnRFbC5pbnNlcnRCZWZvcmUoc2VsZi5lbCwgZWxlbWVudCk7XHJcbiAgICAgICAgICBzZWxmLnBhcmVudEVsLnJlbW92ZUNoaWxkKGVsZW1lbnQpO1xyXG4gICAgICAgICAgc2VsZi5lbC5zZXRBdHRyaWJ1dGUoJ3dpZHRoJywgJzEwMCUnKTtcclxuICAgICAgICAgIHNlbGYuZWwuc2V0QXR0cmlidXRlKCdoZWlnaHQnLCAnMTAwJScpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBzZWxmLmlzUmVhZHkgPSB0cnVlO1xyXG4gICAgICAgIHNlbGYuaW5pdCgpO1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGlmICghb25Mb2FkKCkpIHtcclxuICAgICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgb25Mb2FkKTtcclxuICAgIH1cclxuICAgIGJyZWFrO1xyXG5cclxuICBkZWZhdWx0OlxyXG4gICAgdGhyb3cgbmV3IEVycm9yKCdWaXZ1cyBbY29uc3RydWN0b3JdOiBcImVsZW1lbnRcIiBwYXJhbWV0ZXIgaXMgbm90IHZhbGlkIChvciBtaXNzIHRoZSBcImZpbGVcIiBhdHRyaWJ1dGUpJyk7XHJcbiAgfVxyXG59O1xyXG5cclxuLyoqXHJcbiAqIFNldCB1cCB1c2VyIG9wdGlvbiB0byB0aGUgaW5zdGFuY2VcclxuICogVGhlIG1ldGhvZCB3aWxsIG5vdCByZXR1cm4gYW55dGhpbmcsIGJ1dCB3aWxsIHRocm93IGFuXHJcbiAqIGVycm9yIGlmIHRoZSBwYXJhbWV0ZXIgaXMgaW52YWxpZFxyXG4gKlxyXG4gKiBAcGFyYW0gIHtvYmplY3R9IG9wdGlvbnMgT2JqZWN0IGZyb20gdGhlIGNvbnN0cnVjdG9yXHJcbiAqL1xyXG5WaXZ1cy5wcm90b3R5cGUuc2V0T3B0aW9ucyA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XHJcbiAgdmFyIGFsbG93ZWRUeXBlcyA9IFsnZGVsYXllZCcsICdhc3luYycsICdvbmVCeU9uZScsICdzY2VuYXJpbycsICdzY2VuYXJpby1zeW5jJ107XHJcbiAgdmFyIGFsbG93ZWRTdGFydHMgPSAgWydpblZpZXdwb3J0JywgJ21hbnVhbCcsICdhdXRvc3RhcnQnXTtcclxuXHJcbiAgLy8gQmFzaWMgY2hlY2tcclxuICBpZiAob3B0aW9ucyAhPT0gdW5kZWZpbmVkICYmIG9wdGlvbnMuY29uc3RydWN0b3IgIT09IE9iamVjdCkge1xyXG4gICAgdGhyb3cgbmV3IEVycm9yKCdWaXZ1cyBbY29uc3RydWN0b3JdOiBcIm9wdGlvbnNcIiBwYXJhbWV0ZXIgbXVzdCBiZSBhbiBvYmplY3QnKTtcclxuICB9XHJcbiAgZWxzZSB7XHJcbiAgICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcclxuICB9XHJcblxyXG4gIC8vIFNldCB0aGUgYW5pbWF0aW9uIHR5cGVcclxuICBpZiAob3B0aW9ucy50eXBlICYmIGFsbG93ZWRUeXBlcy5pbmRleE9mKG9wdGlvbnMudHlwZSkgPT09IC0xKSB7XHJcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ1ZpdnVzIFtjb25zdHJ1Y3Rvcl06ICcgKyBvcHRpb25zLnR5cGUgKyAnIGlzIG5vdCBhbiBleGlzdGluZyBhbmltYXRpb24gYHR5cGVgJyk7XHJcbiAgfVxyXG4gIGVsc2Uge1xyXG4gICAgdGhpcy50eXBlID0gb3B0aW9ucy50eXBlIHx8IGFsbG93ZWRUeXBlc1swXTtcclxuICB9XHJcblxyXG4gIC8vIFNldCB0aGUgc3RhcnQgdHlwZVxyXG4gIGlmIChvcHRpb25zLnN0YXJ0ICYmIGFsbG93ZWRTdGFydHMuaW5kZXhPZihvcHRpb25zLnN0YXJ0KSA9PT0gLTEpIHtcclxuICAgIHRocm93IG5ldyBFcnJvcignVml2dXMgW2NvbnN0cnVjdG9yXTogJyArIG9wdGlvbnMuc3RhcnQgKyAnIGlzIG5vdCBhbiBleGlzdGluZyBgc3RhcnRgIG9wdGlvbicpO1xyXG4gIH1cclxuICBlbHNlIHtcclxuICAgIHRoaXMuc3RhcnQgPSBvcHRpb25zLnN0YXJ0IHx8IGFsbG93ZWRTdGFydHNbMF07XHJcbiAgfVxyXG5cclxuICB0aGlzLmlzSUUgICAgICAgID0gKHdpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoJ01TSUUnKSAhPT0gLTEgfHwgd2luZG93Lm5hdmlnYXRvci51c2VyQWdlbnQuaW5kZXhPZignVHJpZGVudC8nKSAhPT0gLTEgfHwgd2luZG93Lm5hdmlnYXRvci51c2VyQWdlbnQuaW5kZXhPZignRWRnZS8nKSAhPT0gLTEgKTtcclxuICB0aGlzLmR1cmF0aW9uICAgID0gcGFyc2VQb3NpdGl2ZUludChvcHRpb25zLmR1cmF0aW9uLCAxMjApO1xyXG4gIHRoaXMuZGVsYXkgICAgICAgPSBwYXJzZVBvc2l0aXZlSW50KG9wdGlvbnMuZGVsYXksIG51bGwpO1xyXG4gIHRoaXMuZGFzaEdhcCAgICAgPSBwYXJzZVBvc2l0aXZlSW50KG9wdGlvbnMuZGFzaEdhcCwgMSk7XHJcbiAgdGhpcy5mb3JjZVJlbmRlciA9IG9wdGlvbnMuaGFzT3duUHJvcGVydHkoJ2ZvcmNlUmVuZGVyJykgPyAhIW9wdGlvbnMuZm9yY2VSZW5kZXIgOiB0aGlzLmlzSUU7XHJcbiAgdGhpcy5zZWxmRGVzdHJveSA9ICEhb3B0aW9ucy5zZWxmRGVzdHJveTtcclxuICB0aGlzLm9uUmVhZHkgICAgID0gb3B0aW9ucy5vblJlYWR5O1xyXG4gIHRoaXMuZnJhbWVMZW5ndGggPSB0aGlzLmN1cnJlbnRGcmFtZSA9IHRoaXMubWFwID0gdGhpcy5kZWxheVVuaXQgPSB0aGlzLnNwZWVkID0gdGhpcy5oYW5kbGUgPSBudWxsO1xyXG5cclxuICB0aGlzLmlnbm9yZUludmlzaWJsZSA9IG9wdGlvbnMuaGFzT3duUHJvcGVydHkoJ2lnbm9yZUludmlzaWJsZScpID8gISFvcHRpb25zLmlnbm9yZUludmlzaWJsZSA6IGZhbHNlO1xyXG5cclxuICB0aGlzLmFuaW1UaW1pbmdGdW5jdGlvbiA9IG9wdGlvbnMuYW5pbVRpbWluZ0Z1bmN0aW9uIHx8IFZpdnVzLkxJTkVBUjtcclxuICB0aGlzLnBhdGhUaW1pbmdGdW5jdGlvbiA9IG9wdGlvbnMucGF0aFRpbWluZ0Z1bmN0aW9uIHx8IFZpdnVzLkxJTkVBUjtcclxuXHJcbiAgaWYgKHRoaXMuZGVsYXkgPj0gdGhpcy5kdXJhdGlvbikge1xyXG4gICAgdGhyb3cgbmV3IEVycm9yKCdWaXZ1cyBbY29uc3RydWN0b3JdOiBkZWxheSBtdXN0IGJlIHNob3J0ZXIgdGhhbiBkdXJhdGlvbicpO1xyXG4gIH1cclxufTtcclxuXHJcbi8qKlxyXG4gKiBTZXQgdXAgY2FsbGJhY2sgdG8gdGhlIGluc3RhbmNlXHJcbiAqIFRoZSBtZXRob2Qgd2lsbCBub3QgcmV0dXJuIGVueXRoaW5nLCBidXQgd2lsbCB0aHJvdyBhblxyXG4gKiBlcnJvciBpZiB0aGUgcGFyYW1ldGVyIGlzIGludmFsaWRcclxuICpcclxuICogQHBhcmFtICB7RnVuY3Rpb259IGNhbGxiYWNrIENhbGxiYWNrIGZvciB0aGUgYW5pbWF0aW9uIGVuZFxyXG4gKi9cclxuVml2dXMucHJvdG90eXBlLnNldENhbGxiYWNrID0gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XHJcbiAgLy8gQmFzaWMgY2hlY2tcclxuICBpZiAoISFjYWxsYmFjayAmJiBjYWxsYmFjay5jb25zdHJ1Y3RvciAhPT0gRnVuY3Rpb24pIHtcclxuICAgIHRocm93IG5ldyBFcnJvcignVml2dXMgW2NvbnN0cnVjdG9yXTogXCJjYWxsYmFja1wiIHBhcmFtZXRlciBtdXN0IGJlIGEgZnVuY3Rpb24nKTtcclxuICB9XHJcbiAgdGhpcy5jYWxsYmFjayA9IGNhbGxiYWNrIHx8IGZ1bmN0aW9uICgpIHt9O1xyXG59O1xyXG5cclxuXHJcbi8qKlxyXG4gKiBDb3JlXHJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gKi9cclxuXHJcbi8qKlxyXG4gKiBNYXAgdGhlIHN2ZywgcGF0aCBieSBwYXRoLlxyXG4gKiBUaGUgbWV0aG9kIHJldHVybiBub3RoaW5nLCBpdCBqdXN0IGZpbGwgdGhlXHJcbiAqIGBtYXBgIGFycmF5LiBFYWNoIGl0ZW0gaW4gdGhpcyBhcnJheSByZXByZXNlbnRcclxuICogYSBwYXRoIGVsZW1lbnQgZnJvbSB0aGUgU1ZHLCB3aXRoIGluZm9ybWF0aW9ucyBmb3JcclxuICogdGhlIGFuaW1hdGlvbi5cclxuICpcclxuICogYGBgXHJcbiAqIFtcclxuICogICB7XHJcbiAqICAgICBlbDogPERPTW9iaj4gdGhlIHBhdGggZWxlbWVudFxyXG4gKiAgICAgbGVuZ3RoOiA8bnVtYmVyPiBsZW5ndGggb2YgdGhlIHBhdGggbGluZVxyXG4gKiAgICAgc3RhcnRBdDogPG51bWJlcj4gdGltZSBzdGFydCBvZiB0aGUgcGF0aCBhbmltYXRpb24gKGluIGZyYW1lcylcclxuICogICAgIGR1cmF0aW9uOiA8bnVtYmVyPiBwYXRoIGFuaW1hdGlvbiBkdXJhdGlvbiAoaW4gZnJhbWVzKVxyXG4gKiAgIH0sXHJcbiAqICAgLi4uXHJcbiAqIF1cclxuICogYGBgXHJcbiAqXHJcbiAqL1xyXG5WaXZ1cy5wcm90b3R5cGUubWFwcGluZyA9IGZ1bmN0aW9uICgpIHtcclxuICB2YXIgaSwgcGF0aHMsIHBhdGgsIHBBdHRycywgcGF0aE9iaiwgdG90YWxMZW5ndGgsIGxlbmd0aE1ldGVyLCB0aW1lUG9pbnQ7XHJcbiAgdGltZVBvaW50ID0gdG90YWxMZW5ndGggPSBsZW5ndGhNZXRlciA9IDA7XHJcbiAgcGF0aHMgPSB0aGlzLmVsLnF1ZXJ5U2VsZWN0b3JBbGwoJ3BhdGgnKTtcclxuXHJcbiAgZm9yIChpID0gMDsgaSA8IHBhdGhzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICBwYXRoID0gcGF0aHNbaV07XHJcbiAgICBpZiAodGhpcy5pc0ludmlzaWJsZShwYXRoKSkge1xyXG4gICAgICBjb250aW51ZTtcclxuICAgIH1cclxuICAgIHBhdGhPYmogPSB7XHJcbiAgICAgIGVsOiBwYXRoLFxyXG4gICAgICBsZW5ndGg6IE1hdGguY2VpbChwYXRoLmdldFRvdGFsTGVuZ3RoKCkpXHJcbiAgICB9O1xyXG4gICAgLy8gVGVzdCBpZiB0aGUgcGF0aCBsZW5ndGggaXMgY29ycmVjdFxyXG4gICAgaWYgKGlzTmFOKHBhdGhPYmoubGVuZ3RoKSkge1xyXG4gICAgICBpZiAod2luZG93LmNvbnNvbGUgJiYgY29uc29sZS53YXJuKSB7XHJcbiAgICAgICAgY29uc29sZS53YXJuKCdWaXZ1cyBbbWFwcGluZ106IGNhbm5vdCByZXRyaWV2ZSBhIHBhdGggZWxlbWVudCBsZW5ndGgnLCBwYXRoKTtcclxuICAgICAgfVxyXG4gICAgICBjb250aW51ZTtcclxuICAgIH1cclxuICAgIHRoaXMubWFwLnB1c2gocGF0aE9iaik7XHJcbiAgICBwYXRoLnN0eWxlLnN0cm9rZURhc2hhcnJheSAgPSBwYXRoT2JqLmxlbmd0aCArICcgJyArIChwYXRoT2JqLmxlbmd0aCArIHRoaXMuZGFzaEdhcCAqIDIpO1xyXG4gICAgcGF0aC5zdHlsZS5zdHJva2VEYXNob2Zmc2V0ID0gcGF0aE9iai5sZW5ndGggKyB0aGlzLmRhc2hHYXA7XHJcbiAgICBwYXRoT2JqLmxlbmd0aCArPSB0aGlzLmRhc2hHYXA7XHJcbiAgICB0b3RhbExlbmd0aCArPSBwYXRoT2JqLmxlbmd0aDtcclxuXHJcbiAgICB0aGlzLnJlbmRlclBhdGgoaSk7XHJcbiAgfVxyXG5cclxuICB0b3RhbExlbmd0aCA9IHRvdGFsTGVuZ3RoID09PSAwID8gMSA6IHRvdGFsTGVuZ3RoO1xyXG4gIHRoaXMuZGVsYXkgPSB0aGlzLmRlbGF5ID09PSBudWxsID8gdGhpcy5kdXJhdGlvbiAvIDMgOiB0aGlzLmRlbGF5O1xyXG4gIHRoaXMuZGVsYXlVbml0ID0gdGhpcy5kZWxheSAvIChwYXRocy5sZW5ndGggPiAxID8gcGF0aHMubGVuZ3RoIC0gMSA6IDEpO1xyXG5cclxuICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5tYXAubGVuZ3RoOyBpKyspIHtcclxuICAgIHBhdGhPYmogPSB0aGlzLm1hcFtpXTtcclxuXHJcbiAgICBzd2l0Y2ggKHRoaXMudHlwZSkge1xyXG4gICAgY2FzZSAnZGVsYXllZCc6XHJcbiAgICAgIHBhdGhPYmouc3RhcnRBdCA9IHRoaXMuZGVsYXlVbml0ICogaTtcclxuICAgICAgcGF0aE9iai5kdXJhdGlvbiA9IHRoaXMuZHVyYXRpb24gLSB0aGlzLmRlbGF5O1xyXG4gICAgICBicmVhaztcclxuXHJcbiAgICBjYXNlICdvbmVCeU9uZSc6XHJcbiAgICAgIHBhdGhPYmouc3RhcnRBdCA9IGxlbmd0aE1ldGVyIC8gdG90YWxMZW5ndGggKiB0aGlzLmR1cmF0aW9uO1xyXG4gICAgICBwYXRoT2JqLmR1cmF0aW9uID0gcGF0aE9iai5sZW5ndGggLyB0b3RhbExlbmd0aCAqIHRoaXMuZHVyYXRpb247XHJcbiAgICAgIGJyZWFrO1xyXG5cclxuICAgIGNhc2UgJ2FzeW5jJzpcclxuICAgICAgcGF0aE9iai5zdGFydEF0ID0gMDtcclxuICAgICAgcGF0aE9iai5kdXJhdGlvbiA9IHRoaXMuZHVyYXRpb247XHJcbiAgICAgIGJyZWFrO1xyXG5cclxuICAgIGNhc2UgJ3NjZW5hcmlvLXN5bmMnOlxyXG4gICAgICBwYXRoID0gcGF0aHNbaV07XHJcbiAgICAgIHBBdHRycyA9IHRoaXMucGFyc2VBdHRyKHBhdGgpO1xyXG4gICAgICBwYXRoT2JqLnN0YXJ0QXQgPSB0aW1lUG9pbnQgKyAocGFyc2VQb3NpdGl2ZUludChwQXR0cnNbJ2RhdGEtZGVsYXknXSwgdGhpcy5kZWxheVVuaXQpIHx8IDApO1xyXG4gICAgICBwYXRoT2JqLmR1cmF0aW9uID0gcGFyc2VQb3NpdGl2ZUludChwQXR0cnNbJ2RhdGEtZHVyYXRpb24nXSwgdGhpcy5kdXJhdGlvbik7XHJcbiAgICAgIHRpbWVQb2ludCA9IHBBdHRyc1snZGF0YS1hc3luYyddICE9PSB1bmRlZmluZWQgPyBwYXRoT2JqLnN0YXJ0QXQgOiBwYXRoT2JqLnN0YXJ0QXQgKyBwYXRoT2JqLmR1cmF0aW9uO1xyXG4gICAgICB0aGlzLmZyYW1lTGVuZ3RoID0gTWF0aC5tYXgodGhpcy5mcmFtZUxlbmd0aCwgKHBhdGhPYmouc3RhcnRBdCArIHBhdGhPYmouZHVyYXRpb24pKTtcclxuICAgICAgYnJlYWs7XHJcblxyXG4gICAgY2FzZSAnc2NlbmFyaW8nOlxyXG4gICAgICBwYXRoID0gcGF0aHNbaV07XHJcbiAgICAgIHBBdHRycyA9IHRoaXMucGFyc2VBdHRyKHBhdGgpO1xyXG4gICAgICBwYXRoT2JqLnN0YXJ0QXQgPSBwYXJzZVBvc2l0aXZlSW50KHBBdHRyc1snZGF0YS1zdGFydCddLCB0aGlzLmRlbGF5VW5pdCkgfHwgMDtcclxuICAgICAgcGF0aE9iai5kdXJhdGlvbiA9IHBhcnNlUG9zaXRpdmVJbnQocEF0dHJzWydkYXRhLWR1cmF0aW9uJ10sIHRoaXMuZHVyYXRpb24pO1xyXG4gICAgICB0aGlzLmZyYW1lTGVuZ3RoID0gTWF0aC5tYXgodGhpcy5mcmFtZUxlbmd0aCwgKHBhdGhPYmouc3RhcnRBdCArIHBhdGhPYmouZHVyYXRpb24pKTtcclxuICAgICAgYnJlYWs7XHJcbiAgICB9XHJcbiAgICBsZW5ndGhNZXRlciArPSBwYXRoT2JqLmxlbmd0aDtcclxuICAgIHRoaXMuZnJhbWVMZW5ndGggPSB0aGlzLmZyYW1lTGVuZ3RoIHx8IHRoaXMuZHVyYXRpb247XHJcbiAgfVxyXG59O1xyXG5cclxuLyoqXHJcbiAqIEludGVydmFsIG1ldGhvZCB0byBkcmF3IHRoZSBTVkcgZnJvbSBjdXJyZW50XHJcbiAqIHBvc2l0aW9uIG9mIHRoZSBhbmltYXRpb24uIEl0IHVwZGF0ZSB0aGUgdmFsdWUgb2ZcclxuICogYGN1cnJlbnRGcmFtZWAgYW5kIHJlLXRyYWNlIHRoZSBTVkcuXHJcbiAqXHJcbiAqIEl0IHVzZSB0aGlzLmhhbmRsZSB0byBzdG9yZSB0aGUgcmVxdWVzdEFuaW1hdGlvbkZyYW1lXHJcbiAqIGFuZCBjbGVhciBpdCBvbmUgdGhlIGFuaW1hdGlvbiBpcyBzdG9wcGVkLiBTbyB0aGlzXHJcbiAqIGF0dHJpYnV0ZSBjYW4gYmUgdXNlZCB0byBrbm93IGlmIHRoZSBhbmltYXRpb24gaXNcclxuICogcGxheWluZy5cclxuICpcclxuICogT25jZSB0aGUgYW5pbWF0aW9uIGF0IHRoZSBlbmQsIHRoaXMgbWV0aG9kIHdpbGxcclxuICogdHJpZ2dlciB0aGUgVml2dXMgY2FsbGJhY2suXHJcbiAqXHJcbiAqL1xyXG5WaXZ1cy5wcm90b3R5cGUuZHJhd2VyID0gZnVuY3Rpb24gKCkge1xyXG4gIHZhciBzZWxmID0gdGhpcztcclxuICB0aGlzLmN1cnJlbnRGcmFtZSArPSB0aGlzLnNwZWVkO1xyXG5cclxuICBpZiAodGhpcy5jdXJyZW50RnJhbWUgPD0gMCkge1xyXG4gICAgdGhpcy5zdG9wKCk7XHJcbiAgICB0aGlzLnJlc2V0KCk7XHJcbiAgICB0aGlzLmNhbGxiYWNrKHRoaXMpO1xyXG4gIH0gZWxzZSBpZiAodGhpcy5jdXJyZW50RnJhbWUgPj0gdGhpcy5mcmFtZUxlbmd0aCkge1xyXG4gICAgdGhpcy5zdG9wKCk7XHJcbiAgICB0aGlzLmN1cnJlbnRGcmFtZSA9IHRoaXMuZnJhbWVMZW5ndGg7XHJcbiAgICB0aGlzLnRyYWNlKCk7XHJcbiAgICBpZiAodGhpcy5zZWxmRGVzdHJveSkge1xyXG4gICAgICB0aGlzLmRlc3Ryb3koKTtcclxuICAgIH1cclxuICAgIHRoaXMuY2FsbGJhY2sodGhpcyk7XHJcbiAgfSBlbHNlIHtcclxuICAgIHRoaXMudHJhY2UoKTtcclxuICAgIHRoaXMuaGFuZGxlID0gcmVxdWVzdEFuaW1GcmFtZShmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHNlbGYuZHJhd2VyKCk7XHJcbiAgICB9KTtcclxuICB9XHJcbn07XHJcblxyXG4vKipcclxuICogRHJhdyB0aGUgU1ZHIGF0IHRoZSBjdXJyZW50IGluc3RhbnQgZnJvbSB0aGVcclxuICogYGN1cnJlbnRGcmFtZWAgdmFsdWUuIEhlcmUgaXMgd2hlcmUgbW9zdCBvZiB0aGUgbWFnaWMgaXMuXHJcbiAqIFRoZSB0cmljayBpcyB0byB1c2UgdGhlIGBzdHJva2VEYXNob2Zmc2V0YCBzdHlsZSBwcm9wZXJ0eS5cclxuICpcclxuICogRm9yIG9wdGltaXNhdGlvbiByZWFzb25zLCBhIG5ldyBwcm9wZXJ0eSBjYWxsZWQgYHByb2dyZXNzYFxyXG4gKiBpcyBhZGRlZCBpbiBlYWNoIGl0ZW0gb2YgYG1hcGAuIFRoaXMgb25lIGNvbnRhaW4gdGhlIGN1cnJlbnRcclxuICogcHJvZ3Jlc3Mgb2YgdGhlIHBhdGggZWxlbWVudC4gT25seSBpZiB0aGUgbmV3IHZhbHVlIGlzIGRpZmZlcmVudFxyXG4gKiB0aGUgbmV3IHZhbHVlIHdpbGwgYmUgYXBwbGllZCB0byB0aGUgRE9NIGVsZW1lbnQuIFRoaXNcclxuICogbWV0aG9kIHNhdmUgYSBsb3Qgb2YgcmVzb3VyY2VzIHRvIHJlLXJlbmRlciB0aGUgU1ZHLiBBbmQgY291bGRcclxuICogYmUgaW1wcm92ZWQgaWYgdGhlIGFuaW1hdGlvbiBjb3VsZG4ndCBiZSBwbGF5ZWQgZm9yd2FyZC5cclxuICpcclxuICovXHJcblZpdnVzLnByb3RvdHlwZS50cmFjZSA9IGZ1bmN0aW9uICgpIHtcclxuICB2YXIgaSwgcHJvZ3Jlc3MsIHBhdGgsIGN1cnJlbnRGcmFtZTtcclxuICBjdXJyZW50RnJhbWUgPSB0aGlzLmFuaW1UaW1pbmdGdW5jdGlvbih0aGlzLmN1cnJlbnRGcmFtZSAvIHRoaXMuZnJhbWVMZW5ndGgpICogdGhpcy5mcmFtZUxlbmd0aDtcclxuICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5tYXAubGVuZ3RoOyBpKyspIHtcclxuICAgIHBhdGggPSB0aGlzLm1hcFtpXTtcclxuICAgIHByb2dyZXNzID0gKGN1cnJlbnRGcmFtZSAtIHBhdGguc3RhcnRBdCkgLyBwYXRoLmR1cmF0aW9uO1xyXG4gICAgcHJvZ3Jlc3MgPSB0aGlzLnBhdGhUaW1pbmdGdW5jdGlvbihNYXRoLm1heCgwLCBNYXRoLm1pbigxLCBwcm9ncmVzcykpKTtcclxuICAgIGlmIChwYXRoLnByb2dyZXNzICE9PSBwcm9ncmVzcykge1xyXG4gICAgICBwYXRoLnByb2dyZXNzID0gcHJvZ3Jlc3M7XHJcbiAgICAgIHBhdGguZWwuc3R5bGUuc3Ryb2tlRGFzaG9mZnNldCA9IE1hdGguZmxvb3IocGF0aC5sZW5ndGggKiAoMSAtIHByb2dyZXNzKSk7XHJcbiAgICAgIHRoaXMucmVuZGVyUGF0aChpKTtcclxuICAgIH1cclxuICB9XHJcbn07XHJcblxyXG4vKipcclxuICogTWV0aG9kIGZvcmNpbmcgdGhlIGJyb3dzZXIgdG8gcmUtcmVuZGVyIGEgcGF0aCBlbGVtZW50XHJcbiAqIGZyb20gaXQncyBpbmRleCBpbiB0aGUgbWFwLiBEZXBlbmRpbmcgb24gdGhlIGBmb3JjZVJlbmRlcmBcclxuICogdmFsdWUuXHJcbiAqIFRoZSB0cmljayBpcyB0byByZXBsYWNlIHRoZSBwYXRoIGVsZW1lbnQgYnkgaXQncyBjbG9uZS5cclxuICogVGhpcyBwcmFjdGljZSBpcyBub3QgcmVjb21tZW5kZWQgYmVjYXVzZSBpdCdzIGFza2luZyBtb3JlXHJcbiAqIHJlc3NvdXJjZXMsIHRvbyBtdWNoIERPTSBtYW51cHVsYXRpb24uLlxyXG4gKiBidXQgaXQncyB0aGUgb25seSB3YXkgdG8gbGV0IHRoZSBtYWdpYyBoYXBwZW4gb24gSUUuXHJcbiAqIEJ5IGRlZmF1bHQsIHRoaXMgZmFsbGJhY2sgaXMgb25seSBhcHBsaWVkIG9uIElFLlxyXG4gKiBcclxuICogQHBhcmFtICB7TnVtYmVyfSBpbmRleCBQYXRoIGluZGV4XHJcbiAqL1xyXG5WaXZ1cy5wcm90b3R5cGUucmVuZGVyUGF0aCA9IGZ1bmN0aW9uIChpbmRleCkge1xyXG4gIGlmICh0aGlzLmZvcmNlUmVuZGVyICYmIHRoaXMubWFwICYmIHRoaXMubWFwW2luZGV4XSkge1xyXG4gICAgdmFyIHBhdGhPYmogPSB0aGlzLm1hcFtpbmRleF0sXHJcbiAgICAgICAgbmV3UGF0aCA9IHBhdGhPYmouZWwuY2xvbmVOb2RlKHRydWUpO1xyXG4gICAgcGF0aE9iai5lbC5wYXJlbnROb2RlLnJlcGxhY2VDaGlsZChuZXdQYXRoLCBwYXRoT2JqLmVsKTtcclxuICAgIHBhdGhPYmouZWwgPSBuZXdQYXRoO1xyXG4gIH1cclxufTtcclxuXHJcbi8qKlxyXG4gKiBXaGVuIHRoZSBTVkcgb2JqZWN0IGlzIGxvYWRlZCBhbmQgcmVhZHksXHJcbiAqIHRoaXMgbWV0aG9kIHdpbGwgY29udGludWUgdGhlIGluaXRpYWxpc2F0aW9uLlxyXG4gKlxyXG4gKiBUaGlzIHRoaXMgbWFpbmx5IGR1ZSB0byB0aGUgY2FzZSBvZiBwYXNzaW5nIGFuXHJcbiAqIG9iamVjdCB0YWcgaW4gdGhlIGNvbnN0cnVjdG9yLiBJdCB3aWxsIHdhaXRcclxuICogdGhlIGVuZCBvZiB0aGUgbG9hZGluZyB0byBpbml0aWFsaXNlLlxyXG4gKiBcclxuICovXHJcblZpdnVzLnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24gKCkge1xyXG4gIC8vIFNldCBvYmplY3QgdmFyaWFibGVzXHJcbiAgdGhpcy5mcmFtZUxlbmd0aCA9IDA7XHJcbiAgdGhpcy5jdXJyZW50RnJhbWUgPSAwO1xyXG4gIHRoaXMubWFwID0gW107XHJcblxyXG4gIC8vIFN0YXJ0XHJcbiAgbmV3IFBhdGhmb3JtZXIodGhpcy5lbCk7XHJcbiAgdGhpcy5tYXBwaW5nKCk7XHJcbiAgdGhpcy5zdGFydGVyKCk7XHJcblxyXG4gIGlmICh0aGlzLm9uUmVhZHkpIHtcclxuICAgIHRoaXMub25SZWFkeSh0aGlzKTtcclxuICB9XHJcbn07XHJcblxyXG4vKipcclxuICogVHJpZ2dlciB0byBzdGFydCBvZiB0aGUgYW5pbWF0aW9uLlxyXG4gKiBEZXBlbmRpbmcgb24gdGhlIGBzdGFydGAgdmFsdWUsIGEgZGlmZmVyZW50IHNjcmlwdFxyXG4gKiB3aWxsIGJlIGFwcGxpZWQuXHJcbiAqXHJcbiAqIElmIHRoZSBgc3RhcnRgIHZhbHVlIGlzIG5vdCB2YWxpZCwgYW4gZXJyb3Igd2lsbCBiZSB0aHJvd24uXHJcbiAqIEV2ZW4gaWYgdGVjaG5pY2FsbHksIHRoaXMgaXMgaW1wb3NzaWJsZS5cclxuICpcclxuICovXHJcblZpdnVzLnByb3RvdHlwZS5zdGFydGVyID0gZnVuY3Rpb24gKCkge1xyXG4gIHN3aXRjaCAodGhpcy5zdGFydCkge1xyXG4gIGNhc2UgJ21hbnVhbCc6XHJcbiAgICByZXR1cm47XHJcblxyXG4gIGNhc2UgJ2F1dG9zdGFydCc6XHJcbiAgICB0aGlzLnBsYXkoKTtcclxuICAgIGJyZWFrO1xyXG5cclxuICBjYXNlICdpblZpZXdwb3J0JzpcclxuICAgIHZhciBzZWxmID0gdGhpcyxcclxuICAgIGxpc3RlbmVyID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICBpZiAoc2VsZi5pc0luVmlld3BvcnQoc2VsZi5wYXJlbnRFbCwgMSkpIHtcclxuICAgICAgICBzZWxmLnBsYXkoKTtcclxuICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgbGlzdGVuZXIpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIGxpc3RlbmVyKTtcclxuICAgIGxpc3RlbmVyKCk7XHJcbiAgICBicmVhaztcclxuICB9XHJcbn07XHJcblxyXG5cclxuLyoqXHJcbiAqIENvbnRyb2xzXHJcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gKi9cclxuXHJcbi8qKlxyXG4gKiBHZXQgdGhlIGN1cnJlbnQgc3RhdHVzIG9mIHRoZSBhbmltYXRpb24gYmV0d2VlblxyXG4gKiB0aHJlZSBkaWZmZXJlbnQgc3RhdGVzOiAnc3RhcnQnLCAncHJvZ3Jlc3MnLCAnZW5kJy5cclxuICogQHJldHVybiB7c3RyaW5nfSBJbnN0YW5jZSBzdGF0dXNcclxuICovXHJcblZpdnVzLnByb3RvdHlwZS5nZXRTdGF0dXMgPSBmdW5jdGlvbiAoKSB7XHJcbiAgcmV0dXJuIHRoaXMuY3VycmVudEZyYW1lID09PSAwID8gJ3N0YXJ0JyA6IHRoaXMuY3VycmVudEZyYW1lID09PSB0aGlzLmZyYW1lTGVuZ3RoID8gJ2VuZCcgOiAncHJvZ3Jlc3MnO1xyXG59O1xyXG5cclxuLyoqXHJcbiAqIFJlc2V0IHRoZSBpbnN0YW5jZSB0byB0aGUgaW5pdGlhbCBzdGF0ZSA6IHVuZHJhd1xyXG4gKiBCZSBjYXJlZnVsLCBpdCBqdXN0IHJlc2V0IHRoZSBhbmltYXRpb24sIGlmIHlvdSdyZVxyXG4gKiBwbGF5aW5nIHRoZSBhbmltYXRpb24sIHRoaXMgd29uJ3Qgc3RvcCBpdC4gQnV0IGp1c3RcclxuICogbWFrZSBpdCBzdGFydCBmcm9tIHN0YXJ0LlxyXG4gKlxyXG4gKi9cclxuVml2dXMucHJvdG90eXBlLnJlc2V0ID0gZnVuY3Rpb24gKCkge1xyXG4gIHJldHVybiB0aGlzLnNldEZyYW1lUHJvZ3Jlc3MoMCk7XHJcbn07XHJcblxyXG4vKipcclxuICogU2V0IHRoZSBpbnN0YW5jZSB0byB0aGUgZmluYWwgc3RhdGUgOiBkcmF3blxyXG4gKiBCZSBjYXJlZnVsLCBpdCBqdXN0IHNldCB0aGUgYW5pbWF0aW9uLCBpZiB5b3UncmVcclxuICogcGxheWluZyB0aGUgYW5pbWF0aW9uIG9uIHJld2luZCwgdGhpcyB3b24ndCBzdG9wIGl0LlxyXG4gKiBCdXQganVzdCBtYWtlIGl0IHN0YXJ0IGZyb20gdGhlIGVuZC5cclxuICpcclxuICovXHJcblZpdnVzLnByb3RvdHlwZS5maW5pc2ggPSBmdW5jdGlvbiAoKSB7XHJcbiAgcmV0dXJuIHRoaXMuc2V0RnJhbWVQcm9ncmVzcygxKTtcclxufTtcclxuXHJcbi8qKlxyXG4gKiBTZXQgdGhlIGxldmVsIG9mIHByb2dyZXNzIG9mIHRoZSBkcmF3aW5nLlxyXG4gKiBcclxuICogQHBhcmFtIHtudW1iZXJ9IHByb2dyZXNzIExldmVsIG9mIHByb2dyZXNzIHRvIHNldFxyXG4gKi9cclxuVml2dXMucHJvdG90eXBlLnNldEZyYW1lUHJvZ3Jlc3MgPSBmdW5jdGlvbiAocHJvZ3Jlc3MpIHtcclxuICBwcm9ncmVzcyA9IE1hdGgubWluKDEsIE1hdGgubWF4KDAsIHByb2dyZXNzKSk7XHJcbiAgdGhpcy5jdXJyZW50RnJhbWUgPSBNYXRoLnJvdW5kKHRoaXMuZnJhbWVMZW5ndGggKiBwcm9ncmVzcyk7XHJcbiAgdGhpcy50cmFjZSgpO1xyXG4gIHJldHVybiB0aGlzO1xyXG59O1xyXG5cclxuLyoqXHJcbiAqIFBsYXkgdGhlIGFuaW1hdGlvbiBhdCB0aGUgZGVzaXJlZCBzcGVlZC5cclxuICogU3BlZWQgbXVzdCBiZSBhIHZhbGlkIG51bWJlciAobm8gemVybykuXHJcbiAqIEJ5IGRlZmF1bHQsIHRoZSBzcGVlZCB2YWx1ZSBpcyAxLlxyXG4gKiBCdXQgYSBuZWdhdGl2ZSB2YWx1ZSBpcyBhY2NlcHRlZCB0byBnbyBmb3J3YXJkLlxyXG4gKlxyXG4gKiBBbmQgd29ya3Mgd2l0aCBmbG9hdCB0b28uXHJcbiAqIEJ1dCBkb24ndCBmb3JnZXQgd2UgYXJlIGluIEphdmFTY3JpcHQsIHNlIGJlIG5pY2VcclxuICogd2l0aCBoaW0gYW5kIGdpdmUgaGltIGEgMS8yXnggdmFsdWUuXHJcbiAqXHJcbiAqIEBwYXJhbSAge251bWJlcn0gc3BlZWQgQW5pbWF0aW9uIHNwZWVkIFtvcHRpb25hbF1cclxuICovXHJcblZpdnVzLnByb3RvdHlwZS5wbGF5ID0gZnVuY3Rpb24gKHNwZWVkKSB7XHJcbiAgaWYgKHNwZWVkICYmIHR5cGVvZiBzcGVlZCAhPT0gJ251bWJlcicpIHtcclxuICAgIHRocm93IG5ldyBFcnJvcignVml2dXMgW3BsYXldOiBpbnZhbGlkIHNwZWVkJyk7XHJcbiAgfVxyXG4gIHRoaXMuc3BlZWQgPSBzcGVlZCB8fCAxO1xyXG4gIGlmICghdGhpcy5oYW5kbGUpIHtcclxuICAgIHRoaXMuZHJhd2VyKCk7XHJcbiAgfVxyXG4gIHJldHVybiB0aGlzO1xyXG59O1xyXG5cclxuLyoqXHJcbiAqIFN0b3AgdGhlIGN1cnJlbnQgYW5pbWF0aW9uLCBpZiBvbiBwcm9ncmVzcy5cclxuICogU2hvdWxkIG5vdCB0cmlnZ2VyIGFueSBlcnJvci5cclxuICpcclxuICovXHJcblZpdnVzLnByb3RvdHlwZS5zdG9wID0gZnVuY3Rpb24gKCkge1xyXG4gIGlmICh0aGlzLmhhbmRsZSkge1xyXG4gICAgY2FuY2VsQW5pbUZyYW1lKHRoaXMuaGFuZGxlKTtcclxuICAgIHRoaXMuaGFuZGxlID0gbnVsbDtcclxuICB9XHJcbiAgcmV0dXJuIHRoaXM7XHJcbn07XHJcblxyXG4vKipcclxuICogRGVzdHJveSB0aGUgaW5zdGFuY2UuXHJcbiAqIFJlbW92ZSBhbGwgYmFkIHN0eWxpbmcgYXR0cmlidXRlcyBvbiBhbGxcclxuICogcGF0aCB0YWdzXHJcbiAqXHJcbiAqL1xyXG5WaXZ1cy5wcm90b3R5cGUuZGVzdHJveSA9IGZ1bmN0aW9uICgpIHtcclxuICB2YXIgaSwgcGF0aDtcclxuICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5tYXAubGVuZ3RoOyBpKyspIHtcclxuICAgIHBhdGggPSB0aGlzLm1hcFtpXTtcclxuICAgIHBhdGguZWwuc3R5bGUuc3Ryb2tlRGFzaG9mZnNldCA9IG51bGw7XHJcbiAgICBwYXRoLmVsLnN0eWxlLnN0cm9rZURhc2hhcnJheSA9IG51bGw7XHJcbiAgICB0aGlzLnJlbmRlclBhdGgoaSk7XHJcbiAgfVxyXG59O1xyXG5cclxuXHJcbi8qKlxyXG4gKiBVdGlscyBtZXRob2RzXHJcbiAqIGluY2x1ZGUgbWV0aG9kcyBmcm9tIENvZHJvcHNcclxuICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIE1ldGhvZCB0byBiZXN0IGd1ZXNzIGlmIGEgcGF0aCBzaG91bGQgYWRkZWQgaW50b1xyXG4gKiB0aGUgYW5pbWF0aW9uIG9yIG5vdC5cclxuICpcclxuICogMS4gVXNlIHRoZSBgZGF0YS12aXZ1cy1pZ25vcmVgIGF0dHJpYnV0ZSBpZiBzZXRcclxuICogMi4gQ2hlY2sgaWYgdGhlIGluc3RhbmNlIG11c3QgaWdub3JlIGludmlzaWJsZSBwYXRoc1xyXG4gKiAzLiBDaGVjayBpZiB0aGUgcGF0aCBpcyB2aXNpYmxlXHJcbiAqXHJcbiAqIEZvciBub3cgdGhlIHZpc2liaWxpdHkgY2hlY2tpbmcgaXMgdW5zdGFibGUuXHJcbiAqIEl0IHdpbGwgYmUgdXNlZCBmb3IgYSBiZXRhIHBoYXNlLlxyXG4gKlxyXG4gKiBPdGhlciBpbXByb3ZtZW50cyBhcmUgcGxhbm5lZC4gTGlrZSBkZXRlY3RpbmdcclxuICogaXMgdGhlIHBhdGggZ290IGEgc3Ryb2tlIG9yIGEgdmFsaWQgb3BhY2l0eS5cclxuICovXHJcblZpdnVzLnByb3RvdHlwZS5pc0ludmlzaWJsZSA9IGZ1bmN0aW9uIChlbCkge1xyXG4gIHZhciByZWN0LFxyXG4gICAgaWdub3JlQXR0ciA9IGVsLmdldEF0dHJpYnV0ZSgnZGF0YS1pZ25vcmUnKTtcclxuXHJcbiAgaWYgKGlnbm9yZUF0dHIgIT09IG51bGwpIHtcclxuICAgIHJldHVybiBpZ25vcmVBdHRyICE9PSAnZmFsc2UnO1xyXG4gIH1cclxuXHJcbiAgaWYgKHRoaXMuaWdub3JlSW52aXNpYmxlKSB7XHJcbiAgICByZWN0ID0gZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICByZXR1cm4gIXJlY3Qud2lkdGggJiYgIXJlY3QuaGVpZ2h0O1xyXG4gIH1cclxuICBlbHNlIHtcclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcbn07XHJcblxyXG4vKipcclxuICogUGFyc2UgYXR0cmlidXRlcyBvZiBhIERPTSBlbGVtZW50IHRvXHJcbiAqIGdldCBhbiBvYmplY3Qgb2Yge2F0dHJpYnV0ZU5hbWUgPT4gYXR0cmlidXRlVmFsdWV9XHJcbiAqXHJcbiAqIEBwYXJhbSAge29iamVjdH0gZWxlbWVudCBET00gZWxlbWVudCB0byBwYXJzZVxyXG4gKiBAcmV0dXJuIHtvYmplY3R9ICAgICAgICAgT2JqZWN0IG9mIGF0dHJpYnV0ZXNcclxuICovXHJcblZpdnVzLnByb3RvdHlwZS5wYXJzZUF0dHIgPSBmdW5jdGlvbiAoZWxlbWVudCkge1xyXG4gIHZhciBhdHRyLCBvdXRwdXQgPSB7fTtcclxuICBpZiAoZWxlbWVudCAmJiBlbGVtZW50LmF0dHJpYnV0ZXMpIHtcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZWxlbWVudC5hdHRyaWJ1dGVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIGF0dHIgPSBlbGVtZW50LmF0dHJpYnV0ZXNbaV07XHJcbiAgICAgIG91dHB1dFthdHRyLm5hbWVdID0gYXR0ci52YWx1ZTtcclxuICAgIH1cclxuICB9XHJcbiAgcmV0dXJuIG91dHB1dDtcclxufTtcclxuXHJcbi8qKlxyXG4gKiBSZXBseSBpZiBhbiBlbGVtZW50IGlzIGluIHRoZSBwYWdlIHZpZXdwb3J0XHJcbiAqXHJcbiAqIEBwYXJhbSAge29iamVjdH0gZWwgRWxlbWVudCB0byBvYnNlcnZlXHJcbiAqIEBwYXJhbSAge251bWJlcn0gaCAgUGVyY2VudGFnZSBvZiBoZWlnaHRcclxuICogQHJldHVybiB7Ym9vbGVhbn1cclxuICovXHJcblZpdnVzLnByb3RvdHlwZS5pc0luVmlld3BvcnQgPSBmdW5jdGlvbiAoZWwsIGgpIHtcclxuICB2YXIgc2Nyb2xsZWQgICA9IHRoaXMuc2Nyb2xsWSgpLFxyXG4gICAgdmlld2VkICAgICAgID0gc2Nyb2xsZWQgKyB0aGlzLmdldFZpZXdwb3J0SCgpLFxyXG4gICAgZWxCQ1IgICAgICAgID0gZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksXHJcbiAgICBlbEhlaWdodCAgICAgPSBlbEJDUi5oZWlnaHQsXHJcbiAgICBlbFRvcCAgICAgICAgPSBzY3JvbGxlZCArIGVsQkNSLnRvcCxcclxuICAgIGVsQm90dG9tICAgICA9IGVsVG9wICsgZWxIZWlnaHQ7XHJcblxyXG4gIC8vIGlmIDAsIHRoZSBlbGVtZW50IGlzIGNvbnNpZGVyZWQgaW4gdGhlIHZpZXdwb3J0IGFzIHNvb24gYXMgaXQgZW50ZXJzLlxyXG4gIC8vIGlmIDEsIHRoZSBlbGVtZW50IGlzIGNvbnNpZGVyZWQgaW4gdGhlIHZpZXdwb3J0IG9ubHkgd2hlbiBpdCdzIGZ1bGx5IGluc2lkZVxyXG4gIC8vIHZhbHVlIGluIHBlcmNlbnRhZ2UgKDEgPj0gaCA+PSAwKVxyXG4gIGggPSBoIHx8IDA7XHJcblxyXG4gIHJldHVybiAoZWxUb3AgKyBlbEhlaWdodCAqIGgpIDw9IHZpZXdlZCAmJiAoZWxCb3R0b20pID49IHNjcm9sbGVkO1xyXG59O1xyXG5cclxuLyoqXHJcbiAqIEFsaWFzIGZvciBkb2N1bWVudCBlbGVtZW50XHJcbiAqXHJcbiAqIEB0eXBlIHtET01lbGVtZW50fVxyXG4gKi9cclxuVml2dXMucHJvdG90eXBlLmRvY0VsZW0gPSB3aW5kb3cuZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50O1xyXG5cclxuLyoqXHJcbiAqIEdldCB0aGUgdmlld3BvcnQgaGVpZ2h0IGluIHBpeGVsc1xyXG4gKlxyXG4gKiBAcmV0dXJuIHtpbnRlZ2VyfSBWaWV3cG9ydCBoZWlnaHRcclxuICovXHJcblZpdnVzLnByb3RvdHlwZS5nZXRWaWV3cG9ydEggPSBmdW5jdGlvbiAoKSB7XHJcbiAgdmFyIGNsaWVudCA9IHRoaXMuZG9jRWxlbS5jbGllbnRIZWlnaHQsXHJcbiAgICBpbm5lciA9IHdpbmRvdy5pbm5lckhlaWdodDtcclxuXHJcbiAgaWYgKGNsaWVudCA8IGlubmVyKSB7XHJcbiAgICByZXR1cm4gaW5uZXI7XHJcbiAgfVxyXG4gIGVsc2Uge1xyXG4gICAgcmV0dXJuIGNsaWVudDtcclxuICB9XHJcbn07XHJcblxyXG4vKipcclxuICogR2V0IHRoZSBwYWdlIFkgb2Zmc2V0XHJcbiAqXHJcbiAqIEByZXR1cm4ge2ludGVnZXJ9IFBhZ2UgWSBvZmZzZXRcclxuICovXHJcblZpdnVzLnByb3RvdHlwZS5zY3JvbGxZID0gZnVuY3Rpb24gKCkge1xyXG4gIHJldHVybiB3aW5kb3cucGFnZVlPZmZzZXQgfHwgdGhpcy5kb2NFbGVtLnNjcm9sbFRvcDtcclxufTtcclxuXHJcbi8qKlxyXG4gKiBBbGlhcyBmb3IgYHJlcXVlc3RBbmltYXRpb25GcmFtZWAgb3JcclxuICogYHNldFRpbWVvdXRgIGZ1bmN0aW9uIGZvciBkZXByZWNhdGVkIGJyb3dzZXJzLlxyXG4gKlxyXG4gKi9cclxucmVxdWVzdEFuaW1GcmFtZSA9IChmdW5jdGlvbiAoKSB7XHJcbiAgcmV0dXJuIChcclxuICAgIHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUgICAgICAgfHxcclxuICAgIHdpbmRvdy53ZWJraXRSZXF1ZXN0QW5pbWF0aW9uRnJhbWUgfHxcclxuICAgIHdpbmRvdy5tb3pSZXF1ZXN0QW5pbWF0aW9uRnJhbWUgICAgfHxcclxuICAgIHdpbmRvdy5vUmVxdWVzdEFuaW1hdGlvbkZyYW1lICAgICAgfHxcclxuICAgIHdpbmRvdy5tc1JlcXVlc3RBbmltYXRpb25GcmFtZSAgICAgfHxcclxuICAgIGZ1bmN0aW9uKC8qIGZ1bmN0aW9uICovIGNhbGxiYWNrKXtcclxuICAgICAgcmV0dXJuIHdpbmRvdy5zZXRUaW1lb3V0KGNhbGxiYWNrLCAxMDAwIC8gNjApO1xyXG4gICAgfVxyXG4gICk7XHJcbn0pKCk7XHJcblxyXG4vKipcclxuICogQWxpYXMgZm9yIGBjYW5jZWxBbmltYXRpb25GcmFtZWAgb3JcclxuICogYGNhbmNlbFRpbWVvdXRgIGZ1bmN0aW9uIGZvciBkZXByZWNhdGVkIGJyb3dzZXJzLlxyXG4gKlxyXG4gKi9cclxuY2FuY2VsQW5pbUZyYW1lID0gKGZ1bmN0aW9uICgpIHtcclxuICByZXR1cm4gKFxyXG4gICAgd2luZG93LmNhbmNlbEFuaW1hdGlvbkZyYW1lICAgICAgIHx8XHJcbiAgICB3aW5kb3cud2Via2l0Q2FuY2VsQW5pbWF0aW9uRnJhbWUgfHxcclxuICAgIHdpbmRvdy5tb3pDYW5jZWxBbmltYXRpb25GcmFtZSAgICB8fFxyXG4gICAgd2luZG93Lm9DYW5jZWxBbmltYXRpb25GcmFtZSAgICAgIHx8XHJcbiAgICB3aW5kb3cubXNDYW5jZWxBbmltYXRpb25GcmFtZSAgICAgfHxcclxuICAgIGZ1bmN0aW9uKGlkKXtcclxuICAgICAgcmV0dXJuIHdpbmRvdy5jbGVhclRpbWVvdXQoaWQpO1xyXG4gICAgfVxyXG4gICk7XHJcbn0pKCk7XHJcblxyXG4vKipcclxuICogUGFyc2Ugc3RyaW5nIHRvIGludGVnZXIuXHJcbiAqIElmIHRoZSBudW1iZXIgaXMgbm90IHBvc2l0aXZlIG9yIG51bGxcclxuICogdGhlIG1ldGhvZCB3aWxsIHJldHVybiB0aGUgZGVmYXVsdCB2YWx1ZVxyXG4gKiBvciAwIGlmIHVuZGVmaW5lZFxyXG4gKlxyXG4gKiBAcGFyYW0ge3N0cmluZ30gdmFsdWUgU3RyaW5nIHRvIHBhcnNlXHJcbiAqIEBwYXJhbSB7Kn0gZGVmYXVsdFZhbHVlIFZhbHVlIHRvIHJldHVybiBpZiB0aGUgcmVzdWx0IHBhcnNlZCBpcyBpbnZhbGlkXHJcbiAqIEByZXR1cm4ge251bWJlcn1cclxuICpcclxuICovXHJcbnBhcnNlUG9zaXRpdmVJbnQgPSBmdW5jdGlvbiAodmFsdWUsIGRlZmF1bHRWYWx1ZSkge1xyXG4gIHZhciBvdXRwdXQgPSBwYXJzZUludCh2YWx1ZSwgMTApO1xyXG4gIHJldHVybiAob3V0cHV0ID49IDApID8gb3V0cHV0IDogZGVmYXVsdFZhbHVlO1xyXG59O1xyXG5cclxuXHJcbiAgaWYgKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xyXG4gICAgLy8gQU1ELiBSZWdpc3RlciBhcyBhbiBhbm9ueW1vdXMgbW9kdWxlLlxyXG4gICAgZGVmaW5lKFtdLCBmdW5jdGlvbigpIHtcclxuICAgICAgcmV0dXJuIFZpdnVzO1xyXG4gICAgfSk7XHJcbiAgfSBlbHNlIGlmICh0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpIHtcclxuICAgIC8vIE5vZGUuIERvZXMgbm90IHdvcmsgd2l0aCBzdHJpY3QgQ29tbW9uSlMsIGJ1dFxyXG4gICAgLy8gb25seSBDb21tb25KUy1saWtlIGVudmlyb25tZW50cyB0aGF0IHN1cHBvcnQgbW9kdWxlLmV4cG9ydHMsXHJcbiAgICAvLyBsaWtlIE5vZGUuXHJcbiAgICBtb2R1bGUuZXhwb3J0cyA9IFZpdnVzO1xyXG4gIH0gZWxzZSB7XHJcbiAgICAvLyBCcm93c2VyIGdsb2JhbHNcclxuICAgIHdpbmRvdy5WaXZ1cyA9IFZpdnVzO1xyXG4gIH1cclxuXHJcbn0od2luZG93LCBkb2N1bWVudCkpO1xyXG5cclxuKGZ1bmN0aW9uKCAkICkge1xyXG5cdHZhclxyXG5cdFZhbGlkU3RhdGU9ZmFsc2UsXHJcblx0UmVxdWlyZWRTdGF0ZT1mYWxzZSxcclxuXHRWaXNpYmxlU3RhdGU9ZmFsc2UsXHJcblx0Rk9STSwvL9CT0LvQvtCx0LDQu9GM0L3Ri9C5ICjQtNC70Y8g0L/Qu9Cw0LPQuNC90LApINC+0LHRitC10LrRgiDRhNC+0YDQvNGLLCDQvdCwINC60L7RgtC+0YDRg9GOINC+0L0g0L3QsNGC0YDQsNCy0LvQtdC9XHJcblx0QUNDRVBUQUJMRTtcclxuXHJcblxyXG5cdGpRdWVyeS5mbi5sZW1vbmdyYWIgPSBmdW5jdGlvbihvcHRpb25zLFJVTEVTKSB7XHJcblx0XHRBQ0NFUFRBQkxFPSQuZXh0ZW5kKHtcdFwic3Vic2VsZWN0b3JcIjpcIipcIixcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImNsYXNzVmFsaWRcIjpcIkFDQ0VQVEFCTEVcIixcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImNsYXNzSW52YWxpZFwiOlwiVU5BQ0NFUFRBQkxFXCIsXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJjbGFzc1JlcXVpcmVkXCI6XCJSRVFVSVJFRFwiLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiY2xhc3NOb3JlcXVpcmVkXCI6XCJOT1JFUVVJUkVEXCIsXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJjbGFzc0VuYWJsZWRcIjpcIkVOQUJMRURcIixcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImNsYXNzRGlzYWJsZWRcIjpcIkRJU0FCTEVEXCIsXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJjbGFzc1Zpc2libGVcIjpcIlZJU0lCTEVcIixcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImNsYXNzSGlkZGVuXCI6XCJISURERU5cIixcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImNsYXNzQ2hhbmdlZFwiOlwiQ0hBTkdFRFwiLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiY2xhc3NVbmNoYW5nZWRcIjpcIlVOQ0hBTkdFRFwiLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiYWN0aW9uXCI6XCJpbnB1dFwiLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiYWN0aW9uc1wiOnt9LFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwib25BY3Rpb25cIjpmYWxzZSxcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImFsbG93SW52YWxpZFN1Ym1pdFwiOnRydWUsXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJ1c2VSZXF1aXJlZEF0dHJcIjp0cnVlLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwibmF0aXZlRW5hYmxlZFwiOnRydWUsXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJuYXRpdmVWaXNpYmxlXCI6dHJ1ZSxcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImF1dG9ncmFiXCI6dHJ1ZSxcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImlnbm9yZV9hdXRvZ3JhYl9jaGFuZ2VcIjp0cnVlLC8v0JjQs9C90L7RgNC40YDQvtCy0LDRgtGMINGB0LzQtdC90YMg0YHQvtGB0YLQvtGP0L3QuNC5IENoYW5nZWQvVW5jaGFuZ2VkINC/0YDQuCBhdXRvZ3JhYj10cnVlXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJpZ25vcmVfYXV0b2dyYWJfYWN0aW9uXCI6ZmFsc2UvL9CY0LPQvdC+0YDQuNGA0L7QstCw0YLRjCDQstGL0LfQvtCyINGB0L7QsdGL0YLQuNGPIG9uQWN0aW9uINC/0YDQuCBhdXRvZ3JhYj10cnVlXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdH0sb3B0aW9ucyk7XHJcblx0XHRGT1JNPXRoaXM7XHJcblx0XHRcclxuXHRcdHZhciBydWxlLGNvbmRpdGlvbnMsc2VsZWN0b3JfY291bnQsY29uZGl0aW9uc19jb3VudCxydWxlX29wdGlvbnM9e30sbGVtb25zLGxlbW9uLHJ1bGVzO1xyXG5cdFx0XHJcblx0XHQvL9C10YHQu9C4INCyIFJVTEVTINGH0YLQvi3RgtC+INC30LDQtNCw0L3QviAtINGB0YfQuNGC0LDQtdC8LCDRh9GC0L4g0YLQsNC8IEpTT04sINC/0LDRgNGB0LjQvCDQtdCz0L4uXHJcblx0XHRpZiAodHlwZW9mKFJVTEVTKSE9PSd1bmRlZmluZWQnICYmIFJVTEVTIT09JycpIHtcclxuXHRcdFx0dmFyIGZvcl9zZWxlY3RvcjtcclxuXHRcdFx0Zm9yIChzZWxlY3Rvcl9jb3VudD0wO3NlbGVjdG9yX2NvdW50PFJVTEVTLmxlbmd0aDtzZWxlY3Rvcl9jb3VudCsrKXsvL9Cg0LDQt9Cx0LjQstCw0LXQvCDQv9C10YDQtdC00LDQvdC90YvQuSDQv9Cw0YDQsNC80LXRgtGAINC90LAg0YHQtdC70LXQutGC0L7RgNGLXHJcblx0XHRcdFx0Zm9yX3NlbGVjdG9yPVJVTEVTW3NlbGVjdG9yX2NvdW50XS5zZWxlY3RvcjtcclxuXHRcdFx0XHQvL9Cf0LXRgNC10LHQuNGA0LDQtdC8INCy0YHQtSDRg9GB0LvQvtCy0LjRjyDQstCw0LvQuNC00LDRhtC40LhcclxuXHRcdFx0XHRmb3IgKGVfcnVsZSBpbiBSVUxFU1tzZWxlY3Rvcl9jb3VudF0ucnVsZSkge1xyXG5cdFx0XHRcdFx0cnVsZT1lX3J1bGUudG9VcHBlckNhc2UoKTtcclxuXHRcdFx0XHRcdGNvbmRpdGlvbnM9UlVMRVNbc2VsZWN0b3JfY291bnRdLnJ1bGVbZV9ydWxlXTtcclxuXHRcdFx0XHRcdC8v0JIgY29uZGl0aW9ucy3QvdCw0LHQvtGAINGD0YHQu9C+0LLQuNC5INC/0YDQsNCy0LjQu9CwLiDQkiBydWxlLdGB0LDQvNC+INC/0YDQsNCy0LjQu9C+LlxyXG5cdFx0XHRcdFx0cnVsZV9vcHRpb25zPXt9O1xyXG5cdFx0XHRcdFx0Zm9yIChjb25kaXRpb25zX2NvdW50PTA7Y29uZGl0aW9uc19jb3VudDxjb25kaXRpb25zLmxlbmd0aDtjb25kaXRpb25zX2NvdW50Kyspey8v0J/RgNC+0LnQtNGR0LzRgdGPINC/0L4g0YPRgdC70L7QstC40Y/QvCwg0LTQvtC/0L7Qu9C90LjQsiDRg9C80L7Qu9GH0LDQvdC40Y8uINCc0L7QttC90L4g0LjRgdC/0L7Qu9GM0LfQvtCy0LDRgtGMINGB0YPRidC10YHRgtCy0YPRjtGJ0LjQuSDQvtCx0YrQtdC60YJcclxuXHRcdFx0XHRcdFx0cnVsZV9vcHRpb25zW2NvbmRpdGlvbnNfY291bnRdPWNvbXBsZXRlX2NvbmRpdGlvbihjb25kaXRpb25zW2NvbmRpdGlvbnNfY291bnRdKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdHJ1bGVfb3B0aW9ucy5hY3Rpb25zPVJVTEVTW3NlbGVjdG9yX2NvdW50XS5hY3Rpb25zO1xyXG5cdFx0XHRcdFx0YXBwbHlfcnVsZSgkKGZvcl9zZWxlY3RvcikscnVsZSxydWxlX29wdGlvbnMpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRsZW1vbnM9Rk9STS5maW5kKEFDQ0VQVEFCTEUuc3Vic2VsZWN0b3IpO1xyXG5cdFx0bGVtb25zLmVhY2goZnVuY3Rpb24oKXtcclxuXHRcdFx0bGVtb249JCh0aGlzKTtcclxuXHRcdFx0cnVsZXM9bGVtb24uZGF0YSgpOy8v0J/QvtC70YPRh9C40LvQuCDRgdC/0LjRgdC+0LogZGF0YS3QsNGC0YDQuNCx0YPRgtC+0LJcclxuXHRcdFx0Zm9yICh2YXIgcnVsZV8gaW4gcnVsZXMpIHtcclxuXHRcdFx0XHRpZiAoWydydWxlVmFsaWQnLCdydWxlUmVxdWlyZWQnLCdydWxlRW5hYmxlZCcsJ3J1bGVWaXNpYmxlJywncnVsZUxlbW9uZ3JhYiddLmluZGV4T2YocnVsZV8pPT09LTEpIGNvbnRpbnVlO1xyXG5cdFx0XHRcdGNvbmRpdGlvbnM9cnVsZXNbcnVsZV9dO1xyXG5cdFx0XHRcdHN3aXRjaCAocnVsZV8pe1xyXG5cdFx0XHRcdFx0Y2FzZSAncnVsZVZhbGlkJzpcclxuXHRcdFx0XHRcdFx0cnVsZT1cIlZBTElEXCI7XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdGNhc2UgJ3J1bGVSZXF1aXJlZCc6XHJcblx0XHRcdFx0XHRcdHJ1bGU9XCJSRVFVSVJFRFwiO1xyXG5cdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHRjYXNlICdydWxlRW5hYmxlZCc6XHJcblx0XHRcdFx0XHRcdHJ1bGU9XCJFTkFCTEVEXCI7XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdGNhc2UgJ3J1bGVWaXNpYmxlJzpcclxuXHRcdFx0XHRcdFx0cnVsZT1cIlZJU0lCTEVcIjtcclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0Y2FzZSAncnVsZUxlbW9uZ3JhYic6IC8v0J7RgtC00LXQu9GM0L3Ri9C5INGB0LvRg9GH0LDQuTog0L7QtNC90L4g0LHQvtC70YzRiNC+0LUg0L/RgNCw0LLQuNC70L5cclxuXHRcdFx0XHRcdFx0dmFyIHBhcmFtZXRlcnM9Y29uZGl0aW9ucyxcclxuXHRcdFx0XHRcdFx0XHRcdHBhcmFtZXRlcjtcclxuXHRcdFx0XHRcdFx0Zm9yIChwYXJhbWV0ZXIgaW4gcGFyYW1ldGVycykge1xyXG5cdFx0XHRcdFx0XHRcdGNvbmRpdGlvbnM9cGFyYW1ldGVyc1twYXJhbWV0ZXJdO1xyXG5cdFx0XHRcdFx0XHRcdHN3aXRjaCAocGFyYW1ldGVyKXtcclxuXHRcdFx0XHRcdFx0XHRcdGNhc2UgJ3ZhbGlkJzpcclxuXHRcdFx0XHRcdFx0XHRcdFx0cnVsZT1cIlZBTElEXCI7XHJcblx0XHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdFx0XHRcdGNhc2UgJ3JlcXVpcmVkJzpcclxuXHRcdFx0XHRcdFx0XHRcdFx0cnVsZT1cIlJFUVVJUkVEXCI7XHJcblx0XHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdFx0XHRcdGNhc2UgJ2VuYWJsZWQnOlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRydWxlPVwiRU5BQkxFRFwiO1xyXG5cdFx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHRcdFx0XHRjYXNlICd2aXNpYmxlJzpcclxuXHRcdFx0XHRcdFx0XHRcdFx0cnVsZT1cIlZJU0lCTEVcIjtcclxuXHRcdFx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHQvL9CSIGNvbmRpdGlvbnMt0L3QsNCx0L7RgCDRg9GB0LvQvtCy0LjQuSDQv9GA0LDQstC40LvQsC4g0JIgcnVsZS3RgdCw0LzQviDQv9GA0LDQstC40LvQvi5cclxuXHRcdFx0XHRcdFx0XHRydWxlX29wdGlvbnM9e307XHJcblx0XHRcdFx0XHRcdFx0Zm9yIChjb25kaXRpb25zX2NvdW50PTA7Y29uZGl0aW9uc19jb3VudDxjb25kaXRpb25zLmxlbmd0aDtjb25kaXRpb25zX2NvdW50Kyspey8v0J/RgNC+0LnQtNGR0LzRgdGPINC/0L4g0YPRgdC70L7QstC40Y/QvCwg0LTQvtC/0L7Qu9C90LjQsiDRg9C80L7Qu9GH0LDQvdC40Y8uINCc0L7QttC90L4g0LjRgdC/0L7Qu9GM0LfQvtCy0LDRgtGMINGB0YPRidC10YHRgtCy0YPRjtGJ0LjQuSDQvtCx0YrQtdC60YJcclxuXHRcdFx0XHRcdFx0XHRcdHJ1bGVfb3B0aW9uc1tjb25kaXRpb25zX2NvdW50XT1jb21wbGV0ZV9jb25kaXRpb24oY29uZGl0aW9uc1tjb25kaXRpb25zX2NvdW50XSk7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdGFwcGx5X3J1bGUobGVtb24scnVsZSxydWxlX29wdGlvbnMpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0Ly/QkiBjb25kaXRpb25zLdC90LDQsdC+0YAg0YPRgdC70L7QstC40Lkg0L/RgNCw0LLQuNC70LAuINCSIHJ1bGUt0YHQsNC80L4g0L/RgNCw0LLQuNC70L4uXHJcblx0XHRcdFx0cnVsZV9vcHRpb25zPXt9O1xyXG5cdFx0XHRcdGZvciAoY29uZGl0aW9uc19jb3VudD0wO2NvbmRpdGlvbnNfY291bnQ8Y29uZGl0aW9ucy5sZW5ndGg7Y29uZGl0aW9uc19jb3VudCsrKXsvL9Cf0YDQvtC50LTRkdC80YHRjyDQv9C+INGD0YHQu9C+0LLQuNGP0LwsINC00L7Qv9C+0LvQvdC40LIg0YPQvNC+0LvRh9Cw0L3QuNGPLiDQnNC+0LbQvdC+INC40YHQv9C+0LvRjNC30L7QstCw0YLRjCDRgdGD0YnQtdGB0YLQstGD0Y7RidC40Lkg0L7QsdGK0LXQutGCXHJcblx0XHRcdFx0XHRydWxlX29wdGlvbnNbY29uZGl0aW9uc19jb3VudF09Y29tcGxldGVfY29uZGl0aW9uKGNvbmRpdGlvbnNbY29uZGl0aW9uc19jb3VudF0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHQvL3J1bGVfb3B0aW9ucy5hY3Rpb25zPVJVTEVTW3NlbGVjdG9yX2NvdW50XS5hY3Rpb25zO1xyXG5cdFx0XHRcdGFwcGx5X3J1bGUobGVtb24scnVsZSxydWxlX29wdGlvbnMpO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHRcdFZhbGlkU3RhdGU9Rk9STS5pc1ZhbGlkKCk7XHJcblx0XHRSZXF1aXJlZFN0YXRlPSFGT1JNLmlzTm90UmVxdWlyZWQoKTtcclxuXHRcdFZpc2libGVTdGF0ZT1GT1JNLmlzKCc6dmlzaWJsZScpO1xyXG5cdFx0Ly/Qo9GB0YLQsNC90LDQstC70LjQstCw0LXQvCDQvtCx0YDQsNCx0L7RgtGH0LjQuiBvbnN1Ym1pdFxyXG5cdFx0Rk9STS5vbignc3VibWl0JyxmdW5jdGlvbigpe1xyXG5cdFx0XHRpZiAoIUZPUk0uaXNOb3RSZXF1aXJlZCgpKSByZXR1cm4gKGZhbHNlKTtcclxuXHRcdFx0aWYgKEZPUk0uaXNWYWxpZCgpKSByZXR1cm4gKHRydWUpO1xyXG5cdFx0XHRyZXR1cm4gKEFDQ0VQVEFCTEUuYWxsb3dJbnZhbGlkU3VibWl0PT09dHJ1ZSk7XHJcblx0XHR9KTtcclxuXHR9O1xyXG5cclxuXHRqUXVlcnkuZm4uaXNWYWxpZCA9IGZ1bmN0aW9uKCl7XHJcblx0XHRyZXR1cm4gKHRoaXMuZmluZChcIi5cIitBQ0NFUFRBQkxFLmNsYXNzSW52YWxpZCkubGVuZ3RoPT09MCk7XHJcblx0fTtcclxuXHRcclxuXHRqUXVlcnkuZm4uaXNOb3RSZXF1aXJlZCA9IGZ1bmN0aW9uKCl7XHJcblx0XHRyZXR1cm4gKHRoaXMuZmluZChcIi5cIitBQ0NFUFRBQkxFLmNsYXNzUmVxdWlyZWQpLmxlbmd0aD09PTApO1xyXG5cdH07XHJcblx0XHJcblx0ZnVuY3Rpb24gY29tcGxldGVfY29uZGl0aW9uKGNvbmRpdGlvbil7Ly/Qn9GA0L7QstC10YDRj9C10YIg0YPRgdC70L7QstC40LUg0L3QsCDQstGB0LUg0L/QsNGA0LDQvNC10YLRgNGLLCDQtNC+0L/QvtC70L3Rj9C10YIg0YPQvNC+0LvRh9Cw0YLQtdC70YzQvdGL0LzQuC4gVE9ETzog0YDQsNC30YDQtdGI0LjRgtGMINC40YHQv9C+0LvRjNC30L7QstCw0YLRjCDRgdC+0LrRgNCw0YnRkdC90L3Rg9GOINC30LDQv9C40YHRjCAoJiDQstC80LXRgdGC0L4gYW5kINC4INGC0LQpLlxyXG5cdFx0Y29uZGl0aW9uLmtleT0oY29uZGl0aW9uLmtleXx8J25hdGl2ZScpOy8v0J/QvtC70YPRh9Cw0LXQvCDQutC70Y7RhyDQv9GA0LDQstC40LvQsCwg0LXRgdC70Lgg0L3QtSDQt9Cw0LTQsNC9IC0g0YHRh9C40YLQsNC10LwsINGH0YLQviDRgtCw0Lwg0L3QsNGC0LjQstC90YvQuSBKUy5cclxuXHRcdFxyXG5cdFx0Y29uZGl0aW9uLmludmVydD0oY29uZGl0aW9uLmludmVydHx8ZmFsc2UpOy8v0JjQvdCy0LXRgNGC0LjRgNC+0LLQsNGC0Ywg0LvQuCDQv9C+0LvRg9GH0LXQvdC90L7QtSDQt9C90LDRh9C10L3QuNC1LlxyXG5cdFx0aWYgKGNvbmRpdGlvbi5rZXlbMF09PT0nIScpIHsvL9CY0L3QstC10YDRgtC40YDQvtCy0LDQvdC40LUg0LzQvtC20LXRgiDQsdGL0YLRjCDQt9Cw0LTQsNC90L4g0LIg0LrQu9GO0YfQtS4g0JIg0YHQu9GD0YfQsNC1LCDQtdGB0LvQuCDQvtC90L4g0LfQsNC00LDQvdC+INC4INCyINC60LvRjtGH0LUsINC4INCyINC/0LDRgNCw0LzQtdGC0YDQtSwg0LrQu9GO0Ycg0LjQvNC10LXRgiDQv9GA0LjQvtGA0LjRgtC10YJcclxuXHRcdFx0Y29uZGl0aW9uLmludmVydD10cnVlO1xyXG5cdFx0XHRjb25kaXRpb24ua2V5PWNvbmRpdGlvbi5rZXkuc3Vic3RyKDEpO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRzd2l0Y2ggKGNvbmRpdGlvbi5rZXkudG9Mb3dlckNhc2UoKSl7Ly/Ql9Cw0LTQsNGR0Lwg0LDQu9C40LDRgdGLINC60LvRjtGH0LXQuVxyXG5cdFx0XHRjYXNlICdyJzpcclxuXHRcdFx0Y2FzZSAncmVnZXhwJzpcclxuXHRcdFx0XHRjb25kaXRpb24ua2V5PVwicmVnZXhwXCI7XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlICdjJzpcclxuXHRcdFx0Y2FzZSAnY2hlY2tlZCc6XHJcblx0XHRcdGNhc2UgJ2NoZWNrYm94JzpcclxuXHRcdFx0Y2FzZSAncmFkaW8nOlxyXG5cdFx0XHRcdGNvbmRpdGlvbi5rZXk9XCJjaGVja2VkXCI7XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlICdzZWxlY3QnOlxyXG5cdFx0XHRjYXNlICdzJzpcclxuXHRcdFx0XHRjb25kaXRpb24ua2V5PVwic2VsZWN0XCI7XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlICdzZXQnOlxyXG5cdFx0XHRcdGNvbmRpdGlvbi5rZXk9XCJzZXRcIjtcclxuXHRcdFx0YnJlYWs7XHJcblx0XHRcdGNhc2UgJ2onOlxyXG5cdFx0XHRjYXNlICduJzpcclxuXHRcdFx0Y2FzZSAnZic6XHJcblx0XHRcdGNhc2UgJ2phdmFzY3JpcHQnOlxyXG5cdFx0XHRjYXNlICduYXRpdmUnOlxyXG5cdFx0XHRjYXNlICdmdW5jdGlvbic6XHJcblx0XHRcdFx0Y29uZGl0aW9uLmtleT1cIm5hdGl2ZVwiO1xyXG5cdFx0XHRicmVhaztcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0aWYgKGNvbmRpdGlvbi5rZXk9PT1cInNlbGVjdFwiKXsvL9Cd0YPQttC90L4g0L/RgNC+0LLQtdGA0LjRgtGMLCDRh9GC0L4g0LIgdmFsdWVcclxuXHRcdFx0dmFyIGksdG1wX3ZhbHVlPVwiW1wiO1xyXG5cdFx0XHRpZiAodHlwZW9mKGNvbmRpdGlvbi52YWx1ZSk9PT0nb2JqZWN0Jyl7Ly/QntGC0LvQuNGH0L3Qviwg0Y3RgtC+INC80LDRgdGB0LjQslxyXG5cdFx0XHRcdGZvciAoaT0wO2k8Y29uZGl0aW9uLnZhbHVlLmxlbmd0aDtpKyspe1xyXG5cdFx0XHRcdFx0dG1wX3ZhbHVlKz1cIlxcXCJcIitjb25kaXRpb24udmFsdWVbaV0rXCJcXFwiLFwiO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSBlbHNlIGlmICh0eXBlb2YoY29uZGl0aW9uLnZhbHVlKT09PSdzdHJpbmcnKXsvL9Ct0YLQviDQvdC1INC80LDRgdGB0LjQsiwg0L3QviDQvNGLINC00L7QsdGA0Ysg0Log0L/QvtC70YzQt9C+0LLQsNGC0LXQu9GOXHJcblx0XHRcdFx0Y29uZGl0aW9uLnZhbHVlPWNvbmRpdGlvbi52YWx1ZS5zcGxpdCgnLCcpO1xyXG5cdFx0XHRcdGZvciAoaT0wO2k8Y29uZGl0aW9uLnZhbHVlLmxlbmd0aDtpKyspe1xyXG5cdFx0XHRcdFx0dG1wX3ZhbHVlKz1cIlxcXCJcIitjb25kaXRpb24udmFsdWVbaV0rXCJcXFwiLFwiO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHR0bXBfdmFsdWU9XCJbIFwiO1xyXG5cdFx0XHR9XHJcblx0XHRcdGNvbmRpdGlvbi52YWx1ZT10bXBfdmFsdWUuc3Vic3RyKDAsdG1wX3ZhbHVlLmxlbmd0aC0xKStcIl1cIjtcclxuXHRcdH0gZWxzZSBpZiAoY29uZGl0aW9uLmtleT09PVwic2V0XCIpIHtcclxuXHRcdFx0Y29uZGl0aW9uLnZhbHVlPShjb25kaXRpb24udmFsdWV8fFtcIlwiXSk7XHJcblx0XHR9XHJcblxyXG5cdFx0Y29uZGl0aW9uLnNlbGVjdG9yPShjb25kaXRpb24uc2VsZWN0b3J8fCcnKTsvL9Ch0LXQu9C10LrRgtC+0YAg0YHQstGP0LfRg9C10LzQvtCz0L4g0L/QvtC70Y8sINC10YHQu9C4INC90LUg0LfQsNC00LDQvSAtINC/0YDQuNCy0Y/Qt9GL0LLQsNC10LzRgdGPINC6INC/0L7Qu9GOINC/0YDQsNCy0LjQu9CwLlxyXG5cdFx0Y29uZGl0aW9uLnN0cmljdD0oY29uZGl0aW9uLnN0cmljdHx8ZmFsc2UpOy8v0KHRgtGA0L7Qs9C+0YHRgtGMINC/0YDQvtCy0LXRgNC60Lgg0LTQu9GPIHNlbGVjdC9zZXRcclxuXHRcdGNvbmRpdGlvbi5sb2dpYz0oY29uZGl0aW9uLmxvZ2ljfHwnJiYnKTsvL9Cb0L7Qs9C40LrQsCDRgdC+0LLQvNC10YnQtdC90LjRjyDQv9GA0LDQstC40LtcclxuXHRcdGNvbmRpdGlvbi5taW49KGNvbmRpdGlvbi5taW58fGZhbHNlKTsvKtC80LjQvdC40LzQsNC70YzQvdC+0LUqL1xyXG5cdFx0Y29uZGl0aW9uLm1heD0oY29uZGl0aW9uLm1heHx8ZmFsc2UpOy8q0Lgg0LzQsNC60YHQuNC80LDQu9GM0L3QvtC1INC60L7Qu9C40YfQtdGB0YLQstC+INCy0YvQsdGA0LDQvdC90YvRhSDQt9C90LDRh9C10L3QuNC5Ki9cclxuXHRcdFxyXG5cdFx0c3dpdGNoIChjb25kaXRpb24ubG9naWMudG9Mb3dlckNhc2UoKSkge1xyXG5cdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRjYXNlIFwiJlwiOlxyXG5cdFx0XHRjYXNlIFwiJiZcIjpcclxuXHRcdFx0Y2FzZSBcImFuZFwiOlxyXG5cdFx0XHRcdGNvbmRpdGlvbi5sb2dpYz1cIiYmXCI7XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlIFwifFwiOlxyXG5cdFx0XHRjYXNlIFwifHxcIjpcclxuXHRcdFx0Y2FzZSBcIm9yXCI6XHJcblx0XHRcdFx0Y29uZGl0aW9uLmxvZ2ljPVwifHxcIjtcclxuXHRcdFx0YnJlYWs7XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gKGNvbmRpdGlvbik7XHJcblx0fVxyXG5cclxuXHQvKtCS0L7Qt9Cy0YDQsNGJ0LDQtdGCINCyINCy0LjQtNC1INC80LDRgdGB0LjQstCwINC90LDQsdC+0YAg0LfQvdCw0YfQtdC90LjQuSDRg9C60LDQt9Cw0L3QvdC+0LPQviA8c2VsZWN0PlxyXG5cdFx00JLRhdC+0LTQvdC+0LUg0LfQvdCw0YfQtdC90LjQtSAtIGpRdWVyeS3QvtCx0YrQtdC60YIg0YEg0YHQtdC70LXQutGC0L7QvFxyXG5cdCovXHJcblx0ZnVuY3Rpb24gZ2V0X3NlbGVjdGVkX29wdGlvbnMoc2VsZWN0KXtcclxuXHRcdHZhclx0b3B0aW9ucz1zZWxlY3QuZmluZChcIm9wdGlvbjpzZWxlY3RlZFwiKSxcclxuXHRcdFx0XHRvcHRpb25fY291bnQsXHJcblx0XHRcdFx0cmV0PVtdO1xyXG5cdFx0Zm9yIChvcHRpb25fY291bnQ9MDtvcHRpb25fY291bnQ8b3B0aW9ucy5sZW5ndGg7b3B0aW9uX2NvdW50Kyspe1xyXG5cdFx0XHRyZXQucHVzaCgkKG9wdGlvbnNbb3B0aW9uX2NvdW50XSkudmFsKCkpO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIChyZXQpO1xyXG5cdH1cclxuXHRcclxuXHQvKlxyXG5cdFx00L/RgNC+0LLQtdGA0Y/QtdGCLCDRh9C10LrQvdGD0YLRiyDQu9C4INCyIGJsb2NrINC/0L7Qu9GPLCDQvtGC0LzQtdGH0LXQvdC90YvQtSDQsiBzZWxlY3RvcnMuXHJcblx0XHTQtdGB0LvQuCBzdHJpY3Q9PXRydWUsINGB0YfQuNGC0LDQtdGC0YHRjyDQvdC+0YDQvCwg0LXRgdC70Lgg0YfQtdC60L3Rg9GC0L4g0YXQvtGC0Y8g0LHRiyDQvtC00L3QviDQv9C+0LvQtSwg0L/QvtC/0LDQtNCw0Y7RidC10LUg0L/QvtC0INC60LDQttC00YvQuSDRgdC10LvQtdC60YLQvtGAXHJcblx0XHTQuNC90LDRh9C1INC00L7Qu9C20L3RiyDQsdGL0YLRjCDRh9C10LrQvdGD0YLRiyDQstGB0LUg0L/QvtC70Y8sINC/0L7Qv9Cw0LTQsNGO0YnQuNC1INCyINGB0LXQu9C10LrRgtC+0YBcclxuXHQqL1xyXG5cdGZ1bmN0aW9uIGlzX2ZpZWxkc19jaGVja2VkKGJsb2NrLHNlbGVjdG9ycyxzdHJpY3Qpe1xyXG5cdFx0aWYgKHR5cGVvZihzdHJpY3QpPT09J3VuZGVmaW5lZCcpIHN0cmljdD1mYWxzZTtcclxuXHRcdHZhciBpbmRleCxzZWxlY3RvcixjbD0wO1xyXG5cdFx0Zm9yIChpbmRleD0wO2luZGV4PHNlbGVjdG9ycy5sZW5ndGg7aW5kZXgrKyl7XHJcblx0XHRcdHNlbGVjdG9yPXNlbGVjdG9yc1tpbmRleF07XHJcblx0XHRcdGNsKz1ibG9jay5maW5kKHNlbGVjdG9yKyc6Y2hlY2tlZCcpLmxlbmd0aDsvL9Ca0L7Qu9C40YfQtdGB0YLQstC+INGH0LXQutC90YPRgtGL0YUg0LTQu9GPINC/0YDQvtGI0LvQvtCz0L4g0YHQtdC70LXQutGC0L7RgNCwINC90LUg0YHQsdGA0LDRgdGL0LLQsNC10LxcclxuXHRcdFx0aWYgKHN0cmljdCkgey8vVE9ETzog0YPRgdC70L7QstC40LUg0LzQvtC20L3QviDQuCDRgdC+0LrRgNCw0YLQuNGC0Ywg0LTQu9GPINC60L7QvNC/0LDQutGC0L3QvtGB0YLQuFxyXG5cdFx0XHRcdGlmIChjbCE9PWJsb2NrLmZpbmQoc2VsZWN0b3IpLmxlbmd0aCkgcmV0dXJuIChmYWxzZSk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0aWYgKGNsPmJsb2NrLmZpbmQoc2VsZWN0b3IpLmxlbmd0aCB8fCBjbD09PTApIHJldHVybiAoZmFsc2UpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gKHRydWUpO1xyXG5cdH1cclxuXHRcclxuXHQvKlxyXG5cdCAqINCf0L4g0LfQsNC00LDQvdC90L7QvNGDINC90LDQsdC+0YDRgyDQv9Cw0YDQsNC80LXRgtGA0L7QsiDQs9C10L3QtdGA0LjRgNGD0LXRgiBKUy3QutC+0LQg0YPRgdC70L7QstC40Y9cclxuXHQqL1xyXG5cdGZ1bmN0aW9uIGdldF9qc19jb25kaXRpb24gKGNvbmRpdGlvbixmaWVsZCl7XHJcblx0XHR2YXIgcmV0PXtcclxuXHRcdFx0aGFuZGxlcjpbXSxcclxuXHRcdFx0Y29uZGl0aW9uOlwiXCJcclxuXHRcdH0sX2NvbmRpdGlvbixzdW1tYXJ5X3NlbGVjdG9yLGluZGV4O1xyXG5cdFx0XHJcblx0XHRzd2l0Y2ggKGNvbmRpdGlvbi5rZXkudG9Mb3dlckNhc2UoKSl7XHJcblx0XHRcdGNhc2UgJ3JlZ2V4cCc6XHJcblx0XHRcdFx0aWYgKGNvbmRpdGlvbi5zZWxlY3RvciE9PScnKSB7Ly/Ql9Cw0LTQsNC9INGB0LXQu9C10LrRgtC+0YAg0LLQvdC10YjQvdC10LPQviDQv9C+0LvRjywg0LTQvtC/0L7Qu9C90LjRgtC10LvRjNC90L4g0LLQtdGI0LDQtdC8INC+0LHRgNCw0LHQvtGC0YfQuNC6INC90LAg0L3QtdCz0L5cclxuXHRcdFx0XHRcdHJldC5jb25kaXRpb24gPSBcIlJlZ0V4cCgvXCIrY29uZGl0aW9uLnZhbHVlK1wiLykudGVzdCgkKCdcIitjb25kaXRpb24uc2VsZWN0b3IrXCInKS52YWwoKSlcIjtcclxuXHRcdFx0XHRcdHJldC5oYW5kbGVyLnB1c2goXCIkKCdcIitjb25kaXRpb24uc2VsZWN0b3IrXCInKVwiKTtcclxuXHRcdFx0XHR9IGVsc2Ugey8v0KHQtdC70LXQutGC0L7RgCDQvdC1INC30LDQtNCw0L0sINC/0YDQvtCy0LXRgNGP0LXQvCDRgdC+0LHRgdGC0LLQtdC90L3QvtC1INC/0L7Qu9C1XHJcblx0XHRcdFx0XHRyZXQuY29uZGl0aW9uID0gXCJSZWdFeHAoL1wiK2NvbmRpdGlvbi52YWx1ZStcIi8pLnRlc3QoZmllbGQudmFsKCkpXCI7Ly9maWVsZCDQtNCw0LvRjNGI0LUg0LHRg9C00LXRgiDQvtCx0YDQsNCx0LDRgtGL0LLQsNGC0YzRgdGPIGV2YWxcclxuXHRcdFx0XHR9XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlICdjaGVja2VkJzpcclxuXHRcdFx0XHRpZiAoY29uZGl0aW9uLnNlbGVjdG9yIT09JycpIHsvL9CX0LDQtNCw0L0g0YHQtdC70LXQutGC0L7RgCDQstC90LXRiNC90LXQs9C+INC/0L7Qu9GPLCDQtNC+0L/QvtC70L3QuNGC0LXQu9GM0L3QviDQstC10YjQsNC10Lwg0L7QsdGA0LDQsdC+0YLRh9C40Log0L3QsCDQvdC10LPQvlxyXG5cdFx0XHRcdFx0cmV0LmNvbmRpdGlvbiA9IFwiJCgnXCIrY29uZGl0aW9uLnNlbGVjdG9yK1wiJykuaXMoJzpjaGVja2VkJylcIjtcclxuXHRcdFx0XHRcdHJldC5oYW5kbGVyLnB1c2goXCIkKCdcIitjb25kaXRpb24uc2VsZWN0b3IrXCInKVwiKTtcclxuXHRcdFx0XHR9IGVsc2Ugey8v0KHQtdC70LXQutGC0L7RgCDQvdC1INC30LDQtNCw0L0sINC/0YDQvtCy0LXRgNGP0LXQvCDRgdC+0LHRgdGC0LLQtdC90L3QvtC1INC/0L7Qu9C1XHJcblx0XHRcdFx0XHRyZXQuY29uZGl0aW9uID0gXCJmaWVsZC5pcygnOmNoZWNrZWQnKVwiO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0YnJlYWs7XHJcblx0XHRcdGNhc2UgJ3NlbGVjdCc6XHJcblx0XHRcdFx0X2NvbmRpdGlvbj0nJztcclxuXHRcdFx0XHQvL9CSIGNvbmRpdGlvbi52YWx1ZSDQtNC+0LvQttC10L0g0LHRi9GC0Ywg0LzQsNGB0YHQuNCyINC/0LXRgNC10LLQtdC00ZHQvdC90YvQuSDQsiDRgdGC0YDQvtC60YMgLSDQvtCxINGN0YLQvtC8INC30LDQsdC+0YLQuNC80YHRjyDQsiBjb21wbGV0ZV9jb25kaXRpb25cclxuXHRcdFx0XHRpZiAoY29uZGl0aW9uLnNlbGVjdG9yIT09JycpIHsvL9CX0LDQtNCw0L0g0YHQtdC70LXQutGC0L7RgCDQstC90LXRiNC90LXQs9C+INC/0L7Qu9GPLCDQtNC+0L/QvtC70L3QuNGC0LXQu9GM0L3QviDQstC10YjQsNC10Lwg0L7QsdGA0LDQsdC+0YLRh9C40Log0L3QsCDQvdC10LPQvlxyXG5cdFx0XHRcdFx0aWYgKGNvbmRpdGlvbi5taW4pIHsvL9CT0LXQvdC10YDQuNGA0YPQtdC8INGD0YHQu9C+0LLQuNC1INC00LvRjyBtaW4sINC10YHQu9C4INC+0L3QviDQt9Cw0LTQsNC90L5cclxuXHRcdFx0XHRcdFx0X2NvbmRpdGlvbis9XCIgJiYgKGdldF9zZWxlY3RlZF9vcHRpb25zKCQoJ1wiK2NvbmRpdGlvbi5zZWxlY3RvcitcIicpKS5sZW5ndGggPj0gXCIrY29uZGl0aW9uLm1pbitcIilcIjtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGlmIChjb25kaXRpb24ubWF4KSB7Ly/Qk9C10L3QtdGA0LjRgNGD0LXQvCDRg9GB0LvQvtCy0LjQtSDQtNC70Y8gbWF4LCDQtdGB0LvQuCDQvtC90L4g0LfQsNC00LDQvdC+XHJcblx0XHRcdFx0XHRcdF9jb25kaXRpb24rPVwiICYmIChnZXRfc2VsZWN0ZWRfb3B0aW9ucygkKCdcIitjb25kaXRpb24uc2VsZWN0b3IrXCInKSkubGVuZ3RoIDw9IFwiK2NvbmRpdGlvbi5tYXgrXCIpXCI7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRyZXQuY29uZGl0aW9uID0gY29uZGl0aW9uLnZhbHVlK1wiLmVxdWFscyAoZ2V0X3NlbGVjdGVkX29wdGlvbnMoJCgnXCIrY29uZGl0aW9uLnNlbGVjdG9yK1wiJykpLFwiK2NvbmRpdGlvbi5zdHJpY3QrXCIpXCIrX2NvbmRpdGlvbjtcclxuXHRcdFx0XHRcdHJldC5oYW5kbGVyLnB1c2goXCIkKCdcIitjb25kaXRpb24uc2VsZWN0b3IrXCInKVwiKTtcclxuXHRcdFx0XHR9IGVsc2Ugey8v0KHQtdC70LXQutGC0L7RgCDQvdC1INC30LDQtNCw0L0sINC/0YDQvtCy0LXRgNGP0LXQvCDRgdC+0LHRgdGC0LLQtdC90L3QvtC1INC/0L7Qu9C1XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGlmIChjb25kaXRpb24ubWluKSB7Ly/Qk9C10L3QtdGA0LjRgNGD0LXQvCDRg9GB0LvQvtCy0LjQtSDQtNC70Y8gbWluLCDQtdGB0LvQuCDQvtC90L4g0LfQsNC00LDQvdC+XHJcblx0XHRcdFx0XHRcdF9jb25kaXRpb24rPVwiICYmIChnZXRfc2VsZWN0ZWRfb3B0aW9ucyhmaWVsZCkubGVuZ3RoID49IFwiK2NvbmRpdGlvbi5taW4rXCIpXCI7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRpZiAoY29uZGl0aW9uLm1heCkgey8v0JPQtdC90LXRgNC40YDRg9C10Lwg0YPRgdC70L7QstC40LUg0LTQu9GPIG1heCwg0LXRgdC70Lgg0L7QvdC+INC30LDQtNCw0L3QvlxyXG5cdFx0XHRcdFx0XHRfY29uZGl0aW9uKz1cIiAmJiAoZ2V0X3NlbGVjdGVkX29wdGlvbnMoZmllbGQpLmxlbmd0aCA8PSBcIitjb25kaXRpb24ubWF4K1wiKVwiO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0cmV0LmNvbmRpdGlvbiA9IGNvbmRpdGlvbi52YWx1ZStcIi5lcXVhbHMgKGdldF9zZWxlY3RlZF9vcHRpb25zKGZpZWxkKSxcIitjb25kaXRpb24uc3RyaWN0K1wiKVwiK19jb25kaXRpb247XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRicmVhaztcclxuXHRcdFx0Y2FzZSAnc2V0JzpcclxuXHRcdFx0XHRfY29uZGl0aW9uPScnO1xyXG5cdFx0XHRcdHN1bW1hcnlfc2VsZWN0b3I9Jyc7XHJcblx0XHRcdFx0Ly/Qn9C+0YHQutC+0LvRjNC60YMg0LIgY29uZGl0aW9uLnZhbHVlINC00L7Qu9C20LXQvSDQsdGL0YLRjCDQvNCw0YHRgdC40LIg0YHQtdC70LXQutGC0L7RgNC+0LIsINC90YPQttC90L4g0YDQsNC30L7QsdGA0LDRgtGMINC10LPQviwg0YHQtNC10LvQsNCyINC+0LTQuNC9INCx0L7Qu9GM0YjQvtC5INGB0LXQu9C10LrRgtC+0YBcclxuXHRcdFx0XHRmb3IgKGluZGV4PTA7aW5kZXg8Y29uZGl0aW9uLnZhbHVlLmxlbmd0aDtpbmRleCsrKXtcclxuXHRcdFx0XHRcdHN1bW1hcnlfc2VsZWN0b3IrPVwiLCBcIitjb25kaXRpb24udmFsdWVbaW5kZXhdK1wiOmNoZWNrZWRcIjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0c3VtbWFyeV9zZWxlY3Rvcj1zdW1tYXJ5X3NlbGVjdG9yLnN1YnN0cigxKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAoY29uZGl0aW9uLnNlbGVjdG9yIT09JycpIHsvL9CX0LDQtNCw0L0g0YHQtdC70LXQutGC0L7RgCDQstC90LXRiNC90LXQs9C+INC/0L7Qu9GPLCDQtNC+0L/QvtC70L3QuNGC0LXQu9GM0L3QviDQstC10YjQsNC10Lwg0L7QsdGA0LDQsdC+0YLRh9C40Log0L3QsCDQvdC10LPQvlxyXG5cdFx0XHRcdFx0aWYgKGNvbmRpdGlvbi5taW4pIHsvL9CT0LXQvdC10YDQuNGA0YPQtdC8INGD0YHQu9C+0LLQuNC1INC00LvRjyBtaW4sINC10YHQu9C4INC+0L3QviDQt9Cw0LTQsNC90L5cclxuXHRcdFx0XHRcdFx0X2NvbmRpdGlvbis9XCIgJiYgKCQoJ1wiK2NvbmRpdGlvbi5zZWxlY3RvcitzdW1tYXJ5X3NlbGVjdG9yK1wiJykubGVuZ3RoID49IFwiK2NvbmRpdGlvbi5taW4rXCIpXCI7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRpZiAoY29uZGl0aW9uLm1heCkgey8v0JPQtdC90LXRgNC40YDRg9C10Lwg0YPRgdC70L7QstC40LUg0LTQu9GPIG1heCwg0LXRgdC70Lgg0L7QvdC+INC30LDQtNCw0L3QvlxyXG5cdFx0XHRcdFx0XHRfY29uZGl0aW9uKz1cIiAmJiAoJCgnXCIrY29uZGl0aW9uLnNlbGVjdG9yK3N1bW1hcnlfc2VsZWN0b3IrXCInKS5sZW5ndGggPD0gXCIrY29uZGl0aW9uLm1heCtcIilcIjtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdHJldC5jb25kaXRpb24gPSBcImlzX2ZpZWxkc19jaGVja2VkKCQoJ1wiK2NvbmRpdGlvbi5zZWxlY3RvcitcIicpLFwiKyBKU09OLnN0cmluZ2lmeShjb25kaXRpb24udmFsdWUpK1wiLFwiK2NvbmRpdGlvbi5zdHJpY3QrXCIpXCIrX2NvbmRpdGlvbjtcclxuXHRcdFx0XHRcdC8q0JTQu9GPINCx0LvQvtC60L7QsiDRgtGA0LXQsdGD0LXRgtGB0Y8g0Y3QvNGD0LvQuNGA0L7QstCw0YLRjCBvbmNoYW5nZSwg0L/QvtGN0YLQvtC80YMg0L/QtdGA0LXQtNCw0ZHQvCDQstC+0YIg0YLQsNC60L7QuSDRgdC10LvQtdC60YLQvtGAKi9cclxuXHRcdFx0XHRcdHJldC5oYW5kbGVyLnB1c2goXCIkKCdcIitjb25kaXRpb24uc2VsZWN0b3IrXCInKS5maW5kKCdpbnB1dFt0eXBlPWNoZWNrYm94XSwgaW5wdXRbdHlwZT1yYWRpb10nKVwiKTtcclxuXHRcdFx0XHR9IGVsc2Ugey8v0KHQtdC70LXQutGC0L7RgCDQvdC1INC30LDQtNCw0L0sINC/0YDQvtCy0LXRgNGP0LXQvCDRgdC+0LHRgdGC0LLQtdC90L3QvtC1INC/0L7Qu9C1XHJcblx0XHRcdFx0XHRpZiAoY29uZGl0aW9uLm1pbikgey8v0JPQtdC90LXRgNC40YDRg9C10Lwg0YPRgdC70L7QstC40LUg0LTQu9GPIG1pbiwg0LXRgdC70Lgg0L7QvdC+INC30LDQtNCw0L3QvlxyXG5cdFx0XHRcdFx0XHRfY29uZGl0aW9uKz1cIiAmJiAoZmllbGQuZmluZCgnXCIrc3VtbWFyeV9zZWxlY3RvcitcIicpLmxlbmd0aCA+PSBcIitjb25kaXRpb24ubWluK1wiKVwiO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYgKGNvbmRpdGlvbi5tYXgpIHsvL9CT0LXQvdC10YDQuNGA0YPQtdC8INGD0YHQu9C+0LLQuNC1INC00LvRjyBtYXgsINC10YHQu9C4INC+0L3QviDQt9Cw0LTQsNC90L5cclxuXHRcdFx0XHRcdFx0X2NvbmRpdGlvbis9XCIgJiYgKGZpZWxkLmZpbmQoJ1wiK3N1bW1hcnlfc2VsZWN0b3IrXCInKS5sZW5ndGggPD0gXCIrY29uZGl0aW9uLm1heCtcIilcIjtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdHJldC5jb25kaXRpb24gPSBcImlzX2ZpZWxkc19jaGVja2VkKGZpZWxkLFwiKyBKU09OLnN0cmluZ2lmeShjb25kaXRpb24udmFsdWUpK1wiLFwiK2NvbmRpdGlvbi5zdHJpY3QrXCIpXCIrX2NvbmRpdGlvbjtcclxuXHRcdFx0XHRcdC8q0JTQu9GPINCx0LvQvtC60L7QsiDRgtGA0LXQsdGD0LXRgtGB0Y8g0Y3QvNGD0LvQuNGA0L7QstCw0YLRjCBvbmNoYW5nZSwg0L/QvtGN0YLQvtC80YMg0L/QtdGA0LXQtNCw0ZHQvCDQstC+0YIg0YLQsNC60L7QuSDRgdC10LvQtdC60YLQvtGAKi9cclxuXHRcdFx0XHRcdHJldC5oYW5kbGVyLnB1c2goXCJmaWVsZC5maW5kKCdpbnB1dFt0eXBlPWNoZWNrYm94XSwgaW5wdXRbdHlwZT1yYWRpb10nKVwiKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlICduYXRpdmUnOlxyXG5cdFx0XHRcdHJldC5jb25kaXRpb249Y29uZGl0aW9uLnZhbHVlO1xyXG5cdFx0XHRcdGlmIChjb25kaXRpb24uc2VsZWN0b3IhPT0nJykgeyAvL9CX0LDQtNCw0L0g0YHQtdC70LXQutGC0L7RgCDQstC90LXRiNC90LXQs9C+INC/0L7Qu9GPLCDQtNC+0L/QvtC70L3QuNGC0LXQu9GM0L3QviDQstC10YjQsNC10Lwg0L7QsdGA0LDQsdC+0YLRh9C40Log0L3QsCDQvdC10LPQvlxyXG5cdFx0XHRcdFx0cmV0LmhhbmRsZXIucHVzaChcIiQoJ1wiK2NvbmRpdGlvbi5zZWxlY3RvcitcIicpXCIpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0YnJlYWs7XHJcblx0XHR9XHJcblx0XHRyZXQuaGFuZGxlci5wdXNoKFwiZmllbGRcIik7XHJcblx0XHRpZiAoY29uZGl0aW9uLmludmVydCkge1xyXG5cdFx0XHRyZXQuY29uZGl0aW9uPWNvbmRpdGlvbi5sb2dpYytcIiAhKFwiK3JldC5jb25kaXRpb24rXCIpXCI7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRyZXQuY29uZGl0aW9uPWNvbmRpdGlvbi5sb2dpYytcIiAoXCIrcmV0LmNvbmRpdGlvbitcIilcIjtcclxuXHRcdH1cclxuXHRcdHJldHVybiAocmV0KTtcclxuXHRcdFxyXG5cdH1cclxuXHJcblx0LypcclxuXHQgKiBmaWVsZCAtIGpRdWVyeS3QvtCx0YrQtdC60YIg0L/QvtC70Y8sINC6INC60L7RgtC+0YDQvtC80YMg0L/RgNC40LzQtdC90Y/QtdGC0YHRjyDQv9GA0LDQstC40LvQvlxyXG5cdCAqIHJ1bGUgLSDQv9GA0LjQvNC10L3Rj9C10LzQvtC1INC/0YDQsNCy0LjQu9C+XHJcblx0ICogY29uZGl0aW9ucyAtINC90LDQsdC+0YAg0YPRgdC70L7QstC40LksINC/0YDQuCDQutC+0YLQvtGA0YvRhSDQv9GA0LDQstC40LvQviDQv9GA0LjQvNC10L3Rj9C10YLRgdGPXHJcblx0Ki9cclxuXHRmdW5jdGlvbiBhcHBseV9ydWxlKGZpZWxkLHJ1bGUsY29uZGl0aW9ucyl7XHJcblx0XHR2YXIgc3VtbWFyeV9jb25kaXRpb25zPXtcclxuXHRcdFx0Y29uZGl0aW9uOlwiXCIsXHJcblx0XHRcdGhhbmRsZXI6W11cclxuXHRcdH0sXHJcblx0XHRqc19jb25kaXRpb24saSxoLGNvbmRpdGlvbiwgYWN0aW9uO1xyXG5cdFx0Zm9yIChjb25kaXRpb24gaW4gY29uZGl0aW9ucyl7XHJcblx0XHRcdGlmICgkLmlzTnVtZXJpYyhjb25kaXRpb24pKSBqc19jb25kaXRpb249Z2V0X2pzX2NvbmRpdGlvbihjb25kaXRpb25zW2NvbmRpdGlvbl0sZmllbGQpOy8v0JIgY29uZGl0aW9ucyDQvNC+0LbQtdGCINCx0YvRgtGMINC90LXRhtC40YTRgNC+0LLQvtC5INC/0LDRgNCw0LzQtdGC0YAsINC00L7Qv9C+0LvQvdGP0Y7RidC40Lkg0L3QsNCx0L7RgCDQv9GA0LDQstC40LtcclxuXHRcdFx0c3VtbWFyeV9jb25kaXRpb25zLmNvbmRpdGlvbis9XCIgXCIranNfY29uZGl0aW9uLmNvbmRpdGlvbjtcclxuXHRcdFx0Zm9yIChpPTA7aTxqc19jb25kaXRpb24uaGFuZGxlci5sZW5ndGg7aSsrKXsvL9CY0LfQsdCw0LLQu9GP0LXQvNGB0Y8g0L7RgiDQvdCw0LLQtdGI0LjQstCw0L3QuNC5INC+0LTQuNC90LDQutC+0LLRi9GFINC+0LHRgNCw0LHQvtGC0YfQuNC60L7QsiDQvdCwINC+0LTQvdC+INC/0L7Qu9C1XHJcblx0XHRcdFx0aWYgKCFzdW1tYXJ5X2NvbmRpdGlvbnMuaGFuZGxlci5vYmplY3RfaW5fYXJyYXkoanNfY29uZGl0aW9uLmhhbmRsZXJbaV0pKSBzdW1tYXJ5X2NvbmRpdGlvbnMuaGFuZGxlci5wdXNoKGpzX2NvbmRpdGlvbi5oYW5kbGVyW2ldKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0Ly/QntCx0YDQtdC30LDQtdC8INC+0YIg0YPRgdC70L7QstC40Y8g0L/QtdGA0LLRi9C1INGH0LXRgtGL0YDQtSDRgdC40LzQstC+0LvQsCDRgSDQv9C10YDQstGL0Lwg0L3QtdC90YPQttC90YvQvCDRg9GB0LvQvtCy0LjQtdC8LiDQldGB0LvQuCDQsdGD0LTQtdGCINCz0LvRjtGH0LjRgtGMIC0g0L/QtdGA0LXQv9C40YHQsNGC0Ywg0YDQtdCz0YPQu9GP0YDQutC+0LkuXHJcblx0XHRzdW1tYXJ5X2NvbmRpdGlvbnMuY29uZGl0aW9uPXN1bW1hcnlfY29uZGl0aW9ucy5jb25kaXRpb24uc3Vic3RyKDQpO1xyXG5cdFx0Zm9yIChpPTA7aTxzdW1tYXJ5X2NvbmRpdGlvbnMuaGFuZGxlci5sZW5ndGg7aSsrKXtcclxuXHRcdFx0aD0oc3VtbWFyeV9jb25kaXRpb25zLmhhbmRsZXJbaV09PT0nJyk/ZmllbGQ6ZXZhbChzdW1tYXJ5X2NvbmRpdGlvbnMuaGFuZGxlcltpXSk7Ly/QndCwINCy0YHRj9C60LjQuSDRgdC70YPRh9Cw0LlcclxuXHRcdFx0XHJcblx0XHRcdGFjdGlvbj1nZXRfcnVsZV9hY3Rpb24ocnVsZSxjb25kaXRpb25zLmFjdGlvbnMpOy8v0J/QvtC70YPRh9C40Lwg0YHQvtCx0YvRgtC40LUsINC30LDQtNCw0L3QvdC+0LUg0LIg0L/RgNCw0LLQuNC70LUg0L3QtdC/0L7RgdGA0LXQtNGB0YLQstC10L3QvdC+IFxyXG5cdFx0XHRhY3Rpb24rPVwiIFwiK2dldF9ydWxlX2FjdGlvbihydWxlLEFDQ0VQVEFCTEUuYWN0aW9ucyk7Ly/Qn9C+0LvRg9GH0LjQvCDRgdC+0LHRi9GC0LjQtSwg0LfQsNC00LDQvdC90L7QtSDQtNC70Y8g0LLRgdC10YUg0L/RgNCw0LLQuNC7XHJcblx0XHRcdFxyXG5cdFx0XHRzd2l0Y2ggKGNoZWNrYWJsZShoKSl7Ly/Qn9C+0LvRg9GH0LjQvCDQs9C70L7QsdCw0LvRjNC90L7QtSDQv9GA0LDQstC40LvQviArINC/0YDQsNCy0LjQu9CwINC00LvRjyBcItC+0YHQvtCx0LXQvdC90YvRhVwiINGN0LvQtdC80LXQvdGC0L7QslxyXG5cdFx0XHRcdGNhc2UgMDpcclxuXHRcdFx0XHRcdGFjdGlvbis9XCIgXCIrIEFDQ0VQVEFCTEUuYWN0aW9uO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdGNhc2UgMTpcclxuXHRcdFx0XHRcdGFjdGlvbj0gXCJjaGFuZ2VcIjtcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRjYXNlIDI6Ly/QoSDRgNCw0LTQuNC+0LrQvdC+0L/QutCw0LzQuCDQstGB0ZEg0L/Qu9C+0YXQviwg0YMg0L3QuNGFINC90LXRgiDQvdC+0YDQvNCw0LvRjNC90L7Qs9C+IG9uQ2hhbmdlXHJcblx0XHRcdFx0XHRpZiAodHlwZW9mKGguYXR0cignbmFtZScpKSE9PSd1bmRlZmluZWQnKSBoPSQoJ2lucHV0W25hbWU9XCInK2guYXR0cignbmFtZScpKydcXFwiXScpO1xyXG5cdFx0XHRcdFx0YWN0aW9uKz1cIiBcIisgXCJjaGFuZ2VcIjtcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRjYXNlIDM6Ly/QlNC70Y8g0YHQtdC70LXQutGC0L7QsiDQvdGD0LbQvdC+INC+0LHRgNCw0LHQsNGC0YvQstCw0YLRjCDQvtCx0LAg0YHQvtCx0YvRgtC40Y9cclxuXHRcdFx0XHRcdGFjdGlvbis9XCIgXCIrIEFDQ0VQVEFCTEUuYWN0aW9uK1wiIGNoYW5nZVwiO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHRhY3Rpb249YWN0aW9uLnRyaW0oKTtcclxuXHRcdFx0XHJcblx0XHRcdGgub24oYWN0aW9uLGZ1bmN0aW9uKGV2ZW50LGlnbm9yZV9jaGFuZ2UsaWdub3JlX2FjdGlvbil7XHJcblx0XHRcdFx0dmFyIHg9c2V0X2NsYXNzKGZpZWxkLHJ1bGUsZXZhbChzdW1tYXJ5X2NvbmRpdGlvbnMuY29uZGl0aW9uKSk7XHJcblx0XHRcdFx0aWYgKHgpIHtcclxuXHRcdFx0XHRcdGgudHJpZ2dlcih4LmFjdGlvbit4LnJ1bGUpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZiAoIWlnbm9yZV9hY3Rpb24gfHwgdHlwZW9mKGlnbm9yZV9hY3Rpb24pPT09J3VuZGVmaW5lZCcpIHsvL9Cy0YvQt9GL0LLQsNC10Lwg0YHRgNCw0LHQsNGC0YvQstCw0L3QuNC4INGE0YPQvdC60YbQuNC4INCyIG9uQWN0aW9uINGC0L7Qu9GM0LrQviDQtNC70Y8g0YDQtdCw0LvRjNC90L4g0L/RgNC+0LjQt9C+0YjQtdC00YjQuNGFINGB0L7QsdGL0YLQuNC5LCDQsCDQvdC1INC00LvRjyDQstGL0LfQstCw0L3QvdGL0YUg0LIg0LrQvtC00LVcclxuXHRcdFx0XHRcdGlmIChBQ0NFUFRBQkxFLm9uQWN0aW9uKSBBQ0NFUFRBQkxFLm9uQWN0aW9uKGZpZWxkKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aW5pdGlhdGVFdmVudHMoRk9STSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0aWYgKCFpZ25vcmVfY2hhbmdlIHx8IHR5cGVvZihpZ25vcmVfY2hhbmdlKT09PSd1bmRlZmluZWQnKSB7Ly/QstGL0LfRi9Cy0LDQtdC8INC40LfQvNC10L3QtdC90LjQtSDRgdC+0YHRgtC+0Y/QvdC40LkgY2hhbmdlZC91bmNoYW5nZWQg0YLQvtC70YzQutC+INC00LvRjyDRgNC10LDQu9GM0L3QviDQv9GA0L7QuNC30L7RiNC10LTRiNC40YUg0YHQvtCx0YvRgtC40LksINCwINC90LUg0LTQu9GPINCy0YvQt9Cy0LDQvdC90YvRhSDQsiDQutC+0LTQtVxyXG5cdFx0XHRcdFx0aWYgKEFDQ0VQVEFCTEUuY2xhc3NVbmNoYW5nZWQhPT1mYWxzZSloLnJlbW92ZUNsYXNzKEFDQ0VQVEFCTEUuY2xhc3NVbmNoYW5nZWQpO1xyXG5cdFx0XHRcdFx0aWYgKEFDQ0VQVEFCTEUuY2xhc3NDaGFuZ2VkIT09ZmFsc2UpIGguYWRkQ2xhc3MoQUNDRVBUQUJMRS5jbGFzc0NoYW5nZWQpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAoQUNDRVBUQUJMRS5jbGFzc1VuY2hhbmdlZCE9PWZhbHNlKSBoLmFkZENsYXNzKEFDQ0VQVEFCTEUuY2xhc3NVbmNoYW5nZWQpO1xyXG5cdFx0XHRcclxuXHRcdFx0aWYgKEFDQ0VQVEFCTEUuYXV0b2dyYWIpIHtcclxuXHRcdFx0XHR2YXIgYT1hY3Rpb24uc3BsaXQoJyAnKTtcclxuXHRcdFx0XHQkKGEpLmVhY2ggKGZ1bmN0aW9uKHZhbCl7XHJcblx0XHRcdFx0XHRoLnRyaWdnZXIoYVt2YWxdLFtBQ0NFUFRBQkxFLmlnbm9yZV9hdXRvZ3JhYl9jaGFuZ2UsQUNDRVBUQUJMRS5pZ25vcmVfYXV0b2dyYWJfYWN0aW9uXSk7Ly/QmNC90LjRhtC40LDQu9C40LfQsNGG0LjRjyDRgdC+0YHRgtC+0Y/QvdC40Y9cclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuXHRmdW5jdGlvbiBnZXRfcnVsZV9hY3Rpb24ocnVsZSxhY3Rpb25zKSB7XHJcblx0XHRpZiAodHlwZW9mKGFjdGlvbnMpPT09J3VuZGVmaW5lZCcpIHJldHVybiBcIlwiO1xyXG5cdFx0cmV0dXJuKGFjdGlvbnNbcnVsZS50b0xvd2VyQ2FzZSgpXXx8XCJcIik7XHJcblx0fVxyXG5cdFxyXG5cdC8v0JLQvtC30LLRgNCw0YnQsNC10YIgdHJ1ZSwg0LXRgdC70Lgg0L/QtdGA0LXQtNCw0L3QvdGL0Lkg0Y3Qu9C10LzQtdC90YIg0L/QvtC00LTQtdGA0LbQuNCy0LDQtdGCINGC0L7Qu9GM0LrQviBvbkNoYW5nZVxyXG5cdGZ1bmN0aW9uIGNoZWNrYWJsZShmaWVsZCl7XHJcblx0XHQvL1RPRE86INCf0YDQvtCy0LXRgNC40YLRjCwg0YfRgtC+INCx0YPQtNC10YIsINC10YHQu9C4INCyIGZpZWxkINC80L3QvtCz0L4g0L/QvtC70LXQuVxyXG5cdFx0aWYgKGZpZWxkLmlzKCdbdHlwZT1jaGVja2JveF0nKSkge1xyXG5cdFx0XHRyZXR1cm4gKDEpO1xyXG5cdFx0fSBlbHNlIGlmIChmaWVsZC5pcygnW3R5cGU9cmFkaW9dJykpIHtcclxuXHRcdFx0cmV0dXJuICgyKVxyXG5cdFx0fSBlbHNlIGlmIChmaWVsZC5pcygnc2VsZWN0JykpIHtcclxuXHRcdFx0cmV0dXJuICgzKTtcclxuXHRcdH0gZWxzZSByZXR1cm4gKDApO1xyXG5cdH1cclxuXHQvKipcclxuXHQgKiDQn9GA0L7QstC10YDRj9C10YIsINC/0YDQvtC40LfQvtGI0LvQviDQu9C4INC/0LXRgNC10LrQu9GO0YfQtdC90LjQtSDRgdC+0YHRgtC+0Y/QvdC40Y8g0LTQu9GPIHZhbGlkINC4IHJlcXVpcmVkLCDQtdGB0LvQuCDQv9GA0L7QuNC30L7RiNC70L4gLSDQuNC90LjRhtC40LjRgNGD0LXRgiDRgdC+0L7RgtCy0LXRgtGB0YLQstGD0Y7RidC10LUg0YHQvtCx0YvRgtC40LUuINCh0L7RgdGC0L7Rj9C90LjQtSB2aXNpYmxlINC/0YDQvtCy0LXRgNGP0LXRgtGB0Y8g0L3QtdC/0L7RgdGA0LXQtNGB0YLQstC10L3QvdC+INCyIHNldENsYXNzXHJcblx0KiovXHJcblx0ZnVuY3Rpb24gaW5pdGlhdGVFdmVudHMobGVtb25mb3JtKXtcclxuXHRcdHZhclx0Y3VyX3ZhbGlkPWxlbW9uZm9ybS5pc1ZhbGlkKCksXHJcblx0XHRcdFx0Y3VyX3JlcXVpcmU9IWxlbW9uZm9ybS5pc05vdFJlcXVpcmVkKCksXHJcblx0XHRcdFx0Y3VyX3Zpc2libGU9bGVtb25mb3JtLmlzKCc6dmlzaWJsZScpO1xyXG5cdFx0aWYgKGN1cl92YWxpZCAmJiAhVmFsaWRTdGF0ZSkge1xyXG5cdFx0XHRsZW1vbmZvcm0udHJpZ2dlcignbGVtb25ncmFiLnZhbGlkJyk7XHJcblx0XHRcdFZhbGlkU3RhdGU9dHJ1ZTtcclxuXHRcdH0gZWxzZSBpZiAoIWN1cl92YWxpZCAmJiBWYWxpZFN0YXRlKSB7XHJcblx0XHRcdGxlbW9uZm9ybS50cmlnZ2VyKCdsZW1vbmdyYWIubm92YWxpZCcpO1xyXG5cdFx0XHRWYWxpZFN0YXRlPWZhbHNlO1xyXG5cdFx0fVxyXG5cdFx0aWYgKGN1cl9yZXF1aXJlICYmICFSZXF1aXJlZFN0YXRlKSB7XHJcblx0XHRcdGxlbW9uZm9ybS50cmlnZ2VyKCdsZW1vbmdyYWIucmVxdWlyZScpO1xyXG5cdFx0XHRSZXF1aXJlZFN0YXRlPXRydWU7XHJcblx0XHR9IGVsc2UgaWYgKCFjdXJfcmVxdWlyZSAmJiBSZXF1aXJlZFN0YXRlKSB7XHJcblx0XHRcdGxlbW9uZm9ybS50cmlnZ2VyKCdsZW1vbmdyYWIubm9yZXF1aXJlJyk7XHJcblx0XHRcdFJlcXVpcmVkU3RhdGU9ZmFsc2U7XHJcblx0XHR9XHJcblx0fVxyXG5cdFxyXG5cdC8v0J/RgNC+0LLQtdGA0Y/QtdGCLCDQtdGB0YLRjCDQu9C4INGC0L7Rh9C90L4g0YLQsNC60L7QuSDQttC1INC+0LHRitC10LrRgiDQsiDQvNCw0YHRgdC40LLQtSAo0LLQv9C70L7RgtGMINC00L4g0LfQvdCw0YfQtdC90LjQuSDQv9C+0LvQtdC5KS5cclxuXHRBcnJheS5wcm90b3R5cGUub2JqZWN0X2luX2FycmF5ID0gZnVuY3Rpb24oc3JjaCkge1xyXG5cdFx0dmFyIGk7XHJcblx0XHRmb3IoaT0wOyBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRpZiAodHlwZW9mKHRoaXNbaV0pPT09J29iamVjdCcpe1xyXG5cdFx0XHRcdC8v0J7QsdC+0LnQtNGR0LzRgdGPINCx0LXQtyDRgNC10LrRg9GA0YHQuNC4LiDQldGB0LvQuCDQv9C+0L3QsNC00L7QsdC40YLRgdGPINGA0LXQutGD0YDRgdC40Y86IGh0dHA6Ly9qYXZhc2NyaXB0LnJ1L2ZvcnVtL21pc2MvMTA3OTItc3Jhdm5lbmllLW9iZWt0b3YtMi5odG1sI3Bvc3QyMDkzNDNcclxuXHRcdFx0XHRpZiAoSlNPTi5zdHJpbmdpZnkodGhpc1tpXSk9PT1KU09OLnN0cmluZ2lmeShzcmNoKSkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fTtcclxuXHRcclxuXHQvKlxyXG5cdCAqINCh0YDQsNCy0L3QuNCy0LDQtdGCINC00LLRg9C80LXRgNC90YvQtSDQvNCw0YHRgdC40LLRiyDQvNC10LbQtNGDINGB0L7QsdC+0LkuINCc0LDRgdGB0LjQstGLINGC0YDQsNC60YLRg9GO0YLRgdGPINC60LDQuiDQvNC90L7QttC10YHRgtCy0LAgLSDRgi7QtS4g0L7QtNC40L3QsNC60L7QstGL0LzQuCDRgdGH0LjRgtCw0Y7RgtGB0Y8g0LzQsNGB0YHQuNCy0Ysg0YEg0YDQsNCy0L3Ri9C8INC60L7Qu9C40YfQtdGB0YLQstC+0Lwg0Y3Qu9C10LzQtdC90YLQvtCyINC4INC40YUg0L7QtNC40L3QsNC60L7QstGL0LzQuCDQt9C90LDRh9C10L3QuNGP0LzQuCwg0L3QviDQv9C+0YDRj9C00L7QuiDRjdC70LXQvNC10L3RgtC+0LIg0LzQvtC20LXRgiDQsdGL0YLRjCDRgNCw0LfQvdGL0LksINGCLtC1LiBbMSwyLDNdPT09WzIsMSwzXVxyXG5cdCAqIHN0cmljdCDQt9Cw0LTQsNGR0YIg0YHRgtGA0L7Qs9C+0YHRgtGMINCy0YXQvtC20LTQtdC90LjRjy4gZmFsc2UgLSBhcnJheSDQvNC+0LbQtdGCINCx0YvRgtGMINC/0L7QtNC80L3QvtC20LXRgdGC0LLQvtC8LCB0cnVlIC0g0LTQvtC70LbQtdC9INGC0L7Rh9C90L4g0YHQvtCy0L/QsNC00LDRgtGMXHJcblx0Ki9cclxuXHRBcnJheS5wcm90b3R5cGUuZXF1YWxzID0gZnVuY3Rpb24gKGFycmF5LHN0cmljdCl7XHJcblx0XHRpZiAodHlwZW9mKHN0cmljdCk9PT0ndW5kZWZpbmVkJykgc3RyaWN0PWZhbHNlO1xyXG5cdFx0aWYgKChzdHJpY3QgJiYgdGhpcy5sZW5ndGghPT1hcnJheS5sZW5ndGgpfHwoIXN0cmljdCAmJiB0aGlzLmxlbmd0aD5hcnJheS5sZW5ndGgpKSByZXR1cm4gKGZhbHNlKTtcclxuXHRcdFxyXG5cdFx0dmFyIGk7XHJcblx0XHRpZiAoIUFycmF5LnByb3RvdHlwZS5pbmRleE9mKSB7LyrRh9C10YHRgtC90L4g0YHQutC+0L/QuNC/0LDRidC10L3QviDQuNC3INC40L3RgtC10YDQvdC10YLQvtCyINC4INC90LUg0L/RgNC+0LLQtdGA0Y/Qu9C+0YHRjCwg0YIu0LouINCy0YHQtSDRgdC+0LLRgNC10LzQtdC90L3Ri9C1INCx0YDQsNGD0LfQtdGA0Ysg0LTQvtC70LbQvdGLINC/0L7QtNC00LXRgNC20LjQstCw0YLRjCBpbmRleE9mKi9cclxuXHRcdFx0QXJyYXkucHJvdG90eXBlLmluZGV4T2YgPSBmdW5jdGlvbihlbHQgLyosIGZyb20qLykge1xyXG5cdFx0XHRcdHZhciBsZW4gPSB0aGlzLmxlbmd0aCA+Pj4gMDtcclxuXHRcdFx0XHR2YXIgZnJvbSA9IE51bWJlcihhcmd1bWVudHNbMV0pIHx8IDA7XHJcblx0XHRcdFx0ZnJvbSA9IChmcm9tIDwgMCkgPyBNYXRoLmNlaWwoZnJvbSkgOiBNYXRoLmZsb29yKGZyb20pO1xyXG5cdFx0XHRcdGlmIChmcm9tIDwgMCkgZnJvbSArPSBsZW47XHJcblx0XHRcdFx0Zm9yICg7IGZyb20gPCBsZW47IGZyb20rKyl7XHJcblx0XHRcdFx0XHRpZiAoZnJvbSBpbiB0aGlzICYmIHRoaXNbZnJvbV0gPT09IGVsdCkgcmV0dXJuIGZyb207XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHJldHVybiAtMTtcclxuXHRcdFx0fTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0Zm9yIChpPTA7aTx0aGlzLmxlbmd0aDtpKyspe1xyXG5cdFx0XHRpZihhcnJheS5pbmRleE9mKHRoaXNbaV0pPT09LTEpIHJldHVybiAoZmFsc2UpO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuICh0cnVlKTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqINCc0LXQvdGP0LXRgiDQutC70LDRgdGBINC4INGB0L7QvtCx0YnQsNC10YIsINC/0YDQvtC40LfQvtGI0LvQviDQu9C4INC40LfQvNC10L3QtdC90LjQtSDRgdC+0YHRgtC+0Y/QvdC40Y8uXHJcblx0ICovXHJcblxyXG5cdGZ1bmN0aW9uIHNldF9jbGFzcyhmaWVsZCxydWxlLHNldGMpe1xyXG5cdFx0dmFyIHJldD1mYWxzZTtcclxuXHRcdHZhciBwb3NpdGl2ZUNsYXNzLG5lZ2F0aXZlQ2xhc3M7XHJcblx0XHRpZiAodHlwZW9mKHJ1bGUpPT09J3VuZGVmaW5lZCcpIHJldHVybigwKTtcclxuXHRcdHN3aXRjaCAocnVsZSl7XHJcblx0XHRcdGNhc2UgJ1ZBTElEJzpcclxuXHRcdFx0XHRwb3NpdGl2ZUNsYXNzPUFDQ0VQVEFCTEUuY2xhc3NWYWxpZDtcclxuXHRcdFx0XHRuZWdhdGl2ZUNsYXNzPUFDQ0VQVEFCTEUuY2xhc3NJbnZhbGlkO1xyXG5cdFx0XHRicmVhaztcclxuXHRcdFx0Y2FzZSAnUkVRVUlSRUQnOlxyXG5cdFx0XHRcdHBvc2l0aXZlQ2xhc3M9QUNDRVBUQUJMRS5jbGFzc1JlcXVpcmVkO1xyXG5cdFx0XHRcdG5lZ2F0aXZlQ2xhc3M9QUNDRVBUQUJMRS5jbGFzc05vcmVxdWlyZWQ7XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlICdFTkFCTEVEJzpcclxuXHRcdFx0XHRwb3NpdGl2ZUNsYXNzPUFDQ0VQVEFCTEUuY2xhc3NFbmFibGVkO1xyXG5cdFx0XHRcdG5lZ2F0aXZlQ2xhc3M9QUNDRVBUQUJMRS5jbGFzc0Rpc2FibGVkO1xyXG5cdFx0XHRicmVhaztcclxuXHRcdFx0Y2FzZSAnVklTSUJMRSc6XHJcblx0XHRcdFx0cG9zaXRpdmVDbGFzcz1BQ0NFUFRBQkxFLmNsYXNzVmlzaWJsZTtcclxuXHRcdFx0XHRuZWdhdGl2ZUNsYXNzPUFDQ0VQVEFCTEUuY2xhc3NIaWRkZW47XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0fVxyXG5cdFx0aWYoc2V0Yyl7XHJcblx0XHRcdGlmIChmaWVsZC5oYXNDbGFzcyhuZWdhdGl2ZUNsYXNzKSkge1xyXG5cdFx0XHRcdHJldD17XHJcblx0XHRcdFx0XHRcInJ1bGVcIjpydWxlLFxyXG5cdFx0XHRcdFx0XCJhY3Rpb25cIjpcIlNFVFwiXHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcclxuXHRcdFx0ZmllbGQucmVtb3ZlQ2xhc3MobmVnYXRpdmVDbGFzcykuYWRkQ2xhc3MocG9zaXRpdmVDbGFzcyk7XHJcblx0XHRcdGlmIChydWxlPT09J1JFUVVJUkVEJyAmJiBBQ0NFUFRBQkxFLnVzZVJlcXVpcmVkQXR0cikgZmllbGQuYXR0cihcInJlcXVpcmVkXCIsXCJyZXF1aXJlZFwiKTtcclxuXHRcdFx0aWYgKHJ1bGU9PT0nRU5BQkxFRCcgJiYgQUNDRVBUQUJMRS5uYXRpdmVFbmFibGVkICkgZmllbGQucmVtb3ZlQXR0cihcImRpc2FibGVkXCIpO1xyXG5cdFx0XHRpZiAocnVsZT09PSdWSVNJQkxFJyAmJiBBQ0NFUFRBQkxFLm5hdGl2ZVZpc2libGUpIHtcclxuXHRcdFx0XHRmaWVsZC5zaG93KCk7XHJcblx0XHRcdFx0ZmllbGQudHJpZ2dlcignbGVtb25ncmFiLnZpc2libGUnKTtcclxuXHRcdFx0fVxyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFxyXG5cdFx0XHRpZiAoZmllbGQuaGFzQ2xhc3MocG9zaXRpdmVDbGFzcykpIHtcclxuXHRcdFx0XHRyZXQ9e1xyXG5cdFx0XHRcdFx0XCJydWxlXCI6cnVsZSxcclxuXHRcdFx0XHRcdFwiYWN0aW9uXCI6XCJSRU1PVkVcIlxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHJcblx0XHRcdGZpZWxkLnJlbW92ZUNsYXNzKHBvc2l0aXZlQ2xhc3MpLmFkZENsYXNzKG5lZ2F0aXZlQ2xhc3MpO1xyXG5cdFx0XHRpZiAocnVsZT09PSdSRVFVSVJFRCcgJiYgQUNDRVBUQUJMRS51c2VSZXF1aXJlZEF0dHIpIGZpZWxkLnJlbW92ZUF0dHIoXCJyZXF1aXJlZFwiKTtcclxuXHRcdFx0aWYgKHJ1bGU9PT0nRU5BQkxFRCcgJiYgQUNDRVBUQUJMRS5uYXRpdmVFbmFibGVkICkgZmllbGQuYXR0cihcImRpc2FibGVkXCIsXCJkaXNhYmxlZFwiKTtcclxuXHRcdFx0aWYgKHJ1bGU9PT0nVklTSUJMRScgJiYgQUNDRVBUQUJMRS5uYXRpdmVWaXNpYmxlKSB7XHJcblx0XHRcdFx0ZmllbGQuaGlkZSgpO1xyXG5cdFx0XHRcdGZpZWxkLnRyaWdnZXIoJ2xlbW9uZ3JhYi5oaWRlJyk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdHJldHVybiAocmV0KTtcclxuXHR9XHJcblxyXG59KCBqUXVlcnkgKSk7XHJcblxyXG5cclxuLypcclxuICAgIEN1c3RvbVxyXG4gKi9cclxuXHJcblxyXG4kKCcubWVudV9faXRlbS1kcm9wbWVudScpLmhvdmVyKFxyXG5cclxuICAgIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgaWYgKCEkKHRoaXMpLmhhc0NsYXNzKCdtZW51X19pdGVtLWRyb3BtZW51LWFjdGl2ZScpICYmICQodGhpcykuc2libGluZ3MoJy5tZW51X19pdGVtLWRyb3BtZW51LWFjdGl2ZScpLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAkKHRoaXMpLnNpYmxpbmdzKCcubWVudV9faXRlbS1kcm9wbWVudS1hY3RpdmUnKS5hZGRDbGFzcygnbWVudV9faXRlbS1kcm9wbWVudS1hY3RpdmUtb2ZmJyk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdxd2Vxd2Vxd2Vxd2UnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfSxcclxuICAgIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgaWYgKCEkKHRoaXMpLmhhc0NsYXNzKCdtZW51X19pdGVtLWRyb3BtZW51LWFjdGl2ZScpICYmICQodGhpcykuc2libGluZ3MoJy5tZW51X19pdGVtLWRyb3BtZW51LWFjdGl2ZScpLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAkKHRoaXMpLnNpYmxpbmdzKCcubWVudV9faXRlbS1kcm9wbWVudS1hY3RpdmUtb2ZmJykucmVtb3ZlQ2xhc3MoJ21lbnVfX2l0ZW0tZHJvcG1lbnUtYWN0aXZlLW9mZicpO1xyXG5cclxuICAgICAgICB9XHJcbiAgICB9XHJcbik7XHJcblxyXG5cclxuXHJcblxyXG4kKHdpbmRvdykubG9hZChmdW5jdGlvbigpIHtcclxuICAgICQoJyNmZWF0dXJlZCcpLm9yYml0KHtcclxuICAgICAgICAnYnVsbGV0cyc6IGZhbHNlLFxyXG4gICAgICAgICd0aW1lcicgOiB0cnVlLFxyXG4gICAgICAgICdudW1iZXJzJzp0cnVlLFxyXG4gICAgICAgICdhZHZhbmNlU3BlZWQnOiA2MDAwLFxyXG4gICAgICAgICdhbmltYXRpb24nIDogJ2hvcml6b250YWwtcm9tJyxcclxuICAgICAgICBjYXB0aW9uczogdHJ1ZSxcclxuICAgICAgICBjYXB0aW9uQW5pbWF0aW9uOiAnZmFkZScsXHJcbiAgICAgICAgY2FwdGlvbkFuaW1hdGlvblNwZWVkOiA4MDAsXHJcbiAgICAgICAgYnVsbGV0VGh1bWJzOiB0cnVlLFx0XHRcdFx0Ly8gdGh1bWJuYWlscyBmb3IgdGhlIGJ1bGxldHNcclxuICAgICAgICBidWxsZXRUaHVtYkxvY2F0aW9uOiAnaW1nL2NhdGFsb2cvJ1xyXG4gICAgfSk7XHJcblxyXG5cclxuXHJcblxyXG5cclxufSk7XHJcblxyXG4kKCcjZmVhdHVyZWQyJykub3JiaXQoe1xyXG4gICAgJ2J1bGxldHMnOiBmYWxzZSxcclxuICAgICd0aW1lcicgOiBmYWxzZSxcclxuICAgICdudW1iZXJzJzp0cnVlLFxyXG4gICAgJ2FkdmFuY2VTcGVlZCc6IDYwMDAsXHJcbiAgICAnYW5pbWF0aW9uJyA6ICdmYWRlJyxcclxuICAgIGNhcHRpb25zOiBmYWxzZSxcclxuICAgIGNhcHRpb25BbmltYXRpb246ICdmYWRlJyxcclxuICAgIGNhcHRpb25BbmltYXRpb25TcGVlZDogODAwXHJcblxyXG59KTtcclxuXHJcbiQoJyNzbGlkZXJSZXZpZXdzJykub3JiaXQoe1xyXG4gICAgJ2J1bGxldHMnOiBmYWxzZSxcclxuICAgICd0aW1lcicgOiBmYWxzZSxcclxuICAgICdhZHZhbmNlU3BlZWQnOiA2MDAwLFxyXG4gICAgJ2FuaW1hdGlvbicgOiAnZmFkZScsXHJcbiAgICBjYXB0aW9uczogZmFsc2UsXHJcbiAgICBjYXB0aW9uQW5pbWF0aW9uOiAnZmFkZScsXHJcbiAgICBjYXB0aW9uQW5pbWF0aW9uU3BlZWQ6IDgwMFxyXG5cclxufSk7XHJcblxyXG4kKCcjc2xpZGVyUG9zaXRpb24nKS5vcmJpdCh7XHJcbiAgICAnYnVsbGV0cyc6IHRydWUsXHJcbiAgICAndGltZXInIDogZmFsc2UsXHJcbiAgICAnYWR2YW5jZVNwZWVkJzogNjAwMCxcclxuICAgICdhbmltYXRpb24nIDogJ2ZhZGUnLFxyXG4gICAgY2FwdGlvbnM6IGZhbHNlLFxyXG4gICAgY2FwdGlvbkFuaW1hdGlvbjogJ2ZhZGUnLFxyXG4gICAgY2FwdGlvbkFuaW1hdGlvblNwZWVkOiA4MDAsXHJcbiAgICBidWxsZXRUaHVtYnM6IHRydWUsXHRcdFx0XHQvLyB0aHVtYm5haWxzIGZvciB0aGUgYnVsbGV0c1xyXG4gICAgYnVsbGV0VGh1bWJMb2NhdGlvbjogJ2ltZy9jYXRhbG9nLydcclxuXHJcbn0pO1xyXG5cclxuJCgnI3NsaWRlclBvc3QnKS5vcmJpdCh7XHJcbiAgICAnYnVsbGV0cyc6IGZhbHNlLFxyXG4gICAgJ3RpbWVyJyA6IGZhbHNlLFxyXG4gICAgJ251bWJlcnMnOnRydWUsXHJcbiAgICAnYWR2YW5jZVNwZWVkJzogNjAwMCxcclxuICAgICdhbmltYXRpb24nIDogJ2ZhZGUnLFxyXG4gICAgY2FwdGlvbnM6IGZhbHNlLFxyXG4gICAgY2FwdGlvbkFuaW1hdGlvbjogJ2ZhZGUnLFxyXG4gICAgY2FwdGlvbkFuaW1hdGlvblNwZWVkOiA4MDBcclxuXHJcblxyXG59KTtcclxuXHJcbiQoZnVuY3Rpb24oKSB7XHJcbiAgICAkLmZuLmdvU3ZnID0gZnVuY3Rpb24oKXtcclxuXHJcbiAgICAgICAgdmFyIHN2ZyA9ICQodGhpcyksXHJcbiAgICAgICAgICAgIG1hcmdpbiA9ICQod2luZG93KS5oZWlnaHQoKS81LCAvLyDvv73vv73vv73vv70g77+977+9IDMzJSDvv73vv73vv73vv73vv73vv70g77+977+977+977+977+977+977+977+977+977+977+9XHJcbiAgICAgICAgICAgIHRvcFNjcm9sbCA9IDAsXHJcbiAgICAgICAgICAgIHByb2NlbnQsIHRvcFBvcywgd2luZG93SCwgb3JuYW1lbnRJZDtcclxuXHJcbiAgICAgICAgZnVuY3Rpb24gTG9hZCgpe1xyXG5cclxuICAgICAgICAgICAgaWYoc3ZnLmxlbmd0aCA9PSAwKSByZXR1cm4gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICB0b3BQb3MgPSBzdmcub2Zmc2V0KCkudG9wLFxyXG4gICAgICAgICAgICAgICAgd2luZG93SCA9ICQod2luZG93KS5oZWlnaHQoKTtcclxuXHJcbiAgICAgICAgICAgIG9ybmFtZW50SWQgPSBuZXcgVml2dXMoc3ZnWzBdLmlkLCB7XHJcbiAgICAgICAgICAgICAgICBzdGFydDogJ21hbnVhbCcsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnYXN5bmMnLFxyXG4gICAgICAgICAgICAgICAgZGFzaEdhcDoyMFxyXG5cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIH1Mb2FkKCk7XHJcblxyXG4gICAgICAgICQod2luZG93KS5vbihcInNjcm9sbFwiLCBmdW5jdGlvbigpe1xyXG5cclxuICAgICAgICAgICAgdG9wU2Nyb2xsID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRvcFNjcm9sbCt3aW5kb3dIID49IHRvcFBvcyAmJiB0b3BTY3JvbGwgPD0gdG9wUG9zKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgcHJvY2VudCA9ICh0b3BTY3JvbGwtdG9wUG9zK3dpbmRvd0gpLyh3aW5kb3dILW1hcmdpbik7XHJcbiAgICAgICAgICAgICAgICBvcm5hbWVudElkLnNldEZyYW1lUHJvZ3Jlc3MocHJvY2VudCk7XHJcblxyXG4gICAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICB9KTtcclxuXHJcbiAgICB9XHJcbn0pO1xyXG5cclxuKGZ1bmN0aW9uKCQpe1xyXG4gICAgJChmdW5jdGlvbigpe1xyXG5cclxuICAgICAgICAkKFwiI29ybmFtZW50SWQxXCIpLmdvU3ZnKCk7XHJcbiAgICAgICAgJChcIiNvcm5hbWVudElkMlwiKS5nb1N2ZygpO1xyXG5cclxuICAgIH0pO1xyXG59KShqUXVlcnkpO1xyXG5cclxuXHJcbi8qXHJcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7XHJcbiAgICBzdmdBbmltYXRpb24oKTtcclxufSk7XHJcblxyXG5mdW5jdGlvbiBzdmdBbmltYXRpb24oKSB7XHJcblxyXG4gICAgaWYoJChcIiNvcm5hbWVudElkMlwiKS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgXHJcbiAgICAgICAgb3JuYW1lbnRJZDJYID0gJCgnI29ybmFtZW50SWQyJykub2Zmc2V0KCkudG9wOyAvL9C+0YLQvNC10YLQutCwINC90LDRh9Cw0LvQsCDQvtGA0L3QsNC80LXQvdGC0LBcclxuICAgICAgICB3aW5kb3dIID0gJCh3aW5kb3cpLmhlaWdodCgpOyAvL9Cy0YvRgdC+0YLQsCDQsdGA0LDRg9C30LXRgNCwXHJcbiAgICAgICAgdmFyIG9ybmFtZW50SWQyID0gbmV3IFZpdnVzKCdvcm5hbWVudElkMicsIHtcclxuICAgICAgICAgICAgc3RhcnQ6J21hbnVhbCcsXHJcbiAgICAgICAgICAgIHR5cGU6J2FzeW5jJ1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHZhciBzPTAsXHJcbiAgICAgICAgICAgIHN0ZXA9IDAsXHJcbiAgICAgICAgICAgIHNjcm9sbEN1cnNvcixcclxuICAgICAgICAgICAgcHJldlNjcm9sbEN1cnNvcj0wO1xyXG4gICAgICAgIHN0ZXAgPSAob3JuYW1lbnRJZDJYLSQod2luZG93KS5zY3JvbGxUb3AoKSt3aW5kb3dIKS8xMDAwMDA7XHJcbiAgICAgICAgJCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbigpe1xyXG5cclxuICAgICAgICAgICAgc2Nyb2xsQ3Vyc29yID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpO1xyXG5cclxuXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdzY3JvbGxDdXJzb3IgPSAnK3Njcm9sbEN1cnNvcisnINCy0YvRgdC+0YLQsCDQsdGA0LDRg9C30LXRgNCwID0gJyt3aW5kb3dIKycg0L7RgtC80LXRgtC60LAg0L3QsNGH0LDQu9CwINC+0YDQvdCw0LzQtdC90YLQsCA9ICcrb3JuYW1lbnRJZDJYKTtcclxuXHJcbiAgICAgICAgICAgICBpZiAoc2Nyb2xsQ3Vyc29yK3dpbmRvd0g+PSBvcm5hbWVudElkMlggJiYgc2Nyb2xsQ3Vyc29yPD0gb3JuYW1lbnRJZDJYKSB7XHJcbiAgICAgICAgICAgICAgICAgaWYgKHNjcm9sbEN1cnNvciA+PSBwcmV2U2Nyb2xsQ3Vyc29yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgIG9ybmFtZW50SWQyLnNldEZyYW1lUHJvZ3Jlc3Mocz1zK3N0ZXApO1xyXG5cclxuICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICBvcm5hbWVudElkMi5zZXRGcmFtZVByb2dyZXNzKHM9cy1zdGVwKTtcclxuICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgcHJldlNjcm9sbEN1cnNvciA9IHNjcm9sbEN1cnNvcjtcclxuICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhzKTtcclxuICAgICAgICAgICAgICAgICBpZiAocz4xKSBzPT0xO1xyXG4gICAgICAgICAgICAgICAgIGlmKHM8MCkgcz09MDtcclxuXHJcbiAgICAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbn1cclxuKi9cclxuXHJcblxyXG5cclxuLyokKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpe1xyXG4gICAgdmFyICBvcm5hbWVudElkMlg7XHJcblxyXG5cclxuICBcclxuXHJcbn0pOyovXHJcblxyXG5cclxuXHJcbi8qbmV3IFZpdnVzKCdvcm5hbWVudElkMicsIHtcclxuICAgIC8hKiAgdHlwZTonYXN5bmMnLCohL1xyXG4gICAgZGVsYXk6NTAsXHJcbiAgICBkdXJhdGlvbjogMjAwXHJcbn0pOyovXHJcblxyXG4vKlxyXG5uZXcgVml2dXMoJ29ybmFtZW50SWQnLCB7XHJcbiAgICBkdXJhdGlvbjogMjAwLFxyXG4gICAgZmlsZTogJ2ltZy9vcm5hbWVudC5zdmcnLFxyXG4gICAgb25SZWFkeTogZnVuY3Rpb24gKG15Vml2dXMpIHtcclxuICAgICAgICAvLyBgZWxgIHByb3BlcnR5IGlzIHRoZSBTVkcgZWxlbWVudFxyXG4gICAgICAgIG15Vml2dXMuZWwuc2V0QXR0cmlidXRlKCdoZWlnaHQnLCAnODdweCcpO1xyXG4gICAgfVxyXG59KTsqL1xyXG5cclxuXHJcblxyXG4kKFwiLm9yZGVyX19saWtlZC1saW5rLWNsZWFyXCIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZSl7XHJcbiAgICAkKCcub3JkZXJfX2xpa2VkJykudG9nZ2xlQ2xhc3MoXCJvcmRlcl9fbGlrZWQtY2xlYXJcIik7XHJcbiAgICBjb25zb2xlLmxvZygnb3JkZXJfX2xpa2VkLWxpbmsgY2xpY2snKTtcclxuXHJcbn0pO1xyXG5cclxuJCgnLnJhZGlvLWNhdGFsb2cgLnJhZGlvLWlucHV0MScpLm9uKFwiY2xpY2tcIixmdW5jdGlvbihlKXtcclxuICAgIGNvbnNvbGUubG9nKCdzZGZzZGZkc2YnKTtcclxuICAgICQoJy5jYXRhbG9nX19saXN0JykuYWRkQ2xhc3MoJ2NhdGFsb2dfX2xpc3QtdmlldycpO1xyXG59KTtcclxuXHJcbiQoJy5yYWRpby1jYXRhbG9nIC5yYWRpby1pbnB1dDInKS5jbGljayhmdW5jdGlvbigpe1xyXG4gICAgJCgnLmNhdGFsb2dfX2xpc3QnKS5yZW1vdmVDbGFzcygnY2F0YWxvZ19fbGlzdC12aWV3Jyk7XHJcbn0pO1xyXG5cclxuJCgnLnJhZGlvLWNvbGxlY3Rpb24gLnJhZGlvLWlucHV0MScpLm9uKFwiY2xpY2tcIixmdW5jdGlvbihlKXtcclxuICAgIGNvbnNvbGUubG9nKCdzZGZzZGZkc2YnKTtcclxuICAgICQoJy5tYXJrZXJzJykuYWRkQ2xhc3MoJ21hcmtlcnMteWVzJyk7XHJcbn0pO1xyXG5cclxuJCgnLnJhZGlvLWNvbGxlY3Rpb24gLnJhZGlvLWlucHV0MicpLmNsaWNrKGZ1bmN0aW9uKCl7XHJcbiAgICAkKCcubWFya2VycycpLnJlbW92ZUNsYXNzKCdtYXJrZXJzLXllcycpO1xyXG59KTtcclxuXHJcblxyXG4kKCcucmFkaW8tc2xpZGVyIC5yYWRpby1pbnB1dDEnKS5vbihcImNsaWNrXCIsZnVuY3Rpb24oZSl7XHJcbiAgICBjb25zb2xlLmxvZygnc2Rmc2RmZHNmJyk7XHJcbiAgICAkKCcuY29sbGVjdGlvbl9fc2xpZGVyMScpLnRvZ2dsZUNsYXNzKCdjb2xsZWN0aW9uX19zbGlkZXItbm9uZScpO1xyXG4gICAgJCgnLmNvbGxlY3Rpb25fX3NsaWRlcjInKS50b2dnbGVDbGFzcygnY29sbGVjdGlvbl9fc2xpZGVyLW5vbmUnKTtcclxufSk7XHJcblxyXG4kKCcucmFkaW8tc2xpZGVyIC5yYWRpby1pbnB1dDInKS5jbGljayhmdW5jdGlvbigpe1xyXG4gICAgY29uc29sZS5sb2coJ3NkZnNkZmRzZicpO1xyXG4gICAgJCgnLmNvbGxlY3Rpb25fX3NsaWRlcjEnKS50b2dnbGVDbGFzcygnY29sbGVjdGlvbl9fc2xpZGVyLW5vbmUnKTtcclxuICAgICQoJy5jb2xsZWN0aW9uX19zbGlkZXIyJykudG9nZ2xlQ2xhc3MoJ2NvbGxlY3Rpb25fX3NsaWRlci1ub25lJyk7XHJcbn0pO1xyXG5cclxuXHJcbi8qJCgnLmNhdGFsb2dfX2Ryb3BtZW51X19pdGVtJykuY2xpY2soZnVuY3Rpb24oKXtcclxuICAgIHZhciB0ZXh0ID0gJCh0aGlzKS50ZXh0KCksXHJcbiAgICAgICAgaWQgPSQodGhpcykuYXR0cignaWQnKSxcclxuICAgICAgICB0ZXh0SGVhZGVyID0gJCgnY2F0YWxvZ19fZHJvcG1lbnVfX2hlYWRlcicpLnRleHQoKSxcclxuICAgICAgICBpZEhlYWRlciA9ICQoJ2NhdGFsb2dfX2Ryb3BtZW51X19oZWFkZXInKS5hdHRyKCdpZCcpO1xyXG4gICAgY29uc29sZS5sb2codGV4dEhlYWRlcixpZEhlYWRlcik7XHJcbiAgICAkKHRoaXMpLnRleHQoJCgnY2F0YWxvZ19fZHJvcG1lbnVfX2hlYWRlcicpLnRleHQoKSkuYXR0cignaWQnLCgkKCdjYXRhbG9nX19kcm9wbWVudV9faGVhZGVyJykuYXR0cignaWQnKSkpO1xyXG5cclxufSk7Ki9cclxuXHJcbiQoJy5jYXRhbG9nX19kcm9wbWVudV9fbGlzdCcpLnN0eWxlcigpO1xyXG5cclxuXHJcblxyXG5cclxuXHJcbnZhciBjaGVja0hhc2ggPSBmdW5jdGlvbigpe1xyXG4gICAgaWYod2luZG93LmxvY2F0aW9uLmhhc2ggPT0gXCIjcG9wdXAtcG9zaXRpb25cIiB8fCB3aW5kb3cubG9jYXRpb24uaGFzaCA9PSBcIiNwb3B1cC1wb3N0XCIpIHtcclxuICAgICAgICBjb25zb2xlLmxvZygn0YHQutGA0L7QuycpO1xyXG4gICAgICAgICQoJy5wb3NpdGlvbl9fY2xvc2UnKS5jc3Moe3JpZ2h0OjB9KTtcclxuICAgICAgICAkKCcucG9zaXRpb25fX2Fycm93LXJpZ2h0JykuY3NzKHtyaWdodDoyM30pO1xyXG5cclxuICAgICAgICAkKHdpbmRvdykub24oXCJyZXNpemVcIiwgZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgJCgnLnBvc2l0aW9uX19jbG9zZScpLmNzcyh7cmlnaHQ6NX0pO1xyXG4gICAgICAgICAgICAkKCcucG9zaXRpb25fX2Fycm93LXJpZ2h0JykuY3NzKHtyaWdodDoyM30pO1xyXG5cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgJCgnLnBvcHVwLXBvc2l0aW9uJykuc2Nyb2xsKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIHZhciB0b3AgPSAkKHRoaXMpLnNjcm9sbFRvcCgpLFxyXG4gICAgICAgICAgICAgICAgYXJyb3cgPSQoJy5wb3NpdGlvbl9fbmF2Jykub2Zmc2V0KCkudG9wO1xyXG4gICAgICAgICAgICAvKmlmKCQod2luZG93KS5oZWlnaHQoKS8yID49ICQoJy5wb3NpdGlvbl9fbmF2Jykub2Zmc2V0KCkudG9wKSB7XHJcbiAgICAgICAgICAgICAgICAkKCcucG9zaXRpb25fX25hdicpLmFkZENsYXNzKCdwb3NpdGlvbl9fbmF2LWZpeGVkJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAkKCcucG9zaXRpb25fX25hdicpLnJlbW92ZUNsYXNzKCdwb3NpdGlvbl9fbmF2LWZpeGVkJyk7XHJcbiAgICAgICAgICAgIH0qL1xyXG4gICAgICAgICAgICBpZigxMzcgPD0gdG9wKSB7XHJcbiAgICAgICAgICAgICAgICAkKCcucG9zaXRpb25fX25hdicpLmFkZENsYXNzKCdwb3NpdGlvbl9fbmF2LWZpeGVkJyk7XHJcbiAgICAgICAgICAgICAgICAkKCcucG9zaXRpb25fX2Fycm93LXJpZ2h0JykuY3NzKHtyaWdodDo0MX0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgJCgnLnBvc2l0aW9uX19uYXYnKS5yZW1vdmVDbGFzcygncG9zaXRpb25fX25hdi1maXhlZCcpO1xyXG4gICAgICAgICAgICAgICAgJCgnLnBvc2l0aW9uX19hcnJvdy1yaWdodCcpLmNzcyh7cmlnaHQ6MjN9KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc29sZS5sb2codG9wKTtcclxuICAgICAgICAgICAgaWYgKDMyMiAgPD0gdG9wICkge1xyXG4gICAgICAgICAgICAgICAgJCgnLnBvc2l0aW9uX19jbG9zZScpLmNzcyh7cmlnaHQ6MTd9KS5hZGRDbGFzcygncG9zaXRpb25fX2Nsb3NlLWZpeGVkJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAkKCcucG9zaXRpb25fX2Nsb3NlJykuY3NzKHtyaWdodDowfSkucmVtb3ZlQ2xhc3MoJ3Bvc2l0aW9uX19jbG9zZS1maXhlZCcpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIGlmKHdpbmRvdy5sb2NhdGlvbi5oYXNoLmluZGV4T2YoXCIjcG9wdXBcIikgIT0gLTEvKndpbmRvdy5sb2NhdGlvbi5oYXNoID09IFwiI3BvcHVwLXBvc2l0aW9uXCIgfHwgd2luZG93LmxvY2F0aW9uLmhhc2ggPT0gXCIjcG9wdXAtZ2FsbGVyeVwiKi8pIHtcclxuICAgICAgICAkKCdib2R5JykuYWRkQ2xhc3MoJ292ZXJmbG93LWhpZGRlbicpO1xyXG5cclxuICAgIH0gZWxzZSAkKCdib2R5JykucmVtb3ZlQ2xhc3MoJ292ZXJmbG93LWhpZGRlbicpO1xyXG4gICAgLyogaGlzdG9yeS5wdXNoU3RhdGUoJycsIGRvY3VtZW50LnRpdGxlLCB3aW5kb3cubG9jYXRpb24ucGF0aG5hbWUpOyovIC8v0YPQtNCw0LvRj9C10YIgaGFzaFxyXG59O1xyXG5cclxuY2hlY2tIYXNoKCk7XHJcblxyXG4kKHdpbmRvdykuYmluZCgnaGFzaGNoYW5nZScsIGZ1bmN0aW9uKCkge1xyXG4gICAgY2hlY2tIYXNoKCk7XHJcbn0pO1xyXG5cclxuXHJcblxyXG4vKiQoJy5wb3B1cC1vcGVuJykuY2xpY2soZnVuY3Rpb24oKXtcclxuICAgICQoJ2JvZHknKS5hZGRDbGFzcygnb3ZlcmZsb3ctaGlkZGVuJyk7XHJcbn0pO1xyXG5cclxuJCgnLnBvc2l0aW9uX19jbG9zZScpLmNsaWNrKGZ1bmN0aW9uKCl7XHJcbiAgICAkKCdib2R5JykucmVtb3ZlQ2xhc3MoJ292ZXJmbG93LWhpZGRlbicpO1xyXG59KTsqL1xyXG5cclxuXHJcbiQoXCIuY2F0YWxvZ19faWNvblwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGUpe1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgJCh0aGlzKS50b2dnbGVDbGFzcyhcImNhdGFsb2dfX2ljb24tYWN0aXZlXCIpO1xyXG4gICAgY29uc29sZS5sb2coJ2NhdGFsb2dfX2ljb24gY2xpY2snKTtcclxufSk7XHJcblxyXG4kKFwiLm9yZGVyX19saWtlZCAuY2F0YWxvZ19faWNvblwiKS5lYWNoKGZ1bmN0aW9uKGluZHgsIGVsZW1lbnQpe1xyXG4gICAgJChlbGVtZW50KS5hZGRDbGFzcyhcImNhdGFsb2dfX2ljb24tYWN0aXZlXCIpO1xyXG59KTtcclxuXHJcbiQoXCIuZ2FsbGVyeV9faWNvblwiKS5jbGljayhmdW5jdGlvbihlKXtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICQodGhpcykudG9nZ2xlQ2xhc3MoXCJnYWxsZXJ5X19pY29uLWFjdGl2ZVwiKTtcclxufSk7XHJcblxyXG4kKFwiLnBvc2l0aW9uX19pY29uXCIpLmNsaWNrKGZ1bmN0aW9uKGUpe1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgJCh0aGlzKS50b2dnbGVDbGFzcyhcInBvc2l0aW9uX19pY29uLWFjdGl2ZVwiKTtcclxufSk7XHJcblxyXG4kKFwiLmxpbmstb3RoZXItdGltZVwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGUpe1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgJCh0aGlzKS5oaWRlKCk7XHJcbiAgICAkKCcucG9wdXAtY2FsbCAuZm9ybScpLnRvZ2dsZUNsYXNzKFwiZm9ybV9fY2FsbC1oaWRkZW5cIik7XHJcbiAgICBjb25zb2xlLmxvZygnb3JkZXJfX2xpa2VkLWxpbmsgY2xpY2snKTtcclxuICAgICQoXCIjZm9ybS1jYWxsLXRleHRhcmVhXCIpLmxlbW9uZ3JhYigpO1xyXG5cclxufSk7XHJcblxyXG52YXIgcnVsZXMgPSBbXHJcbiAgICB7XHJcbiAgICAgICAgc2VsZWN0b3I6JyNuYW1lJyxcclxuICAgICAgICBydWxlOntcclxuICAgICAgICAgICAgcmVxdWlyZWQ6W1xyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGtleTogXCIhcmVnZXhwXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICdeW8OQwrAtw5HCj8OQwpAtw5DCr117Myx9JFwifV0nXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIHNlbGVjdG9yOicjbW9iaWwtcGhvbmUnLFxyXG4gICAgICAgIHJ1bGU6e1xyXG4gICAgICAgICAgICByZXF1aXJlZDpbXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAga2V5OiBcIiFyZWdleHBcIixcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJ14oKDh8XFwrNylbXFwtIF0/KT8oXFwoP1xcZHszfVxcKT9bXFwtIF0/KT9bXFxkXFwtIF17NywxMH0kXCJ9XSdcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXTtcclxudmFyIGlucHV0V3JhcD1cIjxkaXYgY2xhc3M9J2Zvcm1fX2lucHV0LXdyYXAnIHN0eWxlPSdkaXNwbGF5OiBpbmxpbmUtYmxvY2snPjwvZGl2PlwiO1xyXG4kKFwiLmZvcm0gLmZvcm1fX2lucHV0LW5hbWVcIikud3JhcChpbnB1dFdyYXApLmFmdGVyKFwiPGRpdiBjbGFzcz0naW5wdXRfX3ZhbGlkJz7QndC10L/RgNCw0LLQuNC70YzQvdC+INCy0LLQtdC00LXQvdC+INC40LzRjzwvZGl2PlwiKTtcclxuJChcIi5mb3JtIC5mb3JtX19pbnB1dC1waG9uZVwiKS53cmFwKGlucHV0V3JhcCkuYWZ0ZXIoXCI8ZGl2IGNsYXNzPSdpbnB1dF9fdmFsaWQnPtCd0LXQv9GA0LDQstC40LvRjNC90L4g0LLQstC10LTQtdC9INCy0LLQtdC00LXQvSDQvdC+0LzQtdGAINGC0LXQu9C10YTQvtC90LA8L2Rpdj5cIik7XHJcbiQoXCIuZm9ybSAuZm9ybV9faW5wdXQtZW1haWxcIikud3JhcChpbnB1dFdyYXApLmFmdGVyKFwiPGRpdiBjbGFzcz0naW5wdXRfX3ZhbGlkJz7QndC10L/RgNCw0LLQuNC70YzQvdC+INCy0LLQtdC00LXQvSBlbWFpbDwvZGl2PlwiKTtcclxuJChcIi5mb3JtIC5mb3JtX19pbnB1dC10ZXh0YXJlYVwiKS53cmFwKGlucHV0V3JhcCkuYWZ0ZXIoXCI8ZGl2IGNsYXNzPSdpbnB1dF9fdmFsaWQnPtCh0LvQuNGI0LrQvtC8INC60L7RgNC+0YLQutC+0LUg0YHQvtC+0LHRidC10L3QuNC1PC9kaXY+XCIpO1xyXG5cclxuLyokKFwiI2Zvcm0tY2FsbC10ZXh0YXJlYVwiKS5sZW1vbmdyYWIoJycpOyovXHJcblxyXG5cclxuaWYoJChcIiNmb3JtLWNhbGxcIikubGVuZ3RoID4gMCkgeyAkKFwiI2Zvcm0tY2FsbFwiKS5sZW1vbmdyYWIoJycpOyB9XHJcbmlmKCQoXCIjZm9ybS1xdWVzdGlvblwiKS5sZW5ndGggPiAwKSB7ICQoXCIjZm9ybS1xdWVzdGlvblwiKS5sZW1vbmdyYWIoJycpOyB9XHJcbmlmKCQoXCIjZm9ybS1zdWJzY3JpYmVcIikubGVuZ3RoID4gMCkgeyAkKFwiI2Zvcm0tc3Vic2NyaWJlXCIpLmxlbW9uZ3JhYignJyk7IH1cclxuXHJcblxyXG5cclxuXHJcblxyXG4kKFwiLmZvcm1cIikub24oXCJsZW1vbmdyYWIucmVxdWlyZVwiLGZ1bmN0aW9uKCl7XHJcbiAgICAkKHRoaXMpLmZpbmQoJy5idG4yJykucmVtb3ZlQ2xhc3MoJ2J0bjItYWN0aXZlJyk7XHJcbiAgICBjb25zb2xlLmxvZygn0L3QtSDQstGB0LUg0L7QsdGP0LfQsNGC0LXQu9GM0L3Ri9C1INC/0L7Qu9GPJyk7XHJcbn0pO1xyXG4kKFwiLmZvcm1cIikub24oXCJsZW1vbmdyYWIubm9yZXF1aXJlXCIsZnVuY3Rpb24oKXtcclxuICAgICQodGhpcykuZmluZCgnLmJ0bjInKS5hZGRDbGFzcygnYnRuMi1hY3RpdmUnKTtcclxuICAgIGNvbnNvbGUubG9nKCfQstGB0LUg0L7QsdGP0LfQsNGC0LXQu9GM0L3Ri9C1INC/0L7Qu9GPJyk7XHJcbn0pO1xyXG5cclxuXHJcblxyXG5cclxuJCgnLmdhbGxlcnlfX3RydW1iJykuY2xpY2soZnVuY3Rpb24oZSl7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAkKHRoaXMpLmFkZENsYXNzKCdnYWxsZXJ5X190cnVtYi1hY3RpdmUnKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKCdnYWxsZXJ5X190cnVtYi1hY3RpdmUnKTtcclxuICAgIHZhciBzcmNJbWcgPSAkKHRoaXMpLmNoaWxkcmVuKCkuYXR0cihcInNyY1wiKTtcclxuICAgICQoJy5nYWxsZXJ5X19pbWdiaWcnKS5hdHRyKFwic3JjXCIsc3JjSW1nKTtcclxufSk7Il0sImZpbGUiOiJtYWluLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
